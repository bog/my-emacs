#!/bin/sh

echo "The installation script will totally erase your emacs configuration"
echo "Do you want to take my own emacs ? (y/N)"

read ANSWER

if [ "$ANSWER" != "o" ] && [ "$ANSWER" != "O" ] && [ "$ANSWER" != "y" ] && [ "$ANSWER" != "Y" ]
then
    echo "Installation aborted."
    exit 1
fi

rm -fv ~/.emacs
rm -rvf ~/.emacs.d

cp -rvf .emacs ~/
cp -rvf .emacs.d ~/


exit 0
