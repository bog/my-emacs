
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-enabled-themes (quote (wombat)))
 '(inhibit-startup-screen t)
 '(package-selected-packages (quote (treemacs))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; change window navigation keys
(windmove-default-keybindings 'shift)
;; enable treemacs
(treemacs)

;; enable lines numbers
(global-linum-mode t)

;; show parenthesis mode
(show-paren-mode t)

;; abbrev mode
(setq default-abbrev-mode t)
(setq-default abbrev-mode t)

;; all c mode
(setq-default c-basic-offset 2)

;; cpp stuff
(add-to-list 'auto-mode-alist '("\\.cpp\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hpp\\'" . c++-mode))

(defun bog-c++-hook ()
  (auto-fill-mode)
  (c-set-offset 'case-label 2))

(add-hook 'c++-mode-hook 'bog-c++-hook)

;; bog-cpp
(load "~/.emacs.d/lisp/bog-cpp.el")

;; bog-comments
(load "~/.emacs.d/lisp/bog-comments.el")

;; allow use to erase buffer
(put 'erase-buffer 'disabled nil)

(add-to-list 'load-path "~/.emacs.d/lisp/")

;; lua-mode
(load "~/.emacs.d/lisp/lua-mode.el")

;; web-mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js?\\'" . web-mode))

;; emmet
(require 'emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
(add-hook 'web-mode-hook  'emmet-mode) ;; enable when web-mode

;; nesc launch c-mode
(setq auto-mode-alist
      (append '(("\\.nc\\'" . c-mode))
	      auto-mode-alist))

;; dot launch c-mode
(setq auto-mode-alist
      (append '(("\\.dot\\'" . c-mode))
	      auto-mode-alist))

;; text launch org mode
(setq auto-mode-alist 
      (append '(("\\.txt\\'" . org-mode))
	      auto-mode-alist))

;; cmake-mode
(setq auto-mode-alist
      (append
       '(("CMakeLists\\.txt\\'" . cmake-mode))
       '(("\\.cmake\\'" . cmake-mode))
       auto-mode-alist))

;; doxygen
(add-to-list 'auto-mode-alist  '("\\Doxyfile\\'" . conf-unix-mode))
(load "~/.emacs.d/lisp/cmake-mode.el")

;; Tuareg (ocaml)
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)

(setq auto-mode-alist 
        (append '(("\\.ml[ily]?$" . tuareg-mode)
	          ("\\.topml$" . tuareg-mode))
		auto-mode-alist))

(load "~/.emacs.d/lisp/tuareg.el")

;; MELPA
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; global keybinding
(global-set-key (kbd "C-x C-b") 'electric-buffer-list )
(global-set-key (kbd "<f11>") 'compile )
(global-set-key (kbd "<f9>") 'recompile )
