;;; lyqi.el --- an emacs major mode for editing LilyPond scores.
;;;
;;; Copyright (C) 2009 Nicolas Sceaux <nicolas.sceaux@free.fr>
;;;
;;; Lyqi is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Lyqi is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with LilyPond.  If not, see <http://www.gnu.org/licenses/>.

;;; lyqi homepage: <http://nicolas.sceaux.free.fr/lilypond>

;;; The lexing and parsing mechanism is largely inspired by Drei
;;; component from McClim: <http://common-lisp.net/project/mcclim/>

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Base incremental lexing and parsing
;;;

(eval-when-compile (require 'cl))
(require 'eieio)

;;;
;;; regex and match utilities
;;;

;; for XEmacs21 compatibility
(if (not (fboundp 'match-string-no-properties))
    (defalias 'match-string-no-properties 'match-string))

(defun lp:forward-match ()
  (forward-char (- (match-end 0) (match-beginning 0))))

;;;
;;; Buffer syntax
;;;
(defclass lp:syntax ()
  ((default-parser-state
     :initform nil
     :initarg :default-parser-state
     :accessor lp:default-parser-state)
   (first-line :initform nil
               :accessor lp:first-line)
   (last-line :initform nil
              :accessor lp:last-line)
   (current-line :initform nil
                 :accessor lp:current-line)
   ;; set by a before-change function, so that
   ;; an after-change-function can update the parse:
   (first-modified-line :initform nil)
   (last-modified-line :initform nil))
  :documentation "Base class for defining a buffer syntax, which
  instance shall be buffer-local.  It contains a double-linked
  list of lines, containing the parsed forms on each line of the
  buffer.")

(defvar lp:*current-syntax* nil
  "The current buffer syntax object")

(defun lp:current-syntax ()
  lp:*current-syntax*)

(defmethod object-print ((this lp:syntax) &rest strings)
  (format "#<%s>" (object-class this)))

;;;
;;; The "parse tree" of a buffer is represented as a double-linked
;;; list of lines, each line containing a list of forms, each form
;;; possibly containing lexemes
;;;

;;; Lines
(defclass lp:line-parse ()
  ((marker :initarg :marker
           :accessor lp:marker)
   (forms :initarg :forms
          :accessor lp:line-forms)
   (parser-state :initform nil
                 :initarg :parser-state)
   (previous-line :initform nil
                  :initarg :previous-line
                  :accessor lp:previous-line)
   (next-line :initform nil
              :initarg :next-line
              :accessor lp:next-line)))

(defmethod lp:line-end-position ((this lp:line-parse))
  (save-excursion
    (goto-char (lp:marker this))
    (point-at-eol)))

(defmethod object-print ((this lp:line-parse) &rest strings)
  (format "#<%s [%s] %s forms>"
            (object-class this)
            (marker-position (lp:marker this))
            (length (lp:line-forms this))))

(defmethod lp:debug-display ((this lp:syntax) &optional indent)
  (let ((indent (or indent 0)))
    (loop for line = (lp:first-line this) then (lp:next-line line)
          for i from 1
          while line
          do (princ (format "Line %d " i))
          do (lp:debug-display line indent))
    t))

(defmethod lp:debug-display ((this lp:line-parse) &optional indent)
  (let ((indent (or indent 0)))
    (princ
     (format "%s[%s] %s (%s forms)\n"
             (make-string indent ?\ )
             (marker-position (lp:marker this))
             (object-class (slot-value this 'parser-state))
             (length (lp:line-forms this))))
    (loop for form in (lp:line-forms this)
          do (lp:debug-display form (+ indent 2)))
    t))

(defun lp:link-lines (first next)
  (when first
    (set-slot-value first 'next-line next))
  (when next
    (set-slot-value next 'previous-line first)))

(defun lp:previous-form (line &optional rest-forms)
  "For backward iteration on forms, across lines.
Return three values:
 - the previous form
 - the form line
 - the remaining forms on this line

See also `lp:form-before-point'."
  (if rest-forms
      (values (first rest-forms) line (rest rest-forms))
      (when line
        (let* ((prev-line (lp:previous-line line))
               (prev-forms (and prev-line (reverse (lp:line-forms prev-line)))))
          (if prev-forms
              (values (first prev-forms) prev-line (rest prev-forms))
              (lp:previous-form prev-line))))))

(defun lp:form-before-point (syntax position)
  "Return three values:
- the form preceding position
- the form line
- the remaining forms on this line

To perform a backward search on forms from (point), do e.g.:

  (loop for (form line rest-forms) = (lp:form-before-point syntax position)
        then (lp:previous-form line rest-forms)
        ...)"
  (let ((line (lp:find-line syntax position)))
    (when line
      (loop for forms on (reverse (lp:line-forms line))
            if (< (lp:marker (first forms)) position)
            return (values (first forms) line (rest forms))
            finally return (lp:previous-form line)))))

;;; Base class for forms and lexemes
(defclass lp:parser-symbol ()
  ((marker :initform nil
           :initarg :marker
           :accessor lp:marker)
   (size :initform nil
         :initarg :size
         :accessor lp:size)
   (children :initarg :children
             :initform nil
             :accessor lp:children)))

(defmethod object-write ((this lp:parser-symbol) &optional comment)
  (let* ((marker (lp:marker this))
         (size (lp:size this))
         (start (and marker (marker-position marker)))
         (end (and marker size (+ start size))))
    (princ
     (format "#<%s [%s-%s] \"%s\""
             (object-class this)
             (or start "?") (or end "?")
             (buffer-substring-no-properties start end)))
    (mapcar (lambda (lexeme)
              (princ " ")
              (princ (object-class lexeme)))
            (lp:children this))
    (princ ">\n")))

(defmethod object-print ((this lp:parser-symbol) &rest strings)
  (let* ((marker (lp:marker this))
         (size (lp:size this))
         (start (and marker (marker-position marker)))
         (end (and marker size (+ start size))))
    (format "#<%s [%s-%s]>"
            (object-class this)
            (or start "?") (or end "?"))))

(defmethod lp:debug-display ((this lp:parser-symbol) &optional indent)
  (let ((indent (or indent 0)))
    (princ (format "%s[%s-%s] %s: %s\n"
                   (make-string indent ?\ )
                   (marker-position (lp:marker this))
                   (+ (lp:marker this) (lp:size this))
                   (object-class this)
                   (lp:string this)))))

(defmethod lp:string ((this lp:parser-symbol))
  (with-slots (marker size) this
    (buffer-substring-no-properties marker (+ marker size))))

;;; Forms (produced by reducing lexemes)
(defclass lp:form (lp:parser-symbol) ())

;;; Lexemes (produced when lexing)
(defclass lp:lexeme (lp:parser-symbol) ())

(defclass lp:comment-lexeme (lp:lexeme) ())
(defclass lp:comment-delimiter-lexeme (lp:lexeme) ())
(defclass lp:string-lexeme (lp:lexeme) ())
(defclass lp:doc-lexeme (lp:lexeme) ())
(defclass lp:keyword-lexeme (lp:lexeme) ())
(defclass lp:builtin-lexeme (lp:lexeme) ())
(defclass lp:function-name-lexeme (lp:lexeme) ())
(defclass lp:variable-name-lexeme (lp:lexeme) ())
(defclass lp:type-lexeme (lp:lexeme) ())
(defclass lp:constant-lexeme (lp:lexeme) ())
(defclass lp:warning-lexeme (lp:lexeme) ())
(defclass lp:negation-char-lexeme (lp:lexeme) ())
(defclass lp:preprocessor-lexeme (lp:lexeme) ())

(defclass lp:delimiter-lexeme (lp:lexeme) ())
(defclass lp:opening-delimiter-lexeme (lp:delimiter-lexeme) ())
(defclass lp:closing-delimiter-lexeme (lp:delimiter-lexeme) ())

(defmethod lp:opening-delimiter-p ((this lp:parser-symbol))
  nil)
(defmethod lp:opening-delimiter-p ((this lp:opening-delimiter-lexeme))
  t)
(defmethod lp:closing-delimiter-p ((this lp:parser-symbol))
  nil)
(defmethod lp:closing-delimiter-p ((this lp:closing-delimiter-lexeme))
  t)

;;; Parsing data (used when reducing lexemes to produce forms)
(defclass lp:parser-state ()
  ((lexemes :initarg :lexemes
            :initform nil)
   (form-class :initarg :form-class)
   (next-parser-state :initarg :next-parser-state
                      :initform nil
                      :accessor lp:next-parser-state)))

(defmethod lp:push-lexeme ((this lp:parser-state) lexeme)
  (set-slot-value this 'lexemes
                  (cons lexeme (slot-value this 'lexemes))))

(defmethod lp:reduce-lexemes ((this lp:parser-state) &optional form-class)
  (let ((reversed-lexemes (slot-value this 'lexemes)))
    (when reversed-lexemes
      (let* ((last-lexeme (first reversed-lexemes))
             (lexemes (nreverse (slot-value this 'lexemes)))
             (first-lexeme (first lexemes)))
        (set-slot-value this 'lexemes nil)
        (make-instance (or form-class (slot-value this 'form-class))
                       :children lexemes
                       :marker (lp:marker first-lexeme)
                       :size (- (+ (lp:marker last-lexeme)
                                   (lp:size last-lexeme))
                                (lp:marker first-lexeme)))))))

(defmethod lp:same-parser-state-p ((this lp:parser-state) other-state)
  (and other-state
       (eql (object-class this)
            (object-class other-state))
       (let ((next-class (and (lp:next-parser-state this)
                              (object-class (lp:next-parser-state this))))
             (other-next-class (and (lp:next-parser-state other-state)
                                    (object-class (lp:next-parser-state other-state)))))
         (eql next-class other-next-class))))

(defmethod lp:change-parser-state ((original lp:parser-state) new-class)
  (with-slots (lexemes form-class next-parser-state) original
    (make-instance new-class
                   :lexemes lexemes
                   :form-class form-class
                   :next-parser-state next-parser-state)))

;;;
;;; Lex function
;;;

(defgeneric lp:lex (parser-state syntax)
  "Lex or parse one element.

Depending on `parser-state' and the text at current point, either
lex a lexeme, or reduce previous lexemes (accumulated up to now
in `parser-state') to build a form, or both.

Return three values:
- the new parser state. The input `parser-state' may be modified,
  in particular its `lexemes' slot;
- a list of forms, if lexemes have been reduced, or NIL otherwise;
- NIL if the line parsing is finished, T otherwise.")

;; a default implementation to avoid compilation warnings
(defmethod lp:lex (parser-state syntax)
  (when (looking-at "[ \t]+")
    (lp:forward-match))
  (if (eolp)
      (values parser-state nil nil)
      (let ((marker (point-marker)))
        (looking-at "\\S-+")
        (lp:forward-match)
        (values parser-state
                (list (make-instance 'lp:lexeme
                                     :marker marker
                                     :size (- (point) marker)))
                (not (eolp))))))

;;;
;;; Parse functions
;;;

(defun lp:parse (syntax &optional first-parser-state end-position)
  "Parse lines in current buffer from point up to `end-position'.
Return three values: the first parse line, the last parse
line (i.e. both ends of double linked parse line list.), and the
lexer state applicable to the following line.

Default values:
  parser-state (lp:default-parser-state syntax)
  end-position (point-max)"
  (let ((first-parser-state (or first-parser-state (lp:default-parser-state syntax)))
        (end-position (save-excursion
                        (goto-char (or end-position (point-max)))
                        (point-at-bol))))
    (loop with result = nil
          with first-line = nil
          for previous-line = nil then line
          for parser-state = first-parser-state then next-parser-state
          for marker = (point-marker)
          for (forms next-parser-state) = (lp:parse-line syntax parser-state)
          do (assert (= marker (point-at-bol))
                     nil
                     "lp:parse error: lp:parse-line outreached a line end (%d, %d)"
                     (marker-position marker) (point-at-bol)) ;; debug
          for line = (make-instance 'lp:line-parse
                                    :marker marker
                                    :previous-line previous-line
                                    :parser-state parser-state
                                    :forms forms)
          unless first-line do (setf first-line line)
          if previous-line do (set-slot-value previous-line 'next-line line)
          if (>= (point) end-position)
          ;; end position has been reached: stop parsing
          return (values first-line line next-parser-state)
          ;; otherwise go to next line
          do (forward-line 1))))

(defun lp:parse-line (syntax parser-state)
  "Return a form list, built by parsing current buffer starting
from current point up to the end of the current line."
  (loop for (new-parser-state forms continue)
        = (lp:lex (or parser-state (lp:default-parser-state syntax)) syntax)
        then (lp:lex new-parser-state syntax)
        nconc forms into result
        while continue
        finally return (values result new-parser-state)))

(defun lp:reparse-line (syntax line)
  "Perform a new parse of `line' (the surrounding context is
supposed to be unchanged)"
  (save-excursion
    (goto-char (lp:marker line))
    (forward-line 0)
    (let ((marker (point-marker)))
      (set-marker-insertion-type marker nil)
      (set-slot-value line 'marker marker))
    (multiple-value-bind (forms next-state)
        (lp:parse-line syntax (slot-value line 'parser-state))
      (set-slot-value line 'forms forms))))

;;;
;;; Parse search
;;;

(defun lp:find-lines (syntax position &optional length)
  "Search parse lines covering the region starting from
`position' and covering `length' (which defaults to 0). Return
two values: the first and the last parse line."
  ;; Compare the region position with the syntax current line (its
  ;; previously modified line, saved for quicker access), the first
  ;; line and the last line, to determine from which end start the
  ;; search.
  (let* ((beginning-position (save-excursion
                               (goto-char position)
                               (point-at-bol)))
         (end-position (if length
                           (save-excursion
                             (goto-char (+ position length))
                             (point-at-bol))
                           beginning-position)))
    (multiple-value-bind (search-type from-line)
        (let ((current-line (lp:current-line syntax)))
          (if current-line
              (let* ((point-0/4 (point-min))
                     (point-4/4 (point-max))
                     (point-2/4 (lp:marker current-line))
                     (point-1/4 (/ (- point-2/4 point-0/4) 2))
                     (point-3/4 (/ (+ point-2/4 point-4/4) 2)))
                (cond ((<= point-3/4 end-position)
                       (values 'backward (lp:last-line syntax)))
                      ((<= beginning-position point-1/4)
                       (values 'forward (lp:first-line syntax)))
                      ((and (<= point-2/4 beginning-position) (<= beginning-position point-3/4))
                       (values 'forward current-line))
                      ((and (<= point-1/4 end-position) (<= end-position point-2/4))
                       (values 'backward current-line))
                      (t ;; (<= point-1/4 beginning-position point-1/2 end-position point-3/4)
                       (values 'both current-line))))
              (let* ((point-1/2 (/ (+ (point-max) (point-min)) 2)))
                (if (>= end-position point-1/2)
                    (values 'backward (lp:last-line syntax))
                    (values 'forward (lp:first-line syntax))))))
      (case search-type
        ((forward) ;; forward search from `from-line'
         (loop with first-line = nil
               for line = from-line then (lp:next-line line)
               if (= (lp:marker line) beginning-position)
               do (setf first-line line)
               if (= (lp:marker line) end-position)
               return (values first-line line)))
        ((backward) ;; backward search from `from-line'
         (loop with last-line = nil
               for line = from-line then (lp:previous-line line)
               if (= (lp:marker line) end-position)
               do (setf last-line line)
               if (= (lp:marker line) beginning-position)
               return (values line last-line)))
        (t ;; search first line backward, and last-line forward from `from-line'
         (values (loop for line = from-line then (lp:previous-line line)
                       if (= (lp:marker line) beginning-position) return line)
                 (loop for line = from-line then (lp:next-line line)
                       if (= (lp:marker line) end-position) return line)))))))

(defun lp:find-line (syntax position)
  (let ((current-line (lp:current-line syntax)))
    (if (and current-line
             (= (lp:marker current-line)
                (save-excursion
                  (goto-char position)
                  (point-at-bol))))
        current-line
        (first (lp:find-lines syntax position)))))

;;;
;;; Parse update
;;;

(defmacro lp:without-parse-update (&rest body)
  `(let ((before-change-functions nil)
         (after-change-functions nil))
     ,@body))
(put 'lp:without-parse-update 'lisp-indent-function 0)

(defun lp:parse-and-highlight-buffer ()
  "Make a full parse of current buffer and highlight text.  Set
current syntax parse data (`first-line' and `last-line' slots)."
  (let ((syntax (lp:current-syntax)))
    (lp:without-parse-update
      ;; initialize the parse tree
      (save-excursion
        (goto-char (point-min))
        (multiple-value-bind (first last state) (lp:parse syntax)
          (set-slot-value syntax 'first-line first)
          (set-slot-value syntax 'last-line last)))
      ;; remove previous fontification
      (lp:unfontify syntax)
      ;; fontify the buffer
      (loop for line = (lp:first-line syntax)
            then (lp:next-line line)
            while line
            do (mapcar #'lp:fontify (lp:line-forms line))))))

(defun lp:update-line-if-different-parser-state (line parser-state syntax)
  (when (and line
           (not (lp:same-parser-state-p
                 parser-state
                 (slot-value line 'parser-state))))
    (multiple-value-bind (forms next-state)
        (lp:parse-line syntax parser-state)
      (set-slot-value line 'forms forms)
      (set-slot-value line 'parser-state parser-state)
      (lp:unfontify line)
      (lp:fontify line)
      (forward-line 1)
      (lp:update-line-if-different-parser-state (lp:next-line line) next-state syntax))))

(defun lp:before-parse-update (beginning end)
  "Find the modified parse lines, covering the region starting
from `beginning' to `end'.  Set the `first-modified-line' and
`last-modified-line' slots of the current syntax."
  (let ((syntax (lp:current-syntax)))
    (unless (lp:first-line syntax)
      (lp:parse-and-highlight-buffer))
    ;; find the portion of the parse-tree that needs an update
    (multiple-value-bind (first-modified-line last-modified-line)
        (lp:find-lines syntax beginning (- end beginning))
      (set-slot-value syntax 'first-modified-line first-modified-line)
      (set-slot-value syntax 'last-modified-line last-modified-line))))

(defun lp:parse-update (beginning end old-length)
  "Update current syntax parse-tree after a buffer modification,
and fontify the changed text.

  `beginning' is the beginning of the changed text.
  `end' is the end of the changed text.
  `length' is the length the pre-changed text."
  (save-match-data
    (save-excursion
      (let ((syntax (lp:current-syntax))
            (end-position (progn
                            (goto-char end)
                            (point-at-eol))))
        (goto-char beginning)
        (forward-line 0)
        (let ((first-modified-line (slot-value syntax 'first-modified-line))
              (last-modified-line (slot-value syntax 'last-modified-line)))
          ;; re-parse the modified lines
          (multiple-value-bind (first-new-line last-new-line next-state)
              (lp:parse syntax
                        (if first-modified-line
                            (slot-value first-modified-line 'parser-state)
                            (slot-value syntax 'default-parser-state))
                        end-position)
            ;; fontify new lines
            (loop for line = first-new-line then (lp:next-line line)
                  while line
                  do (lp:unfontify line)
                  do (lp:fontify line))
            ;; replace the old lines with the new ones in the
            ;; double-linked list
            (if (or (not first-modified-line)
                    (eql (lp:first-line syntax) first-modified-line))
                (set-slot-value syntax 'first-line first-new-line)
                (lp:link-lines (lp:previous-line first-modified-line)
                               first-new-line))
            (if (or (not last-modified-line)
                    (eql (lp:last-line syntax) last-modified-line))
                (set-slot-value syntax 'last-line last-new-line)
                (lp:link-lines last-new-line
                               (lp:next-line last-modified-line)))
            ;; debug
            ;;(princ (format "old: [%s-%s] new: [%s-%s]"
            ;;               (marker-position (lp:marker first-modified-line))
            ;;               (marker-position (lp:marker last-modified-line))
            ;;               (marker-position (lp:marker first-new-line))
            ;;               (marker-position (lp:marker last-new-line))))
            ;; Update the syntax `current-line', from quick access
            (set-slot-value syntax 'current-line last-new-line)
            (set-slot-value syntax 'first-modified-line nil)
            (set-slot-value syntax 'last-modified-line nil)
            ;; If the lexer state at the end of last-new-line is
            ;; different from the lexer state at the beginning of
            ;; the next line, then parse next line again (and so
            ;; on)
            (lp:update-line-if-different-parser-state
             (lp:next-line last-new-line) next-state syntax)))))))

;;;
;;; Fontification
;;;

(defun lp:fontify-region (start end face)
  (let ((overlay (make-overlay start end nil t nil)))
    (overlay-put overlay 'face face)))

(defgeneric lp:unfontify (thing)
  "Remove all fontification from `thing'")

(defmethod lp:unfontify ((this lp:line-parse))
  (remove-overlays (lp:marker this) (lp:line-end-position this)))

(defmethod lp:unfontify ((this lp:syntax))
  (remove-overlays))

(defgeneric lp:fontify (parser-symbol)
  "Fontify a lexeme or form")

(defgeneric lp:face (parser-symbol)
  "The face of a lexeme or form, used in fontification.")

(defmethod lp:fontify ((this lp:line-parse))
  (mapcar #'lp:fontify (lp:line-forms this)))

(defmethod lp:fontify ((this lp:parser-symbol))
  (let* ((start (marker-position (lp:marker this)))
         (end (+ start (lp:size this))))
    (when (> end start)
      (lp:fontify-region start end (lp:face this)))))

(defmethod lp:fontify ((this lp:form))
  (let ((children (slot-value this 'children)))
    (if children
        (mapcar 'lp:fontify children)
        (call-next-method))))

(defmethod lp:face ((this lp:parser-symbol))
  nil)

(defmethod lp:face ((this lp:comment-lexeme))
  '(face font-lock-comment-face))
(defmethod lp:face ((this lp:comment-delimiter-lexeme))
  '(face font-lock-comment-delimiter-face))
(defmethod lp:face ((this lp:string-lexeme))
  '(face font-lock-string-face))
(defmethod lp:face ((this lp:doc-lexeme))
  '(face font-lock-doc-face))
(defmethod lp:face ((this lp:keyword-lexeme))
  '(face font-lock-keyword-face))
(defmethod lp:face ((this lp:builtin-lexeme))
  '(face font-lock-builtin-face))
(defmethod lp:face ((this lp:function-name-lexeme))
  '(face font-lock-function-name-face))
(defmethod lp:face ((this lp:variable-name-lexeme))
  '(face font-lock-variable-name-face))
(defmethod lp:face ((this lp:type-lexeme))
  '(face font-lock-type-face))
(defmethod lp:face ((this lp:constant-lexeme))
  '(face font-lock-constant-face))
(defmethod lp:face ((this lp:warning-lexeme))
  '(face font-lock-warning-face))
(defmethod lp:face ((this lp:negation-char-lexeme))
  '(face font-lock-negation-char-face))
(defmethod lp:face ((this lp:preprocessor-lexeme))
  '(face font-lock-preprocessor-face))

;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Customization
;;;

(defgroup lyqi nil
  "LilyPond quick insert mode."
  :prefix "lyqi:"
  :group 'applications)

(defcustom lyqi:prefered-languages '(nederlands italiano)
  "Prefered languages for note names.  The first choice is used
in new files, or when the language of an existing file cannot be
guessed."
  :group 'lyqi
  :type '(set (const :tag "Italian/French" italiano)
              (const :tag "Dutch" nederlands)
              (const :tag "German" deutsch)
              (const :tag "English" english)))

(defcustom lyqi:keyboard-mapping 'qwerty
  "Keyboard mapping, used to associate keys to commands in quick
insert mode map."
  :group 'lyqi
  :type '(choice (const :tag "AZERTY" azerty)
                 (const :tag "QWERTY" qwerty)))

(defcustom lyqi:custom-key-map nil
  "Key/command alist, for customizing the quick insertion mode map.

The keys of the alist are eiter strings or vector meaning a sequence of
keystrokes, e.g:
   \"a\"   for key a
   \"\\C-ca\" for C-c a
   [enter] for RET

Values can be either:
- an existing command;
- a string, in which case a command which inserts the given string
will be implicitely created;
- a cons, which car is space-around and cdr is the string to be
inserted.

For example, if `lyqi:custom-key-map' is set to:

  '((\"w\" \"\\prall\")
    (\"x\" '(space-around . \"\\appoggiatura\")))

then when pressing the key \"w\" in quick insert mode, \\prall
is inserted, and when pressing \"x\", \\appoggiatura is inserted with
space around it (unless at the beginning or end of a line).

This variable is normally read when `lyqi-mode' is loaded.  To
force the redefinition of `lyqi:quick-insert-mode-map', invoke
command `lyqi:force-mode-map-definition'.
"
  :group 'lyqi
  :type '(alist :key-type (choice string vector)
                :value-type (choice function string alist)))

(defcustom lyqi:projects-language nil
  "Specify which note language use for projects in given directories.
For instance, if `lyqi:projects-language' is set to:

  '((\"~/src/lilypond\" nederlands)
    (\"~/Documents/MyProjects\" italiano)
    (\"~/Documents/MyProjects/MyOpera\" nederlands))

then nederlands language will be used when opening a file in \"~/src/lilypond/...\"
or \"~/Documents/MyProjects/MyOpera/...\", and italiano language will be used for
files located in other places in \"~/Documents/MyProjects\"."
  :group 'lyqi
  :type '(alist :key-type string
                :value-type (group (choice (const :tag "Italian/French" italiano)
                                           (const :tag "Dutch" nederlands)
                                           (const :tag "German" deutsch)
                                           (const :tag "English" english)))))

(defcustom lyqi:midi-backend nil
  "Midi backend to use to play notes when entering music in quick insert mode: 'osx or 'alsa"
  :group 'lyqi
  :type '(choice (const :tag "Linux/ALSA backend (lyqikbd)" alsa)
                 (const :tag "Mac OS X backend (MidiScript)" osx)
                 (const :tag "None" nil)))

(defcustom lyqi:lilypond-command "lilypond"
  "Command used to compile .ly files"
  :group 'lyqi
  :type 'string)

(defcustom lyqi:pdf-command "xpdf"
  "Command used to open .pdf files"
  :group 'lyqi
  :type 'string)

(defcustom lyqi:midi-command "timidity"
  "Command used to open .midi files"
  :group 'lyqi
  :type 'string)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; LilyPond pitchnames
;;;

(defconst lyqi:+italian-pitchnames+
  '(("dobb" 0 -4) ("dobsb" 0 -3) ("dob" 0 -2) ("dosb" 0 -1)
    ("do" 0 0) ("dosd" 0 1) ("dod" 0 2) ("dodsd" 0 3) ("dodd" 0 4)
    ("rebb" 1 -4) ("rebsb" 1 -3) ("reb" 1 -2) ("resb" 1 -1)
    ("re" 1 0) ("resd" 1 1) ("red" 1 2) ("redsd" 1 3) ("redd" 1 4)
    ("mibb" 2 -4) ("mibsb" 2 -3) ("mib" 2 -2) ("misb" 2 -1)
    ("mi" 2 0) ("misd" 2 1) ("mid" 2 2) ("midsd" 2 3) ("midd" 2 4)
    ("fabb" 3 -4) ("fabsb" 3 -3) ("fab" 3 -2) ("fasb" 3 -1)
    ("fa" 3 0) ("fasd" 3 1) ("fad" 3 2) ("fadsd" 3 3) ("fadd" 3 4)
    ("solbb" 4 -4) ("solbsb" 4 -3) ("solb" 4 -2) ("solsb" 4 -1)
    ("sol" 4 0) ("solsd" 4 1) ("sold" 4 2) ("soldsd" 4 3) ("soldd" 4 4)
    ("labb" 5 -4) ("labsb" 5 -3) ("lab" 5 -2) ("lasb" 5 -1)
    ("la" 5 0) ("lasd" 5 1) ("lad" 5 2) ("ladsd" 5 3) ("ladd" 5 4)
    ("sibb" 6 -4) ("sibsb" 6 -3) ("sib" 6 -2) ("sisb" 6 -1)
    ("si" 6 0) ("sisd" 6 1) ("sid" 6 2) ("sidsd" 6 3) ("sidd" 6 4)))

(defconst lyqi:+dutch-pitchnames+
  '(("ceses" 0 -4) ("ceseh" 0 -3) ("ces" 0 -2) ("ceh" 0 -1)
    ("c" 0 0) ("cih" 0 1) ("cis" 0 2) ("cisih" 0 3) ("cisis" 0 4)
    ("deses" 1 -4) ("deseh" 1 -3) ("des" 1 -2) ("deh" 1 -1)
    ("d" 1 0) ("dih" 1 1) ("dis" 1 2) ("disih" 1 3) ("disis" 1 4)
    ("eeses" 2 -4) ("eses" 2 -4) ("eeseh" 2 -3) ("ees" 2 -2) ("es" 2 -2) ("eeh" 2 -1)
    ("e" 2 0) ("eih" 2 1) ("eis" 2 2) ("eisih" 2 3) ("eisis" 2 4)
    ("feses" 3 -4) ("feseh" 3 -3) ("fes" 3 -2) ("feh" 3 -1)
    ("f" 3 0) ("fih" 3 1) ("fis" 3 2) ("fisih" 3 3) ("fisis" 3 4)
    ("geses" 4 -4) ("geseh" 4 -3) ("ges" 4 -2) ("geh" 4 -1)
    ("g" 4 0) ("gih" 4 1) ("gis" 4 2) ("gisih" 4 3) ("gisis" 4 4)
    ("aeses" 5 -4) ("ases" 5 -4) ("aeseh" 5 -3) ("aes" 5 -2) ("as" 5 -2) ("aeh" 5 -1)
    ("a" 5 0) ("aih" 5 1) ("ais" 5 2) ("aisih" 5 3) ("aisis" 5 4)
    ("beses" 6 -4) ("beseh" 6 -3) ("bes" 6 -2) ("beh" 6 -1)
    ("b" 6 0) ("bih" 6 1) ("bis" 6 2) ("bisih" 6 3) ("bisis" 6 4)))

(defconst lyqi:+english-pitchnames+
  '(("cflatflat" 0 -4) ("cff" 0 -4) ("ctqf" 0 -3) ("cflat" 0 -2) ("cf" 0 -2) ("cqf" 0 -1) ("c" 0 0)
    ("cqs" 0 1) ("csharp" 0 2) ("cs" 0 2) ("ctqs" 0 3) ("css" 0 4) ("cx" 0 4)  ("csharpsharp" 0 4)
    ("dflatflat" 1 -4) ("dff" 1 -4) ("dtqf" 1 -3) ("df" 1 -2) ("dflat" 1 -2) ("dqf" 1 -1) ("d" 1 0)
    ("dqs" 1 1) ("ds" 1 2) ("dsharp" 1 2) ("dtqs" 1 3) ("dss" 1 4) ("dx" 1 4) ("dsharpsharp" 1 4)
    ("eflatflat" 2 -4) ("eff" 2 -4) ("etqf" 2 -3) ("ef" 2 -2) ("eflat" 2 -2) ("eqf" 2 -1) ("e" 2 0)
    ("eqs" 2 1) ("es" 2 2) ("esharp" 2 2) ("etqs" 2 3) ("ess" 2 4) ("ex" 2 4) ("esharpsharp" 2 4)
    ("fflatflat" 3 -4) ("fff" 3 -4) ("ftqf" 3 -3) ("ff" 3 -2) ("fflat" 3 -2) ("fqf" 3 -1) ("f" 3 0)
    ("fqs" 3 1) ("fs" 3 2) ("fsharp" 3 2) ("ftqs" 3 3) ("fss" 3 4) ("fx" 3 4) ("fsharpsharp" 3 4)
    ("gflatflat" 4 -4) ("gff" 4 -4) ("gtqf" 4 -3) ("gf" 4 -2) ("gflat" 4 -2) ("gqf" 4 -1) ("g" 4 0)
    ("gqs" 4 1) ("gs" 4 2) ("gsharp" 4 2) ("gtqs" 4 3) ("gss" 4 4) ("gx" 4 4) ("gsharpsharp" 4 4)
    ("aflatflat" 5 -4) ("aff" 5 -4) ("atqf" 5 -3) ("af" 5 -2) ("aflat" 5 -2) ("aqf" 5 -1) ("a" 5 0)
    ("aqs" 5 1) ("as" 5 2) ("asharp" 5 2) ("atqs" 5 3) ("ass" 5 4) ("ax" 5 4) ("asharpsharp" 5 4)
    ("bflatflat" 6 -4) ("bff" 6 -4) ("btqf" 6 -3) ("bf" 6 -2) ("bflat" 6 -2) ("bqf" 6 -1) ("b" 6 0)
    ("bqs" 6 1) ("bs" 6 2) ("bsharp" 6 2) ("btqs" 6 3) ("bss" 6 4) ("bx" 6 4) ("bsharpsharp" 6 4)))

(defconst lyqi:+german-pitchnames+
  '(("ceses" 0 -4) ("ceseh" 0 -3) ("ces" 0 -2) ("ceh" 0 -1)
    ("c" 0 0) ("cih" 0 1) ("cis" 0 2) ("cisih" 0 3) ("cisis" 0 4)
    ("deses" 1 -4) ("deseh" 1 -3) ("des" 1 -2) ("deh" 1 -1) ("d" 1 0)
    ("dih" 1 1) ("dis" 1 2) ("disih" 1 3) ("disis" 1 4)
    ("eses" 2 -4) ("eseh" 2 -3) ("es" 2 -2) ("eeh" 2 -1) ("e" 2 0)
    ("eih" 2 1) ("eis" 2 2) ("eisih" 2 3) ("eisis" 2 4)
    ("feses" 3 -4) ("feseh" 3 -3) ("fes" 3 -2) ("feh" 3 -1) ("f" 3 0)
    ("fih" 3 1) ("fis" 3 2) ("fisih" 3 3) ("fisis" 3 4)
    ("geses" 4 -4) ("geseh" 4 -3) ("ges" 4 -2) ("geh" 4 -1) ("g" 4 0)
    ("gih" 4 1) ("gis" 4 2) ("gisih" 4 3) ("gisis" 4 4)
    ("asas" 5 -4) ("ases" 5 -4) ("asah" 5 -3) ("aseh" 5 -3) ("as" 5 -2) ("aeh" 5 -1)
    ("a" 5 0) ("aih" 5 1) ("ais" 5 2) ("aisih" 5 3) ("aisis" 5 4)
    ("heses" 6 -4) ("heseh" 6 -3) ("b" 6 -2) ("beh" 6 -1)
    ("h" 6 0) ("hih" 6 1) ("his" 6 2) ("hisih" 6 3) ("hisis" 6 4)))

;;;
;;;
;;;
(defclass lyqi:language-data ()
  ((name                  :initarg :name)
   (name->pitch           :initarg :name->pitch)
   (pitch->name           :initarg :pitch->name)
   (pitch-regex           :initarg :pitch-regex)
   (octave-regex          :initarg :octave-regex)
   (note-regex            :initarg :note-regex)
   (rest-skip-regex       :initarg :rest-skip-regex)
   (duration-data         :initarg :duration-data)
   (duration-length-regex :initarg :duration-length-regex)
   (duration-regex        :initarg :duration-regex)))

(defmethod lyqi:pitchname ((this lyqi:language-data) pitch alteration)
  (let ((pitchnum (+ (* 10 pitch) alteration 4)))
    (cdr (assoc pitchnum (slot-value this 'pitch->name)))))

(defun lyqi:join (join-string strings)
  "Returns a concatenation of all strings elements, with join-string between elements"
  (apply 'concat 
	 (car strings) 
	 (mapcar (lambda (str) (concat join-string str))
		 (cdr strings))))

(defun lyqi:sort-string-by-length (string-list)
  "Sort the given string list by decreasing string length."
  (nreverse 
   (sort string-list
	 (lambda (str1 str2)
	   (or (< (length str1) (length str2))
	       (and (= (length str1) (length str2))
		    (string< str1 str2)))))))

(defun lyqi:make-language-data (name pitchnames)
  (let* ((pitch-regex (format "\\(%s\\)" 
                              (lyqi:join "\\|" (lyqi:sort-string-by-length
                                                (mapcar 'car pitchnames)))))
         (octave-regex "\\('+\\|,+\\)")
         (note-regex (format "%s%s?\\([^a-zA-Z]\\|$\\)" pitch-regex octave-regex))
         (rest-skip-regex "\\(r\\|R\\|s\\|q\\|\\\\skip\\)\\([^a-zA-Z]\\|$\\)")
         (duration-data '(("4" . 2)
                          ("8" . 3)
                          ("32" . 5)
                          ("64" . 6)
                          ("128" . 7) 
                          ("16" . 4)
                          ("256" . 8)
                          ("2" . 1)
                          ("1" . 0)
                          ("\\breve" . -1)
                          ("\\longa" . -2)
                          ("\\maxima" . -3)))
         (duration-length-regex
          (format "\\(%s\\)"
                  (lyqi:join "\\|"
                             (mapcar 'regexp-quote
                                     (lyqi:sort-string-by-length
                                      (mapcar 'car duration-data))))))
         (duration-regex (format "%s\\.*\\(\\*[0-9]+\\(/[0-9]+\\)?\\)?"
                                 duration-length-regex)))
    (make-instance 'lyqi:language-data
                   :name name
                   :name->pitch pitchnames
                   :pitch->name (loop for (name pitch alt) in pitchnames
                                       collect (cons (+ (* 10 pitch) alt 4) name))
                   :pitch-regex            pitch-regex
                   :octave-regex           octave-regex
                   :note-regex             note-regex
                   :rest-skip-regex        rest-skip-regex
                   :duration-data          duration-data
                   :duration-length-regex  duration-length-regex
                   :duration-regex         duration-regex)))

(defconst lyqi:+italian-language+ (lyqi:make-language-data 'italiano   lyqi:+italian-pitchnames+))
(defconst lyqi:+dutch-language+   (lyqi:make-language-data 'nederlands lyqi:+dutch-pitchnames+))
(defconst lyqi:+german-language+  (lyqi:make-language-data 'deutsch    lyqi:+german-pitchnames+))
(defconst lyqi:+english-language+ (lyqi:make-language-data 'english    lyqi:+english-pitchnames+))

(defun lyqi:select-language (language-name)
  (case language-name
    ((italiano) lyqi:+italian-language+)
    ((english)  lyqi:+english-language+)
    ((deutsch)  lyqi:+german-language+)
    (t          lyqi:+dutch-language+)))


(defconst lyqi:lilypond-keywords
  '(accepts addlyrics alias alternative book bookpart change chordmode chords 
    consists context default defaultchild denies description drummode drums 
    figuremode figures header layout lyricmode lyrics lyricsto markup 
    markuplist midi name new notemode override paper remove repeat rest revert 
    score sequential set simultaneous tempo type unset with))

(defconst lyqi:lilypond-music-variables
  '(accent aikenHeads aikenHeadsMinor arpeggio arpeggioArrowDown 
    arpeggioArrowUp arpeggioBracket arpeggioNormal arpeggioParenthesis 
    arpeggioParenthesisDashed autoBeamOff autoBeamOn balloonLengthOff 
    balloonLengthOn bassFigureExtendersOff bassFigureExtendersOn 
    bassFigureStaffAlignmentDown bassFigureStaffAlignmentNeutral 
    bassFigureStaffAlignmentUp bracketCloseSymbol bracketOpenSymbol break 
    breakDynamicSpan cadenzaOff cadenzaOn coda compressFullBarRests cr cresc 
    crescHairpin crescTextCresc deadNotesOff deadNotesOn decr decresc 
    defaultTimeSignature deprecatedcresc deprecateddim deprecatedendcresc 
    deprecatedenddim dim dimHairpin dimTextDecr dimTextDecresc dimTextDim 
    dotsDown dotsNeutral dotsUp downbow downmordent downprall dynamicDown 
    dynamicNeutral dynamicUp easyHeadsOff easyHeadsOn endcr endcresc enddecr 
    enddecresc enddim endincipit episemFinis episemInitium escapedBiggerSymbol 
    escapedExclamationSymbol escapedParenthesisCloseSymbol 
    escapedParenthesisOpenSymbol escapedSmallerSymbol espressivo 
    expandFullBarRests f fermata fermataMarkup ff fff ffff fffff flageolet fp 
    frenchChords funkHeads funkHeadsMinor fz germanChords glissando halfopen 
    harmonic harmonicsOff hideNotes hideSplitTiedTabNotes hideStaffSwitch huge 
    ignatzekExceptionMusic improvisationOff improvisationOn italianChords 
    kievanOff kievanOn laissezVibrer large lheel lineprall longfermata ltoe 
    marcato markLengthOff markLengthOn melisma melismaEnd 
    mergeDifferentlyDottedOff mergeDifferentlyDottedOn 
    mergeDifferentlyHeadedOff mergeDifferentlyHeadedOn mf mordent mp 
    newSpacingSection noBeam noBreak normalsize numericTimeSignature oneVoice 
    open p palmMuteOff parenthesisCloseSymbol parenthesisOpenSymbol 
    partcombineApart partcombineApartOnce partcombineAutomatic 
    partcombineAutomaticOnce partcombineChords partcombineChordsOnce 
    partcombineSoloI partcombineSoloII partcombineSoloIIOnce 
    partcombineSoloIOnce partcombineUnisono partcombineUnisonoOnce 
    partialJazzMusic phrasingSlurDashed phrasingSlurDotted phrasingSlurDown 
    phrasingSlurHalfDashed phrasingSlurHalfSolid phrasingSlurNeutral 
    phrasingSlurSolid phrasingSlurUp pipeSymbol portato powerChordSymbol 
    powerChords pp ppp pppp ppppp prall pralldown prallmordent prallprall 
    prallup predefinedFretboardsOff predefinedFretboardsOn repeatTie 
    reverseturn rfz rheel rtoe sacredHarpHeads sacredHarpHeadsMinor segno 
    semiGermanChords setDefaultDurationToQuarter sf sff sfp sfz shiftOff 
    shiftOn shiftOnn shiftOnnn shortfermata showSplitTiedTabNotes 
    showStaffSwitch signumcongruentiae slurDashed slurDotted slurDown 
    slurHalfDashed slurHalfSolid slurNeutral slurSolid slurUp small 
    snappizzicato sostenutoOff sostenutoOn southernHarmonyHeads 
    southernHarmonyHeadsMinor sp spp staccatissimo staccato 
    startAcciaccaturaMusic startAppoggiaturaMusic startGraceMusic 
    startGraceSlur startGroup startMeasureCount startSlashedGraceMusic 
    startStaff startTextSpan startTrillSpan stemDown stemNeutral stemUp 
    stopAcciaccaturaMusic stopAppoggiaturaMusic stopGraceMusic stopGraceSlur 
    stopGroup stopMeasureCount stopSlashedGraceMusic stopStaff stopTextSpan 
    stopTrillSpan stopped sustainOff sustainOn tabFullNotation teeny tenuto 
    textLengthOff textLengthOn textSpannerDown textSpannerNeutral textSpannerUp 
    thumb tieDashed tieDotted tieDown tieHalfDashed tieHalfSolid tieNeutral 
    tieSolid tieUp tildeSymbol tiny treCorde trill tupletDown tupletNeutral 
    tupletUp turn unHideNotes unaCorda upbow upmordent upprall varcoda 
    verylongfermata voiceFour voiceFourStyle voiceNeutralStyle voiceOne 
    voiceOneStyle voiceThree voiceThreeStyle voiceTwo voiceTwoStyle walkerHeads 
    walkerHeadsMinor xNotesOff))

(defconst lyqi:lilypond-music-functions
  '(absolute acciaccatura accidentalStyle addChordShape addInstrumentDefinition 
    addQuote afterGrace allowPageTurn allowVoltaHook alterBroken appendToTag 
    applyContext applyMusic applyOutput appoggiatura assertBeamQuant 
    assertBeamSlope autochange balloonGrobText balloonText bar barNumberCheck 
    bendAfter bookOutputName bookOutputSuffix breathe chordRepeats clef 
    compoundMeter crossStaff cueClef cueClefUnset cueDuring cueDuringWithClef 
    deadNote defaultNoteHeads defineBarLine displayLilyMusic displayMusic 
    endSpanners eventChords featherDurations finger footnote grace 
    grobdescriptions harmonicByFret harmonicByRatio harmonicNote harmonicsOn 
    hide inStaffSegno instrumentSwitch inversion keepWithTag key killCues label 
    language languageRestore languageSaveAndChange makeClusters 
    makeDefaultStringTuning mark modalInversion modalTranspose musicMap 
    noPageBreak noPageTurn octaveCheck omit once ottava overrideProperty 
    overrideTimeSignatureSettings pageBreak pageTurn palmMute palmMuteOn 
    parallelMusic parenthesize partcombine partcombineDown partcombineForce 
    partcombineUp partial phrasingSlurDashPattern pitchedTrill pointAndClickOff 
    pointAndClickOn pointAndClickTypes pushToTag quoteDuring relative 
    removeWithTag resetRelativeOctave retrograde revertTimeSignatureSettings 
    rightHandFinger scaleDurations settingsFrom shape shiftDurations single 
    skip slashedGrace slurDashPattern spacingTweaks storePredefinedDiagram 
    stringTuning styledNoteHeads tabChordRepeats tabChordRepetition tag 
    temporary tieDashPattern time times tocItem transpose transposedCueDuring 
    transposition tuplet tupletSpan tweak undo unfoldRepeats void 
    withMusicProperty xNote xNotesOn))

(defconst lyqi:lilypond-markup-commands
  '(abs-fontsize arrow-head auto-footnote backslashed-digit beam bold box 
    bracket caps center-align center-column char circle column 
    column-lines-list combine concat conditional-circle-markup customTabClef 
    dir-column doubleflat doublesharp draw-circle draw-dashed-line 
    draw-dotted-line draw-hline draw-line dynamic ellipse epsfile eyeglasses 
    fermata fill-line fill-with-pattern filled-box finger flat fontCaps 
    fontsize footnote fraction fret-diagram fret-diagram-terse 
    fret-diagram-verbose fromproperty general-align halign harp-pedal hbracket 
    hcenter-in hspace huge italic justified-lines-list justify justify-field 
    justify-string large larger left-align left-brace left-column line lookup 
    lower magnify map-commands-markup-list markalphabet markletter medium 
    musicglyph natural normal-size-sub normal-size-super normal-text normalsize 
    note note-by-number null number on-the-fly oval override 
    override-lines-list pad-around pad-markup pad-to-box pad-x page-link 
    page-ref parenthesize path pattern postscript property-recursive 
    put-adjacent raise replace rest rest-by-number right-align right-brace 
    right-column roman rotate rounded-box sans scale score score-lines-list 
    semiflat semisharp sesquiflat sesquisharp sharp simple slashed-digit small 
    smallCaps smaller stencil strut sub super table-of-contents-list teeny text 
    tied-lyric tiny translate translate-scaled transparent triangle typewriter 
    underline upright vcenter verbatim-file vspace whiteout with-color 
    with-dimensions with-link with-url woodwind-diagram wordwrap wordwrap-field 
    wordwrap-internal-list wordwrap-lines-list wordwrap-string 
    wordwrap-string-internal-list))

(defconst lyqi:lilypond-markup-list-commands
  '(column-lines-markup-list justified-lines-markup-list 
    map-markup-commands-markup-list override-lines-markup-list 
    score-lines-markup-list table-of-contents-markup-list 
    wordwrap-internal-markup-list wordwrap-lines-markup-list 
    wordwrap-string-internal-markup-list))

(defconst lyqi:scheme-lily-procedures
  '(!= Measure_counter_engraver Span_stem_engraver _ 
    accidental-interface::calc-alteration accidental-interface::glyph-name 
    add-bar-glyph-print-procedure add-grace-property add-lyrics add-music 
    add-music-fonts add-new-clef add-pango-fonts add-point add-quotable 
    add-score add-stroke-glyph add-stroke-straight add-text 
    adjust-slash-stencil alist->hash-table alist<? all-bar-numbers-visible 
    all-repeat-counts-visible allow-volta-hook alterations-in-key 
    ambitus::print angle-0-2pi angle-0-360 annotate-spacing-spec 
    annotate-y-interval argument-error arrow-stencil-maker assoc-get average 
    backend-testing banter-chord-names bar-check 
    bar-line::calc-break-visibility bar-line::calc-glyph-name 
    bar-line::compound-bar-line bar-line::widen-bar-extent-on-span base-length 
    beam-exceptions beam::align-with-broken-parts beam::get-kievan-positions 
    beam::get-kievan-quantized-positions beam::place-broken-parts-individually 
    beam::slope-like-broken-parts beat-structure bend::print binary-search 
    boolean-or-symbol? box-grob-stencil box-stencil bracketify-stencil 
    cached-file-contents calc-harmonic-pitch calculate-compound-base-beat 
    calculate-compound-beat-grouping calculate-compound-measure-length 
    call-after-session car< car<= centered-stencil chain-assoc-get 
    chain-grob-member-functions change-pitches cheap-list? check-grob-path 
    check-quant-callbacks check-slope-callbacks circle-stencil 
    clef::print-modern-tab-if-set collect-book-music-for-book 
    collect-bookpart-for-book collect-music-aux collect-music-for-book 
    collect-scores-for-book color? column-lines-markup-list completize-formats 
    composed-markup-list constante-hairpin construct-chord-elements 
    context-change context-defs-from-music context-mod-from-music 
    context-spec-music context-specification coord-rotate coord-scale 
    coord-translate copy-repeat-chord count-list create-glyph-flag 
    cross-staff-connect cue-substitute cyclic-base-value debugf 
    decode-byte-string default-auto-beam-check default-dynamic-absolute-volume 
    default-flag default-instrument-equalizer define-bar-line 
    define-event-class define-fonts degrees->radians descend-to-context 
    determine-frets determine-split-list dimension-arrows dir-basename 
    display-lily-music display-music display-scheme-music dots::calc-dot-count 
    dots::calc-staff-position dump-gc-protects dump-live-object-stats 
    duration-dot-factor duration-length duration-log-factor duration-of-note 
    duration-visual duration-visual-length 
    dynamic-text-spanner::before-line-breaking elbowed-hairpin ellipse-radius 
    ellipse-stencil empty-music eps-file->stencil ergonomic-simple-format 
    eval-carefully event-cause event-chord event-chord-notes 
    event-chord-pitches event-chord-wrap! event-class-cons 
    every-nth-bar-number-visible every-nth-repeat-count-visible 
    expand-repeat-chords! extract-music extract-named-music extract-typed-music 
    fancy-format filtered-map find-pitch-entry fingering::calc-text first-assoc 
    first-bar-number-invisible 
    first-bar-number-invisible-and-no-parenthesized-bar-numbers 
    first-bar-number-invisible-save-broken-bars first-member flared-hairpin 
    flatten-alist flatten-list fold-some-music font-name-split font-name-style 
    for-some-music format-bass-figure format-compound-time format-mark-alphabet 
    format-mark-barnumbers format-mark-box-alphabet format-mark-box-barnumbers 
    format-mark-box-letters format-mark-box-numbers format-mark-circle-alphabet 
    format-mark-circle-barnumbers format-mark-circle-letters 
    format-mark-circle-numbers format-mark-letters format-mark-numbers 
    fraction->moment fraction? fret->pitch fret-board::calc-stencil 
    fret-letter-tablature-format fret-number-tablature-format 
    fret-number-tablature-format-banjo fret-parse-terse-definition-string 
    function-chain get-chord-shape get-woodwind-key-list 
    glissando::calc-tab-extra-dy glissando::draw-tab-glissando glyph-flag 
    grace-spacing::calc-shortest-duration grob-list? 
    grob::calc-property-by-copy grob::has-interface 
    grob::inherit-parent-property grob::is-live? 
    grob::unpure-Y-extent-from-stencil gui-main guile-v2 
    hairpin::calc-grow-direction hash-table->alist horizontal-slash-interval 
    ignatzek-chord-names index? internal-add-text-replacements 
    interpret-markup-list interval-bound interval-center interval-empty? 
    interval-end interval-index interval-intersection interval-length 
    interval-sane? interval-scale interval-start interval-union interval-widen 
    invalidate-alterations is-absolute? jazz-chord-names 
    justified-lines-markup-list key-signature-interface::alteration-positions 
    laissez-vibrer::print layout-extract-page-properties layout-line-thickness 
    layout-set-absolute-staff-size layout-set-absolute-staff-size-in-module 
    layout-set-staff-size lilypond-all lilypond-main lilypond-version 
    list-insert-separator list-join log2 lookup-markup-command 
    lookup-markup-list-command ly-getcwd ly:accidental-interface::height 
    ly:accidental-interface::horizontal-skylines ly:accidental-interface::print 
    ly:accidental-interface::pure-height ly:accidental-interface::width 
    ly:accidental-placement::calc-positioning-done ly:add-context-mod 
    ly:add-file-name-alist ly:add-interface ly:add-listener ly:add-option 
    ly:align-interface::align-to-ideal-distances 
    ly:align-interface::align-to-minimum-distances ly:all-grob-interfaces 
    ly:all-options ly:all-output-backend-commands ly:all-stencil-commands 
    ly:all-stencil-expressions ly:apply-context-iterator::constructor 
    ly:arpeggio::brew-chord-bracket ly:arpeggio::brew-chord-slur 
    ly:arpeggio::calc-positions ly:arpeggio::print ly:arpeggio::pure-height 
    ly:arpeggio::width ly:assoc-get ly:auto-change-iterator::constructor 
    ly:axis-group-interface::add-element 
    ly:axis-group-interface::adjacent-pure-heights 
    ly:axis-group-interface::calc-pure-relevant-grobs 
    ly:axis-group-interface::calc-pure-staff-staff-spacing 
    ly:axis-group-interface::calc-pure-y-common 
    ly:axis-group-interface::calc-skylines 
    ly:axis-group-interface::calc-staff-staff-spacing 
    ly:axis-group-interface::calc-x-common 
    ly:axis-group-interface::calc-y-common 
    ly:axis-group-interface::combine-skylines 
    ly:axis-group-interface::cross-staff ly:axis-group-interface::height 
    ly:axis-group-interface::print ly:axis-group-interface::pure-height 
    ly:axis-group-interface::width ly:balloon-interface::print 
    ly:balloon-interface::print-spanner ly:bar-check-iterator::constructor 
    ly:bar-line::calc-anchor ly:bar-line::calc-bar-extent ly:bar-line::print 
    ly:basic-progress ly:beam-score-count ly:beam::calc-beam-gap 
    ly:beam::calc-beam-segments ly:beam::calc-beaming ly:beam::calc-cross-staff 
    ly:beam::calc-direction ly:beam::calc-minimum-length 
    ly:beam::calc-normal-stems ly:beam::calc-springs-and-rods 
    ly:beam::calc-stem-shorten ly:beam::calc-x-positions ly:beam::print 
    ly:beam::pure-rest-collision-callback ly:beam::quanting 
    ly:beam::rest-collision-callback ly:beam::set-stem-lengths 
    ly:book-add-bookpart! ly:book-add-score! ly:book-book-parts ly:book-header 
    ly:book-paper ly:book-process ly:book-process-to-systems ly:book-scores 
    ly:book-set-header! ly:book? ly:box? ly:bp ly:bracket 
    ly:break-alignable-interface::self-align-callback 
    ly:break-aligned-interface::calc-average-anchor 
    ly:break-aligned-interface::calc-break-visibility 
    ly:break-aligned-interface::calc-extent-aligned-anchor 
    ly:break-alignment-interface::calc-positioning-done 
    ly:breathing-sign::divisio-maior ly:breathing-sign::divisio-maxima 
    ly:breathing-sign::divisio-minima ly:breathing-sign::finalis 
    ly:breathing-sign::offset-callback ly:broadcast 
    ly:camel-case->lisp-identifier ly:chain-assoc-get 
    ly:change-iterator::constructor ly:check-expected-warnings 
    ly:chord-name::after-line-breaking ly:chord-tremolo-iterator::constructor 
    ly:clef::calc-glyph-name ly:clef::print ly:cluster-beacon::height 
    ly:cluster::calc-cross-staff ly:cluster::print ly:cm ly:command-line-code 
    ly:command-line-options ly:connect-dispatchers ly:context-current-moment 
    ly:context-def-lookup ly:context-def-modify ly:context-def? 
    ly:context-event-source ly:context-events-below ly:context-find 
    ly:context-grob-definition ly:context-id ly:context-mod-apply! 
    ly:context-mod? ly:context-name ly:context-now ly:context-parent 
    ly:context-property ly:context-property-where-defined 
    ly:context-pushpop-property ly:context-set-property! 
    ly:context-specced-music-iterator::constructor ly:context-unset-property 
    ly:context? ly:custos::print ly:debug ly:default-scale ly:dimension? 
    ly:dir? ly:dispatcher? ly:dot-column::calc-positioning-done ly:dots::print 
    ly:duration->string ly:duration-dot-count ly:duration-factor 
    ly:duration-length ly:duration-log ly:duration-scale ly:duration::less? 
    ly:duration<? ly:duration? ly:effective-prefix ly:enclosing-bracket::print 
    ly:enclosing-bracket::width ly:encode-string-for-pdf 
    ly:engraver-announce-end-grob ly:engraver-make-grob ly:error 
    ly:eval-simple-closure ly:event-chord-iterator::constructor 
    ly:event-deep-copy ly:event-iterator::constructor ly:event-property 
    ly:event-set-property! ly:event-warning ly:event? ly:expand-environment 
    ly:expect-warning ly:figured-bass-continuation::center-on-figures 
    ly:figured-bass-continuation::print ly:find-file 
    ly:fingering-column::calc-positioning-done ly:flag::calc-x-offset 
    ly:flag::calc-y-offset ly:flag::glyph-name ly:flag::print 
    ly:flag::pure-calc-y-offset ly:flag::width ly:font-config-add-directory 
    ly:font-config-add-font ly:font-config-display-fonts 
    ly:font-config-get-font-file ly:font-design-size ly:font-file-name 
    ly:font-get-glyph ly:font-glyph-name-to-charcode 
    ly:font-glyph-name-to-index ly:font-index-to-charcode ly:font-magnification 
    ly:font-metric? ly:font-name ly:font-sub-fonts ly:format ly:format-output 
    ly:get-all-function-documentation ly:get-all-translators 
    ly:get-context-mods ly:get-option ly:get-spacing-spec ly:get-undead 
    ly:gettext ly:grace-iterator::constructor ly:grace-music::start-callback 
    ly:grid-line-interface::print ly:grid-line-interface::width 
    ly:grob-alist-chain ly:grob-array->list ly:grob-array-length 
    ly:grob-array-ref ly:grob-array? ly:grob-basic-properties 
    ly:grob-chain-callback ly:grob-common-refpoint 
    ly:grob-common-refpoint-of-array ly:grob-default-font ly:grob-extent 
    ly:grob-get-vertical-axis-group-index ly:grob-interfaces ly:grob-layout 
    ly:grob-object ly:grob-original ly:grob-parent ly:grob-pq<? 
    ly:grob-properties ly:grob-property ly:grob-property-data 
    ly:grob-pure-height ly:grob-pure-property ly:grob-relative-coordinate 
    ly:grob-robust-relative-extent ly:grob-script-priority-less 
    ly:grob-set-nested-property! ly:grob-set-object! ly:grob-set-parent! 
    ly:grob-set-property! ly:grob-staff-position ly:grob-suicide! 
    ly:grob-system ly:grob-translate-axis! ly:grob-vertical<? 
    ly:grob::horizontal-skylines-from-element-stencils 
    ly:grob::horizontal-skylines-from-stencil 
    ly:grob::pure-horizontal-skylines-from-element-stencils 
    ly:grob::pure-simple-horizontal-skylines-from-extents 
    ly:grob::pure-simple-vertical-skylines-from-extents 
    ly:grob::pure-stencil-height 
    ly:grob::pure-vertical-skylines-from-element-stencils 
    ly:grob::simple-horizontal-skylines-from-extents 
    ly:grob::simple-vertical-skylines-from-extents ly:grob::stencil-height 
    ly:grob::stencil-width ly:grob::vertical-skylines-from-element-stencils 
    ly:grob::vertical-skylines-from-stencil ly:grob::x-parent-positioning 
    ly:grob::y-parent-positioning ly:grob? ly:gulp-file 
    ly:hairpin::broken-bound-padding ly:hairpin::print ly:hairpin::pure-height 
    ly:hara-kiri-group-spanner::calc-skylines 
    ly:hara-kiri-group-spanner::force-hara-kiri-callback 
    ly:hara-kiri-group-spanner::force-hara-kiri-in-y-parent-callback 
    ly:hara-kiri-group-spanner::pure-height 
    ly:hara-kiri-group-spanner::y-extent ly:hash-table-keys 
    ly:horizontal-bracket::print ly:in-event-class? ly:inch ly:inexact->string 
    ly:input-both-locations ly:input-file-line-char-column ly:input-location? 
    ly:input-message ly:input-warning ly:interpret-music-expression 
    ly:interpret-stencil-expression ly:intlog2 ly:item-break-dir ly:item? 
    ly:iterator? ly:key-signature-interface::print ly:kievan-ligature::print 
    ly:ledger-line-spanner::print ly:ledger-line-spanner::set-spacing-rods 
    ly:lexer-keywords ly:lily-lexer? ly:lily-parser? 
    ly:line-spanner::calc-cross-staff ly:line-spanner::calc-left-bound-info 
    ly:line-spanner::calc-left-bound-info-and-text 
    ly:line-spanner::calc-right-bound-info ly:line-spanner::print 
    ly:list->offsets ly:listened-event-class? ly:listened-event-types 
    ly:listener? ly:load ly:lyric-combine-music-iterator::constructor 
    ly:lyric-combine-music::length-callback ly:lyric-extender::print 
    ly:lyric-hyphen::print ly:lyric-hyphen::set-spacing-rods ly:make-book 
    ly:make-book-part ly:make-context-mod ly:make-dispatcher ly:make-duration 
    ly:make-event-class ly:make-global-context ly:make-global-translator 
    ly:make-listener ly:make-moment ly:make-music ly:make-music-function 
    ly:make-music-relative! ly:make-output-def ly:make-page-label-marker 
    ly:make-page-permission-marker ly:make-pango-description-string 
    ly:make-paper-outputter ly:make-pitch ly:make-prob ly:make-scale 
    ly:make-score ly:make-simple-closure ly:make-spring ly:make-stencil 
    ly:make-stream-event ly:make-undead ly:make-unpure-pure-container 
    ly:measure-grouping::print ly:melody-spanner::calc-neutral-stem-direction 
    ly:mensural-ligature::brew-ligature-primitive ly:mensural-ligature::print 
    ly:message ly:minimal-breaking ly:mm ly:module->alist ly:module-copy 
    ly:modules-lookup ly:moment-add ly:moment-div ly:moment-grace 
    ly:moment-grace-denominator ly:moment-grace-numerator ly:moment-main 
    ly:moment-main-denominator ly:moment-main-numerator ly:moment-mod 
    ly:moment-mul ly:moment-sub ly:moment<? ly:moment? 
    ly:multi-measure-rest::height ly:multi-measure-rest::percent 
    ly:multi-measure-rest::print ly:multi-measure-rest::set-spacing-rods 
    ly:multi-measure-rest::set-text-rods ly:music-compress ly:music-deep-copy 
    ly:music-duration-compress ly:music-duration-length 
    ly:music-function-extract ly:music-function-signature ly:music-function? 
    ly:music-iterator::constructor ly:music-length ly:music-list? 
    ly:music-message ly:music-mutable-properties ly:music-output? 
    ly:music-property ly:music-sequence::cumulative-length-callback 
    ly:music-sequence::event-chord-length-callback 
    ly:music-sequence::event-chord-relative-callback 
    ly:music-sequence::first-start-callback 
    ly:music-sequence::maximum-length-callback 
    ly:music-sequence::minimum-start-callback 
    ly:music-sequence::simultaneous-relative-callback ly:music-set-property! 
    ly:music-transpose ly:music-warning ly:music-wrapper-iterator::constructor 
    ly:music-wrapper::length-callback ly:music-wrapper::start-callback 
    ly:music::duration-length-callback ly:music? 
    ly:note-collision-interface::calc-positioning-done 
    ly:note-column-accidentals ly:note-column-dot-column 
    ly:note-head::calc-stem-attachment ly:note-head::include-ledger-line-height 
    ly:note-head::print ly:note-head::stem-attachment 
    ly:note-head::stem-x-shift ly:number->string ly:number-pair->string 
    ly:one-line-breaking ly:optimal-breaking ly:option-usage ly:otf->cff 
    ly:otf-font-glyph-info ly:otf-font-table-data ly:otf-font? 
    ly:otf-glyph-count ly:otf-glyph-list ly:ottava-bracket::print 
    ly:output-def-clone ly:output-def-lookup ly:output-def-parent 
    ly:output-def-scope ly:output-def-set-variable! ly:output-def? 
    ly:output-description ly:output-find-context-def ly:output-formats 
    ly:outputter-close ly:outputter-dump-stencil ly:outputter-dump-string 
    ly:outputter-module ly:outputter-output-scheme ly:outputter-port 
    ly:page-marker? ly:page-turn-breaking ly:pango-font-physical-fonts 
    ly:pango-font? ly:paper-book-header ly:paper-book-pages ly:paper-book-paper 
    ly:paper-book-performances ly:paper-book-scopes ly:paper-book-systems 
    ly:paper-book? ly:paper-column::before-line-breaking ly:paper-column::print 
    ly:paper-fonts ly:paper-get-font ly:paper-get-number ly:paper-outputscale 
    ly:paper-score-paper-systems ly:paper-system-minimum-distance 
    ly:paper-system? ly:parse-file ly:parse-string-expression 
    ly:parsed-undead-list! ly:parser-clear-error ly:parser-clone 
    ly:parser-define! ly:parser-error ly:parser-has-error? 
    ly:parser-include-string ly:parser-lexer ly:parser-lookup 
    ly:parser-output-name ly:parser-parse-string ly:parser-set-note-names 
    ly:part-combine-iterator::constructor ly:partial-iterator::constructor 
    ly:percent-repeat-item-interface::beat-slash 
    ly:percent-repeat-item-interface::double-percent 
    ly:percent-repeat-iterator::constructor ly:performance-write ly:pfb->pfa 
    ly:piano-pedal-bracket::print ly:pitch-alteration ly:pitch-diff 
    ly:pitch-negate ly:pitch-notename ly:pitch-octave ly:pitch-quartertones 
    ly:pitch-semitones ly:pitch-steps ly:pitch-tones ly:pitch-transpose 
    ly:pitch::less? ly:pitch<? ly:pitch? ly:pointer-group-interface::add-grob 
    ly:pop-property-iterator::constructor ly:position-on-line? 
    ly:prob-immutable-properties ly:prob-mutable-properties ly:prob-property 
    ly:prob-property? ly:prob-set-property! ly:prob-type? ly:prob? 
    ly:programming-error ly:progress ly:property-iterator::constructor 
    ly:property-iterator::once-finalization ly:property-lookup-stats 
    ly:property-unset-iterator::constructor ly:protects ly:pt 
    ly:pure-from-neighbor-interface::calc-pure-relevant-grobs 
    ly:push-property-iterator::constructor 
    ly:push-property-iterator::once-finalization ly:quote-iterator::constructor 
    ly:register-stencil-expression ly:relative-group-extent 
    ly:relative-octave-check::relative-callback 
    ly:relative-octave-music::no-relative-callback 
    ly:relative-octave-music::relative-callback ly:repeated-music::first-start 
    ly:repeated-music::folded-music-length ly:repeated-music::minimum-start 
    ly:repeated-music::unfolded-music-length 
    ly:repeated-music::volta-music-length ly:reset-all-fonts 
    ly:rest-collision::calc-positioning-done 
    ly:rest-collision::force-shift-callback-rest ly:rest::calc-cross-staff 
    ly:rest::height ly:rest::print ly:rest::pure-height ly:rest::width 
    ly:rest::y-offset-callback ly:rhythmic-music-iterator::constructor 
    ly:round-filled-box ly:round-filled-polygon ly:run-translator 
    ly:score-add-output-def! ly:score-embedded-format ly:score-error? 
    ly:score-header ly:score-music ly:score-output-defs ly:score-set-header! 
    ly:score? ly:script-column::before-line-breaking 
    ly:script-column::row-before-line-breaking 
    ly:script-interface::calc-cross-staff ly:script-interface::calc-direction 
    ly:script-interface::calc-positioning-done ly:script-interface::print 
    ly:self-alignment-interface::aligned-on-x-parent 
    ly:self-alignment-interface::aligned-on-y-parent 
    ly:self-alignment-interface::centered-on-note-columns 
    ly:self-alignment-interface::centered-on-x-parent 
    ly:self-alignment-interface::centered-on-y-parent 
    ly:self-alignment-interface::pure-y-aligned-on-self 
    ly:self-alignment-interface::x-aligned-on-self 
    ly:self-alignment-interface::x-centered-on-y-parent 
    ly:self-alignment-interface::y-aligned-on-self 
    ly:semi-tie-column::calc-head-direction 
    ly:semi-tie-column::calc-positioning-done ly:semi-tie::calc-control-points 
    ly:separation-item::calc-skylines ly:separation-item::print 
    ly:sequential-iterator::constructor ly:set-default-scale 
    ly:set-grob-modification-callback ly:set-middle-C! ly:set-option 
    ly:set-property-cache-callback ly:side-position-interface::calc-cross-staff 
    ly:side-position-interface::move-to-extremal-staff 
    ly:side-position-interface::pure-y-aligned-side 
    ly:side-position-interface::x-aligned-side 
    ly:side-position-interface::y-aligned-side ly:simple-closure? 
    ly:simple-music-iterator::constructor ly:simplify-scheme 
    ly:simultaneous-music-iterator::constructor ly:skyline-empty? 
    ly:skyline-pair::skyline ly:skyline-pair? ly:skyline::get-distance 
    ly:skyline::get-height ly:skyline::get-max-height 
    ly:skyline::get-max-height-position ly:skyline::get-touching-point 
    ly:skyline? ly:slur-score-count ly:slur::calc-control-points 
    ly:slur::calc-cross-staff ly:slur::calc-direction ly:slur::height 
    ly:slur::outside-slur-callback ly:slur::outside-slur-cross-staff 
    ly:slur::print ly:slur::pure-height ly:slur::pure-outside-slur-callback 
    ly:slur::vertical-skylines ly:smob-protects ly:solve-spring-rod-problem 
    ly:source-file? ly:spacing-spanner::calc-common-shortest-duration 
    ly:spacing-spanner::set-springs ly:span-bar::before-line-breaking 
    ly:span-bar::calc-glyph-name ly:span-bar::print ly:span-bar::width 
    ly:spanner-bound ly:spanner-broken-into ly:spanner-set-bound! 
    ly:spanner::bounds-width ly:spanner::calc-normalized-endpoints 
    ly:spanner::kill-zero-spanned-time ly:spanner::set-spacing-rods ly:spanner? 
    ly:spawn ly:spring-set-inverse-compress-strength! 
    ly:spring-set-inverse-stretch-strength! ly:spring? 
    ly:staff-symbol-line-thickness ly:staff-symbol-referencer::callback 
    ly:staff-symbol-staff-radius ly:staff-symbol-staff-space 
    ly:staff-symbol::height ly:staff-symbol::print ly:start-environment 
    ly:stderr-redirect ly:stem-tremolo::calc-direction 
    ly:stem-tremolo::calc-slope ly:stem-tremolo::calc-style 
    ly:stem-tremolo::calc-width ly:stem-tremolo::calc-y-offset 
    ly:stem-tremolo::print ly:stem-tremolo::pure-calc-y-offset 
    ly:stem-tremolo::pure-height ly:stem-tremolo::width 
    ly:stem::calc-cross-staff ly:stem::calc-default-direction 
    ly:stem::calc-direction ly:stem::calc-length ly:stem::calc-positioning-done 
    ly:stem::calc-stem-begin-position ly:stem::calc-stem-end-position 
    ly:stem::calc-stem-info ly:stem::height ly:stem::offset-callback 
    ly:stem::print ly:stem::pure-calc-length 
    ly:stem::pure-calc-stem-begin-position ly:stem::pure-calc-stem-end-position 
    ly:stem::pure-height ly:stem::width ly:stencil-add ly:stencil-aligned-to 
    ly:stencil-combine-at-edge ly:stencil-empty? ly:stencil-expr 
    ly:stencil-extent ly:stencil-fonts ly:stencil-in-color ly:stencil-rotate 
    ly:stencil-rotate-absolute ly:stencil-scale ly:stencil-stack 
    ly:stencil-translate ly:stencil-translate-axis ly:stencil? 
    ly:stream-event::dump ly:stream-event::undump ly:stream-event? 
    ly:string-percent-encode ly:string-substitute ly:sustain-pedal::print 
    ly:system ly:system-font-load ly:system-start-delimiter::print 
    ly:system::calc-pure-height ly:system::calc-pure-relevant-grobs 
    ly:system::footnotes-after-line-breaking 
    ly:system::footnotes-before-line-breaking 
    ly:system::get-nonspaceable-staves ly:system::get-spaceable-staves 
    ly:system::get-staves ly:system::get-vertical-alignment ly:system::height 
    ly:system::vertical-skyline-elements ly:text-interface::interpret-string 
    ly:text-interface::print ly:tie-column::before-line-breaking 
    ly:tie-column::calc-positioning-done ly:tie::calc-control-points 
    ly:tie::calc-direction ly:tie::print ly:time-signature::print 
    ly:translate-cpp-warning-scheme ly:translator-context 
    ly:translator-description ly:translator-group? ly:translator-name 
    ly:translator? ly:transpose-key-alist ly:truncate-list! ly:ttf->pfa 
    ly:ttf-ps-name ly:tuplet-bracket::calc-connect-to-neighbors 
    ly:tuplet-bracket::calc-cross-staff ly:tuplet-bracket::calc-direction 
    ly:tuplet-bracket::calc-positions ly:tuplet-bracket::calc-x-positions 
    ly:tuplet-bracket::print ly:tuplet-iterator::constructor 
    ly:tuplet-number::calc-cross-staff ly:tuplet-number::calc-x-offset 
    ly:tuplet-number::calc-y-offset ly:tuplet-number::print ly:undead? 
    ly:unfolded-repeat-iterator::constructor ly:unit 
    ly:unpure-pure-container-pure-part ly:unpure-pure-container-unpure-part 
    ly:unpure-pure-container? ly:usage 
    ly:vaticana-ligature::brew-ligature-primitive ly:vaticana-ligature::print 
    ly:verbose-output? ly:version ly:volta-bracket-interface::print 
    ly:volta-bracket::calc-shorten-pair ly:volta-repeat-iterator::constructor 
    ly:warning ly:warning-located ly:wide-char->utf-8 lyric-combine lyric-event 
    lyric-text::print magnification->font-size magstep make-accidental-rule 
    make-apply-context make-articulation make-autochange-music 
    make-century-schoolbook-tree make-circle-stencil make-clef-set 
    make-column-lines-markup-list make-connected-path-stencil make-cue-clef-set 
    make-cue-clef-unset make-duration-of-length make-ellipse-stencil 
    make-event-chord make-filled-box-stencil make-grace-music 
    make-graceless-rhythmic-location make-grob-property-override 
    make-grob-property-revert make-grob-property-set make-harmonic 
    make-justified-lines-markup-list make-line-stencil make-lyric-event 
    make-map-markup-commands-markup-list make-modal-inverter 
    make-modal-transposer make-multi-measure-rest make-music 
    make-non-relative-music make-oval-stencil make-override-lines-markup-list 
    make-pango-font-tree make-part-combine-music make-partial-ellipse-stencil 
    make-property-set make-property-unset make-repeat make-repeated-music 
    make-rhythmic-location make-safe-lilypond-module 
    make-score-lines-markup-list make-sequential-music make-setting 
    make-simultaneous-music make-skip-music make-span-event make-stencil-boxer 
    make-stencil-circler make-type-checker make-voice-props-override 
    make-voice-props-revert make-voice-props-set 
    make-wordwrap-internal-markup-list make-wordwrap-lines-markup-list 
    make-wordwrap-string-internal-markup-list map-markup-commands-markup-list 
    map-selected-alist-keys map-some-music marked-up-headfoot marked-up-title 
    markup->string markup-command-list? markup-command-signature 
    markup-command-signature-ref markup-command-signature-set! markup-list? 
    markup? mensural-flag midi-program mmrest-of-length modern-straight-flag 
    modified-font-metric-font-scaling modulo-bar-number-visible 
    moment->fraction moment-min moment-pair? moment<=? multi-measure-rest 
    music->make-music music-clone music-filter music-function music-has-type 
    music-invert music-is-of-type? music-map music-separator? myd 
    neo-modern-accidental-rule no-flag normal-flag note-head::brew-ez-stencil 
    note-head::calc-duration-log note-head::calc-glyph-name 
    note-head::calc-kievan-duration-log note-name->markup note-names-language 
    note-to-cluster notes-to-clusters number-list? number-or-grob? 
    number-or-markup? number-or-pair? number-or-string? number-pair? 
    numbered-footnotes object-type object-type-name offset-add offset-flip-y 
    offset-fret offset-scale old-straight-flag only-if-beamed ordered-cons 
    output-scopes outputproperty-compatibility oval-stencil override-head-style 
    override-lines-markup-list override-time-signature-setting 
    pango-pf-file-name pango-pf-font-name pango-pf-fontindex paper-variable 
    parentheses-item::calc-angled-bracket-stencils 
    parentheses-item::calc-parenthesis-stencils parentheses-item::print 
    parenthesize-stencil parse-terse-string percussion? pitch-invert 
    pitch-of-note polar->rectangular postprocess-output postscript->pdf 
    postscript->png prepend-alist-chain print print-book-with-defaults 
    print-book-with-defaults-as-systems print-circled-text-callback print-keys 
    print-keys-verbose property-operation pure-chain-offset-callback 
    pure-from-neighbor-interface::account-for-span-bar 
    pure-from-neighbor-interface::extra-spacing-height 
    pure-from-neighbor-interface::extra-spacing-height-at-beginning-of-line 
    pure-from-neighbor-interface::extra-spacing-height-including-staff 
    pure-from-neighbor-interface::pure-height quote-substitute ratio->fret 
    ratio->pitch read-lily-expression recording-group-emulate 
    relevant-book-systems relevant-dump-systems remove-grace-property 
    remove-stencil-warnings repeat repeat-tie::handle-tab-note-head 
    repetition-chord retrieve-glyph-flag retrograde-music reverse-interval 
    revert-head-style revert-time-signature-setting rgb-color 
    rhythmic-location->file-string rhythmic-location->string 
    rhythmic-location-bar-number rhythmic-location-measure-position 
    rhythmic-location<=? rhythmic-location<? rhythmic-location=? 
    rhythmic-location>=? rhythmic-location>? rhythmic-location? 
    robust-bar-number-function rounded-box-stencil sanitize-command-option 
    scale-layout scheme? scm->string score-lines-markup-list scorify-music 
    script-interface::calc-x-offset script-or-side-position-cross-staff 
    search-executable search-gs select-head-glyph semi-tie::calc-cross-staff 
    sequential-music sequential-music-to-chord-exceptions session-initialize 
    set-accidental-style set-accidentals-properties set-bar-number-visibility 
    set-default-paper-size set-global-staff-size set-mus-properties! 
    set-output-property set-paper-dimension-variables set-paper-size 
    shift-duration-log shift-one-duration-log shift-right-at-line-begin 
    simultaneous-music skip->rest skip-of-length skyline-pair-and-non-empty? 
    skyline-pair::empty? slur::draw-tab-slur space-lines 
    span-bar::compound-bar-line span-bar::notify-grobs-of-my-existence 
    split-list-by-separator stack-lines stack-stencil-line stack-stencils 
    stack-stencils-padding-list stderr stem-stub::extra-spacing-height 
    stem-stub::pure-height stem-stub::width stem-tremolo::calc-tab-width 
    stem::calc-duration-log stem::kievan-offset-callback stencil-whiteout 
    stencil-with-color straight-flag string-encode-integer string-endswith 
    string-number::calc-text string-or-music? string-or-pair? string-or-symbol? 
    string-regexp-substitute string-startswith stroke-finger::calc-text 
    style-note-heads symbol-concatenate symbol-footnotes symbol-key<? 
    symbol-list-or-music? symbol-list-or-symbol? symbol-list? 
    symbol-or-boolean? symbol<? symmetric-interval 
    system-start-text::calc-x-offset system-start-text::calc-y-offset 
    system-start-text::print tab-note-head::calc-glyph-name 
    tab-note-head::print tab-note-head::print-custom-fret-label 
    tab-note-head::whiteout-if-style-set tablature-position-on-lines 
    tabvoice::draw-double-stem-for-half-notes 
    tabvoice::make-double-stem-width-for-half-notes teaching-accidental-rule 
    tempo tie::handle-tab-note-head tuplet-number::append-note-wrapper 
    tuplet-number::calc-denominator-text tuplet-number::calc-direction 
    tuplet-number::calc-fraction-text tuplet-number::fraction-with-notes 
    tuplet-number::non-default-fraction-with-notes 
    tuplet-number::non-default-tuplet-denominator-text 
    tuplet-number::non-default-tuplet-fraction-text type-name 
    ugh-compat-double-plus-new-chord->markup unfold-repeats uniq-list 
    uniqued-alist unrelativable-music value-for-spanner-piece vector-for-each 
    version-not-seen-message voice-separator voicify-music void-music void? 
    volta-bracket-interface::pure-height volta-bracket::calc-hook-visibility 
    wordwrap-internal-markup-list wordwrap-lines-markup-list 
    wordwrap-string-internal-markup-list write-me write-performances-midis 
    write-system-signature write-system-signatures x11-color))

(defconst lyqi:scheme-lily-macros
  '(_i def-grace-function define-event-function define-markup-command 
    define-markup-list-command define-music-function define-scheme-function 
    define-session define-session-public define-syntax-function 
    define-void-function make-engraver make-relative make-stream-event markup))

(defconst lyqi:scheme-lily-variables
  '(CENTER DOS DOUBLE-FLAT DOUBLE-FLAT-QTS DOUBLE-SHARP DOUBLE-SHARP-QTS DOWN 
    FLAT FLAT-QTS INFINITY-INT LEFT NATURAL NATURAL-QTS PI PI-OVER-180 
    PI-OVER-TWO PLATFORM RIGHT SEMI-FLAT SEMI-FLAT-QTS SEMI-SHARP 
    SEMI-SHARP-QTS SEMI-TONE SEMI-TONE-QTS SHARP SHARP-QTS START STOP 
    THREE-PI-OVER-TWO THREE-Q-FLAT THREE-Q-FLAT-QTS THREE-Q-SHARP 
    THREE-Q-SHARP-QTS TWO-PI UP X Y ZERO-MOMENT absolute-volume-alist 
    accidental-interface::height all-backend-properties all-grob-descriptions 
    all-internal-grob-properties all-internal-translation-properties 
    all-invisible all-music-font-encodings all-music-properties 
    all-text-font-encodings all-translation-properties all-user-grob-properties 
    all-user-translation-properties all-visible 
    alteration-default-glyph-name-alist alteration-hufnagel-glyph-name-alist 
    alteration-kievan-glyph-name-alist alteration-medicaea-glyph-name-alist 
    alteration-mensural-glyph-name-alist alteration-vaticana-glyph-name-alist 
    axis-group-interface::height begin-of-line-invisible begin-of-line-visible 
    black blue cancellation-glyph-name-alist center-invisible center-visible 
    current-outfile-name cyan darkblue darkcyan darkgreen darkmagenta darkred 
    darkyellow default-chord-modifier-list default-language 
    default-melisma-properties default-script-alist 
    default-string-replacement-alist default-time-signature-settings 
    dynamic-default-volume empty-interval empty-markup empty-stencil 
    end-of-line-invisible end-of-line-visible feta-design-size-mapping green 
    grey grob::always-Y-extent-from-stencil 
    grob::always-horizontal-skylines-from-element-stencils 
    grob::always-horizontal-skylines-from-stencil 
    grob::always-vertical-skylines-from-element-stencils 
    grob::always-vertical-skylines-from-stencil 
    grob::unpure-horizontal-skylines-from-stencil 
    grob::unpure-vertical-skylines-from-stencil guile-predicates 
    instrument-equalizer-alist language-pitch-names latin1-coding-vector 
    lily-unit->bigpoint-factor lily-unit->mm-factor 
    lilypond-exported-predicates lilypond-scheme-predicates magenta 
    makam-alteration-glyph-name-alist markup-functions-by-category 
    markup-functions-properties markup-list-functions music-descriptions 
    music-name-to-property-table paper-alist parser pitchnames point-stencil 
    previous-pitchnames pure-from-neighbor-interface::height-if-pure 
    r5rs-primary-predicates r5rs-secondary-predicates red 
    self-alignment-interface::y-aligned-on-self 
    side-position-interface::y-aligned-side slur::height 
    standard-alteration-glyph-name-alist supported-clefs 
    toplevel-music-functions white woodwind-instrument-list yellow))

(defconst lyqi:scheme-guile-procedures
  '($abs $acos $acosh $asin $asinh $atan $atan2 $atanh $cos $cosh $exp $expt 
    $log $sin $sinh $sqrt $tan $tanh %get-pre-modules-obarray %get-stack-size 
    %init-goops-builtins %init-rdelim-builtins %init-rw-builtins 
    %init-weaks-builtins %library-dir %load-announce %load-hook %make-void-port 
    %package-data-dir %print-module %print-values %record-type-error 
    %search-load-path %site-dir * + - ->bool ->char-set / 1+ 1- < <= = > >= abs 
    accept access? acons acos acosh add-hook! alarm all-threads and-map and=> 
    angle any->c32vector any->c64vector any->f32vector any->f64vector 
    any->s16vector any->s32vector any->s64vector any->s8vector any->u16vector 
    any->u32vector any->u64vector any->u8vector append append! apply 
    apply-to-args apply:nconc2last array->list array-contents array-copy! 
    array-copy-in-order! array-dimensions array-equal? array-fill! 
    array-for-each array-in-bounds? array-index-map! array-map! 
    array-map-in-order! array-prototype array-rank array-ref array-set! 
    array-shape array-type array? ash asin asinh assert-defmacro?! 
    assert-load-verbosity assert-repl-print-unspecified assert-repl-silence 
    assert-repl-verbosity assoc assoc-ref assoc-remove! assoc-set! assq 
    assq-ref assq-remove! assq-set! assv assv-ref assv-remove! assv-set! async 
    async-mark atan atanh autoload-done! autoload-done-or-in-progress? 
    autoload-in-progress! backtrace bad-throw basename basic-load batch-mode? 
    beautify-user-module! bind bind-textdomain-codeset bindtextdomain bit-count 
    bit-count* bit-extract bit-invert! bit-position bit-set*! bitvector 
    bitvector->list bitvector-fill! bitvector-length bitvector-ref 
    bitvector-set! bitvector? boolean? broadcast-condition-variable 
    builtin-variable c-clear-registered-modules c-registered-modules c32vector 
    c32vector->list c32vector-length c32vector-ref c32vector-set! c32vector? 
    c64vector c64vector->list c64vector-length c64vector-ref c64vector-set! 
    c64vector? caaaar caaadr caaar caadar caaddr caadr caar cadaar cadadr cadar 
    caddar cadddr caddr cadr call-with-blocked-asyncs 
    call-with-current-continuation call-with-deferred-observers 
    call-with-dynamic-root call-with-input-file call-with-input-string 
    call-with-new-thread call-with-output-file call-with-output-string 
    call-with-unblocked-asyncs call-with-values call/cc car catch cdaaar cdaadr 
    cdaar cdadar cdaddr cdadr cdar cddaar cddadr cddar cdddar cddddr cdddr cddr 
    cdr ceiling char->integer char-alphabetic? char-ci<=? char-ci<? char-ci=? 
    char-ci>=? char-ci>? char-downcase char-is-both? char-lower-case? 
    char-numeric? char-ready? char-set char-set->list char-set->string 
    char-set-adjoin char-set-adjoin! char-set-any char-set-complement 
    char-set-complement! char-set-contains? char-set-copy char-set-count 
    char-set-cursor char-set-cursor-next char-set-delete char-set-delete! 
    char-set-diff+intersection char-set-diff+intersection! char-set-difference 
    char-set-difference! char-set-every char-set-filter char-set-filter! 
    char-set-fold char-set-for-each char-set-hash char-set-intersection 
    char-set-intersection! char-set-map char-set-ref char-set-size 
    char-set-unfold char-set-unfold! char-set-union char-set-union! 
    char-set-xor char-set-xor! char-set<= char-set= char-set? char-upcase 
    char-upper-case? char-whitespace? char<=? char<? char=? char>=? char>? 
    char? chdir chmod chown chroot close close-all-ports-except close-fdes 
    close-input-port close-io-port close-output-port close-port closedir 
    closure? command-line compile-define-module-args compile-interface-spec 
    complex? cond-expand-provide connect cons cons* cons-source 
    convert-c-registered-modules copy-file copy-random-state copy-tree cos cosh 
    crypt ctermid current-dynamic-state current-error-port current-input-port 
    current-load-port current-module current-output-port current-thread 
    current-time debug-disable debug-enable debug-object? debug-options 
    debug-options-interface default-duplicate-binding-handler 
    default-duplicate-binding-procedures default-lazy-handler defined? 
    defmacro-transformer defmacro:syntax-transformer defmacro:transformer 
    defmacro? delete delete! delete-file delete1! delq delq! delq1! delv delv! 
    delv1! denominator destroy-guardian! dimensions->uniform-array 
    directory-stream? dirname display display-application display-backtrace 
    display-error display-usage-report doubly-weak-hash-table? drain-input dup 
    dup->fdes dup->inport dup->outport dup->port dup2 duplicate-port 
    dynamic-args-call dynamic-call dynamic-func dynamic-link dynamic-maybe-call 
    dynamic-maybe-link dynamic-object? dynamic-root dynamic-state? 
    dynamic-unlink dynamic-wind effective-version enclose-array 
    end-of-char-set? endgrent endhostent endnetent endprotoent endpwent 
    endservent entity? env-module environ environment-bound? environment-cell 
    environment-define environment-fold environment-module environment-observe 
    environment-observe-weak environment-ref environment-set! 
    environment-undefine environment-unobserve environment? eof-object? eq? 
    equal? eqv? error error-catching-loop error-catching-repl eval eval-disable 
    eval-enable eval-environment-imported eval-environment-local 
    eval-environment-set-imported! eval-environment-set-local! 
    eval-environment? eval-options eval-options-interface eval-string 
    evaluator-traps-interface even? exact->inexact exact? execl execle execlp 
    exit exp export-environment-private export-environment-set-private! 
    export-environment-set-signature! export-environment-signature 
    export-environment? expt f32vector f32vector->list f32vector-length 
    f32vector-ref f32vector-set! f32vector? f64vector f64vector->list 
    f64vector-length f64vector-ref f64vector-set! f64vector? fcntl fdes->inport 
    fdes->outport fdes->ports fdopen feature? file-exists? file-is-directory? 
    file-port? file-position file-set-position fileno filter filter! 
    find-and-link-dynamic-module flock floor fluid-ref fluid-set! fluid? 
    flush-all-ports for-each for-next-option force force-output format 
    frame-arguments frame-evaluating-args? frame-next frame-number 
    frame-overflow? frame-previous frame-procedure frame-procedure? frame-real? 
    frame-source frame? fsync ftell gc gc-live-object-stats gc-run-time 
    gc-stats gcd generalized-vector->list generalized-vector-length 
    generalized-vector-ref generalized-vector-set! generalized-vector? gensym 
    gentemp get-internal-real-time get-internal-run-time get-option 
    get-output-string get-print-state getcwd getegid getenv geteuid getgid 
    getgr getgrent getgrgid getgrnam getgroups gethost gethostbyaddr 
    gethostbyname gethostent gethostname getitimer getlogin getnet getnetbyaddr 
    getnetbyname getnetent getpass getpeername getpgrp getpid getppid 
    getpriority getproto getprotobyname getprotobynumber getprotoent getpw 
    getpwent getpwnam getpwuid getserv getservbyname getservbyport getservent 
    getsockname getsockopt gettext gettimeofday getuid gmtime group:gid 
    group:mem group:name group:passwd guardian-destroyed? guardian-greedy? 
    handle-system-error has-suffix? hash hash-clear! hash-create-handle! 
    hash-fold hash-for-each hash-for-each-handle hash-get-handle hash-map->list 
    hash-ref hash-remove! hash-set! hash-table? hashq hashq-create-handle! 
    hashq-get-handle hashq-ref hashq-remove! hashq-set! hashv 
    hashv-create-handle! hashv-get-handle hashv-ref hashv-remove! hashv-set! 
    hashx-create-handle! hashx-get-handle hashx-ref hashx-remove! hashx-set! 
    hook->list hook-empty? hook? hostent:addr-list hostent:addrtype 
    hostent:aliases hostent:length hostent:name htonl htons identity imag-part 
    import-environment-imports import-environment-set-imports! 
    import-environment? in-vicinity include-deprecated-features inet-aton 
    inet-lnaof inet-makeaddr inet-netof inet-ntoa inet-ntop inet-pton 
    inexact->exact inexact? inf inf? inherit-print-state init-dynamic-module 
    input-port? integer->char integer-expt integer-length integer? 
    interaction-environment intern-symbol iota isatty? 
    issue-deprecation-warning join-thread keyword->symbol keyword-dash-symbol 
    keyword-like-symbol->keyword keyword? kill kw-arg-ref last-pair 
    last-stack-frame lazy-catch lazy-handler-dispatch lcm leaf-environment? 
    length link link-dynamic-module list list* list->array list->bitvector 
    list->c32vector list->c64vector list->char-set list->char-set! 
    list->f32vector list->f64vector list->s16vector list->s32vector 
    list->s64vector list->s8vector list->string list->symbol list->typed-array 
    list->u16vector list->u32vector list->u64vector list->u8vector 
    list->uniform-array list->uniform-vector list->vector list-cdr-ref 
    list-cdr-set! list-copy list-head list-index list-ref list-set! list-tail 
    list? listen load load-emacs-interface load-extension load-from-path 
    load-module load-user-init local-define local-eval local-ref local-remove 
    local-set! localtime lock-mutex log log10 logand logbit? logcount logior 
    lognot logtest logxor lookup-duplicates-handlers lstat macro-name 
    macro-transformer macro-type macro? macroexpand macroexpand-1 magnitude 
    major-version make-arbiter make-array make-autoload-interface 
    make-bitvector make-c32vector make-c64vector make-class-object 
    make-condition-variable make-doubly-weak-hash-table 
    make-duplicates-interface make-dynamic-state make-eval-environment 
    make-export-environment make-f32vector make-f64vector make-fluid 
    make-guardian make-hash-table make-hook make-import-environment 
    make-keyword-from-dash-symbol make-leaf-environment make-list make-module 
    make-modules-in make-mutable-parameter make-mutex make-object-property 
    make-polar make-procedure-with-setter make-record-type make-rectangular 
    make-recursive-mutex make-regexp make-root-module make-s16vector 
    make-s32vector make-s64vector make-s8vector make-scm-module 
    make-shared-array make-socket-address make-soft-port make-stack make-string 
    make-struct make-struct-layout make-subclass-object make-symbol 
    make-typed-array make-u16vector make-u32vector make-u64vector make-u8vector 
    make-undefined-variable make-uniform-array make-uniform-vector 
    make-variable make-vector make-vtable make-vtable-vtable 
    make-weak-key-hash-table make-weak-value-hash-table map map-in-order 
    mask-signals max member memoized-environment memoized? memq memv merge 
    merge! micro-version min minor-version mkdir mknod mkstemp! mktime 
    module-add! module-binder module-bound? module-call-observers module-clear! 
    module-constructor module-define! module-defined? 
    module-duplicates-handlers module-duplicates-interface 
    module-ensure-local-variable! module-eval-closure module-export! 
    module-for-each module-import-interface module-kind module-local-variable 
    module-locally-bound? module-make-local-var! module-map module-modified 
    module-name module-obarray module-obarray-get-handle module-obarray-ref 
    module-obarray-remove! module-obarray-set! module-observe 
    module-observe-weak module-observer-id module-observers 
    module-public-interface module-re-export! module-ref module-remove! 
    module-replace! module-search module-set! module-symbol-binding 
    module-symbol-interned? module-symbol-local-binding 
    module-symbol-locally-interned? module-transformer module-unobserve 
    module-use! module-use-interfaces! module-uses module-variable 
    module-weak-observers module? modulo modulo-expt move->fdes 
    named-module-use! nan nan? negative? nested-define! nested-ref 
    nested-remove! nested-set! netent:addrtype netent:aliases netent:name 
    netent:net newline ngettext nice noop not ntohl ntohs null? number->string 
    number? numerator object->string object-address object-properties 
    object-property odd? open open-fdes open-file open-input-file 
    open-input-string open-io-file open-output-file open-output-string opendir 
    operator? or-map output-port? pair? parse-path passwd:dir passwd:gecos 
    passwd:gid passwd:name passwd:passwd passwd:shell passwd:uid pause peek 
    peek-char pipe pk port->fdes port-closed? port-column port-filename 
    port-for-each port-line port-mode port-revealed port-with-print-state port? 
    positive? primitive-_exit primitive-eval primitive-exit primitive-fork 
    primitive-load primitive-load-path primitive-macro? primitive-make-property 
    primitive-move->fdes primitive-property-del! primitive-property-ref 
    primitive-property-set! print-disable print-enable print-options 
    print-options-interface procedure procedure->macro 
    procedure->memoizing-macro procedure->syntax procedure-documentation 
    procedure-environment procedure-name procedure-properties 
    procedure-property procedure-source procedure-with-setter? procedure? 
    process-define-module process-duplicates process-use-modules 
    program-arguments promise? protoent:aliases protoent:name protoent:proto 
    provide provided? purify-module! putenv quit quotient raise random 
    random:exp random:hollow-sphere! random:normal random:normal-vector! 
    random:solid-sphere! random:uniform rational? rationalize read 
    read-and-eval! read-char read-disable read-enable read-hash-extend 
    read-options read-options-interface readdir readlink real-part real? 
    record-accessor record-constructor record-modifier record-predicate 
    record-type-descriptor record-type-fields record-type-name record-type? 
    record? recv! recvfrom! redirect-port regexp-exec regexp? register-modules 
    release-arbiter release-port-handle remainder remove-hook! rename-file repl 
    repl-reader reset-hook! resolve-interface resolve-module restore-signals 
    restricted-vector-sort! reverse reverse! reverse-list->string rewinddir 
    rmdir round run-asyncs run-hook s16vector s16vector->list s16vector-length 
    s16vector-ref s16vector-set! s16vector? s32vector s32vector->list 
    s32vector-length s32vector-ref s32vector-set! s32vector? s64vector 
    s64vector->list s64vector-length s64vector-ref s64vector-set! s64vector? 
    s8vector s8vector->list s8vector-length s8vector-ref s8vector-set! 
    s8vector? save-module-excursion save-stack scheme-file-suffix scm-error 
    scm-style-repl search-path seed->random-state seek select self-evaluating? 
    send sendto servent:aliases servent:name servent:port servent:proto 
    set-autoloaded! set-batch-mode?! set-car! set-cdr! 
    set-current-dynamic-state set-current-error-port set-current-input-port 
    set-current-module set-current-output-port set-defmacro-transformer! 
    set-module-binder! set-module-duplicates-handlers! 
    set-module-duplicates-interface! set-module-eval-closure! set-module-kind! 
    set-module-name! set-module-obarray! set-module-observer-id! 
    set-module-observers! set-module-public-interface! set-module-transformer! 
    set-module-uses! set-object-procedure! set-object-properties! 
    set-object-property! set-port-column! set-port-filename! set-port-line! 
    set-port-revealed! set-procedure-properties! set-procedure-property! 
    set-program-arguments set-repl-prompt! set-source-properties! 
    set-source-property! set-struct-vtable-name! set-symbol-property! 
    set-system-module! set-tm:gmtoff set-tm:hour set-tm:isdst set-tm:mday 
    set-tm:min set-tm:mon set-tm:sec set-tm:wday set-tm:yday set-tm:year 
    set-tm:zone setegid setenv seteuid setgid setgr setgrent setgroups sethost 
    sethostent sethostname setitimer setlocale setnet setnetent setpgid 
    setpriority setproto setprotoent setpw setpwent setserv setservent setsid 
    setsockopt setter setuid setvbuf shared-array-increments 
    shared-array-offset shared-array-root shutdown sigaction 
    signal-condition-variable simple-format sin sinh sleep sloppy-assoc 
    sloppy-assq sloppy-assv sloppy-member sloppy-memq sloppy-memv sockaddr:addr 
    sockaddr:fam sockaddr:flowinfo sockaddr:path sockaddr:port sockaddr:scopeid 
    socket socketpair sort sort! sort-list sort-list! sorted? source-properties 
    source-property split-c-module-name sqrt stable-sort stable-sort! stack-id 
    stack-length stack-ref stack? standard-eval-closure 
    standard-interface-eval-closure stat stat:atime stat:blksize stat:blocks 
    stat:ctime stat:dev stat:gid stat:ino stat:mode stat:mtime stat:nlink 
    stat:perms stat:rdev stat:size stat:type stat:uid status:exit-val 
    status:stop-sig status:term-sig strerror strftime string string->char-set 
    string->char-set! string->list string->number string->obarray-symbol 
    string->symbol string-any string-any-c-code string-append 
    string-append/shared string-capitalize string-capitalize! string-ci->symbol 
    string-ci< string-ci<= string-ci<=? string-ci<> string-ci<? string-ci= 
    string-ci=? string-ci> string-ci>= string-ci>=? string-ci>? string-compare 
    string-compare-ci string-concatenate string-concatenate-reverse 
    string-concatenate-reverse/shared string-concatenate/shared string-contains 
    string-contains-ci string-copy string-copy! string-count string-delete 
    string-downcase string-downcase! string-drop string-drop-right string-every 
    string-every-c-code string-fill! string-filter string-fold 
    string-fold-right string-for-each string-for-each-index string-hash 
    string-hash-ci string-index string-index-right string-join string-length 
    string-map string-map! string-null? string-pad string-pad-right 
    string-prefix-ci? string-prefix-length string-prefix-length-ci 
    string-prefix? string-ref string-replace string-reverse string-reverse! 
    string-rindex string-set! string-skip string-skip-right string-split 
    string-suffix-ci? string-suffix-length string-suffix-length-ci 
    string-suffix? string-tabulate string-take string-take-right 
    string-titlecase string-titlecase! string-tokenize string-trim 
    string-trim-both string-trim-right string-unfold string-unfold-right 
    string-upcase string-upcase! string-xcopy! string< string<= string<=? 
    string<> string<? string= string=? string> string>= string>=? string>? 
    string? strptime struct-layout struct-ref struct-set! struct-vtable 
    struct-vtable-name struct-vtable-tag struct-vtable? struct? substring 
    substring-fill! substring-move! substring-move-left! substring-move-right! 
    substring/copy substring/read-only substring/shared symbol symbol->keyword 
    symbol->string symbol-append symbol-binding symbol-bound? symbol-fref 
    symbol-fset! symbol-hash symbol-interned? symbol-pref symbol-prefix-proc 
    symbol-property symbol-property-remove! symbol-pset! symbol-set! symbol? 
    symlink sync system system* system-async system-async-mark 
    system-error-errno tan tanh tcgetpgrp tcsetpgrp textdomain thread-exited? 
    throw thunk? times tm:gmtoff tm:hour tm:isdst tm:mday tm:min tm:mon tm:sec 
    tm:wday tm:yday tm:year tm:zone tmpnam tms:clock tms:cstime tms:cutime 
    tms:stime tms:utime top-repl transform-usage-lambda transpose-array 
    trap-disable trap-enable traps truncate truncate-file try-arbiter 
    try-load-module try-module-autoload try-module-dynamic-link 
    try-module-linked try-mutex try-using-libtool-name try-using-sharlib-name 
    ttyname turn-on-debugging typed-array? tzset u16vector u16vector->list 
    u16vector-length u16vector-ref u16vector-set! u16vector? u32vector 
    u32vector->list u32vector-length u32vector-ref u32vector-set! u32vector? 
    u64vector u64vector->list u64vector-length u64vector-ref u64vector-set! 
    u64vector? u8vector u8vector->list u8vector-length u8vector-ref 
    u8vector-set! u8vector? ucs-range->char-set ucs-range->char-set! umask 
    uname uniform-array-read! uniform-array-write uniform-vector->list 
    uniform-vector-fill! uniform-vector-length uniform-vector-read! 
    uniform-vector-ref uniform-vector-set! uniform-vector-write uniform-vector? 
    unintern-symbol unlock-mutex unmask-signals unmemoize-expr unread-char 
    unread-string unsetenv unspecified? use-srfis using-readline? usleep utime 
    utsname:machine utsname:nodename utsname:release utsname:sysname 
    utsname:version valid-object-procedure? values variable-bound? variable-ref 
    variable-set! variable-set-name-hint! variable? vector vector->list 
    vector-copy vector-fill! vector-length vector-move-left! vector-move-right! 
    vector-ref vector-set! vector? version wait-condition-variable waitpid warn 
    warn-autoload-deprecation weak-key-hash-table? weak-value-hash-table? 
    with-continuation-barrier with-dynamic-state with-error-to-file 
    with-error-to-port with-error-to-string with-fluid* with-fluids* 
    with-input-from-file with-input-from-port with-input-from-string 
    with-output-to-file with-output-to-port with-output-to-string 
    with-throw-handler with-traps write write-char xsubstring yield zero?))

(defconst lyqi:scheme-guile-macros
  '(@ @@ @apply @bind @call-with-current-continuation @call-with-values @fop 
    and begin begin-deprecated case collect cond cond-expand debug-set! define 
    define-macro define-module define-option-interface define-private 
    define-public define-syntax-macro defmacro defmacro-public delay do 
    eval-case eval-set! export export-syntax false-if-exception if lambda let 
    let* letrec nil-cond or print-set! quasiquote quote re-export 
    re-export-syntax read-set! require-extension set! start-stack 
    the-environment trap-set! undefine use-modules use-syntax while with-fluids))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; LilyPond syntax
;;;

(require 'cl)
(require 'eieio)





;;;
;;; Lexer states
;;;
(defclass lyqi:lilypond-parser-state (lp:parser-state)
  ((embedded-lilypond :initform nil
                      :initarg :embedded-lilypond
                      :accessor lyqi:embedded-lilypond-state-p)))

(defclass lyqi:base-parser-state (lyqi:lilypond-parser-state)
  ((form-class :initform 'lyqi:verbatim-form)))

(defclass lyqi:toplevel-parser-state (lyqi:base-parser-state) ())
(defclass lyqi:embedded-toplevel-parser-state (lyqi:toplevel-parser-state)
  ((embedded-lilypond :initform t)))
(defclass lyqi:duration?-parser-state (lyqi:lilypond-parser-state) ())
(defclass lyqi:note-duration?-parser-state (lyqi:lilypond-parser-state) ())
(defclass lyqi:note-rest?-parser-state (lyqi:lilypond-parser-state) ())
(defclass lyqi:chord-parser-state (lyqi:base-parser-state) ())
(defclass lyqi:string-parser-state (lyqi:lilypond-parser-state)
  ((lexeme-class :initform 'lyqi:string-lexeme)
   (end-lexeme-class :initform 'lyqi:string-end-lexeme)))
(defclass lyqi:scheme-string-parser-state (lyqi:string-parser-state)
  ((lexeme-class :initform 'lyqi:scheme-string-lexeme)
   (end-lexeme-class :initform 'lyqi:scheme-string-end-lexeme)))
(defclass lyqi:comment-parser-state (lyqi:lilypond-parser-state) ())

(defclass lyqi:scheme-list-parser-state (lyqi:lilypond-parser-state)
  ((depth :initform 1
          :initarg :depth)))
(defmethod lp:same-parser-state-p ((this lyqi:scheme-list-parser-state) other-state)
  (and (call-next-method)
       (= (slot-value this 'depth) (slot-value other-state 'depth))))

(defmethod lyqi:scheme-state-p ((this lyqi:lilypond-parser-state))
  nil)
(defmethod lyqi:scheme-state-p ((this lyqi:scheme-list-parser-state))
  t)

(defun lyqi:make-parser-state (class next-parser-state &rest initargs)
  (apply 'make-instance
         class
         :next-parser-state next-parser-state
         :embedded-lilypond (and next-parser-state
                                 (lyqi:embedded-lilypond-state-p next-parser-state))
         initargs))

;;;
;;; LilyPond syntax (language dependent)
;;;
(defclass lyqi:lilypond-syntax (lp:syntax)
  ((language              :initarg :language
                          :accessor lyqi:language)
   (possible-languages)
   (quick-edit-mode       :initform nil)))

(defmethod initialize-instance :AFTER ((this lyqi:lilypond-syntax) &optional fields)
  (set-slot-value this 'possible-languages
                  (copy-list lyqi:prefered-languages))
  (set-slot-value this 'default-parser-state
                  (make-instance 'lyqi:toplevel-parser-state)))

;;;
;;; Music type mixins
;;;
(defclass lyqi:note-mixin ()
  ((pitch :initarg :pitch)
   (alteration :initarg :alteration
               :initform 0)
   (octave-modifier :initarg :octave-modifier
                    :initform 0)
   (accidental :initform nil
               :initarg :accidental)))

(defclass lyqi:duration-mixin ()
  ((length      :initarg :length
                :initform nil)
   (dot-count   :initarg :dot-count
                :initform 0)
   (numerator   :initarg :numerator
                :initform 1)
   (denominator :initarg :denominator
                :initform 1)))

;;;
;;; Lexemes
;;;

(defclass lyqi:verbatim-lexeme (lp:lexeme) ())

(defclass lyqi:note-lexeme (lp:lexeme lyqi:note-mixin) ())
(defclass lyqi:rest-skip-etc-lexeme (lp:lexeme) ())
(defclass lyqi:rest-lexeme (lyqi:rest-skip-etc-lexeme) ())
(defclass lyqi:note-rest-lexeme (lyqi:rest-lexeme) ())
(defclass lyqi:mm-rest-lexeme (lyqi:rest-skip-etc-lexeme) ())
(defclass lyqi:space-lexeme (lyqi:rest-skip-etc-lexeme) ())
(defclass lyqi:skip-lexeme (lyqi:rest-skip-etc-lexeme) ())
(defclass lyqi:chord-repetition-lexeme (lyqi:rest-skip-etc-lexeme) ())
(defclass lyqi:chord-start-lexeme (lp:lexeme) ())
(defclass lyqi:chord-end-lexeme (lp:lexeme) ())
(defclass lyqi:base-duration-lexeme (lp:lexeme) ())
(defclass lyqi:duration-lexeme (lyqi:base-duration-lexeme lyqi:duration-mixin) ())
(defclass lyqi:no-duration-lexeme (lyqi:base-duration-lexeme) ())

(defclass lyqi:simultaneous-start-lexeme (lp:opening-delimiter-lexeme) ())
(defclass lyqi:simultaneous-end-lexeme (lp:closing-delimiter-lexeme) ())
(defclass lyqi:sequential-start-lexeme (lp:opening-delimiter-lexeme) ())
(defclass lyqi:sequential-end-lexeme (lp:closing-delimiter-lexeme) ())

(defclass lyqi:backslashed-lexeme (lp:lexeme) ())
(defclass lyqi:keyword-lexeme (lp:builtin-lexeme lyqi:backslashed-lexeme) ())
(defclass lyqi:variable-lexeme (lp:variable-name-lexeme lyqi:backslashed-lexeme) ())
(defclass lyqi:function-lexeme (lp:function-name-lexeme lyqi:backslashed-lexeme) ())
(defclass lyqi:user-command-lexeme (lp:keyword-lexeme lyqi:backslashed-lexeme) ())

(defclass lyqi:string-lexeme (lp:string-lexeme) ())

(defclass lyqi:one-line-comment-lexeme (lp:comment-lexeme)
  ((level :initarg :level)))
(defclass lyqi:multi-line-comment-lexeme (lp:comment-lexeme) ())

(defgeneric lyqi:explicit-duration-p (duration)
  "Return T iff `duration' is an explicit duration lexeme,
and NIL if it is an implicit duration lexeme.")
(defmethod lyqi:explicit-duration-p ((this lyqi:base-duration-lexeme))
  nil)
(defmethod lyqi:explicit-duration-p ((this lyqi:duration-lexeme))
  t)

;; scheme
(defclass lyqi:scheme-lexeme (lp:lexeme) ())
(defclass lyqi:scheme-number-lexeme (lyqi:scheme-lexeme) ())
(defclass lyqi:scheme-symbol-lexeme (lyqi:scheme-lexeme)
  ((special-args :initarg :special-args
                 :initform nil)))
(defclass lyqi:scheme-macro-lexeme (lyqi:scheme-symbol-lexeme lp:builtin-lexeme) ())
(defclass lyqi:scheme-function-lexeme (lyqi:scheme-symbol-lexeme lp:function-name-lexeme) ())
(defclass lyqi:scheme-variable-lexeme (lyqi:scheme-symbol-lexeme lp:variable-name-lexeme) ())
(defclass lyqi:sharp-lexeme (lyqi:scheme-lexeme) ())
(defclass lyqi:left-parenthesis-lexeme (lyqi:scheme-lexeme
                                        lp:opening-delimiter-lexeme) ())
(defclass lyqi:right-parenthesis-lexeme (lyqi:scheme-lexeme
                                         lp:closing-delimiter-lexeme) ())
(defclass lyqi:embedded-lilypond-start-lexeme (lyqi:scheme-lexeme
                                               lp:opening-delimiter-lexeme) ())
(defclass lyqi:embedded-lilypond-end-lexeme (lyqi:scheme-lexeme
                                             lp:closing-delimiter-lexeme) ())
(defclass lyqi:scheme-comment-lexeme (lyqi:one-line-comment-lexeme lyqi:scheme-lexeme) ())
(defclass lyqi:scheme-string-lexeme (lp:string-lexeme lyqi:scheme-lexeme) ())

;;;
;;; forms
;;;
(defclass lyqi:verbatim-form (lp:form) ())
(defclass lyqi:music-form (lp:form)
  ((duration :initarg :duration
             ;;:initform nil
             :accessor lyqi:duration-of)))
(defclass lyqi:simple-note-form (lyqi:music-form)
  ((rest :initarg :rest
         :initform nil)))
(defclass lyqi:rest-skip-etc-form (lyqi:music-form) ())
(defclass lyqi:chord-end-form (lyqi:music-form) ())

(defclass lyqi:scheme-list-form (lp:form) ())

(defgeneric lyqi:form-with-duration-p (parser-symbol)
  "If `parser-symbol' is a music form, i.e. somthing
with a duration, then return its duration.")
(defmethod lyqi:form-with-duration-p ((this lp:parser-symbol))
  nil)
(defmethod lyqi:form-with-duration-p ((this lyqi:music-form))
  (lyqi:duration-of this))

(defun lyqi:form-with-note-p (parser-symbol)
  "If `parser-symbol' has, or is a note lexeme, then return the note.
Oterwise, return NIL."
  (cond ((lyqi:note-lexeme-p parser-symbol)
         parser-symbol)
        ((lyqi:simple-note-form-p parser-symbol)
         (loop for lexeme in (slot-value parser-symbol 'children)
               for note = (lyqi:form-with-note-p lexeme)
               if note return note))))

;;;
;;; Fontification of lexemes and forms
;;;

(defmethod lp:fontify ((this lyqi:verbatim-form))
  nil)

(defmethod lp:face ((this lyqi:note-lexeme))
  '(face font-lock-constant-face))

(defmethod lp:face ((this lyqi:chord-start-lexeme))
  '(face font-lock-constant-face))

(defmethod lp:face ((this lyqi:music-form))
  '(face font-lock-constant-face))

(defmethod lp:fontify ((this lyqi:music-form))
  (let* ((start (marker-position (lp:marker this)))
         (end (+ start (lp:size this))))
    (when (> end start)
      (lp:fontify-region start end (lp:face this)))))

;;;
;;; Lex functions
;;;
(defmacro lyqi:with-forward-match (args &rest body)
  (destructuring-bind (regex marker-symbol size-symbol) (if (= (length args) 2)
                                                            (cons nil args)
                                                            args)
    `(let* ((,marker-symbol (point-marker))
            (,size-symbol (progn
                            ,@(if regex `((looking-at ,regex)))
                            (lp:forward-match)
                            (- (point) ,marker-symbol))))
       ,@body)))
(put 'lyqi:with-forward-match 'lisp-indent-function 1)

(defmethod lyqi:reduce-lexemes ((this lp:parser-state) &rest additional-forms)
  (remove-if-not #'identity
                 (cons (lp:reduce-lexemes this)
                       additional-forms)))

(defmethod lyqi:reduce-lexemes ((this lyqi:chord-parser-state) &rest additional-forms)
  (let ((forms (nreverse (slot-value this 'lexemes))))
    (set-slot-value this 'lexemes nil)
    (if additional-forms
        (nconc forms additional-forms)
        forms)))

(defmethod lp:lex ((parser-state lyqi:base-parser-state) syntax)
  (lyqi:skip-whitespace)
  (cond ((eolp)
         ;; at end of line, reduce remaining lexemes
         (values parser-state (lyqi:reduce-lexemes parser-state) nil))
        ;; multi-line comment
        ((looking-at "%{")
         (multiple-value-bind (lexeme comment-ended)
             (lyqi:lex-comment syntax 'lyqi:multi-line-comment-lexeme t)
           (values (if comment-ended
                       parser-state
                       (lyqi:make-parser-state 'lyqi:comment-parser-state parser-state))
                   (lyqi:reduce-lexemes parser-state
                                        lexeme)
                   (not (eolp)))))
        ;; one line comment
        ((looking-at "\\(%+\\).*$")
         (lyqi:with-forward-match (marker size)
           (values parser-state
                   (lyqi:reduce-lexemes parser-state
                                        (make-instance 'lyqi:one-line-comment-lexeme
                                                       :level (- (match-end 1) (match-beginning 1))
                                                       :marker marker
                                                       :size size))
                   nil)))
        ;; string
        ((looking-at "#?\"")
         (multiple-value-bind (lexeme string-ended)
             (lyqi:lex-string syntax 'lyqi:string-lexeme t)
           (values (if string-ended
                       parser-state
                       (lyqi:make-parser-state 'lyqi:string-parser-state parser-state))
                   (lyqi:reduce-lexemes parser-state
                                        lexeme)
                   (not (eolp)))))
        ;; a scheme form
        ((looking-at "#")
         (lyqi:with-forward-match (marker size)
           (let ((sharp-lexeme (make-instance 'lyqi:sharp-lexeme
                                              :marker marker
                                              :size size)))
             (cond ((eolp)
                    (values parser-state
                            (lyqi:reduce-lexemes parser-state sharp-lexeme)
                            nil))
                   ((looking-at "['`$]?(")
                    (lyqi:with-forward-match (marker size)
                      ;; a list form
                      (values (lyqi:make-parser-state 'lyqi:scheme-list-parser-state
                                                      parser-state)
                              (lyqi:reduce-lexemes parser-state
                                                   sharp-lexeme
                                                   (make-instance 'lyqi:left-parenthesis-lexeme
                                                                  :marker marker
                                                                  :size size))
                              (not (eolp)))))
                   ;; a simple token
                   ((lyqi:with-forward-match ("[^ \t\r\n]+" marker size)
                      (values parser-state
                              (lyqi:reduce-lexemes parser-state
                                                   sharp-lexeme
                                                   (make-instance 'lyqi:scheme-lexeme
                                                                  :marker marker
                                                                  :size size))
                              (not (eolp)))))))))
        ;; a backslashed keyword, command or variable
        ((looking-at "[_^-]?\\\\\\([a-zA-Z-]+\\|[<>!]\\)")
         (lyqi:with-forward-match (marker size)
           (let ((sym (intern (match-string-no-properties 1))))
             (values parser-state
                     (lyqi:reduce-lexemes
                      parser-state
                      (make-instance (cond ((memq sym lyqi:lilypond-keywords)
                                            'lyqi:keyword-lexeme)
                                           ((memq sym lyqi:lilypond-music-variables)
                                            'lyqi:variable-lexeme)
                                           ((or (memq sym lyqi:lilypond-music-functions)
                                                (memq sym lyqi:lilypond-markup-commands)
                                                (memq sym lyqi:lilypond-markup-list-commands))
                                            'lyqi:function-lexeme)
                                           (t
                                            'lyqi:user-command-lexeme))
                                     :marker marker
                                     :size size))
                     t))))
        ;; other top level expressions are treated as verbatim
        ;; - lex the verbatim word and add the lexeme to the output parse data
        ;; - continue lexing in {toplevel} state
        (t
         (lp:push-lexeme parser-state (lyqi:lex-verbatim syntax))
         (values parser-state
                 nil
                 t))))

(defmethod lp:lex ((parser-state lyqi:toplevel-parser-state) syntax)
  (lyqi:skip-whitespace)
  (cond ((eolp)
         ;; at end of line, reduce remaining lexemes
         (values parser-state (lyqi:reduce-lexemes parser-state) nil))
        ;; a note
        ;; - reduce preceding verbatim lexemes (if any)
        ;; - lex the note and add the lexeme to the output parse data
        ;; - switch to {note-duration?} lexer state
        ((looking-at (slot-value (lyqi:language syntax) 'note-regex))
         (values (lyqi:make-parser-state 'lyqi:note-duration?-parser-state
                                         parser-state
                                         :form-class 'lyqi:simple-note-form
                                         :lexemes (list (lyqi:lex-note syntax)))
                 (lyqi:reduce-lexemes parser-state)
                 t))
        ;; rest, mm-rest, skip or spacer
        ;; - reduce preceding verbatim lexemes (if any)
        ;; - lex the rest/skip/etc and add the lexeme to the output parse data
        ;; - switch to {duration?} lexer state
        ((looking-at (slot-value (lyqi:language syntax) 'rest-skip-regex))
         (values (lyqi:make-parser-state 'lyqi:duration?-parser-state
                                         parser-state
                                         :form-class 'lyqi:rest-skip-etc-form
                                         :lexemes (list (lyqi:lex-rest-skip-etc syntax)))
                 (lyqi:reduce-lexemes parser-state)
                 t))
        ;; block delimiters
        ((looking-at "\\(<<\\|>>\\|{\\|}\\)")
         (lyqi:with-forward-match (marker size)
           (values parser-state
                   (lyqi:reduce-lexemes parser-state
                                        (make-instance (case (char-after marker)
                                                         ((?\<) 'lyqi:simultaneous-start-lexeme)
                                                         ((?\>) 'lyqi:simultaneous-end-lexeme)
                                                         ((?\{) 'lyqi:sequential-start-lexeme)
                                                         ((?\}) 'lyqi:sequential-end-lexeme))
                                                       :marker marker
                                                       :size size))
                   (not (eolp)))))
        ;; a chord start: '<'
        ;; - reduce preceding verbatim lexemes (if any)
        ;; - lex the chord start and add the lexeme to the output parse data
        ;; - switch to {chord} state
        ((looking-at "<")
         (lyqi:with-forward-match (marker size)
           (values (lyqi:make-parser-state 'lyqi:chord-parser-state
                                           parser-state
                                           :form-class 'lyqi:chord-end-form
                                           :lexemes (list (make-instance 'lyqi:chord-start-lexeme
                                                                         :marker marker
                                                                         :size size)))
                   (lyqi:reduce-lexemes parser-state)
                   t)))
        (t
         (call-next-method))))

(defmethod lp:lex ((parser-state lyqi:duration?-parser-state) syntax)
  (let ((duration (lyqi:lex-duration syntax)))
    (lyqi:skip-whitespace)
    (lp:push-lexeme parser-state duration)
    (let ((music-form (lp:reduce-lexemes parser-state)))
      (set-slot-value music-form 'duration duration)
      (values (lp:next-parser-state parser-state)
              (list music-form)
              (not (eolp))))))

(defmethod lp:lex ((parser-state lyqi:note-duration?-parser-state) syntax)
  (lp:push-lexeme parser-state (lyqi:lex-duration syntax))
  (values (lp:change-parser-state parser-state 'lyqi:note-rest?-parser-state)
          nil
          t))

(defmethod lp:lex ((parser-state lyqi:note-rest?-parser-state) syntax)
  (lyqi:skip-whitespace)
  (let* ((marker (point-marker))
         (rest-lexeme (when (looking-at "\\\\rest")
                        (lp:forward-match)
                        (make-instance 'lyqi:note-rest-lexeme
                                       :marker marker
                                       :size (- (point) marker))))
         (duration-lexeme (first (slot-value parser-state 'lexemes))))
    (when rest-lexeme
      (lp:push-lexeme parser-state rest-lexeme))
    (let ((note-form (lp:reduce-lexemes parser-state)))
      (set-slot-value note-form 'rest (not (not rest-lexeme)))
      (set-slot-value note-form 'duration duration-lexeme)
      (lyqi:skip-whitespace)
      (values (lp:next-parser-state parser-state)
              (list note-form)
              (not (eolp))))))

(defmethod lp:lex ((parser-state lyqi:string-parser-state) syntax)
  (multiple-value-bind (lexeme string-ended)
      (lyqi:lex-string syntax (slot-value parser-state 'lexeme-class) nil)
    (values (if string-ended
                (lp:next-parser-state parser-state)
                parser-state)
            (list lexeme)
            (not (eolp)))))

(defmethod lp:lex ((parser-state lyqi:comment-parser-state) syntax)
  (multiple-value-bind (lexeme comment-ended)
      (lyqi:lex-comment syntax 'lyqi:multi-line-comment-lexeme nil)
    (values (if comment-ended
                (lp:next-parser-state parser-state)
                parser-state)
            (list lexeme)
            (not (eolp)))))

(defmethod lp:lex ((parser-state lyqi:chord-parser-state) syntax)
  (lyqi:skip-whitespace)
  (cond ((eolp)
         ;; this form is on several lines.
         ;; return all lexemes
         (values parser-state
                 (lyqi:reduce-lexemes parser-state)
                 nil))
        ;; a note
        ((looking-at (slot-value (lyqi:language syntax) 'note-regex))
         (lp:push-lexeme parser-state (lyqi:lex-note syntax))
         (values parser-state
                 nil
                 t))
        ;; at chord end '>', switch to {duration?} state
        ((eql (char-after) ?\>)
         (let* ((marker (point-marker)))
           (forward-char 1)
           (let ((forms (lyqi:reduce-lexemes parser-state))
                 (new-state (lp:change-parser-state parser-state 'lyqi:duration?-parser-state)))
             (lp:push-lexeme new-state (make-instance 'lyqi:chord-end-lexeme
                                                      :marker marker
                                                      :size (- (point) marker)))
             (values new-state
                     forms
                     t))))
        (t
         (call-next-method))))

;;;
;;; Basic scheme lexing
;;;

(defmethod lp:lex ((parser-state lyqi:scheme-list-parser-state) syntax)
  (lyqi:skip-whitespace)
  (cond ((eolp)
         (values parser-state
                 nil
                 nil))
        ((looking-at "(")
         (lyqi:with-forward-match (marker size)
           (values (lyqi:make-parser-state 'lyqi:scheme-list-parser-state
                                           parser-state
                                           :depth (1+ (slot-value parser-state 'depth)))
                   (list (make-instance 'lyqi:left-parenthesis-lexeme
                                        :marker marker
                                        :size size))
                   (not (eolp)))))
        ((looking-at ")")
         (lyqi:with-forward-match (marker size)
           (values (lp:next-parser-state parser-state)
                   (list (make-instance 'lyqi:right-parenthesis-lexeme
                                        :marker marker
                                        :size size))
                   (not (eolp)))))
        ((looking-at "#{")
         (lyqi:with-forward-match (marker size)
           (values (lyqi:make-parser-state 'lyqi:embedded-toplevel-parser-state
                                           parser-state)
                   (list (make-instance 'lyqi:embedded-lilypond-start-lexeme
                                        :marker marker
                                        :size size))
                   (not (eolp)))))
        ((looking-at "\"")
         (multiple-value-bind (lexeme string-ended)
             (lyqi:lex-string syntax 'lyqi:scheme-string-lexeme t)
           (values (if string-ended
                       parser-state
                       (lyqi:make-parser-state 'lyqi:scheme-string-parser-state parser-state))
                   (list lexeme)
                   (not (eolp)))))
        ((looking-at "\\(;+\\).*$")
         (lyqi:with-forward-match (marker size)
           (values parser-state
                   (list (make-instance 'lyqi:scheme-comment-lexeme
                                        :level (- (match-end 1) (match-beginning 1))
                                        :marker marker
                                        :size size))
                   nil)))
        ((looking-at "#[ieobx][^ \t\r\n()]+")
         (lyqi:with-forward-match (marker size)
           (values parser-state
                   (list (make-instance 'lyqi:scheme-number-lexeme
                                        :marker marker :size size))
                   (not (eolp)))))
        ((looking-at "[0-9.]+[0-9.#]*\\([esfdl][+-]?[0-9]+\\)?")
         (lyqi:with-forward-match (marker size)
           (values parser-state
                   (list (make-instance 'lyqi:scheme-number-lexeme
                                        :marker marker :size size))
                   (not (eolp)))))
        ;; TODO: literal vectors, etc
        (t ;; symbols
         (lyqi:with-forward-match ("[^ \t\r\n()]+" marker size)
           (let* ((symbol-name (match-string-no-properties 0))
                  (symbol (intern symbol-name))
                  (special-args
                   (case symbol
                     ;; TODO: define a method
                     ((define-markup-command define-music-function
                        defmacro defmacro*-public defmacro-public)
                      '(2 t))
                     ((lambda case let let* parameterize define
                        define-public define-macro)
                      '(1 t))
                     ((begin) '(0 t))
                     (t (if (string-match "define.*" symbol-name)
                            '(1 t)
                            '(nil nil))))))
             (values parser-state
                     (list (make-instance (cond ((or (memq symbol lyqi:scheme-guile-macros)
                                                     (memq symbol lyqi:scheme-lily-macros))
                                                 'lyqi:scheme-macro-lexeme)
                                                ((or (memq symbol lyqi:scheme-guile-procedures)
                                                     (memq symbol lyqi:scheme-lily-procedures))
                                                 'lyqi:scheme-function-lexeme)
                                                ((memq symbol lyqi:scheme-lily-variables)
                                                 'lyqi:scheme-variable-lexeme)
                                                (t
                                                 'lyqi:scheme-symbol-lexeme))
                                            :special-args special-args
                                            :marker marker
                                            :size size))
                       (not (eolp))))))))

(defmethod lp:lex ((parser-state lyqi:embedded-toplevel-parser-state) syntax)
  (lyqi:skip-whitespace)
  (if (looking-at "#}")
      (lyqi:with-forward-match (marker size)
        (values (lp:next-parser-state parser-state)
                (list (make-instance 'lyqi:embedded-lilypond-end-lexeme
                                     :marker marker
                                     :size size))
                (not (eolp))))
      (call-next-method)))

;;;
;;; specific lexing functions
;;;

(defun lyqi:skip-whitespace ()
  "Skip white space (except new lines)."
  (when (looking-at "[ 	]+")
    (lp:forward-match)))

(defun lyqi:lex-verbatim (syntax &optional verbatim-regex)
  (lyqi:with-forward-match ((or verbatim-regex ".[^ \t\r\n\"%<>{}\\]*") marker size)
    (make-instance 'lyqi:verbatim-lexeme
                   :marker marker
                   :size size)))

(defun lyqi:lex-string (syntax lexeme-class with-start)
  "Lex a double-quote delimited string. If `with-start' is true,
then the first double-quote character found after point is the
opening string delimiter. Any character found before this first
double quote is considered as taking part in the string, as the
sharp in: #\" ... \". If `with-start' is NIL, then the opening
double quote is not searched.

Return two values:
- a string lexeme, of class `lexeme-class'
- T if the string is terminated, or NIL otherwise."
  (let ((marker (point-marker))
        (size 0))
    (when with-start
      (looking-at "[^\"]*\"")
      (lp:forward-match)
      (incf size (- (match-end 0) (match-beginning 0))))
    (loop for char = (char-after)
          for next-char = (char-after (1+ (point)))
          if (eolp)
          return (values (make-instance lexeme-class
                                        :marker marker
                                        :size size)
                         nil)
          else if (eql char ?\")
          ;; string end
          do (forward-char 1) and do (incf size)
          and return (values (make-instance lexeme-class
                                            :marker marker
                                            :size size)
                             t)
          ;; an escaped double quote
          else if (and (eql char ?\\) next-char (eql next-char ?\"))
          do (forward-char 2) and do (incf size 2)
          ;; continue scan
          else do (forward-char 1) and do (incf size))))

(defun lyqi:lex-comment (syntax lexeme-class with-start)
  "Lex a multi line lilypond comment. If `with-start' is true,
then %{ is supposed to be found after point. If `with-start' is
NIL, then the opening %{ is not searched.

Return two values:
- a comment lexeme, of class `lexeme-class'
- T if the comment is terminated, or NIL otherwise."
  (let ((marker (point-marker))
        (size 0))
    (when with-start
      (lyqi:with-forward-match ("%{" m s)
        (incf size s)))
    (loop for char = (char-after)
          for next-char = (char-after (1+ (point)))
          if (eolp)
          ;; end of line => comment is not finished
          return (values (make-instance lexeme-class
                                        :marker marker
                                        :size size)
                         nil)
          else if (and next-char (eql char ?\%) (eql next-char ?\}))
          ;; comment end
          do (forward-char 2) and do (incf size 2)
          and return (values (make-instance lexeme-class
                                            :marker marker
                                            :size size)
                             t)
          ;; continue scan
          else do (forward-char 1) and do (incf size))))

(defun lyqi:lex-note (syntax)
  (let ((pitch 0)
        (alteration 0)
        (octave-modifier 0)
        (marker (point-marker))
        (accidental nil))
    (when (looking-at (slot-value (lyqi:language syntax) 'pitch-regex))
      ;; pitch and alteration
      (let ((pitch-data (assoc (match-string-no-properties 0)
                               (slot-value (lyqi:language syntax) 'name->pitch))))
        (setf pitch (second pitch-data))
        (setf alteration (third pitch-data)))
      (lp:forward-match)
      ;; octave
      (when (looking-at (slot-value (lyqi:language syntax) 'octave-regex))
        (setf octave-modifier (* (if (eql (char-after) ?\,) -1 1)
                                 (- (match-end 0) (match-beginning 0))))
        (lp:forward-match))
      ;; accidental
      (cond ((eql (char-after) ?\!)
             (forward-char 1)
             (setf accidental 'forced))
            ((eql (char-after) ?\?)
             (forward-char 1)
             (setf accidental 'cautionary))))
    (make-instance 'lyqi:note-lexeme
                   :pitch pitch
                   :alteration alteration
                   :octave-modifier octave-modifier
                   :accidental accidental
                   :marker marker
                   :size (- (point) marker))))

(defun lyqi:lex-rest-skip-etc (syntax)
  (let* ((marker (point-marker))
         (size 1))
    (make-instance (cond ((looking-at "r")
                          (forward-char 1)
                          'lyqi:rest-lexeme)
                         ((looking-at "R")
                          (forward-char 1)
                          'lyqi:mm-rest-lexeme)
                         ((looking-at "s")
                          (forward-char 1)
                          'lyqi:space-lexeme)
                         ((looking-at "q")
                          (forward-char 1)
                          'lyqi:chord-repetition-lexeme)
                         ((looking-at "\\\\skip")
                          (lp:forward-match)
                          (setf size (- (point) marker))
                          (lyqi:skip-whitespace)
                          'lyqi:skip-lexeme))
                   :marker marker :size size)))

(defun lyqi:lex-duration (syntax)
  (if (or (eolp)
          (not (looking-at (slot-value (lyqi:language syntax) 'duration-regex))))
      ;; implicit duration
      (make-instance 'lyqi:no-duration-lexeme
                     :marker (point-marker)
                     :size 0)
      ;; explicit duration
      (let ((length 2)
            (dot-count 0)
            (num 1)
            (den 1)
            (marker (point-marker)))
        (when (looking-at (slot-value (lyqi:language syntax) 'duration-length-regex))
          ;; length
          (setf length (cdr (assoc (match-string-no-properties 0)
                                   (slot-value (lyqi:language syntax) 'duration-data))))
          (lp:forward-match)
          ;; dots
          (when (and (not (eolp))
                     (looking-at "\\.+"))
            (setf dot-count (- (match-end 0) (match-beginning 0)))
            (lp:forward-match))
          ;; numerator
          (when (and (not (eolp))
                     (looking-at "\\*\\([0-9]+\\)"))
            (setf num (string-to-number (match-string-no-properties 1)))
            (lp:forward-match)
            ;; denominator
            (when (and (not (eolp))
                       (looking-at "/\\([0-9]+\\)"))
              (setf den (string-to-number (match-string-no-properties 1)))
              (lp:forward-match))))
        (make-instance 'lyqi:duration-lexeme
                       :length length
                       :dot-count dot-count
                       :numerator num
                       :denominator den
                       :marker marker
                       :size (- (point) marker)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Midi interface
;;;

(require 'eieio)


(defvar lyqi:midi-backend-object nil)

(defun lyqi:start-midi-backend ()
  (when (and lyqi:midi-backend (not lyqi:midi-backend-object))
    (setq lyqi:midi-backend-object
          (case lyqi:midi-backend
            ;; TODO: alsa backend
            ((osx)  (make-instance 'lyqi:osx-midi-player))))))

(defun lyqi:stop-midi-backend ()
  (when lyqi:midi-backend-object
    (lyqi:player-terminate)
    (setq lyqi:midi-backend-object nil)))
    
(defun lyqi:play-note (note)
  (when lyqi:midi-backend-object
    (lyqi:player-play-note lyqi:midi-backend-object note)))

;;;
;;; Backends
;;;

(defclass lyqi:midi-player ()
  ((last-note :initform nil)))

(defgeneric lyqi:player-play-midi-note (player midi-note)
  "Tell `player' to play `midi-note', a number.")
(defgeneric lyqi:player-terminate (player)
  "Tell `player' to terminate.")

(defmethod lyqi:player-terminate ((this lyqi:midi-player))
  t)

(defmethod lyqi:player-play-note ((player lyqi:midi-player) note &optional ref-note)
  "Tell `player' to play `note'.  If `ref-note' is non-NIL, then
consider that `note' is relative to `ref-note'."
  (let ((midi-note (if ref-note
                       (lyqi:midi-relative-pitch note ref-note)
                       (lyqi:midi-absolute-pitch note))))
    (lyqi:player-play-midi-note player midi-note)))

(defun lyqi:midi-absolute-pitch (note)
  (with-slots (pitch alteration octave-modifier) note
    (+ (aref [0 2 4 5 7 9 11] pitch)
       (/ alteration 2)
       (* octave-modifier 12)
       48)))

(defun lyqi:midi-relative-pitch (note ref-note)
  ;; TODO
  (lyqi:midi-absolute-pitch note))

;;;
;;; Mac OS X: MidiScript
;;;

(defclass lyqi:osx-midi-player (lyqi:midi-player) ())

(defmethod lyqi:player-play-midi-note ((player lyqi:osx-midi-player) midi-note)
  ;(do-applescript (format "ignoring application responses tell application \"MidiScript\" to playnote %d" midi-note))
  (start-process-shell-command
   "midiscript" nil
   (format "osascript -e 'tell application \"MidiScript\" to playnote %d'"
           midi-note)))

;;;
;;; Alsa (Linux): lyqikbd
;;;

;;; TODO


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Editing commands
;;;

(require 'cl)






(defvar lyqi:*alterations* (make-vector 7 0))

(defmacro lyqi:save-excursion-unless-farther (&rest body)
  (let ((final-position (gensym "final-position"))
        (result (gensym "result")))
    `(let* (,final-position
            (,result (save-excursion
                       (prog1
                           (progn
                             ,@body)
                         (setf ,final-position (point))))))
       (when (< (point) ,final-position)
         (goto-char ,final-position)))))
(put 'lyqi:save-excursion-unless-farther 'lisp-indent-function 0)

;;;
;;; Commands on duration
;;; - change last duration
;;; - change last duration dot number
;;;
(defun lyqi:find-durations-backward (syntax position)
  "Find one duration (implicit or explicit) plus one explicit duration, backwards.
Return two values."
  (loop with duration1 = nil
        for (form current-line rest-forms) = (lp:form-before-point syntax position)
        then (lp:previous-form current-line rest-forms)
        while form
        for duration = (lyqi:form-with-duration-p form)
        if (and (not duration1) duration)
        do (setf duration1 duration)
        else if (and duration1 duration (lyqi:explicit-duration-p duration))
        return (values duration1 duration)
        finally return (values duration1 nil)))

(defun lyqi:same-duration-p (duration-number duration-object)
  "Test if `duration-object' is equal to `duration-number',
i.e. if (log duration 2) == length of `duration-object', and
duration object has no dot nor numerator nor denominator."
  (and (= (slot-value duration-object 'length)
          (round (log duration-number 2)))
       (= (slot-value duration-object 'dot-count) 0)
       (= (slot-value duration-object 'numerator) 1)
       (= (slot-value duration-object 'denominator) 1)))

(defun lyqi:change-duration (syntax duration)
  "Change the duration of the music found at or before point.
`duration': a number, e.g. 1 2 4 8 16 32 etc"
  (when (and (not (eolp))
             (eql (char-after (point)) ?\>)
             (not (eql (char-after (1+ (point))) ?\>)))
    (forward-char))
  (multiple-value-bind (current-duration previous-duration)
      (lyqi:find-durations-backward syntax (point))
    (when current-duration
      (lyqi:save-excursion-unless-farther
        (goto-char (lp:marker current-duration))
        (cond ((not (lyqi:explicit-duration-p current-duration))
               ;; if current duration is implicit, insert the new
               ;; duration
               (insert (number-to-string duration)))
              ((or (and previous-duration (lyqi:same-duration-p duration previous-duration))
                   (lyqi:same-duration-p duration current-duration))
               ;; if the new duration is equal to previous duration or
               ;; current duration, then just delete current duration
               (delete-char (lp:size current-duration)))
              (t
               ;; delete the current duration and insert the new one
               (combine-after-change-calls
                 (delete-char (lp:size current-duration))
                 (insert (number-to-string duration)))))))))

(defun lyqi:change-duration-1 ()
  "Change the duration of the music found at or before point to 1."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 1))

(defun lyqi:change-duration-2 ()
  "Change the duration of the music found at or before point to 2."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 2))

(defun lyqi:change-duration-4 ()
  "Change the duration of the music found at or before point to 4."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 4))

(defun lyqi:change-duration-8 ()
  "Change the duration of the music found at or before point to 8."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 8))

(defun lyqi:change-duration-16 ()
  "Change the duration of the music found at or before point to 16."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 16))

(defun lyqi:change-duration-32 ()
  "Change the duration of the music found at or before point to 32."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 32))

(defun lyqi:change-duration-64 ()
  "Change the duration of the music found at or before point to 64."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 64))

(defun lyqi:change-duration-128 ()
  "Change the duration of the music found at or before point to 128."
  (interactive)
  (lyqi:change-duration (lp:current-syntax) 128))

(defun lyqi:change-duration-dots ()
  "Increase the previous music duration dot number, modulo 5"
  (interactive)
  (let ((syntax (lp:current-syntax)))
    (multiple-value-bind (current-duration previous-duration)
        (lyqi:find-durations-backward syntax (point))
      (when current-duration
        (let ((explicit (lyqi:explicit-duration-p current-duration)))
          (multiple-value-bind (main-duration dot-count numerator denominator)
              (let ((ref-duration (if explicit
                                      current-duration
                                      previous-duration)))
                (if ref-duration
                    (values (car (rassoc (slot-value ref-duration 'length)
                                         (slot-value (lyqi:language syntax) 'duration-data)))
                            (mod (1+ (slot-value ref-duration 'dot-count)) 5)
                            (slot-value ref-duration 'numerator)
                            (slot-value ref-duration 'denominator))
                    (values "4" 1 1 1)))
            (lyqi:save-excursion-unless-farther
              (goto-char (lp:marker current-duration))
              (combine-after-change-calls
                (when explicit
                  (delete-char (lp:size current-duration)))
                (insert (concat main-duration
                                (make-string dot-count ?\.)
                                (cond ((and (= numerator 1) (= denominator 1))
                                       "")
                                      ((= denominator 1)
                                       (concat "*" (number-to-string numerator)))
                                      (t
                                       (concat "*" (number-to-string numerator)
                                               "/" (number-to-string denominator))))))))))))))

;;;
;;; Commands on rest, skip, etc
;;;
(defun lyqi:maybe-insert-space ()
  (unless (or (bolp)
              (= (char-before (point)) ?\ )
              (= (char-before (point)) ?\t)
              (= (char-before (point)) ?\<))
    (insert " ")))

(defun lyqi:maybe-insert-space-after ()
  (unless (or (eolp)
              (member (char-after)
                      '(?\- ?\_ ?\^ ?\( ?\) ?\[ ?\] ?\\ ?\1 ?\2 ?\4 ?\8 ?\3 ?\6 ?\> ?\ )))
    (insert " ")))

(defmacro lyqi:with-space-around (&rest body)
  `(progn
     (lyqi:maybe-insert-space)
     (prog1
         (progn
           ,@body)
       (lyqi:maybe-insert-space-after))))
(put 'lyqi:with-space-around 'lisp-indent-function 1)

(defun lyqi:insert-rest ()
  "Insert a rest at point"
  (interactive)
  (combine-after-change-calls
    (lyqi:with-space-around
     (insert "r"))))

(defun lyqi:insert-mm-rest ()
  "Insert a rest at point"
  (interactive)
  (combine-after-change-calls
    (lyqi:with-space-around
     (insert "R"))))

(defun lyqi:insert-spacer ()
  "Insert a skip at point"
  (interactive)
  (combine-after-change-calls
    (lyqi:with-space-around
     (insert "s"))))

(defun lyqi:insert-chord-repetition ()
  "Insert a chord repetition at point"
  (interactive)
  (combine-after-change-calls
    (lyqi:with-space-around
     (insert "q"))))

(defun lyqi:insert-skip ()
  "Insert a \\skip ar point"
  (interactive)
  (combine-after-change-calls
    (lyqi:with-space-around
     (insert "\\skip 4")))) ;; TODO: use previous duration?

;;;
;;; Commands on notes
;;; - insert a note
;;; - change a note octave
;;; - change a note alteration
;;;

(defun lyqi:find-note-backward (syntax position &optional stop-on-rests)
  "Find note backwards.  If `stop-on-rests' is not NIL, then stop
searching as soon as a rest, skip, etc is found."
  (loop for (form current-line rest-forms) = (lp:form-before-point syntax position)
        then (lp:previous-form current-line rest-forms)
        while form
        if (and stop-on-rests (lyqi:rest-skip-etc-form-p form)) return nil
        for note = (lyqi:form-with-note-p form)
        if note return note))

(defun lyqi:insert-note (syntax note play-note &optional insert-default-duration)
  (with-slots (pitch alteration octave-modifier accidental) note
    (let ((note-string (lyqi:pitchname (lyqi:language syntax) pitch alteration))
          (octave-string (cond ((= octave-modifier 0)
                                "")
                               ((> octave-modifier 0)
                                (make-string octave-modifier ?\'))
                               (t
                                (make-string (- octave-modifier) ?\,))))
          (accidental-string (case accidental
                               ((forced) "!")
                               ((cautionary) "?")
                               (t ""))))
      (insert (format "%s%s%s%s"
                      note-string octave-string accidental-string
                      (if insert-default-duration "4" "")))))
  (when play-note
    (lyqi:play-note note)))

(defun lyqi:re-insert-note (syntax note play-note)
  (goto-char (lp:marker note))
  (combine-after-change-calls
    (delete-char (lp:size note))
    (lyqi:insert-note syntax note play-note)))

(defun lyqi:insert-note-by-pitch (pitch)
  (let* ((syntax (lp:current-syntax))
         (previous-note (lyqi:find-note-backward syntax (point)))
         (new-note (if previous-note
                       (with-slots ((pitch0 pitch) (octave0 octave-modifier)) previous-note
                         (make-instance 'lyqi:note-mixin
                                        :pitch pitch
                                        :alteration (aref lyqi:*alterations* pitch)
                                        :octave-modifier (cond ((> (- pitch pitch0) 3) (1- octave0))
                                                               ((> (- pitch0 pitch) 3) (1+ octave0))
                                                               (t octave0))))
                       (make-instance 'lyqi:note-mixin
                                      :pitch pitch
                                      :alteration (aref lyqi:*alterations* pitch))))
         (previous-duration
          (multiple-value-bind (current-duration previous-duration)
              (lyqi:find-durations-backward syntax (point))
            current-duration)))
    (combine-after-change-calls
      (lyqi:with-space-around
       (lyqi:insert-note syntax new-note t (not previous-duration))))))

(defun lyqi:insert-note-c ()
  "Insert a c/do note at point"
  (interactive)
  (lyqi:insert-note-by-pitch 0))

(defun lyqi:insert-note-d ()
  "Insert a d/re note at point"
  (interactive)
  (lyqi:insert-note-by-pitch 1))

(defun lyqi:insert-note-e ()
  "Insert a e/mi note at point"
  (interactive)
  (lyqi:insert-note-by-pitch 2))

(defun lyqi:insert-note-f ()
  "Insert a f/fa note at point"
  (interactive)
  (lyqi:insert-note-by-pitch 3))

(defun lyqi:insert-note-g ()
  "Insert a g/sol note at point"
  (interactive)
  (lyqi:insert-note-by-pitch 4))

(defun lyqi:insert-note-a ()
  "Insert a a/la note at point"
  (interactive)
  (lyqi:insert-note-by-pitch 5))

(defun lyqi:insert-note-b ()
  "Insert a b/si note at point"
  (interactive)
  (lyqi:insert-note-by-pitch 6))

(defun lyqi:change-alteration (alteration-direction)
  (let* ((syntax (lp:current-syntax))
         (note (lyqi:find-note-backward syntax (point) t)))
    (when note
      (with-slots (pitch alteration) note
        (let ((new-alteration (cond ((> alteration-direction 0) ;; increment
                                     (cond ((= alteration -2) 0) ;;  flat -> neutral
                                           ((= alteration 0) 2) ;;  neutral -> sharp
                                           ((or (= alteration 2) ;;  sharp -> sharp
                                                (= alteration 4)) alteration)
                                           (t (1+ alteration))))
                                    ((< alteration-direction 0) ;; decrement
                                     (cond ((= alteration 2) 0) ;;  sharp -> neutral
                                           ((= alteration 0) -2) ;;  neutral -> flat
                                           ((or (= alteration -2) ;;  flat -> flat
                                                (= alteration -4)) alteration)
                                           (t (1- alteration))))
                                    (t 0)))) ;; neutral
          (when (/= alteration new-alteration)
            (set-slot-value note 'alteration new-alteration)
            ;; memorize alteration
            (aset lyqi:*alterations* pitch new-alteration)
            ;; delete old note, and insert new one
            (lyqi:save-excursion-unless-farther
              (lyqi:re-insert-note syntax note t))))))))

(defun lyqi:change-alteration-up ()
  "Increase the alteration of the note found at or before point:
flat -> neutral -> sharp"
  (interactive)
  (lyqi:change-alteration 1))

(defun lyqi:change-alteration-down ()
  "Decrease the alteration of the note found at or before point:
sharp -> neutral -> flat"
  (interactive)
  (lyqi:change-alteration -1))

(defun lyqi:change-alteration-neutral ()
  "Set the alteration of the note found at or before point to neutral"
  (interactive)
  (lyqi:change-alteration 0))

(defun lyqi:change-octave (octave-direction)
  (let* ((syntax (lp:current-syntax))
         (note (lyqi:find-note-backward syntax (point) t)))
    (when note
      (with-slots (octave-modifier) note
        (let ((new-octave (if (= 0 octave-direction)
                              0
                              (+ octave-direction octave-modifier))))
          (when (/= octave-modifier new-octave)
            (set-slot-value note 'octave-modifier new-octave)
            ;; delete old note, and insert new one
            (lyqi:save-excursion-unless-farther
              (lyqi:re-insert-note syntax note t))))))))

(defun lyqi:change-octave-up ()
  "Increase the octave of the note found at or before point"
  (interactive)
  (lyqi:change-octave 1))

(defun lyqi:change-octave-down ()
  "Decrease the octave of the note found at or before point"
  (interactive)
  (lyqi:change-octave -1))

(defun lyqi:change-octave-zero ()
  "Reset the octave of the note found at or before point"
  (interactive)
  (lyqi:change-octave 0))

;;;
;;; Transposition
;;;

(defun lyqi:transpose-note (note syntax note-diff alteration-diff)
  "Transpose `note'. Ex: (lyqi:transpose-note [do] -5 -2) -> [mib,]"
  (with-slots (pitch octave-modifier alteration) note
    (let* ((scale-up   [0 2 4 5 7 9 11])
           (scale-down [0 2 3 5 7 8 10])
           (new-pitch (mod (+ pitch note-diff) 7))
           (new-octave (+ octave-modifier
                          (cond ((< (+ pitch note-diff) 0)
                                 (/ (+ pitch note-diff -6) 7))
                                ((> (+ pitch note-diff) 6)
                                 (/ (+ pitch note-diff) 7))
                                (t 0))))
           (half-tones-from-C )
           (half-tones-from-pitch )
           (new-alteration (+ alteration
                              alteration-diff
                              (* 2 (- (aref (if (minusp note-diff)
                                                scale-down
                                                scale-up)
                                            (mod note-diff 7))
                                      (mod (- (aref scale-up new-pitch)
                                              (aref scale-up pitch))
                                           12))))))
      (set-slot-value note 'pitch new-pitch)
      (set-slot-value note 'alteration new-alteration)
      (set-slot-value note 'octave-modifier new-octave)))
  (lyqi:re-insert-note syntax note nil))

(defun lyqi:transpose-region (interval)
  "Transpose current region by `interval', a number possibly followed by `+' or `-'.
For instance:
  \"3\"  transpose a \"do\" to a \"mi\"
  \"3-\" transpose a \"do\" to a \"mib\"
  \"4+\" transpose a \"do\" to a \"fad\"
  \"-3\" transpose a \"do\" to a \"lad,\"
"
  (interactive "sTranspose by interval (interval[+-]): ")
  (let* ((num (string-to-number interval))
         (adj (substring interval -1)))
    (save-excursion
      (loop with syntax = (lp:current-syntax)
            with note-diff = (cond ((< num 0) (1+ num))
                                   ((> num 0) (1- num))
                                   (t num))
            with alteration-diff = (cond ((equal adj "+") 2)
                                         ((equal adj "-") -2)
                                         (t 0))
            with beginning = (region-beginning)
            for (form current-line rest-forms) = (lp:form-before-point syntax (region-end))
            then (lp:previous-form current-line rest-forms)
            while (and form (>= (lp:marker form) beginning))
            if (lyqi:note-lexeme-p form)
            do (lyqi:transpose-note form syntax note-diff alteration-diff)
            if (lyqi:simple-note-form-p form)
            do (loop for lexeme in (slot-value form 'children)
                     if (lyqi:note-lexeme-p lexeme)
                     do (lyqi:transpose-note lexeme syntax note-diff alteration-diff))))))

;;;
;;; misc editing commands
;;;

(defun lyqi:insert-pipe-and-return ()
  "Insert a bar check, a line feed, and indent the line"
  (interactive)
  (if (or (bolp) (= (char-before) ?\|))
      (insert "\n")
      (progn
        (lyqi:maybe-insert-space)
        (insert "|\n")
        (lyqi:indent-line))))

(defun lyqi:insert-opening-delimiter (&optional n)
  "Insert an opening delimiter and the corresponding closing
delimiter.  This depends on the current context: in a LilyPond
block, delimiters are {} and <>,
whereas in a Scheme block, delimiters are parens."
  (interactive "p")
  (multiple-value-bind (form line rest-forms)
      (lp:form-before-point (lp:current-syntax) (point))
    (let ((delim last-command-event))
      (if (and form (object-of-class-p form 'lyqi:scheme-lexeme))
          ;; scheme context
          (if (= delim ?\()
              (progn
                (insert "()")
                (backward-char))
              (insert-char delim n))
          ;; LilyPond context
          (let ((closing-delim (cdr (assq delim '((?\< . ?\>) (?\{ . ?\}))))))
            (if closing-delim
                (progn
                  (lyqi:maybe-insert-space)
                  ;; TODO: account for prefix argument
                  (insert (format "%c%c" delim closing-delim))
                  (backward-char))
                (insert-char delim n)))))))

(defun lyqi:insert-closing-delimiter (&optional n)
  "Insert a closing delimiter, unless char after point is the same closing delimiter."
  (interactive "p")
  (let ((delim last-command-event))
    (if (and (not (eolp))
             (= delim (char-after (point))))
        (forward-char)
        (insert-char delim n))))

(defun lyqi:insert-delimiter (&optional n)
  "Insert a delimiter twice, unless char after point is this delimiter."
  (interactive "p")
  (let ((delim last-command-event))
    (if (and (not (eolp))
             (= delim (char-after (point))))
        (forward-char)
        (progn
          (insert-char delim (* 2 (or n 1)))
          (backward-char (or n 1))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Indentation commands
;;;




(defvar lyqi:indent-level 2)

(defun lyqi:offset-from-bol (token)
  "Return the number of columns from the beginning of the line
where `token' is placed, to `token'"
  (save-excursion
    (goto-char (lp:marker token))
    (- (point) (point-at-bol))))

(defun lyqi:indent-line ()
  "Indent current line."
  (interactive)
  (lp:without-parse-update
    (let (final-position)
      (save-excursion
        (forward-line 0)
        (let* ((syntax (lp:current-syntax))
               (line (first (lp:find-lines syntax (point-marker)))))
          (indent-line-to
           (max 0
                (cond ((bobp) 0)
                      ((or (lyqi:scheme-state-p (slot-value line 'parser-state))
                           (and (lp:line-forms line)
                                (object-of-class-p (first (lp:line-forms line))
                                                   'lyqi:embedded-lilypond-end-lexeme)))
                       (lyqi:scheme-line-indentation line))
                      (t (lyqi:lilypond-line-indentation line)))))
          (setf final-position (point))
          ;; reparse the line
          ;; FIXME
          (lp:reparse-line syntax line)))
      (when (< (point) final-position)
        (goto-char final-position)))))

(defun lyqi:indent-region (start end)
  (save-excursion
    (goto-char end)
    (let ((end-position (point-at-bol))
          (endmark (copy-marker end)))
      (goto-char start)
      (forward-line 0)
      (loop for pos = (point)
            while (<= pos end-position)
            if (and (bolp) (not (eolp))) do (lyqi:indent-line)
            do (forward-line 1))
      (set-marker endmark nil))))

(defun lyqi:sexp-beginning (indent-line)
  "Return two values: the opening parenthesis and the first forms,
if any, of the s-expression containing `indent-line' beginning."
  (loop with depth = 0
        with sexp-forms = nil
        with in-scheme = t
        for (form line rest-forms) = (lp:previous-form indent-line)
        then (lp:previous-form line rest-forms)
        while form
        if (object-of-class-p form 'lyqi:embedded-lilypond-end-lexeme)
        do (setf in-scheme nil)
        if (object-of-class-p form 'lyqi:embedded-lilypond-start-lexeme)
        do (setf in-scheme t)
        if (and in-scheme (lp:closing-delimiter-p form))
        do (incf depth)
        else if (and in-scheme (lp:opening-delimiter-p form))
        do (cond ((> depth 1)
                  (decf depth))
                 ((= depth 1)
                  (decf depth)
                  (push form sexp-forms))
                 (t
                  ;; s-expr starting left parenthesis is found
                  (return (values form sexp-forms))))
        else if (and in-scheme (= depth 0))
        do (push form sexp-forms)))

(defun lyqi:scheme-line-indentation (indent-line)
  "Compute the indentation of a scheme line.  Returns a number of
spaces from the beginning of the line.

`indent-line': line-parse object corresponding to the line to be
indented."
  (multiple-value-bind (sexp-paren sexp-forms)
      (lyqi:sexp-beginning indent-line)
    ;; TODO: indent-line starts with a closing paren
    (if (first sexp-forms)
        ;; line to be indented starts with a function or macro
        ;; argument, or with an element of a literal list (but not
        ;; the first one)
        (let* ((first-form (first sexp-forms))
               (operator-form (and (object-of-class-p first-form 'lyqi:scheme-symbol-lexeme)
                                   first-form))
               (arg-forms (rest sexp-forms))
               (special-args (and operator-form
                                  (slot-value operator-form 'special-args)))
               (is-special (second special-args))
               (nb-special (first special-args))
               (next-arg-is-special (and is-special
                                         (or (eql nb-special t) ;; all specials
                                             (> (- nb-special (length arg-forms)) 0)))))
          (cond (next-arg-is-special
                 (+ (lyqi:offset-from-bol sexp-paren) (* 2 lyqi:indent-level)))
                (is-special
                 (+ (lyqi:offset-from-bol sexp-paren) lyqi:indent-level))
                (arg-forms
                 (lyqi:offset-from-bol (first arg-forms)))
                (operator-form
                 (lyqi:offset-from-bol operator-form))
                (t
                 (+ (lyqi:offset-from-bol sexp-paren) (lp:size sexp-paren)))))
        ;; line to be indented starts with the list first form
        (+ (lyqi:offset-from-bol sexp-paren) (lp:size sexp-paren)))))

(defun lyqi:line-start-with-closing-delimiter (line)
  (and (lp:line-forms line)
       (lp:closing-delimiter-p (first (lp:line-forms line)))))

;; As a start, make things easy: use previous non-empty line
;; indentation, and account for nesting level compared to the
;; beginning of this line
(defun lyqi:lilypond-line-indentation (indent-line)
  (lyqi:calc-lilypond-line-indentation
   (lp:previous-line indent-line)
   (lyqi:embedded-lilypond-state-p (slot-value indent-line 'parser-state))
   (if (lyqi:line-start-with-closing-delimiter indent-line)
       -1
       0)
   t))

(defun lyqi:calc-lilypond-line-indentation (line embedded-lilypond nesting-level in-lilypond)
  (let* ((new-nesting-level nesting-level)
         (new-in-lilypond in-lilypond)
         (line-indent (loop for forms on (reverse (lp:line-forms line))
                            for previous-form = nil then form
                            for form = (first forms)
                            for is-line-first-form = (not (rest forms))
                            if (and embedded-lilypond
                                    (object-of-class-p form 'lyqi:embedded-lilypond-start-lexeme))
                            ;; #{ token starting the embedded lilypond block
                            ;; that means that the line to be indented is a line of
                            ;; embedded lilypond, following a line with #{
                            ;; if #{ is the first token on its line, align next line
                            ;; with it. Otherwise, use an arbitrary lyqi:indent-level
                            ;; indentation.
                            return (cond ((and is-line-first-form previous-form)
                                          (lyqi:offset-from-bol previous-form))
                                         (is-line-first-form
                                          (+ (lyqi:offset-from-bol form)
                                             (lp:size form)
                                             1))
                                         (t
                                          lyqi:indent-level))
                            else if new-in-lilypond
                            do (cond ((object-of-class-p form 'lyqi:scheme-lexeme)
                                      (setf new-in-lilypond nil))
                                     ((lp:opening-delimiter-p form)
                                      (incf new-nesting-level))
                                     ((and (lp:closing-delimiter-p form) (not is-line-first-form))
                                      (decf new-nesting-level)))
                            else if (object-of-class-p form 'lyqi:sharp-lexeme)
                            do (setf new-in-lilypond t)
                            ;; when reaching the first token on line
                            ;; if in lilypond code, then return the line indent
                            if is-line-first-form return (and new-in-lilypond (lyqi:offset-from-bol form)))))
    (cond (line-indent
           (+ line-indent (* new-nesting-level lyqi:indent-level)))
          ((lp:previous-line line)
           (lyqi:calc-lilypond-line-indentation (lp:previous-line line)
                                                embedded-lilypond
                                                new-nesting-level
                                                new-in-lilypond))
          (t
           0))))
        


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Compilation commands
;;;

(eval-when-compile (require 'cl))

;;;
;;; Master file, used for compilation
;;; - if `lyqi:buffer-master-file' (which is buffer local) is non-NIL,
;;;   use its value;
;;; - if `lyqi:global-master-file' is non-NIL, use its value;
;;; - if curent-buffer is a .ly file, use this file.
;;;

(defvar lyqi:global-master-file nil
  "Master .ly file for LilyPond compilation (if
  `lyqi:buffer-master-file' is NIL).")

(defvar lyqi:buffer-master-file nil
  "Master .ly file for LilyPond compilation for current
  buffer (overrides `lyqi:global-master-file').")

(defun lyqi:master-file ()
  (let ((pathname (buffer-file-name)))
    (or (and (string-equal (file-name-extension pathname) "ly")
             pathname)
        lyqi:buffer-master-file
        lyqi:global-master-file)))

(defun lyqi:defined-master-file ()
  (and (not (string-equal (file-name-extension  (buffer-file-name)) "ly"))
       (or lyqi:buffer-master-file
           lyqi:global-master-file)))

(defun lyqi:set-buffer-master-file (filename)
  (interactive "fBuffer master file:")
  (setq lyqi:buffer-master-file filename))

(defun lyqi:set-global-master-file (filename)
  (interactive "fGlobal master file:")
  (setq lyqi:global-master-file filename))

(defun lyqi:unset-master-file ()
  (interactive "")
  (cond (lyqi:buffer-master-file
         (setq lyqi:buffer-master-file nil))
        (lyqi:global-master-file
         (setq lyqi:global-master-file nil))))

;;;
;;; LilyPond compilation
;;;

(defun lyqi:compile-command (command ext)
  (when (lyqi:master-file)
    (let* ((pathname (file-truename (lyqi:master-file)))
           (directory (file-name-directory pathname))
           (basename (file-name-sans-extension (file-name-nondirectory pathname)))
           (command (format "cd %s; %s %s.%s"
                            (shell-quote-argument directory)
                            command
                            (shell-quote-argument basename)
                            ext)))
    (compilation-start command))))

(defun lyqi:compile-ly ()
  (interactive)
  (lyqi:compile-command lyqi:lilypond-command "ly"))

(defun lyqi:open-pdf ()
  (interactive)
  (lyqi:compile-command lyqi:pdf-command "pdf"))

(defun lyqi:open-midi ()
  (interactive)
  (lyqi:compile-command lyqi:midi-command "midi"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Completion
;;;

(defvar lyqi:backslashed-words
  (sort (mapcar 'symbol-name (append lyqi:lilypond-keywords
                                     lyqi:lilypond-music-variables
                                     lyqi:lilypond-music-functions
                                     lyqi:lilypond-markup-commands
                                     lyqi:lilypond-markup-list-commands))
        'string-lessp))

(defvar lyqi:scheme-words
  (sort (mapcar 'symbol-name (append lyqi:scheme-lily-procedures
                                     lyqi:scheme-lily-variables
                                     lyqi:scheme-lily-macros
                                     lyqi:scheme-guile-procedures
                                     lyqi:scheme-guile-macros))
        'string-lessp))

(defclass lyqi:abbrev ()
  ((abbrev :initarg :abbrev)
   (collection :initarg :collection)
   (start-position :initarg :start-position)))

(defgeneric lyqi:form-abbrev (form)
  "Return the completion data of form, or NIL is form cannot be completed.")

(defmethod lyqi:form-abbrev ((this lp:parser-symbol))
  nil)

(defmethod lyqi:form-abbrev ((this lyqi:backslashed-lexeme))
  (multiple-value-bind (start abbrev)
      (let ((string (lp:string this)))
        (string-match ".*\\\\\\(.*\\)" string)
        (values (+ (match-beginning 1) (lp:marker this))
                (match-string 1 string)))
    (make-instance 'lyqi:abbrev
                   :abbrev abbrev
                   :collection lyqi:backslashed-words
                   :start-position start)))

(defmethod lyqi:form-abbrev ((this lyqi:scheme-symbol-lexeme))
  (make-instance 'lyqi:abbrev
                 :abbrev (lp:string this)
                 :collection lyqi:scheme-words
                 :start-position (lp:marker this)))

(defmethod lyqi:complete-abbrev ((this lyqi:abbrev))
  (let* ((abbrev (slot-value this 'abbrev))
         (collection (slot-value this 'collection))
         (completion (try-completion abbrev collection)))
    (cond ((not completion) ;; no completion
           nil)
          ((eql completion t) ;; already complete
           nil)
          ((string-equal abbrev completion) ;; propose completions
           (save-excursion
             (with-output-to-temp-buffer "*Completions*"
               (display-completion-list (all-completions abbrev collection) abbrev)))
           t)
          (t ;; expand abbrev
           (choose-completion-string completion
                                     (current-buffer)
                                     (list (slot-value this 'start-position)))
           t))))

(defun lyqi:complete-word ()
  (interactive)
  (multiple-value-bind (form line rest-forms)
      (lp:form-before-point (lp:current-syntax) (point))
    (when (and form (= (point) (+ (lp:marker form) (lp:size form))))
      (let ((abbrev (lyqi:form-abbrev form)))
        (when abbrev
          (lyqi:complete-abbrev abbrev))))))

(defun lyqi:complete-or-indent ()
  (interactive)
  (or (lyqi:complete-word)
      (lyqi:indent-line)))


(defconst lyqi:help-url-base "http://lilypond.org/doc/v2.13/Documentation/notation")

(defconst lyqi:help-index
  '(("!" ("writing-pitches#index-_0021" "Accidentals" "writing-pitches#accidentals"))
    ("'" ("writing-pitches#index-_0027" "Absolute octave entry" "writing-pitches#absolute-octave-entry"))
    ("," ("writing-pitches#index-_002c" "Absolute octave entry" "writing-pitches#absolute-octave-entry"))
    ("-" ("expressive-marks-attached-to-notes#index-_002d" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("." ("writing-rhythms#index-_002e" "Durations" "writing-rhythms#durations"))
    ("/" ("chord-mode#index-_002f" "Extended and altered chords" "chord-mode#extended-and-altered-chords"))
    ("/+" ("chord-mode#index-_002f_002b" "Extended and altered chords" "chord-mode#extended-and-altered-chords"))
    (":" ("short-repeats#index-_003a" "Tremolo repeats" "short-repeats#tremolo-repeats"))
    ("<" ("single-voice#index-_003c" "Chorded notes" "single-voice#chorded-notes"))
    ("<...>" ("single-voice#index-_003c_002e_002e_002e_003e" "Chorded notes" "single-voice#chorded-notes"))
    ("=" ("changing-multiple-pitches#index-_003d" "Octave checks" "changing-multiple-pitches#octave-checks"))
    (">" ("single-voice#index-_003e" "Chorded notes" "single-voice#chorded-notes"))
    ("?" ("writing-pitches#index-_003f" "Accidentals" "writing-pitches#accidentals"))
    ("Accidental" ("displaying-pitches#index-Accidental-1" "See also" "displaying-pitches#See-also-51")
       ("writing-pitches#index-Accidental" "See also" "writing-pitches#See-also-258"))
    ("AccidentalCautionary" ("writing-pitches#index-AccidentalCautionary" "See also" "writing-pitches#See-also-258"))
    ("AccidentalPlacement" ("displaying-pitches#index-AccidentalPlacement" "See also" "displaying-pitches#See-also-51"))
    ("AccidentalSuggestion" ("typesetting-mensural-music#index-AccidentalSuggestion-1" "See also" "typesetting-mensural-music#See-also-197")
       ("displaying-pitches#index-AccidentalSuggestion" "See also" "displaying-pitches#See-also-51"))
    ("Accidental_engraver" ("typesetting-mensural-music#index-Accidental_005fengraver-2" "See also" "typesetting-mensural-music#See-also-197")
       ("displaying-pitches#index-Accidental_005fengraver-1" "See also" "displaying-pitches#See-also-51")
       ("writing-pitches#index-Accidental_005fengraver" "See also" "writing-pitches#See-also-258"))
    ("Accidentals and key signatures" ("arabic-music#index-Accidentals-and-key-signatures-4" "See also" "arabic-music#See-also-142")
       ("displaying-pitches#index-Accidentals-and-key-signatures-3" "See also" "displaying-pitches#See-also-151")
       ("displaying-pitches#index-Accidentals-and-key-signatures-2" "Key signature" "displaying-pitches#key-signature")
       ("writing-pitches#index-Accidentals-and-key-signatures-1" "See also" "writing-pitches#See-also-258")
       ("writing-pitches#index-Accidentals-and-key-signatures" "Accidentals" "writing-pitches#accidentals"))
    ("Adding and removing engravers" ("displaying-rhythms#index-Adding-and-removing-engravers" "See also" "displaying-rhythms#See-also-37"))
    ("Aligning lyrics to a melody" ("common-notation-for-vocal-music#index-Aligning-lyrics-to-a-melody-1" "See also" "common-notation-for-vocal-music#See-also-98")
       ("common-notation-for-vocal-music#index-Aligning-lyrics-to-a-melody" "See also" "common-notation-for-vocal-music#See-also-94"))
    ("All layout objects" ("technical-glossary#index-All-layout-objects-2" "See also" "technical-glossary#See-also-231")
       ("advanced-tweaks#index-All-layout-objects-1" "See also" "advanced-tweaks#See-also-163")
       ("overview-of-modifying-properties#index-All-layout-objects" "See also" "overview-of-modifying-properties#See-also-227"))
    ("Ambitus" ("displaying-pitches#index-Ambitus" "See also" "displaying-pitches#See-also-144"))
    ("AmbitusAccidental" ("displaying-pitches#index-AmbitusAccidental" "See also" "displaying-pitches#See-also-144"))
    ("AmbitusLine" ("displaying-pitches#index-AmbitusLine" "See also" "displaying-pitches#See-also-144"))
    ("AmbitusNoteHead" ("displaying-pitches#index-AmbitusNoteHead" "See also" "displaying-pitches#See-also-144"))
    ("Ambitus_engraver" ("displaying-pitches#index-Ambitus_005fengraver" "See also" "displaying-pitches#See-also-144"))
    ("An extra staff appears" ("context-layout-order#index-An-extra-staff-appears-2" "See also" "context-layout-order#See-also-204")
       ("long-repeats#index-An-extra-staff-appears-1" "Written-out repeats" "long-repeats#written_002dout-repeats")
       ("long-repeats#index-An-extra-staff-appears" "Normal repeats" "long-repeats#normal-repeats"))
    ("Arpeggio" ("expressive-marks-as-lines#index-Arpeggio" "See also" "expressive-marks-as-lines#See-also-122"))
    ("Articulation and dynamics" ("expressive-marks-attached-to-notes#index-Articulation-and-dynamics" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("AutoChangeMusic" ("common-notation-for-keyboards#index-AutoChangeMusic" "See also" "common-notation-for-keyboards#See-also-247"))
    ("Auto_beam_engraver" ("beams#index-Auto_005fbeam_005fengraver-1" "See also" "beams#See-also-175")
       ("beams#index-Auto_005fbeam_005fengraver" "See also" "beams#See-also-206"))
    ("Backend" ("the-override-command#index-Backend-2" "See also" "the-override-command#See-also-83")
       ("overview-of-modifying-properties#index-Backend-1" "See also" "overview-of-modifying-properties#See-also-227")
       ("navigating-the-program-reference#index-Backend" "5.2.1 Navigating the program reference" "navigating-the-program-reference"))
    ("BalloonTextItem" ("outside-the-staff#index-BalloonTextItem" "See also" "outside-the-staff#See-also-82"))
    ("Balloon_engraver" ("outside-the-staff#index-Balloon_005fengraver" "Balloon help" "outside-the-staff#balloon-help"))
    ("BarLine" ("bars#index-BarLine" "See also" "bars#See-also-87"))
    ("BarNumber" ("bars#index-BarNumber-3" "Known issues and warnings" "bars#Known-issues-and-warnings-32")
       ("bars#index-BarNumber" "Bar numbers" "bars#bar-numbers"))
    ("Bar_engraver" ("displaying-chords#index-Bar_005fengraver" "See also" "displaying-chords#See-also-61"))
    ("Bar_number_engraver" ("bars#index-Bar_005fnumber_005fengraver" "See also" "bars#See-also-22"))
    ("BassFigure" ("figured-bass#index-BassFigure-1" "See also" "figured-bass#See-also-34")
       ("figured-bass#index-BassFigure" "See also" "figured-bass#See-also-65"))
    ("BassFigureAlignment" ("figured-bass#index-BassFigureAlignment-1" "See also" "figured-bass#See-also-34")
       ("figured-bass#index-BassFigureAlignment" "See also" "figured-bass#See-also-65"))
    ("BassFigureBracket" ("figured-bass#index-BassFigureBracket-1" "See also" "figured-bass#See-also-34")
       ("figured-bass#index-BassFigureBracket" "See also" "figured-bass#See-also-65"))
    ("BassFigureContinuation" ("figured-bass#index-BassFigureContinuation-1" "See also" "figured-bass#See-also-34")
       ("figured-bass#index-BassFigureContinuation" "See also" "figured-bass#See-also-65"))
    ("BassFigureLine" ("figured-bass#index-BassFigureLine-1" "See also" "figured-bass#See-also-34")
       ("figured-bass#index-BassFigureLine" "See also" "figured-bass#See-also-65"))
    ("Beam" ("common-notation-for-fretted-strings#index-Beam-4" "See also" "common-notation-for-fretted-strings#See-also-113")
       ("common-notation-for-keyboards#index-Beam-3" "See also" "common-notation-for-keyboards#See-also-86")
       ("beams#index-Beam-2" "See also" "beams#See-also-167")
       ("beams#index-Beam-1" "See also" "beams#See-also-175")
       ("beams#index-Beam" "See also" "beams#See-also-206"))
    ("BeamEvent" ("beams#index-BeamEvent-1" "See also" "beams#See-also-167")
       ("beams#index-BeamEvent" "See also" "beams#See-also-206"))
    ("BeamForbidEvent" ("beams#index-BeamForbidEvent-1" "See also" "beams#See-also-175")
       ("beams#index-BeamForbidEvent" "See also" "beams#See-also-206"))
    ("Beam_engraver" ("beams#index-Beam_005fengraver-1" "See also" "beams#See-also-167")
       ("beams#index-Beam_005fengraver" "See also" "beams#See-also-206"))
    ("BreathingEvent" ("expressive-marks-as-curves#index-BreathingEvent" "See also" "expressive-marks-as-curves#See-also-28"))
    ("BreathingSign" ("typesetting-gregorian-chant#index-BreathingSign-1" "See also" "typesetting-gregorian-chant#See-also-63")
       ("expressive-marks-as-curves#index-BreathingSign" "See also" "expressive-marks-as-curves#See-also-28"))
    ("Breathing_sign_engraver" ("expressive-marks-as-curves#index-Breathing_005fsign_005fengraver" "See also" "expressive-marks-as-curves#See-also-28"))
    ("Callback functions" ("aligning-objects#index-Callback-functions" "See also" "aligning-objects#See-also-69"))
    ("ChoirStaff" ("choral#index-ChoirStaff-2" "See also" "choral#See-also-47")
       ("displaying-staves#index-ChoirStaff-1" "See also" "displaying-staves#See-also-159")
       ("displaying-staves#index-ChoirStaff" "See also" "displaying-staves#See-also-57"))
    ("ChordName" ("displaying-chords#index-ChordName" "See also" "displaying-chords#See-also-61"))
    ("ChordNames" ("displaying-chords#index-ChordNames-3" "See also" "displaying-chords#See-also-61")
       ("common-notation-for-fretted-strings#index-ChordNames-1" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams"))
    ("Chord_name_engraver" ("displaying-chords#index-Chord_005fname_005fengraver" "See also" "displaying-chords#See-also-61"))
    ("Clef" ("displaying-pitches#index-Clef" "See also" "displaying-pitches#See-also-141"))
    ("Clef_engraver" ("displaying-pitches#index-Clef_005fengraver" "See also" "displaying-pitches#See-also-141"))
    ("ClusterSpanner" ("single-voice#index-ClusterSpanner" "See also" "single-voice#See-also-41"))
    ("ClusterSpannerBeacon" ("single-voice#index-ClusterSpannerBeacon" "See also" "single-voice#See-also-41"))
    ("Cluster_spanner_engraver" ("single-voice#index-Cluster_005fspanner_005fengraver" "See also" "single-voice#See-also-41"))
    ("Combining notes into chords" ("single-voice#index-Combining-notes-into-chords" "See also" "single-voice#See-also-106"))
    ("Command line options for" ("alternative-output-formats#index-Command-line-options-for" "3.4.3 Alternative output formats" "alternative-output-formats"))
    ("Common Practice Period" ("common-notation-for-non_002dwestern-music#index-Common-Practice-Period-1" "See also" "common-notation-for-non_002dwestern-music#See-also-200")
       ("writing-pitches#index-Common-Practice-Period" "See also" "writing-pitches#See-also-128"))
    ("Completion_heads_engraver" ("displaying-rhythms#index-Completion_005fheads_005fengraver" "Automatic note splitting" "displaying-rhythms#automatic-note-splitting"))
    ("Completion_rest_engraver" ("displaying-rhythms#index-Completion_005frest_005fengraver" "Automatic note splitting" "displaying-rhythms#automatic-note-splitting"))
    ("ContextChange" ("common-notation-for-keyboards#index-ContextChange" "See also" "common-notation-for-keyboards#See-also-86"))
    ("Contexts" ("modifying-context-plug_002dins#index-Contexts-4" "5.1.4 Modifying context plug-ins" "modifying-context-plug_002dins")
       ("interpretation-contexts#index-Contexts-3" "See also" "interpretation-contexts#See-also-186")
       ("flexible-vertical-spacing-within-systems#index-Contexts-2" "See also" "flexible-vertical-spacing-within-systems#See-also-21")
       ("flexible-vertical-spacing-within-systems#index-Contexts-1" "See also" "flexible-vertical-spacing-within-systems#See-also-32")
       ("flexible-vertical-spacing-within-systems#index-Contexts" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("Contexts and engravers" ("interpretation-contexts#index-Contexts-and-engravers-1" "See also" "interpretation-contexts#See-also-186")
       ("multiple-voices#index-Contexts-and-engravers" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("CueVoice" ("opera-and-stage-musicals#index-CueVoice-1" "See also" "opera-and-stage-musicals#See-also-199")
       ("writing-parts#index-CueVoice" "See also" "writing-parts#See-also-183"))
    ("Custos" ("ancient-notation_002d_002dcommon-features#index-Custos" "See also" "ancient-notation_002d_002dcommon-features#See-also-191"))
    ("Default_bar_line_engraver" ("displaying-rhythms#index-Default_005fbar_005fline_005fengraver" "See also" "displaying-rhythms#See-also-4"))
    ("Displaying music expressions" ("the-tweak-command#index-Displaying-music-expressions-1" "See also" "the-tweak-command#See-also-70"))
    ("DotColumn" ("writing-rhythms#index-DotColumn" "See also" "writing-rhythms#See-also-96"))
    ("Dots" ("writing-rhythms#index-Dots" "See also" "writing-rhythms#See-also-96"))
    ("DoublePercentEvent" ("short-repeats#index-DoublePercentEvent" "See also" "short-repeats#See-also-220"))
    ("DoublePercentRepeat" ("short-repeats#index-DoublePercentRepeat" "See also" "short-repeats#See-also-220"))
    ("DoublePercentRepeatCounter" ("short-repeats#index-DoublePercentRepeatCounter" "See also" "short-repeats#See-also-220"))
    ("DoubleRepeatSlash" ("short-repeats#index-DoubleRepeatSlash" "See also" "short-repeats#See-also-220"))
    ("Double_percent_repeat_engraver" ("short-repeats#index-Double_005fpercent_005frepeat_005fengraver" "See also" "short-repeats#See-also-220"))
    ("DrumStaff" ("common-notation-for-percussion#index-DrumStaff-3" "See also" "common-notation-for-percussion#See-also-116")
       ("displaying-staves#index-DrumStaff" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("DrumVoice" ("common-notation-for-percussion#index-DrumVoice" "See also" "common-notation-for-percussion#See-also-116"))
    ("Duration names notes and rests" ("writing-rhythms#index-Duration-names-notes-and-rests" "See also" "writing-rhythms#See-also-96"))
    ("DynamicLineSpanner" ("expressive-marks-attached-to-notes#index-DynamicLineSpanner-3" "See also" "expressive-marks-attached-to-notes#See-also-157")
       ("expressive-marks-attached-to-notes#index-DynamicLineSpanner" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("DynamicText" ("expressive-marks-attached-to-notes#index-DynamicText" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("Dynamics" ("expressive-marks-attached-to-notes#index-Dynamics" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("Engravers and Performers" ("modifying-context-plug_002dins#index-Engravers-and-Performers-1" "5.1.4 Modifying context plug-ins" "modifying-context-plug_002dins")
       ("interpretation-contexts#index-Engravers-and-Performers" "See also" "interpretation-contexts#See-also-186"))
    ("Engravers explained" ("displaying-rhythms#index-Engravers-explained" "See also" "displaying-rhythms#See-also-37"))
    ("Episema" ("typesetting-gregorian-chant#index-Episema" "See also" "typesetting-gregorian-chant#See-also-229"))
    ("EpisemaEvent" ("typesetting-gregorian-chant#index-EpisemaEvent" "See also" "typesetting-gregorian-chant#See-also-229"))
    ("Episema_engraver" ("typesetting-gregorian-chant#index-Episema_005fengraver" "See also" "typesetting-gregorian-chant#See-also-229"))
    ("Explicitly instantiating voices" ("multiple-voices#index-Explicitly-instantiating-voices-1" "See also" "multiple-voices#See-also-215")
       ("multiple-voices#index-Explicitly-instantiating-voices" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("FiguredBass" ("figured-bass#index-FiguredBass-2" "See also" "figured-bass#See-also-34")
       ("figured-bass#index-FiguredBass-1" "See also" "figured-bass#See-also-65")
       ("modifying-single-staves#index-FiguredBass" "See also" "modifying-single-staves#See-also-233"))
    ("Fingering" ("layout-interfaces#index-Fingering-7" "5.2.2 Layout interfaces" "layout-interfaces")
       ("layout-interfaces#index-Fingering-6" "5.2.2 Layout interfaces" "layout-interfaces")
       ("navigating-the-program-reference#index-Fingering-5" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("navigating-the-program-reference#index-Fingering-4" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("navigating-the-program-reference#index-Fingering-3" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("navigating-the-program-reference#index-Fingering-2" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("common-notation-for-fretted-strings#index-Fingering-1" "See also" "common-notation-for-fretted-strings#See-also-166")
       ("inside-the-staff#index-Fingering" "See also" "inside-the-staff#See-also-119"))
    ("FingeringEvent" ("navigating-the-program-reference#index-FingeringEvent-1" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("inside-the-staff#index-FingeringEvent" "See also" "inside-the-staff#See-also-119"))
    ("Fingering_engraver" ("determining-the-grob-property#index-Fingering_005fengraver-4" "5.2.3 Determining the grob property" "determining-the-grob-property")
       ("navigating-the-program-reference#index-Fingering_005fengraver-3" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("navigating-the-program-reference#index-Fingering_005fengraver-2" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("navigating-the-program-reference#index-Fingering_005fengraver-1" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("inside-the-staff#index-Fingering_005fengraver" "See also" "inside-the-staff#See-also-119"))
    ("Fixing overlapping" ("common-notation-for-keyboards#index-Fixing-overlapping" "Changing staff manually" "common-notation-for-keyboards#changing-staff-manually"))
    ("Fixing overlapping notation" ("common-notation-for-keyboards#index-Fixing-overlapping-notation-1" "See also" "common-notation-for-keyboards#See-also-86")
       ("common-notation-for-keyboards#index-Fixing-overlapping-notation" "Changing staff manually" "common-notation-for-keyboards#changing-staff-manually"))
    ("Forbid_line_break_engraver" ("displaying-rhythms#index-Forbid_005fline_005fbreak_005fengraver" "See also" "displaying-rhythms#See-also-37"))
    ("Formatting text" ("layout-properties#index-Formatting-text-1" "A.15 Layout properties" "layout-properties")
       ("layout-properties#index-Formatting-text" "A.15 Layout properties" "layout-properties"))
    ("Four-part SATB vocal score" ("choral#index-Four_002dpart-SATB-vocal-score-1" "See also" "choral#See-also-47")
       ("choral#index-Four_002dpart-SATB-vocal-score" "References for choral" "choral#references-for-choral"))
    ("Frenched score" ("opera-and-stage-musicals#index-Frenched-score-1" "See also" "opera-and-stage-musicals#See-also-196"))
    ("Frenched staff" ("modifying-single-staves#index-Frenched-staff-2" "See also" "modifying-single-staves#See-also-233")
       ("modifying-single-staves#index-Frenched-staff" "See also" "modifying-single-staves#See-also-16"))
    ("Frenched staves" ("opera-and-stage-musicals#index-Frenched-staves-1" "See also" "opera-and-stage-musicals#See-also-196"))
    ("FretBoards" ("common-notation-for-fretted-strings#index-FretBoards" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams"))
    ("Glissando" ("spanners#index-Glissando-1" "See also" "spanners#See-also-208")
       ("expressive-marks-as-lines#index-Glissando" "See also" "expressive-marks-as-lines#See-also-3"))
    ("GraceMusic" ("special-rhythmic-concerns#index-GraceMusic" "See also" "special-rhythmic-concerns#See-also-64"))
    ("Grace_beam_engraver" ("special-rhythmic-concerns#index-Grace_005fbeam_005fengraver" "See also" "special-rhythmic-concerns#See-also-64"))
    ("Grace_engraver" ("special-rhythmic-concerns#index-Grace_005fengraver" "See also" "special-rhythmic-concerns#See-also-64"))
    ("Grace_spacing_engraver" ("special-rhythmic-concerns#index-Grace_005fspacing_005fengraver" "See also" "special-rhythmic-concerns#See-also-64"))
    ("GrandStaff" ("displaying-staves#index-GrandStaff-1" "See also" "displaying-staves#See-also-57")
       ("displaying-pitches#index-GrandStaff" "See also" "displaying-pitches#See-also-51"))
    ("Graphical Object Interfaces" ("technical-glossary#index-Graphical-Object-Interfaces" "See also" "technical-glossary#See-also-133"))
    ("GregorianTranscriptionStaff" ("displaying-staves#index-GregorianTranscriptionStaff" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("GridLine" ("outside-the-staff#index-GridLine" "See also" "outside-the-staff#See-also-184"))
    ("GridPoint" ("outside-the-staff#index-GridPoint" "See also" "outside-the-staff#See-also-184"))
    ("Grid_line_span_engraver" ("outside-the-staff#index-Grid_005fline_005fspan_005fengraver" "Grid lines" "outside-the-staff#grid-lines"))
    ("Grid_point_engraver" ("outside-the-staff#index-Grid_005fpoint_005fengraver" "Grid lines" "outside-the-staff#grid-lines"))
    ("Hairpin" ("expressive-marks-attached-to-notes#index-Hairpin" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("HorizontalBracket" ("outside-the-staff#index-HorizontalBracket" "See also" "outside-the-staff#See-also-239"))
    ("Horizontal_bracket_engraver" ("outside-the-staff#index-Horizontal_005fbracket_005fengraver" "Analysis brackets" "outside-the-staff#analysis-brackets"))
    ("How LilyPond input files work" ("creating-titles-headers-and-footers#index-How-LilyPond-input-files-work-1" "See also" "creating-titles-headers-and-footers#See-also-138")
       ("file-structure#index-How-LilyPond-input-files-work" "See also" "file-structure#See-also-33"))
    ("I'm hearing Voices" ("common-notation-for-percussion#index-I_0027m-hearing-Voices-1" "Percussion staves" "common-notation-for-percussion#percussion-staves")
       ("multiple-voices#index-I_0027m-hearing-Voices" "See also" "multiple-voices#See-also-109"))
    ("InstrumentName" ("writing-parts#index-InstrumentName" "See also" "writing-parts#See-also-49"))
    ("InstrumentSwitch" ("opera-and-stage-musicals#index-InstrumentSwitch" "See also" "opera-and-stage-musicals#See-also-199"))
    ("Interfaces for programmers" ("advanced-tweaks#index-Interfaces-for-programmers" "See also" "advanced-tweaks#See-also-163"))
    ("Invoking lilypond" ("extracting-fragments-of-music#index-Invoking-lilypond" "3.4.1 Extracting fragments of music" "extracting-fragments-of-music"))
    ("KeyCancellation" ("displaying-pitches#index-KeyCancellation" "See also" "displaying-pitches#See-also-151"))
    ("KeyChangeEvent" ("displaying-pitches#index-KeyChangeEvent" "See also" "displaying-pitches#See-also-151"))
    ("KeySignature" ("arabic-music#index-KeySignature-3" "See also" "arabic-music#See-also-142")
       ("typesetting-gregorian-chant#index-KeySignature-2" "See also" "typesetting-gregorian-chant#See-also-136")
       ("typesetting-mensural-music#index-KeySignature-1" "See also" "typesetting-mensural-music#See-also-256")
       ("displaying-pitches#index-KeySignature" "See also" "displaying-pitches#See-also-151"))
    ("Key_engraver" ("displaying-pitches#index-Key_005fengraver" "See also" "displaying-pitches#See-also-151"))
    ("Key_performer" ("displaying-pitches#index-Key_005fperformer" "See also" "displaying-pitches#See-also-151"))
    ("LaissezVibrerTie" ("writing-rhythms#index-LaissezVibrerTie" "See also" "writing-rhythms#See-also-223"))
    ("LaissezVibrerTieColumn" ("writing-rhythms#index-LaissezVibrerTieColumn" "See also" "writing-rhythms#See-also-223"))
    ("LedgerLineSpanner" ("note-heads#index-LedgerLineSpanner" "See also" "note-heads#See-also-235"))
    ("Ledger_line_engraver" ("note-heads#index-Ledger_005fline_005fengraver" "See also" "note-heads#See-also-235"))
    ("Length and thickness of objects" ("distances-and-measurements#index-Length-and-thickness-of-objects-2" "See also" "distances-and-measurements#See-also-134")
       ("distances-and-measurements#index-Length-and-thickness-of-objects-1" "5.4.4 Distances and measurements" "distances-and-measurements")
       ("modifying-single-staves#index-Length-and-thickness-of-objects" "See also" "modifying-single-staves#See-also-16"))
    ("LineBreakEvent" ("line-breaking#index-LineBreakEvent" "See also" "line-breaking#See-also-219"))
    ("Literature list" ("displaying-chords#index-Literature-list-1" "See also" "displaying-chords#See-also-214")
       ("displaying-chords#index-Literature-list" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("LyricCombineMusic" ("techniques-specific-to-lyrics#index-LyricCombineMusic-1" "See also" "techniques-specific-to-lyrics#See-also-212")
       ("common-notation-for-vocal-music#index-LyricCombineMusic" "See also" "common-notation-for-vocal-music#See-also-207"))
    ("LyricExtender" ("common-notation-for-vocal-music#index-LyricExtender" "See also" "common-notation-for-vocal-music#See-also-152"))
    ("LyricHyphen" ("common-notation-for-vocal-music#index-LyricHyphen" "See also" "common-notation-for-vocal-music#See-also-152"))
    ("LyricText" ("opera-and-stage-musicals#index-LyricText-2" "See also" "opera-and-stage-musicals#See-also-226")
       ("stanzas#index-LyricText-1" "See also" "stanzas#See-also-72")
       ("common-notation-for-vocal-music#index-LyricText" "See also" "common-notation-for-vocal-music#See-also-15"))
    ("Lyrics" ("all-context-properties#index-Lyrics-6" "A.14 All context properties" "all-context-properties")
       ("all-context-properties#index-Lyrics-5" "A.14 All context properties" "all-context-properties")
       ("choral#index-Lyrics-4" "See also" "choral#See-also-47")
       ("techniques-specific-to-lyrics#index-Lyrics-3" "See also" "techniques-specific-to-lyrics#See-also-212")
       ("common-notation-for-vocal-music#index-Lyrics-2" "See also" "common-notation-for-vocal-music#See-also-29")
       ("common-notation-for-vocal-music#index-Lyrics-1" "See also" "common-notation-for-vocal-music#See-also-94")
       ("modifying-single-staves#index-Lyrics" "See also" "modifying-single-staves#See-also-233"))
    ("Manuals" ("index#index-Manuals" "LilyPond - Notation Reference" "index")
       ("index" "LilyPond - Notation Reference" "index"))
    ("MarkEvent" ("writing-text#index-MarkEvent-1" "See also" "writing-text#See-also-78")
       ("bars#index-MarkEvent" "See also" "bars#See-also-60"))
    ("Mark_engraver" ("writing-text#index-Mark_005fengraver-1" "See also" "writing-text#See-also-78")
       ("bars#index-Mark_005fengraver" "See also" "bars#See-also-60"))
    ("Markup construction in Scheme" ("opera-and-stage-musicals#index-Markup-construction-in-Scheme-2" "See also" "opera-and-stage-musicals#See-also-118")
       ("expressive-marks-attached-to-notes#index-Markup-construction-in-Scheme-1" "See also" "expressive-marks-attached-to-notes#See-also-89")
       ("expressive-marks-attached-to-notes#index-Markup-construction-in-Scheme" "New dynamic marks" "expressive-marks-attached-to-notes#new-dynamic-marks"))
    ("MensuralStaff" ("displaying-staves#index-MensuralStaff" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("MetronomeMark" ("displaying-rhythms#index-MetronomeMark" "See also" "displaying-rhythms#See-also-95"))
    ("Moving objects" ("formatting-text#index-Moving-objects-1" "See also" "formatting-text#See-also-224")
       ("formatting-text#index-Moving-objects" "Text alignment" "formatting-text#text-alignment"))
    ("MultiMeasureRest" ("writing-rests#index-MultiMeasureRest" "See also" "writing-rests#See-also-30"))
    ("MultiMeasureRestNumber" ("writing-rests#index-MultiMeasureRestNumber" "See also" "writing-rests#See-also-30"))
    ("MultiMeasureRestText" ("writing-rests#index-MultiMeasureRestText" "Full measure rests" "writing-rests#full-measure-rests"))
    ("Multiple notes at once" ("multiple-voices#index-Multiple-notes-at-once" "See also" "multiple-voices#See-also-177"))
    ("Music classes" ("writing-parts#index-Music-classes" "See also" "writing-parts#See-also-201"))
    ("Music definitions" ("navigating-the-program-reference#index-Music-definitions" "5.2.1 Navigating the program reference" "navigating-the-program-reference"))
    ("Music expressions explained" ("structure-of-a-score#index-Music-expressions-explained" "See also" "structure-of-a-score#See-also-114"))
    ("Music functions" ("substitution-function-syntax#index-Music-functions-2" "See also" "substitution-function-syntax#See-also-9")
       ("substitution-function-syntax#index-Music-functions-1" "5.6.1 Substitution function syntax" "substitution-function-syntax")
       ("using-music-functions#index-Music-functions" "5.6 Using music functions" "using-music-functions"))
    ("Naming conventions of objects and properties" ("technical-glossary#index-Naming-conventions-of-objects-and-properties-1" "See also" "technical-glossary#See-also-133")
       ("technical-glossary#index-Naming-conventions-of-objects-and-properties" "See also" "technical-glossary#See-also-231"))
    ("Nesting music expressions" ("modifying-single-staves#index-Nesting-music-expressions-1" "See also" "modifying-single-staves#See-also-16")
       ("modifying-single-staves#index-Nesting-music-expressions" "Ossia staves" "modifying-single-staves#ossia-staves"))
    ("New markup list command definition" ("formatting-text#index-New-markup-list-command-definition" "See also" "formatting-text#See-also-131"))
    ("New_fingering_engraver" ("navigating-the-program-reference#index-New_005ffingering_005fengraver-1" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("inside-the-staff#index-New_005ffingering_005fengraver" "See also" "inside-the-staff#See-also-119"))
    ("NonMusicalPaperColumn" ("horizontal-spacing-overview#index-NonMusicalPaperColumn" "See also" "horizontal-spacing-overview#See-also-8"))
    ("NoteCollision" ("multiple-voices#index-NoteCollision" "See also" "multiple-voices#See-also-177"))
    ("NoteColumn" ("multiple-voices#index-NoteColumn" "See also" "multiple-voices#See-also-177"))
    ("NoteHead" ("note-heads#index-NoteHead-2" "See also" "note-heads#See-also-130")
       ("note-heads#index-NoteHead-1" "See also" "note-heads#See-also-202")
       ("note-heads#index-NoteHead" "See also" "note-heads#See-also-235"))
    ("NoteSpacing" ("horizontal-spacing-overview#index-NoteSpacing-2" "See also" "horizontal-spacing-overview#See-also-8")
       ("horizontal-spacing-overview#index-NoteSpacing-1" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview")
       ("inside-the-staff#index-NoteSpacing" "See also" "inside-the-staff#See-also-71"))
    ("Note_head_line_engraver" ("common-notation-for-keyboards#index-Note_005fhead_005fline_005fengraver" "See also" "common-notation-for-keyboards#See-also-66"))
    ("Note_heads_engraver" ("defining-new-contexts#index-Note_005fheads_005fengraver-6" "5.1.6 Defining new contexts" "defining-new-contexts")
       ("displaying-rhythms#index-Note_005fheads_005fengraver-5" "See also" "displaying-rhythms#See-also-37")
       ("note-heads#index-Note_005fheads_005fengraver-2" "See also" "note-heads#See-also-130")
       ("note-heads#index-Note_005fheads_005fengraver-1" "See also" "note-heads#See-also-202")
       ("displaying-rhythms#index-Note_005fheads_005fengraver-3" "Automatic note splitting" "displaying-rhythms#automatic-note-splitting"))
    ("Note_spacing_engraver" ("inside-the-staff#index-Note_005fspacing_005fengraver" "See also" "inside-the-staff#See-also-71"))
    ("Objects and interfaces" ("technical-glossary#index-Objects-and-interfaces-1" "See also" "technical-glossary#See-also-133")
       ("technical-glossary#index-Objects-and-interfaces" "See also" "technical-glossary#See-also-231"))
    ("OctavateEight" ("displaying-pitches#index-OctavateEight" "See also" "displaying-pitches#See-also-141"))
    ("On the un-nestedness of brackets and ties" ("expressive-marks-as-curves#index-On-the-un_002dnestedness-of-brackets-and-ties-1" "See also" "expressive-marks-as-curves#See-also-181")
       ("expressive-marks-as-curves#index-On-the-un_002dnestedness-of-brackets-and-ties" "See also" "expressive-marks-as-curves#See-also-176"))
    ("Optical spacing" ("horizontal-spacing-overview#index-Optical-spacing-1" "See also" "horizontal-spacing-overview#See-also-8")
       ("horizontal-spacing-overview#index-Optical-spacing" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview"))
    ("Organizing" ("different-editions-from-one-source#index-Organizing" "Using variables" "different-editions-from-one-source#using-variables"))
    ("Organizing pieces with variables" ("different-editions-from-one-source#index-Organizing-pieces-with-variables-2" "See also" "different-editions-from-one-source#See-also-117")
       ("different-editions-from-one-source#index-Organizing-pieces-with-variables-1" "See also" "different-editions-from-one-source#See-also-67")
       ("multiple-voices#index-Organizing-pieces-with-variables" "See also" "multiple-voices#See-also-153"))
    ("Other sources of information" ("advanced-tweaks#index-Other-sources-of-information-11" "See also" "advanced-tweaks#See-also-163")
       ("layout-interfaces#index-Other-sources-of-information-10" "5.2.2 Layout interfaces" "layout-interfaces")
       ("controlling-midi-dynamics#index-Other-sources-of-information-9" "Dynamic marks" "controlling-midi-dynamics")
       ("midi-block#index-Other-sources-of-information-8" "3.5.2 MIDI block" "midi-block")
       ("replacing-the-notation-font#index-Other-sources-of-information-7" "See also" "replacing-the-notation-font#See-also-217")
       ("replacing-the-notation-font#index-Other-sources-of-information-6" "Installation Instructions for MacOS" "replacing-the-notation-font#Installation-Instructions-for-MacOS")
       ("including-lilypond-files#index-Other-sources-of-information-5" "See also" "including-lilypond-files#See-also-42")
       ("including-lilypond-files#index-Other-sources-of-information-4" "3.3.1 Including LilyPond files" "including-lilypond-files")
       ("including-lilypond-files#index-Other-sources-of-information-3" "3.3.1 Including LilyPond files" "including-lilypond-files")
       ("common-notation-for-non_002dwestern-music#index-Other-sources-of-information-2" "See also" "common-notation-for-non_002dwestern-music#See-also-200")
       ("common-notation-for-non_002dwestern-music#index-Other-sources-of-information-1" "Extending notation and tuning systems" "common-notation-for-non_002dwestern-music#extending-notation-and-tuning-systems")
       ("multiple-voices#index-Other-sources-of-information" "See also" "multiple-voices#See-also-109"))
    ("Other uses for tweaks" ("common-notation-for-keyboards#index-Other-uses-for-tweaks-1" "See also" "common-notation-for-keyboards#See-also-210")
       ("common-notation-for-keyboards#index-Other-uses-for-tweaks" "References for keyboards" "common-notation-for-keyboards#references-for-keyboards"))
    ("OttavaBracket" ("displaying-pitches#index-OttavaBracket" "See also" "displaying-pitches#See-also-126"))
    ("Ottava_spanner_engraver" ("displaying-pitches#index-Ottava_005fspanner_005fengraver" "See also" "displaying-pitches#See-also-126"))
    ("OverrideProperty" ("overview-of-modifying-properties#index-OverrideProperty" "See also" "overview-of-modifying-properties#See-also-227"))
    ("ParenthesesItem" ("inside-the-staff#index-ParenthesesItem" "See also" "inside-the-staff#See-also-132"))
    ("Parenthesis_engraver" ("inside-the-staff#index-Parenthesis_005fengraver" "See also" "inside-the-staff#See-also-132"))
    ("PartCombineMusic" ("multiple-voices#index-PartCombineMusic" "See also" "multiple-voices#See-also-221"))
    ("PercentRepeat" ("short-repeats#index-PercentRepeat" "See also" "short-repeats#See-also-220"))
    ("PercentRepeatCounter" ("short-repeats#index-PercentRepeatCounter" "See also" "short-repeats#See-also-220"))
    ("PercentRepeatedMusic" ("short-repeats#index-PercentRepeatedMusic" "See also" "short-repeats#See-also-220"))
    ("Percent_repeat_engraver" ("short-repeats#index-Percent_005frepeat_005fengraver" "See also" "short-repeats#See-also-220"))
    ("PhrasingSlur" ("expressive-marks-as-curves#index-PhrasingSlur" "See also" "expressive-marks-as-curves#See-also-181"))
    ("PianoPedalBracket" ("piano#index-PianoPedalBracket" "See also" "piano#See-also-44"))
    ("PianoStaff" ("common-notation-for-keyboards#index-PianoStaff-7" "See also" "common-notation-for-keyboards#See-also-210")
       ("choral#index-PianoStaff-4" "See also" "choral#See-also-47")
       ("writing-parts#index-PianoStaff-3" "See also" "writing-parts#See-also-49")
       ("displaying-staves#index-PianoStaff-2" "See also" "displaying-staves#See-also-57")
       ("common-notation-for-keyboards#index-PianoStaff-8" "Changing staff automatically" "common-notation-for-keyboards#changing-staff-automatically")
       ("common-notation-for-keyboards#index-PianoStaff-5" "References for keyboards" "common-notation-for-keyboards#references-for-keyboards"))
    ("Piano_pedal_engraver" ("piano#index-Piano_005fpedal_005fengraver" "See also" "piano#See-also-44"))
    ("Pitch names" ("typesetting-mensural-music#index-Pitch-names-4" "See also" "typesetting-mensural-music#See-also-256")
       ("writing-pitches#index-Pitch-names-3" "See also" "writing-pitches#See-also-128")
       ("writing-pitches#index-Pitch-names-2" "See also" "writing-pitches#See-also-258")
       ("writing-pitches#index-Pitch-names-1" "See also" "writing-pitches#See-also-171")
       ("writing-pitches#index-Pitch-names" "See also" "writing-pitches#See-also-105"))
    ("Pitch_squash_engraver" ("all-context-properties#index-Pitch_005fsquash_005fengraver-5" "A.14 All context properties" "all-context-properties")
       ("defining-new-contexts#index-Pitch_005fsquash_005fengraver-4" "5.1.6 Defining new contexts" "defining-new-contexts")
       ("displaying-rhythms#index-Pitch_005fsquash_005fengraver-3" "See also" "displaying-rhythms#See-also-91")
       ("displaying-rhythms#index-Pitch_005fsquash_005fengraver-1" "Showing melody rhythms" "displaying-rhythms#showing-melody-rhythms"))
    ("Placement of objects" ("writing-text#index-Placement-of-objects-3" "See also" "writing-text#See-also-11")
       ("writing-text#index-Placement-of-objects-2" "Text scripts" "writing-text#text-scripts")
       ("expressive-marks-attached-to-notes#index-Placement-of-objects-1" "See also" "expressive-marks-attached-to-notes#See-also-77")
       ("expressive-marks-attached-to-notes#index-Placement-of-objects" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("Properties found in interfaces" ("technical-glossary#index-Properties-found-in-interfaces" "See also" "technical-glossary#See-also-133"))
    ("Properties of layout objects" ("technical-glossary#index-Properties-of-layout-objects" "See also" "technical-glossary#See-also-231"))
    ("PropertySet" ("overview-of-modifying-properties#index-PropertySet" "See also" "overview-of-modifying-properties#See-also-227"))
    ("QuoteMusic" ("writing-parts#index-QuoteMusic" "See also" "writing-parts#See-also-201"))
    ("R" ("writing-rests#index-R" "Full measure rests" "writing-rests#full-measure-rests"))
    ("Real music example" ("common-notation-for-keyboards#index-Real-music-example-3" "See also" "common-notation-for-keyboards#See-also-210")
       ("common-notation-for-keyboards#index-Real-music-example-2" "References for keyboards" "common-notation-for-keyboards#references-for-keyboards")
       ("multiple-voices#index-Real-music-example-1" "See also" "multiple-voices#See-also-177")
       ("multiple-voices#index-Real-music-example" "Collision resolution" "multiple-voices#collision-resolution"))
    ("RehearsalMark" ("writing-text#index-RehearsalMark-1" "See also" "writing-text#See-also-78")
       ("bars#index-RehearsalMark" "See also" "bars#See-also-60"))
    ("RelativeOctaveCheck" ("changing-multiple-pitches#index-RelativeOctaveCheck" "See also" "changing-multiple-pitches#See-also-205"))
    ("RelativeOctaveMusic" ("writing-pitches#index-RelativeOctaveMusic" "See also" "writing-pitches#See-also-171"))
    ("RepeatSlash" ("short-repeats#index-RepeatSlash" "See also" "short-repeats#See-also-220"))
    ("RepeatSlashEvent" ("short-repeats#index-RepeatSlashEvent" "See also" "short-repeats#See-also-220"))
    ("RepeatedMusic" ("long-repeats#index-RepeatedMusic-2" "See also" "long-repeats#See-also-52")
       ("long-repeats#index-RepeatedMusic-1" "See also" "long-repeats#See-also-243")
       ("long-repeats#index-RepeatedMusic" "See also" "long-repeats#See-also"))
    ("Rest" ("writing-rests#index-Rest" "See also" "writing-rests#See-also-245"))
    ("RestCollision" ("multiple-voices#index-RestCollision" "See also" "multiple-voices#See-also-177"))
    ("Rest_engraver" ("displaying-rhythms#index-Rest_005fengraver" "See also" "displaying-rhythms#See-also-37"))
    ("RevertProperty" ("overview-of-modifying-properties#index-RevertProperty" "See also" "overview-of-modifying-properties#See-also-227"))
    ("RhythmicStaff" ("displaying-staves#index-RhythmicStaff-4" "See also" "displaying-staves#See-also-81")
       ("displaying-rhythms#index-RhythmicStaff-1" "See also" "displaying-rhythms#See-also-91")
       ("displaying-staves#index-RhythmicStaff-2" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("Running a function on all layout objects" ("creating-contexts#index-Running-a-function-on-all-layout-objects" "5.1.2 Creating contexts" "creating-contexts"))
    ("Scheme tutorial" ("changing-defaults#index-Scheme-tutorial" "5. Changing defaults" "changing-defaults"))
    ("Score" ("all-context-properties#index-Score-2" "A.14 All context properties" "all-context-properties")
       ("all-context-properties#index-Score-1" "A.14 All context properties" "all-context-properties")
       ("special-rhythmic-concerns#index-Score" "See also" "special-rhythmic-concerns#See-also-36"))
    ("Score is a (single) compound musical expression" ("structure-of-a-score#index-Score-is-a-_0028single_0029-compound-musical-expression" "See also" "structure-of-a-score#See-also-114"))
    ("Scores and parts" ("including-lilypond-files#index-Scores-and-parts-1" "See also" "including-lilypond-files#See-also-42")
       ("including-lilypond-files#index-Scores-and-parts" "3.3.1 Including LilyPond files" "including-lilypond-files"))
    ("Script" ("typesetting-gregorian-chant#index-Script-2" "See also" "typesetting-gregorian-chant#See-also-229")
       ("expressive-marks-attached-to-notes#index-Script-1" "See also" "expressive-marks-attached-to-notes#See-also-77")
       ("expressive-marks-attached-to-notes#index-Script" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("ScriptEvent" ("typesetting-gregorian-chant#index-ScriptEvent" "See also" "typesetting-gregorian-chant#See-also-229"))
    ("Script_engraver" ("typesetting-gregorian-chant#index-Script_005fengraver" "See also" "typesetting-gregorian-chant#See-also-229"))
    ("Setting simple songs" ("common-notation-for-vocal-music#index-Setting-simple-songs-1" "See also" "common-notation-for-vocal-music#See-also-48")
       ("common-notation-for-vocal-music#index-Setting-simple-songs" "References for vocal music" "common-notation-for-vocal-music#references-for-vocal-music"))
    ("Size of objects" ("modifying-single-staves#index-Size-of-objects" "See also" "modifying-single-staves#See-also-16"))
    ("SkipMusic" ("writing-rests#index-SkipMusic" "See also" "writing-rests#See-also-7"))
    ("Slash_repeat_engraver" ("short-repeats#index-Slash_005frepeat_005fengraver" "See also" "short-repeats#See-also-220"))
    ("Slur" ("expressive-marks-as-lines#index-Slur-1" "See also" "expressive-marks-as-lines#See-also-122")
       ("expressive-marks-as-curves#index-Slur" "See also" "expressive-marks-as-curves#See-also-176"))
    ("Songs" ("songs#index-Songs-1" "See also" "songs#See-also-246")
       ("common-notation-for-vocal-music#index-Songs" "See also" "common-notation-for-vocal-music#See-also-15"))
    ("SostenutoEvent" ("piano#index-SostenutoEvent" "See also" "piano#See-also-44"))
    ("SostenutoPedal" ("piano#index-SostenutoPedal" "See also" "piano#See-also-44"))
    ("SostenutoPedalLineSpanner" ("piano#index-SostenutoPedalLineSpanner" "See also" "piano#See-also-44"))
    ("SpacingSpanner" ("new-spacing-area#index-SpacingSpanner-3" "See also" "new-spacing-area#See-also-135")
       ("horizontal-spacing-overview#index-SpacingSpanner-2" "See also" "horizontal-spacing-overview#See-also-8")
       ("horizontal-spacing-overview#index-SpacingSpanner-1" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview")
       ("horizontal-spacing-overview#index-SpacingSpanner" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview"))
    ("SpanBar" ("bars#index-SpanBar" "See also" "bars#See-also-87"))
    ("Staff" ("all-context-properties#index-Staff-9" "A.14 All context properties" "all-context-properties")
       ("horizontal-spacing-overview#index-Staff-8" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview")
       ("outside-the-staff#index-Staff-7" "See also" "outside-the-staff#See-also-239")
       ("writing-parts#index-Staff-6" "See also" "writing-parts#See-also-49")
       ("modifying-single-staves#index-Staff-5" "See also" "modifying-single-staves#See-also-233")
       ("displaying-staves#index-Staff-4" "See also" "displaying-staves#See-also-57")
       ("displaying-staves#index-Staff-3" "See also" "displaying-staves#See-also-81")
       ("displaying-rhythms#index-Staff-2" "See also" "displaying-rhythms#See-also-4")
       ("displaying-pitches#index-Staff-1" "See also" "displaying-pitches#See-also-144")
       ("displaying-pitches#index-Staff" "See also" "displaying-pitches#See-also-51"))
    ("Staff.midiInstrument" ("creating-midi-files#index-Staff_002emidiInstrument" "Instrument names" "creating-midi-files"))
    ("StaffGroup" ("displaying-staves#index-StaffGroup-2" "See also" "displaying-staves#See-also-159")
       ("displaying-staves#index-StaffGroup-1" "See also" "displaying-staves#See-also-57")
       ("bars#index-StaffGroup" "Known issues and warnings" "bars#Known-issues-and-warnings-32"))
    ("StaffGrouper" ("modifying-alists#index-StaffGrouper-4" "5.3.6 Modifying alists" "modifying-alists")
       ("flexible-vertical-spacing-within-systems#index-StaffGrouper-3" "See also" "flexible-vertical-spacing-within-systems#See-also-73")
       ("flexible-vertical-spacing-within-systems#index-StaffGrouper-2" "See also" "flexible-vertical-spacing-within-systems#See-also-32")
       ("flexible-vertical-spacing-within-systems#index-StaffGrouper-1" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties")
       ("choral#index-StaffGrouper" "See also" "choral#See-also-194"))
    ("StaffSpacing" ("horizontal-spacing-overview#index-StaffSpacing" "See also" "horizontal-spacing-overview#See-also-8"))
    ("StaffSymbol" ("setting-the-staff-size#index-StaffSymbol-3" "4.2.2 Setting the staff size" "setting-the-staff-size")
       ("modifying-single-staves#index-StaffSymbol-2" "See also" "modifying-single-staves#See-also-16")
       ("modifying-single-staves#index-StaffSymbol-1" "See also" "modifying-single-staves#See-also-146")
       ("displaying-staves#index-StaffSymbol" "See also" "displaying-staves#See-also-81"))
    ("Staff_symbol_engraver" ("modifying-single-staves#index-Staff_005fsymbol_005fengraver" "Hiding staves" "modifying-single-staves#hiding-staves"))
    ("StanzaNumber" ("stanzas#index-StanzaNumber" "See also" "stanzas#See-also-72"))
    ("Stem" ("common-notation-for-keyboards#index-Stem-3" "See also" "common-notation-for-keyboards#See-also-145")
       ("common-notation-for-keyboards#index-Stem-1" "Cross-staff stems" "common-notation-for-keyboards#cross_002dstaff-stems"))
    ("Stem_engraver" ("inside-the-staff#index-Stem_005fengraver-1" "See also" "inside-the-staff#See-also-185")
       ("beams#index-Stem_005fengraver" "See also" "beams#See-also-167"))
    ("String quartet" ("common-notation-for-unfretted-strings#index-String-quartet-1" "See also" "common-notation-for-unfretted-strings#See-also-242")
       ("common-notation-for-unfretted-strings#index-String-quartet" "References for unfretted strings" "common-notation-for-unfretted-strings#references-for-unfretted-strings"))
    ("StringNumber" ("common-notation-for-fretted-strings#index-StringNumber" "See also" "common-notation-for-fretted-strings#See-also-166"))
    ("StringTunings" ("common-notation-for-fretted-strings#index-StringTunings" "Custom tablatures" "common-notation-for-fretted-strings#custom-tablatures"))
    ("StrokeFinger" ("common-notation-for-fretted-strings#index-StrokeFinger" "See also" "common-notation-for-fretted-strings#See-also-147"))
    ("Style sheets" ("different-editions-from-one-source#index-Style-sheets-1" "See also" "different-editions-from-one-source#See-also-117")
       ("different-editions-from-one-source#index-Style-sheets" "Using global settings" "different-editions-from-one-source#using-global-settings"))
    ("SustainEvent" ("piano#index-SustainEvent" "See also" "piano#See-also-44"))
    ("SustainPedal" ("piano#index-SustainPedal" "See also" "piano#See-also-44"))
    ("SustainPedalLineSpanner" ("piano#index-SustainPedalLineSpanner" "See also" "piano#See-also-44"))
    ("SystemStartBar" ("displaying-staves#index-SystemStartBar-1" "See also" "displaying-staves#See-also-159")
       ("displaying-staves#index-SystemStartBar" "See also" "displaying-staves#See-also-57"))
    ("SystemStartBrace" ("displaying-staves#index-SystemStartBrace-1" "See also" "displaying-staves#See-also-159")
       ("displaying-staves#index-SystemStartBrace" "See also" "displaying-staves#See-also-57"))
    ("SystemStartBracket" ("displaying-staves#index-SystemStartBracket-1" "See also" "displaying-staves#See-also-159")
       ("displaying-staves#index-SystemStartBracket" "See also" "displaying-staves#See-also-57"))
    ("SystemStartSquare" ("displaying-staves#index-SystemStartSquare-1" "See also" "displaying-staves#See-also-159")
       ("displaying-staves#index-SystemStartSquare" "See also" "displaying-staves#See-also-57"))
    ("TabNoteHead" ("common-notation-for-fretted-strings#index-TabNoteHead" "See also" "common-notation-for-fretted-strings#See-also-113"))
    ("TabStaff" ("common-notation-for-fretted-strings#index-TabStaff-3" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures")
       ("displaying-staves#index-TabStaff" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("TabVoice" ("common-notation-for-fretted-strings#index-TabVoice" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("Tab_note_heads_engraver" ("common-notation-for-fretted-strings#index-Tab_005fnote_005fheads_005fengraver" "See also" "common-notation-for-fretted-strings#See-also-68"))
    ("TextScript" ("woodwind-diagrams#index-TextScript-8" "See also" "woodwind-diagrams#See-also-250")
       ("formatting-text#index-TextScript-7" "See also" "formatting-text#See-also-131")
       ("formatting-text#index-TextScript-6" "See also" "formatting-text#See-also-189")
       ("formatting-text#index-TextScript-5" "See also" "formatting-text#See-also-31")
       ("formatting-text#index-TextScript-4" "See also" "formatting-text#See-also-224")
       ("formatting-text#index-TextScript-3" "See also" "formatting-text#See-also-20")
       ("writing-text#index-TextScript-2" "See also" "writing-text#See-also-169")
       ("writing-text#index-TextScript-1" "See also" "writing-text#See-also-11")
       ("expressive-marks-attached-to-notes#index-TextScript" "See also" "expressive-marks-attached-to-notes#See-also-77"))
    ("TextSpanner" ("spanners#index-TextSpanner-1" "See also" "spanners#See-also-208")
       ("writing-text#index-TextSpanner" "See also" "writing-text#See-also-27"))
    ("The Feta font" ("music#index-The-Feta-font" "A.9.4 Music" "music"))
    ("Tie" ("writing-rhythms#index-Tie" "See also" "writing-rhythms#See-also-223"))
    ("TieColumn" ("writing-rhythms#index-TieColumn" "See also" "writing-rhythms#See-also-223"))
    ("TimeScaledMusic" ("writing-rhythms#index-TimeScaledMusic" "See also" "writing-rhythms#See-also-99"))
    ("TimeSignature" ("displaying-rhythms#index-TimeSignature-1" "See also" "displaying-rhythms#See-also-4")
       ("displaying-rhythms#index-TimeSignature" "See also" "displaying-rhythms#See-also-40"))
    ("Timing_translator" ("all-context-properties#index-Timing_005ftranslator-5" "A.14 All context properties" "all-context-properties")
       ("special-rhythmic-concerns#index-Timing_005ftranslator-4" "See also" "special-rhythmic-concerns#See-also-36")
       ("bars#index-Timing_005ftranslator-3" "See also" "bars#See-also-87")
       ("displaying-rhythms#index-Timing_005ftranslator-2" "See also" "displaying-rhythms#See-also-4")
       ("displaying-rhythms#index-Timing_005ftranslator-1" "See also" "displaying-rhythms#See-also-160")
       ("displaying-rhythms#index-Timing_005ftranslator" "See also" "displaying-rhythms#See-also-40"))
    ("Top" ("changing-defaults#index-Top-1" "5. Changing defaults" "changing-defaults")
       ("index" "LilyPond - Notation Reference" "index")
       ("index#index-Top" "LilyPond - Notation Reference" "index"))
    ("Translation" ("navigating-the-program-reference#index-Translation" "5.2.1 Navigating the program reference" "navigating-the-program-reference"))
    ("TransposedMusic" ("changing-multiple-pitches#index-TransposedMusic" "See also" "changing-multiple-pitches#See-also-254"))
    ("TrillSpanner" ("spanners#index-TrillSpanner-1" "See also" "spanners#See-also-208")
       ("expressive-marks-as-lines#index-TrillSpanner" "See also" "expressive-marks-as-lines#See-also-143"))
    ("Tunable context properties" ("the-set-command#index-Tunable-context-properties-3" "See also" "the-set-command#See-also-14")
       ("common-notation-for-vocal-music#index-Tunable-context-properties-1" "See also" "common-notation-for-vocal-music#See-also-98")
       ("common-notation-for-vocal-music#index-Tunable-context-properties" "Multiple notes to one syllable" "common-notation-for-vocal-music#multiple-notes-to-one-syllable"))
    ("TupletBracket" ("writing-rhythms#index-TupletBracket" "See also" "writing-rhythms#See-also-99"))
    ("TupletNumber" ("writing-rhythms#index-TupletNumber" "Selected Snippets" "writing-rhythms#Selected-Snippets-36"))
    ("Tweaking methods" ("the-tweak-command#index-Tweaking-methods-2" "See also" "the-tweak-command#See-also-70")
       ("writing-rhythms#index-Tweaking-methods" "See also" "writing-rhythms#See-also-99"))
    ("Tweaking output" ("advanced-tweaks#index-Tweaking-output-1" "See also" "advanced-tweaks#See-also-163")
       ("changing-defaults#index-Tweaking-output" "5. Changing defaults" "changing-defaults"))
    ("UnaCordaEvent" ("piano#index-UnaCordaEvent" "See also" "piano#See-also-44"))
    ("UnaCordaPedal" ("piano#index-UnaCordaPedal" "See also" "piano#See-also-44"))
    ("UnaCordaPedalLineSpanner" ("piano#index-UnaCordaPedalLineSpanner" "See also" "piano#See-also-44"))
    ("UnfoldedRepeatedMusic" ("long-repeats#index-UnfoldedRepeatedMusic-1" "See also" "long-repeats#See-also-52")
       ("long-repeats#index-UnfoldedRepeatedMusic" "See also" "long-repeats#See-also"))
    ("VaticanaStaff" ("displaying-staves#index-VaticanaStaff" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("VerticalAxisGroup" ("flexible-vertical-spacing-within-systems#index-VerticalAxisGroup-8" "See also" "flexible-vertical-spacing-within-systems#See-also-21")
       ("flexible-vertical-spacing-within-systems#index-VerticalAxisGroup-7" "See also" "flexible-vertical-spacing-within-systems#See-also-73")
       ("flexible-vertical-spacing-within-systems#index-VerticalAxisGroup-6" "See also" "flexible-vertical-spacing-within-systems#See-also-164")
       ("flexible-vertical-spacing-within-systems#index-VerticalAxisGroup-5" "See also" "flexible-vertical-spacing-within-systems#See-also-32")
       ("flexible-vertical-spacing-within-systems#index-VerticalAxisGroup-4" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties")
       ("choral#index-VerticalAxisGroup-1" "See also" "choral#See-also-194")
       ("flexible-vertical-spacing-within-systems#index-VerticalAxisGroup-2" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("Visibility and color of objects" ("visibility-of-objects#index-Visibility-and-color-of-objects-7" "See also" "visibility-of-objects#See-also-12")
       ("visibility-of-objects#index-Visibility-and-color-of-objects-6" "Using break-visibility" "visibility-of-objects#using-break_002dvisibility")
       ("visibility-of-objects#index-Visibility-and-color-of-objects-5" "5.4.7 Visibility of objects" "visibility-of-objects")
       ("modifying-context-plug_002dins#index-Visibility-and-color-of-objects-4" "5.1.4 Modifying context plug-ins" "modifying-context-plug_002dins")
       ("chants-psalms-and-hymns#index-Visibility-and-color-of-objects-3" "See also" "chants-psalms-and-hymns#See-also-161")
       ("inside-the-staff#index-Visibility-and-color-of-objects-2" "See also" "inside-the-staff#See-also-71")
       ("modifying-single-staves#index-Visibility-and-color-of-objects-1" "See also" "modifying-single-staves#See-also-233")
       ("writing-rests#index-Visibility-and-color-of-objects" "See also" "writing-rests#See-also-7"))
    ("Vocal ensembles" ("aligning-contexts#index-Vocal-ensembles-7" "5.1.7 Aligning contexts" "aligning-contexts")
       ("chants-psalms-and-hymns#index-Vocal-ensembles-6" "See also" "chants-psalms-and-hymns#See-also-188")
       ("chants-psalms-and-hymns#index-Vocal-ensembles-5" "Pointing a psalm" "chants-psalms-and-hymns#pointing-a-psalm")
       ("chants-psalms-and-hymns#index-Vocal-ensembles-4" "See also" "chants-psalms-and-hymns#See-also-161")
       ("choral#index-Vocal-ensembles-3" "See also" "choral#See-also-47")
       ("choral#index-Vocal-ensembles-2" "References for choral" "choral#references-for-choral")
       ("techniques-specific-to-lyrics#index-Vocal-ensembles-1" "See also" "techniques-specific-to-lyrics#See-also-55")
       ("techniques-specific-to-lyrics#index-Vocal-ensembles" "Placing lyrics vertically" "techniques-specific-to-lyrics#placing-lyrics-vertically"))
    ("Voice" ("determining-the-grob-property#index-Voice-10" "5.2.3 Determining the grob property" "determining-the-grob-property")
       ("horizontal-spacing-overview#index-Voice-9" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview")
       ("common-notation-for-vocal-music#index-Voice-8" "See also" "common-notation-for-vocal-music#See-also-29")
       ("writing-parts#index-Voice-7" "See also" "writing-parts#See-also-183")
       ("writing-parts#index-Voice-6" "See also" "writing-parts#See-also-201")
       ("multiple-voices#index-Voice-5" "Known issues and warnings" "multiple-voices#Known-issues-and-warnings-68")
       ("multiple-voices#index-Voice-4" "See also" "multiple-voices#See-also-221")
       ("note-heads#index-Voice-1" "See also" "note-heads#See-also-257")
       ("multiple-voices#index-Voice-2" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("VoiceFollower" ("spanners#index-VoiceFollower-1" "See also" "spanners#See-also-208")
       ("common-notation-for-keyboards#index-VoiceFollower" "See also" "common-notation-for-keyboards#See-also-66"))
    ("Voices contain music" ("multiple-voices#index-Voices-contain-music-2" "See also" "multiple-voices#See-also-177")
       ("multiple-voices#index-Voices-contain-music-1" "See also" "multiple-voices#See-also-215")
       ("writing-rhythms#index-Voices-contain-music" "Known issues and warnings" "writing-rhythms#Known-issues-and-warnings-30"))
    ("VoltaBracket" ("long-repeats#index-VoltaBracket-1" "See also" "long-repeats#See-also-243")
       ("long-repeats#index-VoltaBracket" "See also" "long-repeats#See-also"))
    ("VoltaRepeatedMusic" ("long-repeats#index-VoltaRepeatedMusic-1" "See also" "long-repeats#See-also-243")
       ("long-repeats#index-VoltaRepeatedMusic" "See also" "long-repeats#See-also"))
    ("Volta_engraver" ("displaying-chords#index-Volta_005fengraver" "See also" "displaying-chords#See-also-61"))
    ("Within-staff objects" ("direction-and-placement#index-Within_002dstaff-objects" "5.4.2 Direction and placement" "direction-and-placement"))
    ("Working on input files" ("structure-of-a-score#index-Working-on-input-files" "See also" "structure-of-a-score#See-also-114"))
    ("X-offset" ("flexible-vertical-spacing-within-systems#index-X_002doffset" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("[" ("beams#index-_005b" "Manual beams" "beams#manual-beams"))
    ("\\!" ("expressive-marks-attached-to-notes#index-_005c_0021" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\(" ("expressive-marks-as-curves#index-_005c_0028" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\)" ("expressive-marks-as-curves#index-_005c_0029" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\<" ("expressive-marks-attached-to-notes#index-_005c_003c" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\>" ("expressive-marks-attached-to-notes#index-_005c_003e" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\RemoveEmptyStaves" ("modifying-single-staves#index-_005cRemoveEmptyStaves-2" "Hiding staves" "modifying-single-staves#hiding-staves")
       ("modifying-single-staves#index-_005cRemoveEmptyStaves" "Hiding staves" "modifying-single-staves#hiding-staves"))
    ("\\abs-fontsize" ("font#index-_005cabs_002dfontsize" "A.9.1 Font" "font"))
    ("\\accent" ("expressive-marks-attached-to-notes#index-_005caccent" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\accepts" ("aligning-contexts#index-_005caccepts-4" "5.1.7 Aligning contexts" "aligning-contexts")
       ("defining-new-contexts#index-_005caccepts-2" "5.1.6 Defining new contexts" "defining-new-contexts")
       ("defining-new-contexts#index-_005caccepts" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("\\addChordShape" ("common-notation-for-fretted-strings#index-_005caddChordShape" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams"))
    ("\\addInstrumentDefinition" ("writing-parts#index-_005caddInstrumentDefinition" "Instrument names" "writing-parts#instrument-names"))
    ("\\addQuote" ("writing-parts#index-_005caddQuote" "Quoting other voices" "writing-parts#quoting-other-voices"))
    ("\\addlyrics" ("common-notation-for-vocal-music#index-_005caddlyrics-2" "Automatic syllable durations" "common-notation-for-vocal-music#automatic-syllable-durations")
       ("common-notation-for-vocal-music#index-_005caddlyrics" "Aligning lyrics to a melody" "common-notation-for-vocal-music#aligning-lyrics-to-a-melody"))
    ("\\aeolian" ("displaying-pitches#index-_005caeolian" "Key signature" "displaying-pitches#key-signature"))
    ("\\afterGrace" ("special-rhythmic-concerns#index-_005cafterGrace" "Grace notes" "special-rhythmic-concerns#grace-notes"))
    ("\\aikenHeads" ("note-heads#index-_005caikenHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\aikenHeadsMinor" ("note-heads#index-_005caikenHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\alias" ("defining-new-contexts#index-_005calias" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("\\allowPageTurn" ("optimal-page-turning#index-_005callowPageTurn" "Predefined commands" "optimal-page-turning#Predefined-commands-16"))
    ("\\alternative" ("long-repeats#index-_005calternative" "1.4.1 Long repeats" "long-repeats"))
    ("\\arpeggio" ("expressive-marks-as-lines#index-_005carpeggio" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("\\arpeggioArrowDown" ("expressive-marks-as-lines#index-_005carpeggioArrowDown" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("\\arpeggioArrowUp" ("expressive-marks-as-lines#index-_005carpeggioArrowUp" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("\\arpeggioBracket" ("expressive-marks-as-lines#index-_005carpeggioBracket" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("\\arpeggioNormal" ("expressive-marks-as-lines#index-_005carpeggioNormal" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("\\arpeggioParenthesis" ("expressive-marks-as-lines#index-_005carpeggioParenthesis" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("\\arpeggioParenthesisDashed" ("expressive-marks-as-lines#index-_005carpeggioParenthesisDashed" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("\\arrow-head" ("graphic#index-_005carrow_002dhead-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005carrow_002dhead" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\ascendens" ("typesetting-gregorian-chant#index-_005cascendens-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cascendens" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\auctum" ("typesetting-gregorian-chant#index-_005cauctum-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cauctum" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\augmentum" ("typesetting-gregorian-chant#index-_005caugmentum" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1"))
    ("\\autoBeamOff" ("beams#index-_005cautoBeamOff" "Automatic beams" "beams#automatic-beams"))
    ("\\autoBeamOn" ("beams#index-_005cautoBeamOn" "Automatic beams" "beams#automatic-beams"))
    ("\\autochange" ("common-notation-for-keyboards#index-_005cautochange" "Changing staff automatically" "common-notation-for-keyboards#changing-staff-automatically"))
    ("\\backslashed-digit" ("other#index-_005cbackslashed_002ddigit" "A.9.6 Other" "other"))
    ("\\balloonGrobText" ("outside-the-staff#index-_005cballoonGrobText" "Balloon help" "outside-the-staff#balloon-help"))
    ("\\balloonLengthOff" ("outside-the-staff#index-_005cballoonLengthOff" "Balloon help" "outside-the-staff#balloon-help"))
    ("\\balloonLengthOn" ("outside-the-staff#index-_005cballoonLengthOn" "Balloon help" "outside-the-staff#balloon-help"))
    ("\\balloonText" ("outside-the-staff#index-_005cballoonText" "Balloon help" "outside-the-staff#balloon-help"))
    ("\\bar" ("bars#index-_005cbar-2" "Selected Snippets" "bars#Selected-Snippets-1")
       ("bars#index-_005cbar" "Bar lines" "bars#bar-lines"))
    ("\\barNumberCheck" ("bars#index-_005cbarNumberCheck" "Bar and bar number checks" "bars#bar-and-bar-number-checks"))
    ("\\beam" ("graphic#index-_005cbeam" "A.9.3 Graphic" "graphic"))
    ("\\bendAfter" ("expressive-marks-as-curves#index-_005cbendAfter" "Falls and doits" "expressive-marks-as-curves#falls-and-doits"))
    ("\\bold" ("font#index-_005cbold-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005cbold" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\book" ("file-structure#index-_005cbook-4" "3.1.5 File structure" "file-structure")
       ("multiple-scores-in-a-book#index-_005cbook-2" "3.1.2 Multiple scores in a book" "multiple-scores-in-a-book")
       ("multiple-scores-in-a-book#index-_005cbook" "3.1.2 Multiple scores in a book" "multiple-scores-in-a-book"))
    ("\\bookOutputName" ("output-file-names#index-_005cbookOutputName" "3.1.4 Output file names" "output-file-names"))
    ("\\bookOutputSuffix" ("output-file-names#index-_005cbookOutputSuffix" "3.1.4 Output file names" "output-file-names"))
    ("\\bookpart" ("page-breaking#index-_005cbookpart-4" "4.3.2 Page breaking" "page-breaking")
       ("file-structure#index-_005cbookpart-2" "3.1.5 File structure" "file-structure")
       ("multiple-scores-in-a-book#index-_005cbookpart" "3.1.2 Multiple scores in a book" "multiple-scores-in-a-book"))
    ("\\box" ("font#index-_005cbox-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005cbox" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\bracket" ("graphic#index-_005cbracket-4" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005cbracket-2" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup")
       ("expressive-marks-attached-to-notes#index-_005cbracket" "New dynamic marks" "expressive-marks-attached-to-notes#new-dynamic-marks"))
    ("\\break" ("line-breaking#index-_005cbreak" "Predefined commands" "line-breaking#Predefined-commands-6"))
    ("\\breathe" ("expressive-marks-as-curves#index-_005cbreathe" "Breath marks" "expressive-marks-as-curves#breath-marks"))
    ("\\breve" ("writing-rests#index-_005cbreve-2" "Rests" "writing-rests#rests")
       ("writing-rhythms#index-_005cbreve" "Durations" "writing-rhythms#durations"))
    ("\\cadenzaOff" ("displaying-rhythms#index-_005ccadenzaOff" "Unmetered music" "displaying-rhythms#unmetered-music"))
    ("\\cadenzaOn" ("displaying-rhythms#index-_005ccadenzaOn" "Unmetered music" "displaying-rhythms#unmetered-music"))
    ("\\caesura" ("typesetting-gregorian-chant#index-_005ccaesura" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-26"))
    ("\\caps" ("font#index-_005ccaps" "A.9.1 Font" "font"))
    ("\\cavum" ("typesetting-gregorian-chant#index-_005ccavum-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005ccavum" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\center-align" ("align#index-_005ccenter_002dalign-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005ccenter_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("\\center-column" ("align#index-_005ccenter_002dcolumn-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005ccenter_002dcolumn" "Text alignment" "formatting-text#text-alignment"))
    ("\\change" ("common-notation-for-keyboards#index-_005cchange" "Changing staff manually" "common-notation-for-keyboards#changing-staff-manually"))
    ("\\char" ("other#index-_005cchar" "A.9.6 Other" "other"))
    ("\\chordGlissando" ("common-notation-for-fretted-strings#index-_005cchordGlissando" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("\\chordmode" ("common-notation-for-fretted-strings#index-_005cchordmode-4" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams")
       ("changing-multiple-pitches#index-_005cchordmode-2" "See also" "changing-multiple-pitches#See-also-254")
       ("writing-pitches#index-_005cchordmode" "See also" "writing-pitches#See-also-171"))
    ("\\circle" ("graphic#index-_005ccircle-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005ccircle" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\clef" ("displaying-pitches#index-_005cclef" "Clef" "displaying-pitches#clef"))
    ("\\cm" ("distances-and-measurements#index-_005ccm" "5.4.4 Distances and measurements" "distances-and-measurements"))
    ("\\coda" ("expressive-marks-attached-to-notes#index-_005ccoda" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\column" ("align#index-_005ccolumn-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005ccolumn" "Text alignment" "formatting-text#text-alignment"))
    ("\\column-lines" ("text-markup-list-commands#index-_005ccolumn_002dlines" "A.10 Text markup list commands" "text-markup-list-commands"))
    ("\\combine" ("align#index-_005ccombine-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005ccombine" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\compressFullBarRests" ("writing-rests#index-_005ccompressFullBarRests-2" "Full measure rests" "writing-rests#full-measure-rests")
       ("writing-rests#index-_005ccompressFullBarRests" "Full measure rests" "writing-rests#full-measure-rests"))
    ("\\concat" ("align#index-_005cconcat" "A.9.2 Align" "align"))
    ("\\consists" ("defining-new-contexts#index-_005cconsists" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("\\context" ("creating-contexts#index-_005ccontext" "5.1.2 Creating contexts" "creating-contexts"))
    ("\\contextStringTunings" ("common-notation-for-fretted-strings#index-_005ccontextStringTunings" "Custom tablatures" "common-notation-for-fretted-strings#custom-tablatures"))
    ("\\cr" ("expressive-marks-attached-to-notes#index-_005ccr" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\cresc" ("expressive-marks-attached-to-notes#index-_005ccresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\crescHairpin" ("expressive-marks-attached-to-notes#index-_005ccrescHairpin" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\crescTextCresc" ("expressive-marks-attached-to-notes#index-_005ccrescTextCresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\cueDuring" ("writing-parts#index-_005ccueDuring" "Formatting cue notes" "writing-parts#formatting-cue-notes"))
    ("\\customTabClef" ("music#index-_005ccustomTabClef" "A.9.4 Music" "music"))
    ("\\decr" ("expressive-marks-attached-to-notes#index-_005cdecr" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\decresc" ("expressive-marks-attached-to-notes#index-_005cdecresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\defaultTimeSignature" ("displaying-rhythms#index-_005cdefaultTimeSignature" "Time signature" "displaying-rhythms#time-signature"))
    ("\\deminutum" ("typesetting-gregorian-chant#index-_005cdeminutum-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cdeminutum" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\denies" ("aligning-contexts#index-_005cdenies-4" "5.1.7 Aligning contexts" "aligning-contexts")
       ("defining-new-contexts#index-_005cdenies-2" "5.1.6 Defining new contexts" "defining-new-contexts")
       ("defining-new-contexts#index-_005cdenies" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("\\descendens" ("typesetting-gregorian-chant#index-_005cdescendens-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cdescendens" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\dim" ("expressive-marks-attached-to-notes#index-_005cdim" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\dimHairpin" ("expressive-marks-attached-to-notes#index-_005cdimHairpin" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\dimTextDecr" ("expressive-marks-attached-to-notes#index-_005cdimTextDecr" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\dimTextDecresc" ("expressive-marks-attached-to-notes#index-_005cdimTextDecresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\dimTextDim" ("expressive-marks-attached-to-notes#index-_005cdimTextDim" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\dir-column" ("align#index-_005cdir_002dcolumn" "A.9.2 Align" "align"))
    ("\\displayLilyMusic" ("displaying-lilypond-notation#index-_005cdisplayLilyMusic" "3.3.4 Displaying LilyPond notation" "displaying-lilypond-notation"))
    ("\\divisioMaior" ("typesetting-gregorian-chant#index-_005cdivisioMaior" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-26"))
    ("\\divisioMaxima" ("typesetting-gregorian-chant#index-_005cdivisioMaxima" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-26"))
    ("\\divisioMinima" ("typesetting-gregorian-chant#index-_005cdivisioMinima" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-26"))
    ("\\dorian" ("displaying-pitches#index-_005cdorian" "Key signature" "displaying-pitches#key-signature"))
    ("\\dotsDown" ("writing-rhythms#index-_005cdotsDown" "Durations" "writing-rhythms#durations"))
    ("\\dotsNeutral" ("writing-rhythms#index-_005cdotsNeutral" "Durations" "writing-rhythms#durations"))
    ("\\dotsUp" ("writing-rhythms#index-_005cdotsUp" "Durations" "writing-rhythms#durations"))
    ("\\doubleflat" ("music#index-_005cdoubleflat" "A.9.4 Music" "music"))
    ("\\doublesharp" ("music#index-_005cdoublesharp" "A.9.4 Music" "music"))
    ("\\downbow" ("common-notation-for-unfretted-strings#index-_005cdownbow-2" "Bowing indications" "common-notation-for-unfretted-strings#bowing-indications")
       ("expressive-marks-attached-to-notes#index-_005cdownbow" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\downmordent" ("expressive-marks-attached-to-notes#index-_005cdownmordent" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\downprall" ("expressive-marks-attached-to-notes#index-_005cdownprall" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\draw-circle" ("graphic#index-_005cdraw_002dcircle-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005cdraw_002dcircle" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\draw-hline" ("graphic#index-_005cdraw_002dhline" "A.9.3 Graphic" "graphic"))
    ("\\draw-line" ("graphic#index-_005cdraw_002dline-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005cdraw_002dline" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\drummode" ("displaying-staves#index-_005cdrummode" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("\\dynamic" ("font#index-_005cdynamic-2" "A.9.1 Font" "font")
       ("expressive-marks-attached-to-notes#index-_005cdynamic" "New dynamic marks" "expressive-marks-attached-to-notes#new-dynamic-marks"))
    ("\\dynamicDown" ("expressive-marks-attached-to-notes#index-_005cdynamicDown" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\dynamicNeutral" ("expressive-marks-attached-to-notes#index-_005cdynamicNeutral" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\dynamicUp" ("expressive-marks-attached-to-notes#index-_005cdynamicUp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\easyHeadsOff" ("note-heads#index-_005ceasyHeadsOff" "Easy notation note heads" "note-heads#easy-notation-note-heads"))
    ("\\easyHeadsOn" ("note-heads#index-_005ceasyHeadsOn" "Easy notation note heads" "note-heads#easy-notation-note-heads"))
    ("\\epsfile" ("graphic#index-_005cepsfile-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005cepsfile" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\espressivo" ("expressive-marks-attached-to-notes#index-_005cespressivo-2" "Dynamics" "expressive-marks-attached-to-notes#dynamics")
       ("expressive-marks-attached-to-notes#index-_005cespressivo" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\expandFullBarRests" ("writing-rests#index-_005cexpandFullBarRests-2" "Full measure rests" "writing-rests#full-measure-rests")
       ("writing-rests#index-_005cexpandFullBarRests" "Full measure rests" "writing-rests#full-measure-rests"))
    ("\\eyeglasses" ("other#index-_005ceyeglasses" "A.9.6 Other" "other"))
    ("\\f" ("expressive-marks-attached-to-notes#index-_005cf" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\featherDurations" ("beams#index-_005cfeatherDurations" "Feathered beams" "beams#feathered-beams"))
    ("\\fermata" ("expressive-marks-attached-to-notes#index-_005cfermata" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\fermataMarkup" ("expressive-marks-attached-to-notes#index-_005cfermataMarkup-4" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations")
       ("writing-rests#index-_005cfermataMarkup-2" "Full measure rests" "writing-rests#full-measure-rests")
       ("writing-rests#index-_005cfermataMarkup" "Full measure rests" "writing-rests#full-measure-rests"))
    ("\\ff" ("expressive-marks-attached-to-notes#index-_005cff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\fff" ("expressive-marks-attached-to-notes#index-_005cfff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\ffff" ("expressive-marks-attached-to-notes#index-_005cffff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\fffff" ("expressive-marks-attached-to-notes#index-_005cfffff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\fill-line" ("align#index-_005cfill_002dline-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cfill_002dline" "Text alignment" "formatting-text#text-alignment"))
    ("\\fill-with-pattern" ("align#index-_005cfill_002dwith_002dpattern" "A.9.2 Align" "align"))
    ("\\filled-box" ("graphic#index-_005cfilled_002dbox-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005cfilled_002dbox" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\finalis" ("typesetting-gregorian-chant#index-_005cfinalis" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-26"))
    ("\\finger" ("font#index-_005cfinger-2" "A.9.1 Font" "font")
       ("inside-the-staff#index-_005cfinger" "Fingering instructions" "inside-the-staff#fingering-instructions"))
    ("\\flageolet" ("expressive-marks-attached-to-notes#index-_005cflageolet" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\flat" ("music#index-_005cflat" "A.9.4 Music" "music"))
    ("\\flexa" ("typesetting-gregorian-chant#index-_005cflexa" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1"))
    ("\\fontCaps" ("font#index-_005cfontCaps" "A.9.1 Font" "font"))
    ("\\fontsize" ("font#index-_005cfontsize-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005cfontsize" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\footnote" ("other#index-_005cfootnote" "A.9.6 Other" "other"))
    ("\\fp" ("expressive-marks-attached-to-notes#index-_005cfp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\fraction" ("other#index-_005cfraction" "A.9.6 Other" "other"))
    ("\\frenchChords" ("displaying-chords#index-_005cfrenchChords" "Predefined commands" "displaying-chords#Predefined-commands-23"))
    ("\\fret-diagram" ("instrument-specific-markup#index-_005cfret_002ddiagram-2" "A.9.5 Instrument Specific Markup" "instrument-specific-markup")
       ("common-notation-for-fretted-strings#index-_005cfret_002ddiagram" "Fret diagram markups" "common-notation-for-fretted-strings#fret-diagram-markups"))
    ("\\fret-diagram-terse" ("instrument-specific-markup#index-_005cfret_002ddiagram_002dterse-2" "A.9.5 Instrument Specific Markup" "instrument-specific-markup")
       ("common-notation-for-fretted-strings#index-_005cfret_002ddiagram_002dterse" "Fret diagram markups" "common-notation-for-fretted-strings#fret-diagram-markups"))
    ("\\fret-diagram-verbose" ("instrument-specific-markup#index-_005cfret_002ddiagram_002dverbose-2" "A.9.5 Instrument Specific Markup" "instrument-specific-markup")
       ("common-notation-for-fretted-strings#index-_005cfret_002ddiagram_002dverbose" "Fret diagram markups" "common-notation-for-fretted-strings#fret-diagram-markups"))
    ("\\fromproperty" ("other#index-_005cfromproperty" "A.9.6 Other" "other"))
    ("\\funkHeads" ("note-heads#index-_005cfunkHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\funkHeadsMinor" ("note-heads#index-_005cfunkHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\general-align" ("align#index-_005cgeneral_002dalign-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cgeneral_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("\\germanChords" ("displaying-chords#index-_005cgermanChords" "Predefined commands" "displaying-chords#Predefined-commands-23"))
    ("\\glissando" ("expressive-marks-as-lines#index-_005cglissando" "Glissando" "expressive-marks-as-lines#glissando"))
    ("\\grace" ("special-rhythmic-concerns#index-_005cgrace" "Grace notes" "special-rhythmic-concerns#grace-notes"))
    ("\\halfopen" ("expressive-marks-attached-to-notes#index-_005chalfopen" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\halign" ("align#index-_005chalign-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005chalign" "Text alignment" "formatting-text#text-alignment"))
    ("\\harmonic" ("common-notation-for-fretted-strings#index-_005charmonic-2" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures")
       ("common-notation-for-unfretted-strings#index-_005charmonic" "Harmonics" "common-notation-for-unfretted-strings#harmonics"))
    ("\\harmonicByFret" ("common-notation-for-fretted-strings#index-_005charmonicByFret" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("\\harmonicByRatio" ("common-notation-for-fretted-strings#index-_005charmonicByRatio" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("\\harmonicsOff" ("common-notation-for-unfretted-strings#index-_005charmonicsOff" "Harmonics" "common-notation-for-unfretted-strings#harmonics"))
    ("\\harmonicsOn" ("common-notation-for-unfretted-strings#index-_005charmonicsOn" "Harmonics" "common-notation-for-unfretted-strings#harmonics"))
    ("\\harp-pedal" ("instrument-specific-markup#index-_005charp_002dpedal" "A.9.5 Instrument Specific Markup" "instrument-specific-markup"))
    ("\\hbracket" ("graphic#index-_005chbracket-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005chbracket" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\hcenter-in" ("align#index-_005chcenter_002din" "A.9.2 Align" "align"))
    ("\\header" ("file-structure#index-_005cheader" "3.1.5 File structure" "file-structure"))
    ("\\hideKeySignature" ("bagpipes#index-_005chideKeySignature" "Bagpipe definitions" "bagpipes#bagpipe-definitions"))
    ("\\hideNotes" ("inside-the-staff#index-_005chideNotes" "Hidden notes" "inside-the-staff#hidden-notes"))
    ("\\hideSplitTiedTabNotes" ("common-notation-for-fretted-strings#index-_005chideSplitTiedTabNotes" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("\\hideStaffSwitch" ("common-notation-for-keyboards#index-_005chideStaffSwitch" "Staff-change lines" "common-notation-for-keyboards#staff_002dchange-lines"))
    ("\\hspace" ("align#index-_005chspace" "A.9.2 Align" "align"))
    ("\\huge" ("font#index-_005chuge-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005chuge-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-_005chuge" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("\\improvisationOff" ("displaying-rhythms#index-_005cimprovisationOff-2" "Showing melody rhythms" "displaying-rhythms#showing-melody-rhythms")
       ("note-heads#index-_005cimprovisationOff" "Improvisation" "note-heads#improvisation"))
    ("\\improvisationOn" ("displaying-rhythms#index-_005cimprovisationOn-2" "Showing melody rhythms" "displaying-rhythms#showing-melody-rhythms")
       ("note-heads#index-_005cimprovisationOn" "Improvisation" "note-heads#improvisation"))
    ("\\in" ("distances-and-measurements#index-_005cin" "5.4.4 Distances and measurements" "distances-and-measurements"))
    ("\\inclinatum" ("typesetting-gregorian-chant#index-_005cinclinatum-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cinclinatum" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\include" ("including-lilypond-files#index-_005cinclude" "3.3.1 Including LilyPond files" "including-lilypond-files"))
    ("\\instrumentSwitch" ("writing-parts#index-_005cinstrumentSwitch" "Instrument names" "writing-parts#instrument-names"))
    ("\\inversion" ("changing-multiple-pitches#index-_005cinversion" "Inversion" "changing-multiple-pitches#inversion"))
    ("\\ionian" ("displaying-pitches#index-_005cionian" "Key signature" "displaying-pitches#key-signature"))
    ("\\italianChords" ("displaying-chords#index-_005citalianChords" "Predefined commands" "displaying-chords#Predefined-commands-23"))
    ("\\italic" ("font#index-_005citalic-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005citalic" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\justified-lines" ("text-markup-list-commands#index-_005cjustified_002dlines-2" "A.10 Text markup list commands" "text-markup-list-commands")
       ("formatting-text#index-_005cjustified_002dlines" "Multi-page markup" "formatting-text#multi_002dpage-markup"))
    ("\\justify" ("align#index-_005cjustify-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cjustify" "Text alignment" "formatting-text#text-alignment"))
    ("\\justify-field" ("align#index-_005cjustify_002dfield" "A.9.2 Align" "align"))
    ("\\justify-string" ("align#index-_005cjustify_002dstring" "A.9.2 Align" "align"))
    ("\\keepWithTag" ("different-editions-from-one-source#index-_005ckeepWithTag" "Using tags" "different-editions-from-one-source#using-tags"))
    ("\\key" ("note-heads#index-_005ckey-2" "Shape note heads" "note-heads#shape-note-heads")
       ("displaying-pitches#index-_005ckey" "Key signature" "displaying-pitches#key-signature"))
    ("\\killCues" ("writing-parts#index-_005ckillCues" "Formatting cue notes" "writing-parts#formatting-cue-notes"))
    ("\\label" ("reference-to-page-numbers#index-_005clabel" "Predefined commands" "reference-to-page-numbers#Predefined-commands-27"))
    ("\\laissezVibrer" ("writing-rhythms#index-_005claissezVibrer" "Ties" "writing-rhythms#ties"))
    ("\\large" ("font#index-_005clarge-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005clarge-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-_005clarge" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("\\larger" ("font#index-_005clarger-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005clarger-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("formatting-text#index-_005clarger" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\layout" ("file-structure#index-_005clayout" "3.1.5 File structure" "file-structure"))
    ("\\left-align" ("align#index-_005cleft_002dalign-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cleft_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("\\left-brace" ("other#index-_005cleft_002dbrace" "A.9.6 Other" "other"))
    ("\\left-column" ("align#index-_005cleft_002dcolumn" "A.9.2 Align" "align"))
    ("\\lheel" ("expressive-marks-attached-to-notes#index-_005clheel" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\line" ("align#index-_005cline" "A.9.2 Align" "align"))
    ("\\linea" ("typesetting-gregorian-chant#index-_005clinea-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005clinea" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\lineprall" ("expressive-marks-attached-to-notes#index-_005clineprall" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\locrian" ("displaying-pitches#index-_005clocrian" "Key signature" "displaying-pitches#key-signature"))
    ("\\longa" ("writing-rests#index-_005clonga-2" "Rests" "writing-rests#rests")
       ("writing-rhythms#index-_005clonga" "Durations" "writing-rhythms#durations"))
    ("\\longfermata" ("expressive-marks-attached-to-notes#index-_005clongfermata" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\lookup" ("other#index-_005clookup" "A.9.6 Other" "other"))
    ("\\lower" ("align#index-_005clower-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005clower" "Text alignment" "formatting-text#text-alignment"))
    ("\\ltoe" ("expressive-marks-attached-to-notes#index-_005cltoe" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\lydian" ("displaying-pitches#index-_005clydian" "Key signature" "displaying-pitches#key-signature"))
    ("\\lyricmode" ("common-notation-for-vocal-music#index-_005clyricmode-2" "Aligning lyrics to a melody" "common-notation-for-vocal-music#aligning-lyrics-to-a-melody")
       ("common-notation-for-vocal-music#index-_005clyricmode" "Entering lyrics" "common-notation-for-vocal-music#entering-lyrics"))
    ("\\lyricsto" ("common-notation-for-vocal-music#index-_005clyricsto-2" "Automatic syllable durations" "common-notation-for-vocal-music#automatic-syllable-durations")
       ("common-notation-for-vocal-music#index-_005clyricsto" "Aligning lyrics to a melody" "common-notation-for-vocal-music#aligning-lyrics-to-a-melody"))
    ("\\magnify" ("font#index-_005cmagnify-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005cmagnify" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\major" ("displaying-pitches#index-_005cmajor" "Key signature" "displaying-pitches#key-signature"))
    ("\\makeClusters" ("single-voice#index-_005cmakeClusters" "Clusters" "single-voice#clusters"))
    ("\\makeStringTuning" ("common-notation-for-fretted-strings#index-_005cmakeStringTuning" "Custom tablatures" "common-notation-for-fretted-strings#custom-tablatures"))
    ("\\marcato" ("expressive-marks-attached-to-notes#index-_005cmarcato" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\mark" ("writing-text#index-_005cmark-2" "Text marks" "writing-text#text-marks")
       ("bars#index-_005cmark" "Rehearsal marks" "bars#rehearsal-marks"))
    ("\\markalphabet" ("other#index-_005cmarkalphabet" "A.9.6 Other" "other"))
    ("\\markletter" ("other#index-_005cmarkletter" "A.9.6 Other" "other"))
    ("\\markup" ("formatting-text#index-_005cmarkup-6" "Text markup introduction" "formatting-text#text-markup-introduction")
       ("writing-text#index-_005cmarkup-4" "Separate text" "writing-text#separate-text")
       ("writing-text#index-_005cmarkup-2" "Separate text" "writing-text#separate-text")
       ("writing-text#index-_005cmarkup" "Text marks" "writing-text#text-marks"))
    ("\\markuplines" ("formatting-text#index-_005cmarkuplines-4" "See also" "formatting-text#See-also-131")
       ("formatting-text#index-_005cmarkuplines-2" "Multi-page markup" "formatting-text#multi_002dpage-markup")
       ("writing-text#index-_005cmarkuplines" "Separate text" "writing-text#separate-text"))
    ("\\maxima" ("writing-rests#index-_005cmaxima-2" "Rests" "writing-rests#rests")
       ("writing-rhythms#index-_005cmaxima" "Durations" "writing-rhythms#durations"))
    ("\\medium" ("font#index-_005cmedium" "A.9.1 Font" "font"))
    ("\\melisma" ("common-notation-for-vocal-music#index-_005cmelisma" "Multiple notes to one syllable" "common-notation-for-vocal-music#multiple-notes-to-one-syllable"))
    ("\\melismaEnd" ("common-notation-for-vocal-music#index-_005cmelismaEnd" "Multiple notes to one syllable" "common-notation-for-vocal-music#multiple-notes-to-one-syllable"))
    ("\\mergeDifferentlyDottedOff" ("multiple-voices#index-_005cmergeDifferentlyDottedOff" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\mergeDifferentlyDottedOn" ("multiple-voices#index-_005cmergeDifferentlyDottedOn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\mergeDifferentlyHeadedOff" ("multiple-voices#index-_005cmergeDifferentlyHeadedOff" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\mergeDifferentlyHeadedOn" ("multiple-voices#index-_005cmergeDifferentlyHeadedOn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\mf" ("expressive-marks-attached-to-notes#index-_005cmf" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\midi" ("file-structure#index-_005cmidi" "3.1.5 File structure" "file-structure"))
    ("\\minor" ("displaying-pitches#index-_005cminor" "Key signature" "displaying-pitches#key-signature"))
    ("\\mixolydian" ("displaying-pitches#index-_005cmixolydian" "Key signature" "displaying-pitches#key-signature"))
    ("\\mm" ("distances-and-measurements#index-_005cmm" "5.4.4 Distances and measurements" "distances-and-measurements"))
    ("\\modalInversion" ("changing-multiple-pitches#index-_005cmodalInversion" "Modal inversion" "changing-multiple-pitches#Modal-inversion"))
    ("\\modalTranspose" ("changing-multiple-pitches#index-_005cmodalTranspose" "Modal transposition" "changing-multiple-pitches#Modal-transposition"))
    ("\\mordent" ("expressive-marks-attached-to-notes#index-_005cmordent" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\mp" ("expressive-marks-attached-to-notes#index-_005cmp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\musicglyph" ("music#index-_005cmusicglyph-2" "A.9.4 Music" "music")
       ("bars#index-_005cmusicglyph" "Rehearsal marks" "bars#rehearsal-marks"))
    ("\\name" ("defining-new-contexts#index-_005cname" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("\\natural" ("music#index-_005cnatural" "A.9.4 Music" "music"))
    ("\\new" ("creating-contexts#index-_005cnew" "5.1.2 Creating contexts" "creating-contexts"))
    ("\\noBeam" ("beams#index-_005cnoBeam" "Manual beams" "beams#manual-beams"))
    ("\\noBreak" ("line-breaking#index-_005cnoBreak" "Predefined commands" "line-breaking#Predefined-commands-6"))
    ("\\noPageBreak" ("page-breaking#index-_005cnoPageBreak" "Predefined commands" "page-breaking#Predefined-commands-33"))
    ("\\noPageTurn" ("optimal-page-turning#index-_005cnoPageTurn" "Predefined commands" "optimal-page-turning#Predefined-commands-16"))
    ("\\normal-size-sub" ("font#index-_005cnormal_002dsize_002dsub" "A.9.1 Font" "font"))
    ("\\normal-size-super" ("font#index-_005cnormal_002dsize_002dsuper" "A.9.1 Font" "font"))
    ("\\normal-text" ("font#index-_005cnormal_002dtext" "A.9.1 Font" "font"))
    ("\\normalsize" ("font#index-_005cnormalsize-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005cnormalsize-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-_005cnormalsize" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("\\note" ("music#index-_005cnote" "A.9.4 Music" "music"))
    ("\\note-by-number" ("music#index-_005cnote_002dby_002dnumber" "A.9.4 Music" "music"))
    ("\\null" ("other#index-_005cnull-2" "A.9.6 Other" "other")
       ("formatting-text#index-_005cnull" "Text alignment" "formatting-text#text-alignment"))
    ("\\number" ("font#index-_005cnumber" "A.9.1 Font" "font"))
    ("\\numericTimeSignature" ("displaying-rhythms#index-_005cnumericTimeSignature" "Time signature" "displaying-rhythms#time-signature"))
    ("\\octaveCheck" ("changing-multiple-pitches#index-_005coctaveCheck" "Octave checks" "changing-multiple-pitches#octave-checks"))
    ("\\on-the-fly" ("other#index-_005con_002dthe_002dfly" "A.9.6 Other" "other"))
    ("\\oneVoice" ("multiple-voices#index-_005coneVoice" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("\\open" ("common-notation-for-unfretted-strings#index-_005copen-2" "Bowing indications" "common-notation-for-unfretted-strings#bowing-indications")
       ("expressive-marks-attached-to-notes#index-_005copen" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\oriscus" ("typesetting-gregorian-chant#index-_005coriscus-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005coriscus" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\ottava" ("displaying-pitches#index-_005cottava" "Ottava brackets" "displaying-pitches#ottava-brackets"))
    ("\\override" ("other#index-_005coverride-2" "A.9.6 Other" "other")
       ("the-set-command#index-_005coverride" "See also" "the-set-command#See-also-14"))
    ("\\override in \\lyricmode" ("common-notation-for-vocal-music#index-_005coverride-in-_005clyricmode" "Entering lyrics" "common-notation-for-vocal-music#entering-lyrics"))
    ("\\override-lines" ("text-markup-list-commands#index-_005coverride_002dlines" "A.10 Text markup list commands" "text-markup-list-commands"))
    ("\\overrideTimeSignatureSettings" ("displaying-rhythms#index-_005coverrideTimeSignatureSettings" "Time signature" "displaying-rhythms#time-signature"))
    ("\\p" ("expressive-marks-attached-to-notes#index-_005cp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\pad-around" ("align#index-_005cpad_002daround-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cpad_002daround" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\pad-markup" ("align#index-_005cpad_002dmarkup-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cpad_002dmarkup" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\pad-to-box" ("align#index-_005cpad_002dto_002dbox-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cpad_002dto_002dbox" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\pad-x" ("align#index-_005cpad_002dx-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cpad_002dx" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\page-link" ("other#index-_005cpage_002dlink" "A.9.6 Other" "other"))
    ("\\page-ref" ("other#index-_005cpage_002dref-2" "A.9.6 Other" "other")
       ("reference-to-page-numbers#index-_005cpage_002dref" "Predefined commands" "reference-to-page-numbers#Predefined-commands-27"))
    ("\\pageBreak" ("page-breaking#index-_005cpageBreak" "Predefined commands" "page-breaking#Predefined-commands-33"))
    ("\\pageTurn" ("optimal-page-turning#index-_005cpageTurn" "Predefined commands" "optimal-page-turning#Predefined-commands-16"))
    ("\\paper" ("paper-size-and-automatic-scaling#index-_005cpaper-2" "4.1.2 Paper size and automatic scaling" "paper-size-and-automatic-scaling")
       ("file-structure#index-_005cpaper" "3.1.5 File structure" "file-structure"))
    ("\\parallelMusic" ("multiple-voices#index-_005cparallelMusic" "Writing music in parallel" "multiple-voices#writing-music-in-parallel"))
    ("\\parenthesize" ("graphic#index-_005cparenthesize-2" "A.9.3 Graphic" "graphic")
       ("inside-the-staff#index-_005cparenthesize" "Parentheses" "inside-the-staff#parentheses"))
    ("\\partcombine" ("multiple-voices#index-_005cpartcombine" "Automatic part combining" "multiple-voices#automatic-part-combining"))
    ("\\partial" ("long-repeats#index-_005cpartial-4" "Normal repeats" "long-repeats#normal-repeats")
       ("long-repeats#index-_005cpartial-2" "1.4.1 Long repeats" "long-repeats")
       ("displaying-rhythms#index-_005cpartial" "Upbeats" "displaying-rhythms#upbeats"))
    ("\\path" ("graphic#index-_005cpath" "A.9.3 Graphic" "graphic"))
    ("\\pattern" ("other#index-_005cpattern" "A.9.6 Other" "other"))
    ("\\pes" ("typesetting-gregorian-chant#index-_005cpes" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1"))
    ("\\phrasingSlurDashPattern" ("expressive-marks-as-curves#index-_005cphrasingSlurDashPattern" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurDashed" ("expressive-marks-as-curves#index-_005cphrasingSlurDashed" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurDotted" ("expressive-marks-as-curves#index-_005cphrasingSlurDotted" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurDown" ("expressive-marks-as-curves#index-_005cphrasingSlurDown" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurHalfDashed" ("expressive-marks-as-curves#index-_005cphrasingSlurHalfDashed" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurHalfSolid" ("expressive-marks-as-curves#index-_005cphrasingSlurHalfSolid" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurNeutral" ("expressive-marks-as-curves#index-_005cphrasingSlurNeutral" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurSolid" ("expressive-marks-as-curves#index-_005cphrasingSlurSolid" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrasingSlurUp" ("expressive-marks-as-curves#index-_005cphrasingSlurUp" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("\\phrygian" ("displaying-pitches#index-_005cphrygian" "Key signature" "displaying-pitches#key-signature"))
    ("\\pitchedTrill" ("expressive-marks-as-lines#index-_005cpitchedTrill" "Trills" "expressive-marks-as-lines#trills"))
    ("\\portato" ("expressive-marks-attached-to-notes#index-_005cportato" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\postscript" ("graphic#index-_005cpostscript-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005cpostscript" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\powerChords" ("guitar#index-_005cpowerChords" "Indicating power chords" "guitar#indicating-power-chords"))
    ("\\pp" ("expressive-marks-attached-to-notes#index-_005cpp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\ppp" ("expressive-marks-attached-to-notes#index-_005cppp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\pppp" ("expressive-marks-attached-to-notes#index-_005cpppp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\ppppp" ("expressive-marks-attached-to-notes#index-_005cppppp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\prall" ("expressive-marks-attached-to-notes#index-_005cprall" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\pralldown" ("expressive-marks-attached-to-notes#index-_005cpralldown" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\prallmordent" ("expressive-marks-attached-to-notes#index-_005cprallmordent" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\prallprall" ("expressive-marks-attached-to-notes#index-_005cprallprall" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\prallup" ("expressive-marks-attached-to-notes#index-_005cprallup" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\predefinedFretboardsOff" ("common-notation-for-fretted-strings#index-_005cpredefinedFretboardsOff" "Automatic fret diagrams" "common-notation-for-fretted-strings#automatic-fret-diagrams"))
    ("\\predefinedFretboardsOn" ("common-notation-for-fretted-strings#index-_005cpredefinedFretboardsOn" "Automatic fret diagrams" "common-notation-for-fretted-strings#automatic-fret-diagrams"))
    ("\\pt" ("distances-and-measurements#index-_005cpt" "5.4.4 Distances and measurements" "distances-and-measurements"))
    ("\\put-adjacent" ("align#index-_005cput_002dadjacent" "A.9.2 Align" "align"))
    ("\\quilisma" ("typesetting-gregorian-chant#index-_005cquilisma-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cquilisma" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\quoteDuring" ("writing-parts#index-_005cquoteDuring-2" "Formatting cue notes" "writing-parts#formatting-cue-notes")
       ("writing-parts#index-_005cquoteDuring" "Quoting other voices" "writing-parts#quoting-other-voices"))
    ("\\raise" ("align#index-_005craise-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005craise" "Text alignment" "formatting-text#text-alignment"))
    ("\\relative" ("common-notation-for-keyboards#index-_005crelative-6" "Changing staff automatically" "common-notation-for-keyboards#changing-staff-automatically")
       ("changing-multiple-pitches#index-_005crelative-4" "See also" "changing-multiple-pitches#See-also-254")
       ("writing-pitches#index-_005crelative-2" "See also" "writing-pitches#See-also-171")
       ("writing-pitches#index-_005crelative" "Relative octave entry" "writing-pitches#relative-octave-entry"))
    ("\\removeWithTag" ("different-editions-from-one-source#index-_005cremoveWithTag" "Using tags" "different-editions-from-one-source#using-tags"))
    ("\\repeat" ("long-repeats#index-_005crepeat" "1.4.1 Long repeats" "long-repeats"))
    ("\\repeat percent" ("short-repeats#index-_005crepeat-percent" "Percent repeats" "short-repeats#percent-repeats"))
    ("\\repeat tremolo" ("short-repeats#index-_005crepeat-tremolo" "Tremolo repeats" "short-repeats#tremolo-repeats"))
    ("\\repeatTie" ("techniques-specific-to-lyrics#index-_005crepeatTie-4" "Repeats with alternative endings" "techniques-specific-to-lyrics#Repeats-with-alternative-endings")
       ("long-repeats#index-_005crepeatTie-2" "Normal repeats" "long-repeats#normal-repeats")
       ("writing-rhythms#index-_005crepeatTie" "Ties" "writing-rhythms#ties"))
    ("\\rest" ("writing-rests#index-_005crest" "Rests" "writing-rests#rests"))
    ("\\retrograde" ("changing-multiple-pitches#index-_005cretrograde" "Retrograde" "changing-multiple-pitches#retrograde"))
    ("\\reverseturn" ("expressive-marks-attached-to-notes#index-_005creverseturn" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\revertTimeSignatureSettings" ("displaying-rhythms#index-_005crevertTimeSignatureSettings" "Time signature" "displaying-rhythms#time-signature"))
    ("\\rfz" ("expressive-marks-attached-to-notes#index-_005crfz" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\rheel" ("expressive-marks-attached-to-notes#index-_005crheel" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\right-align" ("align#index-_005cright_002dalign-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cright_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("\\right-brace" ("other#index-_005cright_002dbrace" "A.9.6 Other" "other"))
    ("\\right-column" ("align#index-_005cright_002dcolumn" "A.9.2 Align" "align"))
    ("\\rightHandFinger" ("common-notation-for-fretted-strings#index-_005crightHandFinger" "Right-hand fingerings" "common-notation-for-fretted-strings#right_002dhand-fingerings"))
    ("\\roman" ("font#index-_005croman" "A.9.1 Font" "font"))
    ("\\rotate" ("align#index-_005crotate" "A.9.2 Align" "align"))
    ("\\rounded-box" ("graphic#index-_005crounded_002dbox-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005crounded_002dbox" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\rtoe" ("expressive-marks-attached-to-notes#index-_005crtoe" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\sacredHarpHeads" ("note-heads#index-_005csacredHarpHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\sacredHarpHeadsMinor" ("note-heads#index-_005csacredHarpHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\sans" ("font#index-_005csans" "A.9.1 Font" "font"))
    ("\\scale" ("graphic#index-_005cscale" "A.9.3 Graphic" "graphic"))
    ("\\scaleDurations" ("displaying-rhythms#index-_005cscaleDurations-2" "Polymetric notation" "displaying-rhythms#polymetric-notation")
       ("writing-rhythms#index-_005cscaleDurations" "Scaling durations" "writing-rhythms#scaling-durations"))
    ("\\score" ("music#index-_005cscore-4" "A.9.4 Music" "music")
       ("file-structure#index-_005cscore-2" "3.1.5 File structure" "file-structure")
       ("structure-of-a-score#index-_005cscore" "3.1.1 Structure of a score" "structure-of-a-score"))
    ("\\segno" ("expressive-marks-attached-to-notes#index-_005csegno" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\semiGermanChords" ("displaying-chords#index-_005csemiGermanChords" "Predefined commands" "displaying-chords#Predefined-commands-23"))
    ("\\semiflat" ("music#index-_005csemiflat" "A.9.4 Music" "music"))
    ("\\semisharp" ("music#index-_005csemisharp" "A.9.4 Music" "music"))
    ("\\sesquiflat" ("music#index-_005csesquiflat" "A.9.4 Music" "music"))
    ("\\sesquisharp" ("music#index-_005csesquisharp" "A.9.4 Music" "music"))
    ("\\set" ("beams#index-_005cset" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior"))
    ("\\sf" ("expressive-marks-attached-to-notes#index-_005csf" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\sff" ("expressive-marks-attached-to-notes#index-_005csff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\sfz" ("expressive-marks-attached-to-notes#index-_005csfz" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\sharp" ("music#index-_005csharp" "A.9.4 Music" "music"))
    ("\\shiftOff" ("multiple-voices#index-_005cshiftOff" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\shiftOn" ("multiple-voices#index-_005cshiftOn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\shiftOnn" ("multiple-voices#index-_005cshiftOnn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\shiftOnnn" ("multiple-voices#index-_005cshiftOnnn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("\\shortfermata" ("expressive-marks-attached-to-notes#index-_005cshortfermata" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\showKeySignature" ("bagpipes#index-_005cshowKeySignature" "Bagpipe definitions" "bagpipes#bagpipe-definitions"))
    ("\\showStaffSwitch" ("common-notation-for-keyboards#index-_005cshowStaffSwitch" "Staff-change lines" "common-notation-for-keyboards#staff_002dchange-lines"))
    ("\\signumcongruentiae" ("expressive-marks-attached-to-notes#index-_005csignumcongruentiae" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\simple" ("font#index-_005csimple" "A.9.1 Font" "font"))
    ("\\skip" ("techniques-specific-to-lyrics#index-_005cskip-2" "Repeats with alternative endings" "techniques-specific-to-lyrics#Repeats-with-alternative-endings")
       ("writing-rests#index-_005cskip" "Invisible rests" "writing-rests#invisible-rests"))
    ("\\slashed-digit" ("other#index-_005cslashed_002ddigit" "A.9.6 Other" "other"))
    ("\\slurDashPattern" ("expressive-marks-as-curves#index-_005cslurDashPattern" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurDashed" ("expressive-marks-as-curves#index-_005cslurDashed" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurDotted" ("expressive-marks-as-curves#index-_005cslurDotted" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurDown" ("expressive-marks-as-curves#index-_005cslurDown" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurHalfDashed" ("expressive-marks-as-curves#index-_005cslurHalfDashed" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurHalfSolid" ("expressive-marks-as-curves#index-_005cslurHalfSolid" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurNeutral" ("expressive-marks-as-curves#index-_005cslurNeutral" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurSolid" ("expressive-marks-as-curves#index-_005cslurSolid" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\slurUp" ("expressive-marks-as-curves#index-_005cslurUp" "Slurs" "expressive-marks-as-curves#slurs"))
    ("\\small" ("font#index-_005csmall-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005csmall-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-_005csmall" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("\\smallCaps" ("font#index-_005csmallCaps" "A.9.1 Font" "font"))
    ("\\smaller" ("font#index-_005csmaller-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005csmaller-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("formatting-text#index-_005csmaller" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\snappizzicato" ("expressive-marks-attached-to-notes#index-_005csnappizzicato" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\sostenutoOff" ("piano#index-_005csostenutoOff" "Piano pedals" "piano#piano-pedals"))
    ("\\sostenutoOn" ("piano#index-_005csostenutoOn" "Piano pedals" "piano#piano-pedals"))
    ("\\southernHarmonyHeads" ("note-heads#index-_005csouthernHarmonyHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\southernHarmonyHeadsMinor" ("note-heads#index-_005csouthernHarmonyHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\sp" ("expressive-marks-attached-to-notes#index-_005csp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\spp" ("expressive-marks-attached-to-notes#index-_005cspp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("\\staccatissimo" ("expressive-marks-attached-to-notes#index-_005cstaccatissimo" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\staccato" ("expressive-marks-attached-to-notes#index-_005cstaccato" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\startGroup" ("outside-the-staff#index-_005cstartGroup" "Analysis brackets" "outside-the-staff#analysis-brackets"))
    ("\\startStaff" ("modifying-single-staves#index-_005cstartStaff-2" "Ossia staves" "modifying-single-staves#ossia-staves")
       ("modifying-single-staves#index-_005cstartStaff" "Staff symbol" "modifying-single-staves#staff-symbol"))
    ("\\startTrillSpan" ("expressive-marks-as-lines#index-_005cstartTrillSpan" "Trills" "expressive-marks-as-lines#trills"))
    ("\\stemDown" ("inside-the-staff#index-_005cstemDown" "Stems" "inside-the-staff#stems"))
    ("\\stemNeutral" ("inside-the-staff#index-_005cstemNeutral" "Stems" "inside-the-staff#stems"))
    ("\\stemUp" ("inside-the-staff#index-_005cstemUp" "Stems" "inside-the-staff#stems"))
    ("\\stencil" ("other#index-_005cstencil" "A.9.6 Other" "other"))
    ("\\stopGroup" ("outside-the-staff#index-_005cstopGroup" "Analysis brackets" "outside-the-staff#analysis-brackets"))
    ("\\stopStaff" ("modifying-single-staves#index-_005cstopStaff-4" "Hiding staves" "modifying-single-staves#hiding-staves")
       ("modifying-single-staves#index-_005cstopStaff-2" "Ossia staves" "modifying-single-staves#ossia-staves")
       ("modifying-single-staves#index-_005cstopStaff" "Staff symbol" "modifying-single-staves#staff-symbol"))
    ("\\stopTrillSpan" ("expressive-marks-as-lines#index-_005cstopTrillSpan" "Trills" "expressive-marks-as-lines#trills"))
    ("\\stopped" ("expressive-marks-attached-to-notes#index-_005cstopped" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\storePredefinedDiagram" ("common-notation-for-fretted-strings#index-_005cstorePredefinedDiagram" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams"))
    ("\\stropha" ("typesetting-gregorian-chant#index-_005cstropha-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cstropha" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\strut" ("other#index-_005cstrut" "A.9.6 Other" "other"))
    ("\\sub" ("font#index-_005csub-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005csub" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\super" ("font#index-_005csuper-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005csuper" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\sustainOff" ("piano#index-_005csustainOff" "Piano pedals" "piano#piano-pedals"))
    ("\\sustainOn" ("piano#index-_005csustainOn" "Piano pedals" "piano#piano-pedals"))
    ("\\tabChordRepetition" ("common-notation-for-fretted-strings#index-_005ctabChordRepetition" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("\\tabFullNotation" ("common-notation-for-fretted-strings#index-_005ctabFullNotation" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("\\table-of-contents" ("text-markup-list-commands#index-_005ctable_002dof_002dcontents-2" "A.10 Text markup list commands" "text-markup-list-commands")
       ("table-of-contents#index-_005ctable_002dof_002dcontents" "Predefined commands" "table-of-contents#Predefined-commands-11"))
    ("\\tag" ("different-editions-from-one-source#index-_005ctag" "Using tags" "different-editions-from-one-source#using-tags"))
    ("\\taor" ("bagpipes#index-_005ctaor" "Bagpipe definitions" "bagpipes#bagpipe-definitions"))
    ("\\teeny" ("font#index-_005cteeny-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005cteeny-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-_005cteeny" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("\\tempo" ("displaying-rhythms#index-_005ctempo" "Metronome marks" "displaying-rhythms#metronome-marks"))
    ("\\tenuto" ("expressive-marks-attached-to-notes#index-_005ctenuto" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\text" ("font#index-_005ctext" "A.9.1 Font" "font"))
    ("\\textLengthOff" ("writing-text#index-_005ctextLengthOff-2" "Text scripts" "writing-text#text-scripts")
       ("writing-rests#index-_005ctextLengthOff" "Full measure rests" "writing-rests#full-measure-rests"))
    ("\\textLengthOn" ("writing-text#index-_005ctextLengthOn-2" "Text scripts" "writing-text#text-scripts")
       ("writing-rests#index-_005ctextLengthOn" "Full measure rests" "writing-rests#full-measure-rests"))
    ("\\textSpannerDown" ("writing-text#index-_005ctextSpannerDown" "Text spanners" "writing-text#text-spanners"))
    ("\\textSpannerNeutral" ("writing-text#index-_005ctextSpannerNeutral" "Text spanners" "writing-text#text-spanners"))
    ("\\textSpannerUp" ("writing-text#index-_005ctextSpannerUp" "Text spanners" "writing-text#text-spanners"))
    ("\\thumb" ("inside-the-staff#index-_005cthumb-2" "Fingering instructions" "inside-the-staff#fingering-instructions")
       ("expressive-marks-attached-to-notes#index-_005cthumb" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\tieDashed" ("writing-rhythms#index-_005ctieDashed" "Ties" "writing-rhythms#ties"))
    ("\\tieDotted" ("writing-rhythms#index-_005ctieDotted" "Ties" "writing-rhythms#ties"))
    ("\\tieDown" ("writing-rhythms#index-_005ctieDown" "Ties" "writing-rhythms#ties"))
    ("\\tieNeutral" ("writing-rhythms#index-_005ctieNeutral" "Ties" "writing-rhythms#ties"))
    ("\\tieSolid" ("writing-rhythms#index-_005ctieSolid" "Ties" "writing-rhythms#ties"))
    ("\\tieUp" ("writing-rhythms#index-_005ctieUp" "Ties" "writing-rhythms#ties"))
    ("\\tied-lyric" ("music#index-_005ctied_002dlyric" "A.9.4 Music" "music"))
    ("\\time" ("beams#index-_005ctime-2" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior")
       ("displaying-rhythms#index-_005ctime" "Time signature" "displaying-rhythms#time-signature"))
    ("\\times" ("displaying-rhythms#index-_005ctimes-2" "Polymetric notation" "displaying-rhythms#polymetric-notation")
       ("writing-rhythms#index-_005ctimes" "Tuplets" "writing-rhythms#tuplets"))
    ("\\tiny" ("font#index-_005ctiny-4" "A.9.1 Font" "font")
       ("formatting-text#index-_005ctiny-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-_005ctiny" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("\\tocItem" ("table-of-contents#index-_005ctocItem" "Predefined commands" "table-of-contents#Predefined-commands-11"))
    ("\\translate" ("align#index-_005ctranslate-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005ctranslate" "Text alignment" "formatting-text#text-alignment"))
    ("\\translate-scaled" ("align#index-_005ctranslate_002dscaled-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005ctranslate_002dscaled" "Text alignment" "formatting-text#text-alignment"))
    ("\\transparent" ("other#index-_005ctransparent" "A.9.6 Other" "other"))
    ("\\transpose" ("changing-multiple-pitches#index-_005ctranspose-4" "See also" "changing-multiple-pitches#See-also-254")
       ("changing-multiple-pitches#index-_005ctranspose-2" "Transpose" "changing-multiple-pitches#transpose")
       ("writing-pitches#index-_005ctranspose" "See also" "writing-pitches#See-also-171"))
    ("\\transposedCueDuring" ("writing-parts#index-_005ctransposedCueDuring" "Formatting cue notes" "writing-parts#formatting-cue-notes"))
    ("\\transposition" ("writing-parts#index-_005ctransposition-2" "Quoting other voices" "writing-parts#quoting-other-voices")
       ("displaying-pitches#index-_005ctransposition" "Instrument transpositions" "displaying-pitches#instrument-transpositions"))
    ("\\treCorde" ("piano#index-_005ctreCorde" "Piano pedals" "piano#piano-pedals"))
    ("\\triangle" ("graphic#index-_005ctriangle-2" "A.9.3 Graphic" "graphic")
       ("formatting-text#index-_005ctriangle" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("\\trill" ("expressive-marks-as-lines#index-_005ctrill-2" "Trills" "expressive-marks-as-lines#trills")
       ("expressive-marks-attached-to-notes#index-_005ctrill" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\tupletDown" ("writing-rhythms#index-_005ctupletDown" "Tuplets" "writing-rhythms#tuplets"))
    ("\\tupletNeutral" ("writing-rhythms#index-_005ctupletNeutral" "Tuplets" "writing-rhythms#tuplets"))
    ("\\tupletUp" ("writing-rhythms#index-_005ctupletUp" "Tuplets" "writing-rhythms#tuplets"))
    ("\\turn" ("expressive-marks-attached-to-notes#index-_005cturn" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\type" ("defining-new-contexts#index-_005ctype" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("\\typewriter" ("font#index-_005ctypewriter" "A.9.1 Font" "font"))
    ("\\unHideNotes" ("inside-the-staff#index-_005cunHideNotes" "Hidden notes" "inside-the-staff#hidden-notes"))
    ("\\unaCorda" ("piano#index-_005cunaCorda" "Piano pedals" "piano#piano-pedals"))
    ("\\underline" ("font#index-_005cunderline-2" "A.9.1 Font" "font")
       ("formatting-text#index-_005cunderline" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("\\unfoldRepeats" ("repeats-in-midi#index-_005cunfoldRepeats" "3.5.4 Repeats in MIDI" "repeats-in-midi"))
    ("\\upbow" ("common-notation-for-unfretted-strings#index-_005cupbow-2" "Bowing indications" "common-notation-for-unfretted-strings#bowing-indications")
       ("expressive-marks-attached-to-notes#index-_005cupbow" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\upmordent" ("expressive-marks-attached-to-notes#index-_005cupmordent" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\upprall" ("expressive-marks-attached-to-notes#index-_005cupprall" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\upright" ("font#index-_005cupright" "A.9.1 Font" "font"))
    ("\\varcoda" ("expressive-marks-attached-to-notes#index-_005cvarcoda" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\vcenter" ("align#index-_005cvcenter" "A.9.2 Align" "align"))
    ("\\verbatim-file" ("other#index-_005cverbatim_002dfile" "A.9.6 Other" "other"))
    ("\\verylongfermata" ("expressive-marks-attached-to-notes#index-_005cverylongfermata" "Articulations and ornamentations" "expressive-marks-attached-to-notes#articulations-and-ornamentations"))
    ("\\virga" ("typesetting-gregorian-chant#index-_005cvirga-2" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-1")
       ("typesetting-gregorian-chant#index-_005cvirga" "Gregorian square neume ligatures" "typesetting-gregorian-chant#gregorian-square-neume-ligatures"))
    ("\\virgula" ("typesetting-gregorian-chant#index-_005cvirgula" "Predefined commands" "typesetting-gregorian-chant#Predefined-commands-26"))
    ("\\voiceFourStyle" ("multiple-voices#index-_005cvoiceFourStyle" "Voice styles" "multiple-voices#voice-styles"))
    ("\\voiceNeutralStyle" ("multiple-voices#index-_005cvoiceNeutralStyle" "Voice styles" "multiple-voices#voice-styles"))
    ("\\voiceOne" ("multiple-voices#index-_005cvoiceOne" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("\\voiceOne ... \\voiceFour" ("multiple-voices#index-_005cvoiceOne-_002e_002e_002e-_005cvoiceFour" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("\\voiceOneStyle" ("multiple-voices#index-_005cvoiceOneStyle" "Voice styles" "multiple-voices#voice-styles"))
    ("\\voiceThreeStyle" ("multiple-voices#index-_005cvoiceThreeStyle" "Voice styles" "multiple-voices#voice-styles"))
    ("\\voiceTwoStyle" ("multiple-voices#index-_005cvoiceTwoStyle" "Voice styles" "multiple-voices#voice-styles"))
    ("\\vspace" ("align#index-_005cvspace" "A.9.2 Align" "align"))
    ("\\walkerHeads" ("note-heads#index-_005cwalkerHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\walkerHeadsMinor" ("note-heads#index-_005cwalkerHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("\\whiteout" ("other#index-_005cwhiteout" "A.9.6 Other" "other"))
    ("\\with" ("modifying-context-plug_002dins#index-_005cwith" "5.1.4 Modifying context plug-ins" "modifying-context-plug_002dins"))
    ("\\with-color" ("other#index-_005cwith_002dcolor-2" "A.9.6 Other" "other")
       ("inside-the-staff#index-_005cwith_002dcolor" "Coloring objects" "inside-the-staff#coloring-objects"))
    ("\\with-dimensions" ("other#index-_005cwith_002ddimensions" "A.9.6 Other" "other"))
    ("\\with-link" ("other#index-_005cwith_002dlink" "A.9.6 Other" "other"))
    ("\\with-url" ("graphic#index-_005cwith_002durl" "A.9.3 Graphic" "graphic"))
    ("\\woodwind-diagram" ("instrument-specific-markup#index-_005cwoodwind_002ddiagram" "A.9.5 Instrument Specific Markup" "instrument-specific-markup"))
    ("\\wordwrap" ("align#index-_005cwordwrap-2" "A.9.2 Align" "align")
       ("formatting-text#index-_005cwordwrap" "Text alignment" "formatting-text#text-alignment"))
    ("\\wordwrap-field" ("align#index-_005cwordwrap_002dfield" "A.9.2 Align" "align"))
    ("\\wordwrap-internal" ("text-markup-list-commands#index-_005cwordwrap_002dinternal" "A.10 Text markup list commands" "text-markup-list-commands"))
    ("\\wordwrap-lines" ("text-markup-list-commands#index-_005cwordwrap_002dlines-2" "A.10 Text markup list commands" "text-markup-list-commands")
       ("formatting-text#index-_005cwordwrap_002dlines" "Multi-page markup" "formatting-text#multi_002dpage-markup"))
    ("\\wordwrap-string" ("align#index-_005cwordwrap_002dstring" "A.9.2 Align" "align"))
    ("\\wordwrap-string-internal" ("text-markup-list-commands#index-_005cwordwrap_002dstring_002dinternal" "A.10 Text markup list commands" "text-markup-list-commands"))
    ("]" ("beams#index-_005d" "Manual beams" "beams#manual-beams"))
    ("^" ("chord-mode#index-_005e" "Extended and altered chords" "chord-mode#extended-and-altered-chords"))
    ("_" ("common-notation-for-vocal-music#index-_005f" "Multiple syllables to one note" "common-notation-for-vocal-music#multiple-syllables-to-one-note"))
    ("a due" ("multiple-voices#index-a-due" "See also" "multiple-voices#See-also-221"))
    ("accent" ("expressive-marks-attached-to-notes#index-accent-1" "See also" "expressive-marks-attached-to-notes#See-also-77"))
    ("accepts" ("defining-new-contexts#index-accepts" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("acciaccatura" ("available-music-functions#index-acciaccatura-2" "A.16 Available music functions" "available-music-functions"))
    ("accidental" ("typesetting-gregorian-chant#index-accidental-2" "See also" "typesetting-gregorian-chant#See-also-136")
       ("typesetting-mensural-music#index-accidental-1" "See also" "typesetting-mensural-music#See-also-256"))
    ("accidental-interface" ("writing-pitches#index-accidental_002dinterface" "See also" "writing-pitches#See-also-258"))
    ("accidental-suggestion-interface" ("displaying-pitches#index-accidental_002dsuggestion_002dinterface" "See also" "displaying-pitches#See-also-51"))
    ("addChordShape" ("available-music-functions#index-addChordShape-2" "A.16 Available music functions" "available-music-functions")
       ("common-notation-for-fretted-strings#index-addChordShape" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams"))
    ("addInstrumentDefinition" ("available-music-functions#index-addInstrumentDefinition-2" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-addInstrumentDefinition" "Instrument names" "writing-parts#instrument-names"))
    ("addQuote" ("available-music-functions#index-addQuote-2" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-addQuote" "Quoting other voices" "writing-parts#quoting-other-voices"))
    ("aeolian" ("displaying-pitches#index-aeolian" "Key signature" "displaying-pitches#key-signature"))
    ("afterGrace" ("available-music-functions#index-afterGrace-2" "A.16 Available music functions" "available-music-functions")
       ("special-rhythmic-concerns#index-afterGrace" "Grace notes" "special-rhythmic-concerns#grace-notes"))
    ("aikenHeads" ("note-heads#index-aikenHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("aikenHeadsMinor" ("note-heads#index-aikenHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("al niente" ("expressive-marks-attached-to-notes#index-al-niente-1" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("alias" ("defining-new-contexts#index-alias" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("alignAboveContext" ("aligning-contexts#index-alignAboveContext" "5.1.7 Aligning contexts" "aligning-contexts"))
    ("alignBelowContext" ("aligning-contexts#index-alignBelowContext" "5.1.7 Aligning contexts" "aligning-contexts"))
    ("allowPageTurn" ("available-music-functions#index-allowPageTurn" "A.16 Available music functions" "available-music-functions"))
    ("ambitus" ("common-notation-for-vocal-music#index-ambitus-2" "See also" "common-notation-for-vocal-music#See-also-48")
       ("displaying-pitches#index-ambitus-1" "See also" "displaying-pitches#See-also-144"))
    ("ambitus-interface" ("displaying-pitches#index-ambitus_002dinterface" "See also" "displaying-pitches#See-also-144"))
    ("anacrusis" ("displaying-rhythms#index-anacrusis-1" "See also" "displaying-rhythms#See-also-160"))
    ("annotate-spacing" ("displaying-spacing#index-annotate_002dspacing" "4.6.1 Displaying spacing" "displaying-spacing"))
    ("applyContext" ("available-music-functions#index-applyContext" "A.16 Available music functions" "available-music-functions"))
    ("applyMusic" ("available-music-functions#index-applyMusic" "A.16 Available music functions" "available-music-functions"))
    ("applyOutput" ("available-music-functions#index-applyOutput" "A.16 Available music functions" "available-music-functions"))
    ("appoggiatura" ("available-music-functions#index-appoggiatura-2" "A.16 Available music functions" "available-music-functions"))
    ("arpeggio" ("expressive-marks-as-lines#index-arpeggio-1" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("arpeggioArrowDown" ("expressive-marks-as-lines#index-arpeggioArrowDown" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("arpeggioArrowUp" ("expressive-marks-as-lines#index-arpeggioArrowUp" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("arpeggioBracket" ("expressive-marks-as-lines#index-arpeggioBracket" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("arpeggioNormal" ("expressive-marks-as-lines#index-arpeggioNormal" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("arpeggioParenthesis" ("expressive-marks-as-lines#index-arpeggioParenthesis" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("arpeggioParenthesisDashed" ("expressive-marks-as-lines#index-arpeggioParenthesisDashed" "Arpeggio" "expressive-marks-as-lines#arpeggio"))
    ("arrow-head" ("formatting-text#index-arrow_002dhead" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("assertBeamQuant" ("available-music-functions#index-assertBeamQuant" "A.16 Available music functions" "available-music-functions"))
    ("assertBeamSlope" ("available-music-functions#index-assertBeamSlope" "A.16 Available music functions" "available-music-functions"))
    ("associatedVoice" ("common-notation-for-vocal-music#index-associatedVoice" "Aligning lyrics to a melody" "common-notation-for-vocal-music#aligning-lyrics-to-a-melody"))
    ("aug" ("chord-mode#index-aug" "Common chords" "chord-mode#common-chords"))
    ("autoBeamOff" ("beams#index-autoBeamOff" "Automatic beams" "beams#automatic-beams"))
    ("autoBeamOn" ("beams#index-autoBeamOn" "Automatic beams" "beams#automatic-beams"))
    ("autoBeaming" ("beams#index-autoBeaming" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior"))
    ("autochange" ("available-music-functions#index-autochange-2" "A.16 Available music functions" "available-music-functions")
       ("common-notation-for-keyboards#index-autochange" "Changing staff automatically" "common-notation-for-keyboards#changing-staff-automatically"))
    ("balloon-interface" ("outside-the-staff#index-balloon_002dinterface" "See also" "outside-the-staff#See-also-82"))
    ("balloonGrobText" ("available-music-functions#index-balloonGrobText-2" "A.16 Available music functions" "available-music-functions")
       ("outside-the-staff#index-balloonGrobText" "Balloon help" "outside-the-staff#balloon-help"))
    ("balloonLengthOff" ("outside-the-staff#index-balloonLengthOff" "Balloon help" "outside-the-staff#balloon-help"))
    ("balloonLengthOn" ("outside-the-staff#index-balloonLengthOn" "Balloon help" "outside-the-staff#balloon-help"))
    ("balloonText" ("available-music-functions#index-balloonText-2" "A.16 Available music functions" "available-music-functions")
       ("outside-the-staff#index-balloonText" "Balloon help" "outside-the-staff#balloon-help"))
    ("banjo-c-tuning" ("banjo#index-banjo_002dc_002dtuning" "Banjo tablatures" "banjo#banjo-tablatures"))
    ("banjo-modal-tuning" ("banjo#index-banjo_002dmodal_002dtuning" "Banjo tablatures" "banjo#banjo-tablatures"))
    ("banjo-open-d-tuning" ("banjo#index-banjo_002dopen_002dd_002dtuning" "Banjo tablatures" "banjo#banjo-tablatures"))
    ("banjo-open-dm-tuning" ("banjo#index-banjo_002dopen_002ddm_002dtuning" "Banjo tablatures" "banjo#banjo-tablatures"))
    ("bar" ("available-music-functions#index-bar-4" "A.16 Available music functions" "available-music-functions")
       ("bars#index-bar-2" "Selected Snippets" "bars#Selected-Snippets-1")
       ("bars#index-bar" "Bar lines" "bars#bar-lines"))
    ("bar-line-interface" ("all-context-properties#index-bar_002dline_002dinterface" "A.14 All context properties" "all-context-properties"))
    ("barCheckSynchronize" ("bars#index-barCheckSynchronize" "Bar and bar number checks" "bars#bar-and-bar-number-checks"))
    ("barNumberCheck" ("available-music-functions#index-barNumberCheck-2" "A.16 Available music functions" "available-music-functions")
       ("bars#index-barNumberCheck" "Bar and bar number checks" "bars#bar-and-bar-number-checks"))
    ("barNumberVisibility" ("bars#index-barNumberVisibility" "Bar numbers" "bars#bar-numbers"))
    ("bartype" ("bars#index-bartype" "Selected Snippets" "bars#Selected-Snippets-1"))
    ("base-shortest-duration" ("horizontal-spacing-overview#index-base_002dshortest_002dduration" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview"))
    ("baseMoment" ("beams#index-baseMoment" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior"))
    ("bayati" ("arabic-music#index-bayati" "See also" "arabic-music#See-also-142"))
    ("beam-interface" ("beams#index-beam_002dinterface-2" "See also" "beams#See-also-167")
       ("beams#index-beam_002dinterface-1" "See also" "beams#See-also-175")
       ("beams#index-beam_002dinterface" "See also" "beams#See-also-206"))
    ("beamExceptions" ("beams#index-beamExceptions" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior"))
    ("beatStructure" ("beams#index-beatStructure" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior"))
    ("bendAfter" ("available-music-functions#index-bendAfter-2" "A.16 Available music functions" "available-music-functions")
       ("expressive-marks-as-curves#index-bendAfter" "Falls and doits" "expressive-marks-as-curves#falls-and-doits"))
    ("bold" ("formatting-text#index-bold" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("bookOutputName" ("available-music-functions#index-bookOutputName" "A.16 Available music functions" "available-music-functions"))
    ("bookOutputSuffix" ("available-music-functions#index-bookOutputSuffix" "A.16 Available music functions" "available-music-functions"))
    ("box" ("formatting-text#index-box" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("brace" ("displaying-staves#index-brace" "See also" "displaying-staves#See-also-57"))
    ("bracket" ("piano#index-bracket-5" "Piano pedals" "piano#piano-pedals")
       ("formatting-text#index-bracket-3" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup")
       ("expressive-marks-attached-to-notes#index-bracket" "New dynamic marks" "expressive-marks-attached-to-notes#new-dynamic-marks"))
    ("breakable" ("beams#index-breakable" "Predefined commands" "beams#Predefined-commands-28"))
    ("breathe" ("available-music-functions#index-breathe-2" "A.16 Available music functions" "available-music-functions")
       ("expressive-marks-as-curves#index-breathe" "Breath marks" "expressive-marks-as-curves#breath-marks"))
    ("breve" ("writing-rests#index-breve-3" "Rests" "writing-rests#rests")
       ("writing-rhythms#index-breve" "Durations" "writing-rhythms#durations"))
    ("cadenza" ("special-rhythmic-concerns#index-cadenza-3" "See also" "special-rhythmic-concerns#See-also-111")
       ("displaying-rhythms#index-cadenza-1" "See also" "displaying-rhythms#See-also-102"))
    ("cadenzaOff" ("displaying-rhythms#index-cadenzaOff" "Unmetered music" "displaying-rhythms#unmetered-music"))
    ("cadenzaOn" ("displaying-rhythms#index-cadenzaOn" "Unmetered music" "displaying-rhythms#unmetered-music"))
    ("caesura" ("typesetting-gregorian-chant#index-caesura-2" "See also" "typesetting-gregorian-chant#See-also-59")
       ("expressive-marks-as-curves#index-caesura-1" "See also" "expressive-marks-as-curves#See-also-28"))
    ("center-align" ("formatting-text#index-center_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("center-column" ("formatting-text#index-center_002dcolumn" "Text alignment" "formatting-text#text-alignment"))
    ("change" ("common-notation-for-keyboards#index-change" "Changing staff manually" "common-notation-for-keyboards#changing-staff-manually"))
    ("chord" ("displaying-chords#index-chord-2" "See also" "displaying-chords#See-also-61")
       ("chord-mode#index-chord-1" "See also" "chord-mode#See-also-182")
       ("single-voice#index-chord" "See also" "single-voice#See-also-106"))
    ("chordChanges" ("displaying-chords#index-chordChanges" "Selected Snippets" "displaying-chords#Selected-Snippets-7"))
    ("chordGlissando" ("available-music-functions#index-chordGlissando" "A.16 Available music functions" "available-music-functions"))
    ("chordNameExceptions" ("displaying-chords#index-chordNameExceptions" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("chordNameLowercaseMinor" ("displaying-chords#index-chordNameLowercaseMinor" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("chordNameSeparator" ("displaying-chords#index-chordNameSeparator" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("chordNoteNamer" ("displaying-chords#index-chordNoteNamer" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("chordPrefixSpacer" ("displaying-chords#index-chordPrefixSpacer" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("chordRootNamer" ("displaying-chords#index-chordRootNamer" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("chordmode" ("common-notation-for-fretted-strings#index-chordmode-4" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams")
       ("changing-multiple-pitches#index-chordmode-2" "See also" "changing-multiple-pitches#See-also-254")
       ("writing-pitches#index-chordmode" "See also" "writing-pitches#See-also-171"))
    ("chords" ("displaying-chords#index-chords-2" "Printing chord names" "displaying-chords#printing-chord-names"))
    ("church mode" ("displaying-pitches#index-church-mode" "See also" "displaying-pitches#See-also-151"))
    ("circle" ("formatting-text#index-circle" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("clef" ("available-music-functions#index-clef-6" "A.16 Available music functions" "available-music-functions")
       ("displaying-pitches#index-clef-2" "Clef" "displaying-pitches#clef"))
    ("clef-interface" ("displaying-pitches#index-clef_002dinterface" "See also" "displaying-pitches#See-also-141"))
    ("cluster" ("single-voice#index-cluster-1" "See also" "single-voice#See-also-41"))
    ("color" ("inside-the-staff#index-color" "Coloring objects" "inside-the-staff#coloring-objects"))
    ("column" ("formatting-text#index-column" "Text alignment" "formatting-text#text-alignment"))
    ("combine" ("formatting-text#index-combine" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("common-shortest-duration" ("horizontal-spacing-overview#index-common_002dshortest_002dduration" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview"))
    ("compoundMeter" ("available-music-functions#index-compoundMeter" "A.16 Available music functions" "available-music-functions"))
    ("compressFullBarRests" ("writing-rests#index-compressFullBarRests-2" "Full measure rests" "writing-rests#full-measure-rests")
       ("writing-rests#index-compressFullBarRests" "Full measure rests" "writing-rests#full-measure-rests"))
    ("concert pitch" ("displaying-pitches#index-concert-pitch" "See also" "displaying-pitches#See-also-58"))
    ("consists" ("defining-new-contexts#index-consists" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("contextStringTuning" ("available-music-functions#index-contextStringTuning" "A.16 Available music functions" "available-music-functions"))
    ("contextStringTunings" ("common-notation-for-fretted-strings#index-contextStringTunings" "Custom tablatures" "common-notation-for-fretted-strings#custom-tablatures"))
    ("controlpitch" ("changing-multiple-pitches#index-controlpitch" "Octave checks" "changing-multiple-pitches#octave-checks"))
    ("cr" ("expressive-marks-attached-to-notes#index-cr" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("cresc" ("expressive-marks-attached-to-notes#index-cresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("crescHairpin" ("expressive-marks-attached-to-notes#index-crescHairpin" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("crescTextCresc" ("expressive-marks-attached-to-notes#index-crescTextCresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("crescendo" ("expressive-marks-attached-to-notes#index-crescendo-1" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("cross" ("note-heads#index-cross" "Special note heads" "note-heads#special-note-heads"))
    ("cross-staff" ("common-notation-for-keyboards#index-cross_002dstaff-1" "Cross-staff stems" "common-notation-for-keyboards#cross_002dstaff-stems"))
    ("cue-notes" ("opera-and-stage-musicals#index-cue_002dnotes" "See also" "opera-and-stage-musicals#See-also-199"))
    ("cueClef" ("available-music-functions#index-cueClef" "A.16 Available music functions" "available-music-functions"))
    ("cueClefUnset" ("available-music-functions#index-cueClefUnset" "A.16 Available music functions" "available-music-functions"))
    ("cueDuring" ("available-music-functions#index-cueDuring-2" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-cueDuring" "Formatting cue notes" "writing-parts#formatting-cue-notes"))
    ("cueDuringWithClef" ("available-music-functions#index-cueDuringWithClef" "A.16 Available music functions" "available-music-functions"))
    ("currentBarNumber" ("special-rhythmic-concerns#index-currentBarNumber-2" "Time administration" "special-rhythmic-concerns#time-administration")
       ("bars#index-currentBarNumber" "Bar numbers" "bars#bar-numbers"))
    ("custos" ("ancient-notation_002d_002dcommon-features#index-custos-2" "See also" "ancient-notation_002d_002dcommon-features#See-also-191")
       ("ancient-notation#index-custos" "See also" "ancient-notation#See-also-178"))
    ("deadNote" ("available-music-functions#index-deadNote" "A.16 Available music functions" "available-music-functions"))
    ("decr" ("expressive-marks-attached-to-notes#index-decr" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("decresc" ("expressive-marks-attached-to-notes#index-decresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("decrescendo" ("expressive-marks-attached-to-notes#index-decrescendo-1" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("default" ("displaying-pitches#index-default-2" "Automatic accidentals" "displaying-pitches#automatic-accidentals")
       ("displaying-pitches#index-default" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("default-staff-staff-spacing" ("flexible-vertical-spacing-within-systems#index-default_002dstaff_002dstaff_002dspacing" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("defaultBarType" ("bars#index-defaultBarType" "Selected Snippets" "bars#Selected-Snippets-1"))
    ("defaultNoteHeads" ("available-music-functions#index-defaultNoteHeads" "A.16 Available music functions" "available-music-functions"))
    ("defaultTimeSignature" ("displaying-rhythms#index-defaultTimeSignature" "Time signature" "displaying-rhythms#time-signature"))
    ("denies" ("defining-new-contexts#index-denies" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("dim" ("chord-mode#index-dim-2" "Common chords" "chord-mode#common-chords")
       ("expressive-marks-attached-to-notes#index-dim" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("dimHairpin" ("expressive-marks-attached-to-notes#index-dimHairpin" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("dimTextDecr" ("expressive-marks-attached-to-notes#index-dimTextDecr" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("dimTextDecresc" ("expressive-marks-attached-to-notes#index-dimTextDecresc" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("dimTextDim" ("expressive-marks-attached-to-notes#index-dimTextDim" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("displayLilyMusic" ("available-music-functions#index-displayLilyMusic" "A.16 Available music functions" "available-music-functions"))
    ("displayMusic" ("available-music-functions#index-displayMusic" "A.16 Available music functions" "available-music-functions"))
    ("divisio" ("typesetting-gregorian-chant#index-divisio-1" "See also" "typesetting-gregorian-chant#See-also-59"))
    ("dodecaphonic" ("displaying-pitches#index-dodecaphonic" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("doit" ("expressive-marks-as-curves#index-doit" "See also" "expressive-marks-as-curves#See-also-107"))
    ("dorian" ("displaying-pitches#index-dorian" "Key signature" "displaying-pitches#key-signature"))
    ("dotsDown" ("writing-rhythms#index-dotsDown" "Durations" "writing-rhythms#durations"))
    ("dotsNeutral" ("writing-rhythms#index-dotsNeutral" "Durations" "writing-rhythms#durations"))
    ("dotsUp" ("writing-rhythms#index-dotsUp" "Durations" "writing-rhythms#durations"))
    ("double flat" ("writing-pitches#index-double-flat-1" "See also" "writing-pitches#See-also-258"))
    ("double sharp" ("writing-pitches#index-double-sharp-1" "See also" "writing-pitches#See-also-258"))
    ("draw-circle" ("formatting-text#index-draw_002dcircle" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("draw-line" ("formatting-text#index-draw_002dline" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("drummode" ("displaying-staves#index-drummode" "Instantiating new staves" "displaying-staves#instantiating-new-staves"))
    ("dynamic" ("expressive-marks-attached-to-notes#index-dynamic" "New dynamic marks" "expressive-marks-attached-to-notes#new-dynamic-marks"))
    ("dynamicDown" ("expressive-marks-attached-to-notes#index-dynamicDown" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("dynamicNeutral" ("expressive-marks-attached-to-notes#index-dynamicNeutral" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("dynamicUp" ("expressive-marks-attached-to-notes#index-dynamicUp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("easyHeadsOff" ("note-heads#index-easyHeadsOff" "Easy notation note heads" "note-heads#easy-notation-note-heads"))
    ("easyHeadsOn" ("note-heads#index-easyHeadsOn" "Easy notation note heads" "note-heads#easy-notation-note-heads"))
    ("endSpanners" ("available-music-functions#index-endSpanners" "A.16 Available music functions" "available-music-functions"))
    ("epsfile" ("formatting-text#index-epsfile" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("espressivo" ("expressive-marks-attached-to-notes#index-espressivo-1" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("expandFullBarRests" ("writing-rests#index-expandFullBarRests-2" "Full measure rests" "writing-rests#full-measure-rests")
       ("writing-rests#index-expandFullBarRests" "Full measure rests" "writing-rests#full-measure-rests"))
    ("extra-offset" ("flexible-vertical-spacing-within-systems#index-extra_002doffset" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("f" ("expressive-marks-attached-to-notes#index-f" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("fall" ("expressive-marks-as-curves#index-fall" "See also" "expressive-marks-as-curves#See-also-107"))
    ("featherDurations" ("available-music-functions#index-featherDurations-2" "A.16 Available music functions" "available-music-functions")
       ("beams#index-featherDurations" "Feathered beams" "beams#feathered-beams"))
    ("fermataMarkup" ("writing-rests#index-fermataMarkup-2" "Full measure rests" "writing-rests#full-measure-rests")
       ("writing-rests#index-fermataMarkup" "Full measure rests" "writing-rests#full-measure-rests"))
    ("ff" ("expressive-marks-attached-to-notes#index-ff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("fff" ("expressive-marks-attached-to-notes#index-fff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("ffff" ("expressive-marks-attached-to-notes#index-ffff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("fffff" ("expressive-marks-attached-to-notes#index-fffff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("fifth" ("writing-pitches#index-fifth" "See also" "writing-pitches#See-also-171"))
    ("figured bass" ("ancient-notation_002d_002dcommon-features#index-figured-bass-1" "See also" "ancient-notation_002d_002dcommon-features#See-also-180")
       ("figured-bass#index-figured-bass" "See also" "figured-bass#See-also-253"))
    ("fill-line" ("formatting-text#index-fill_002dline" "Text alignment" "formatting-text#text-alignment"))
    ("filled-box" ("formatting-text#index-filled_002dbox" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("finger" ("inside-the-staff#index-finger" "Fingering instructions" "inside-the-staff#fingering-instructions"))
    ("finger-interface" ("layout-interfaces#index-finger_002dinterface" "5.2.2 Layout interfaces" "layout-interfaces"))
    ("fingering-event" ("navigating-the-program-reference#index-fingering_002devent-2" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("navigating-the-program-reference#index-fingering_002devent-1" "5.2.1 Navigating the program reference" "navigating-the-program-reference")
       ("inside-the-staff#index-fingering_002devent" "See also" "inside-the-staff#See-also-119"))
    ("flag" ("typesetting-mensural-music#index-flag-1" "See also" "typesetting-mensural-music#See-also-190")
       ("overview-of-the-supported-styles#index-flag" "See also" "overview-of-the-supported-styles#See-also-38"))
    ("flag-style" ("common-notation-for-keyboards#index-flag_002dstyle" "Cross-staff stems" "common-notation-for-keyboards#cross_002dstaff-stems"))
    ("flat" ("writing-pitches#index-flat-1" "See also" "writing-pitches#See-also-258"))
    ("followVoice" ("common-notation-for-keyboards#index-followVoice" "Staff-change lines" "common-notation-for-keyboards#staff_002dchange-lines"))
    ("font-interface" ("other#index-font_002dinterface-6" "A.9.6 Other" "other")
       ("fonts#index-font_002dinterface-3" "Fonts explained" "fonts#fonts-explained")
       ("inside-the-staff#index-font_002dinterface" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("font-size" ("inside-the-staff#index-font_002dsize-2" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size")
       ("inside-the-staff#index-font_002dsize" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("fontSize" ("inside-the-staff#index-fontSize" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("fontsize" ("formatting-text#index-fontsize" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("footnote" ("available-music-functions#index-footnote" "A.16 Available music functions" "available-music-functions"))
    ("footnoteGrob" ("available-music-functions#index-footnoteGrob" "A.16 Available music functions" "available-music-functions"))
    ("forget" ("displaying-pitches#index-forget" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("four-string-banjo" ("banjo#index-four_002dstring_002dbanjo" "Banjo tablatures" "banjo#banjo-tablatures"))
    ("fp" ("expressive-marks-attached-to-notes#index-fp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("fret-diagram" ("common-notation-for-fretted-strings#index-fret_002ddiagram" "Fret diagram markups" "common-notation-for-fretted-strings#fret-diagram-markups"))
    ("fret-diagram-interface" ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dinterface-7" "See also" "common-notation-for-fretted-strings#See-also-85")
       ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dinterface-6" "Automatic fret diagrams" "common-notation-for-fretted-strings#automatic-fret-diagrams")
       ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dinterface-5" "See also" "common-notation-for-fretted-strings#See-also-228")
       ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dinterface-4" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams")
       ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dinterface-3" "See also" "common-notation-for-fretted-strings#See-also-24")
       ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dinterface" "Fret diagram markups" "common-notation-for-fretted-strings#fret-diagram-markups"))
    ("fret-diagram-terse" ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dterse" "Fret diagram markups" "common-notation-for-fretted-strings#fret-diagram-markups"))
    ("fret-diagram-verbose" ("common-notation-for-fretted-strings#index-fret_002ddiagram_002dverbose" "Fret diagram markups" "common-notation-for-fretted-strings#fret-diagram-markups"))
    ("funkHeads" ("note-heads#index-funkHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("funkHeadsMinor" ("note-heads#index-funkHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("general-align" ("formatting-text#index-general_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("glissando" ("expressive-marks-as-lines#index-glissando-1" "Glissando" "expressive-marks-as-lines#glissando"))
    ("grace" ("available-music-functions#index-grace-2" "A.16 Available music functions" "available-music-functions")
       ("special-rhythmic-concerns#index-grace" "Grace notes" "special-rhythmic-concerns#grace-notes"))
    ("grace notes" ("special-rhythmic-concerns#index-grace-notes-1" "See also" "special-rhythmic-concerns#See-also-64"))
    ("grand staff" ("displaying-staves#index-grand-staff-1" "See also" "displaying-staves#See-also-57"))
    ("grid-line-interface" ("outside-the-staff#index-grid_002dline_002dinterface" "See also" "outside-the-staff#See-also-184"))
    ("grid-point-interface" ("outside-the-staff#index-grid_002dpoint_002dinterface" "See also" "outside-the-staff#See-also-184"))
    ("gridInterval" ("outside-the-staff#index-gridInterval" "Grid lines" "outside-the-staff#grid-lines"))
    ("grob-interface" ("technical-glossary#index-grob_002dinterface-4" "See also" "technical-glossary#See-also-90")
       ("technical-glossary#index-grob_002dinterface-2" "See also" "technical-glossary#See-also-231")
       ("layout-interfaces#index-grob_002dinterface-1" "5.2.2 Layout interfaces" "layout-interfaces")
       ("layout-interfaces#index-grob_002dinterface" "5.2.2 Layout interfaces" "layout-interfaces"))
    ("grow-direction" ("beams#index-grow_002ddirection" "Feathered beams" "beams#feathered-beams"))
    ("hairpin" ("expressive-marks-attached-to-notes#index-hairpin-1" "See also" "expressive-marks-attached-to-notes#See-also-157"))
    ("halign" ("formatting-text#index-halign" "Text alignment" "formatting-text#text-alignment"))
    ("harmonicByFret" ("available-music-functions#index-harmonicByFret" "A.16 Available music functions" "available-music-functions"))
    ("harmonicByRatio" ("available-music-functions#index-harmonicByRatio" "A.16 Available music functions" "available-music-functions"))
    ("harmonicNote" ("available-music-functions#index-harmonicNote" "A.16 Available music functions" "available-music-functions"))
    ("harmonics" ("common-notation-for-unfretted-strings#index-harmonics" "See also" "common-notation-for-unfretted-strings#See-also-123"))
    ("harmonicsOn" ("available-music-functions#index-harmonicsOn" "A.16 Available music functions" "available-music-functions"))
    ("hbracket" ("formatting-text#index-hbracket" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("hideKeySignature" ("bagpipes#index-hideKeySignature" "Bagpipe definitions" "bagpipes#bagpipe-definitions"))
    ("hideNotes" ("inside-the-staff#index-hideNotes" "Hidden notes" "inside-the-staff#hidden-notes"))
    ("hideStaffSwitch" ("common-notation-for-keyboards#index-hideStaffSwitch" "Staff-change lines" "common-notation-for-keyboards#staff_002dchange-lines"))
    ("horizontal-bracket-interface" ("outside-the-staff#index-horizontal_002dbracket_002dinterface" "See also" "outside-the-staff#See-also-239"))
    ("huge" ("formatting-text#index-huge-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-huge" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("improvisationOff" ("displaying-rhythms#index-improvisationOff-2" "Showing melody rhythms" "displaying-rhythms#showing-melody-rhythms")
       ("note-heads#index-improvisationOff" "Improvisation" "note-heads#improvisation"))
    ("improvisationOn" ("displaying-rhythms#index-improvisationOn-2" "Showing melody rhythms" "displaying-rhythms#showing-melody-rhythms")
       ("note-heads#index-improvisationOn" "Improvisation" "note-heads#improvisation"))
    ("indent" ("line-length#index-indent-4" "4.5.4 Line length" "line-length")
       ("writing-parts#index-indent" "Instrument names" "writing-parts#instrument-names"))
    ("instrument-specific-markup-interface" ("other#index-instrument_002dspecific_002dmarkup_002dinterface-1" "A.9.6 Other" "other")
       ("woodwind-diagrams#index-instrument_002dspecific_002dmarkup_002dinterface" "See also" "woodwind-diagrams#See-also-250"))
    ("instrumentSwitch" ("available-music-functions#index-instrumentSwitch-2" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-instrumentSwitch" "Instrument names" "writing-parts#instrument-names"))
    ("interval" ("writing-pitches#index-interval" "See also" "writing-pitches#See-also-171"))
    ("inversion" ("available-music-functions#index-inversion-1" "A.16 Available music functions" "available-music-functions"))
    ("ionian" ("displaying-pitches#index-ionian" "Key signature" "displaying-pitches#key-signature"))
    ("iraq" ("arabic-music#index-iraq" "See also" "arabic-music#See-also-142"))
    ("italic" ("formatting-text#index-italic" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("item-interface" ("layout-interfaces#index-item_002dinterface" "5.2.2 Layout interfaces" "layout-interfaces"))
    ("justified-lines" ("formatting-text#index-justified_002dlines" "Multi-page markup" "formatting-text#multi_002dpage-markup"))
    ("justify" ("formatting-text#index-justify" "Text alignment" "formatting-text#text-alignment"))
    ("keepWithTag" ("available-music-functions#index-keepWithTag" "A.16 Available music functions" "available-music-functions"))
    ("key" ("note-heads#index-key-2" "Shape note heads" "note-heads#shape-note-heads")
       ("displaying-pitches#index-key" "Key signature" "displaying-pitches#key-signature"))
    ("key signature" ("typesetting-gregorian-chant#index-key-signature-5" "See also" "typesetting-gregorian-chant#See-also-136")
       ("typesetting-mensural-music#index-key-signature-3" "See also" "typesetting-mensural-music#See-also-256"))
    ("key-cancellation-interface" ("displaying-pitches#index-key_002dcancellation_002dinterface" "See also" "displaying-pitches#See-also-151"))
    ("key-signature-interface" ("displaying-pitches#index-key_002dsignature_002dinterface" "See also" "displaying-pitches#See-also-151"))
    ("killCues" ("available-music-functions#index-killCues-2" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-killCues" "Formatting cue notes" "writing-parts#formatting-cue-notes"))
    ("kurd" ("arabic-music#index-kurd" "See also" "arabic-music#See-also-142"))
    ("label" ("available-music-functions#index-label" "A.16 Available music functions" "available-music-functions"))
    ("laissez vibrer" ("writing-rhythms#index-laissez-vibrer-1" "See also" "writing-rhythms#See-also-223"))
    ("laissezVibrer" ("writing-rhythms#index-laissezVibrer" "Ties" "writing-rhythms#ties"))
    ("language" ("available-music-functions#index-language" "A.16 Available music functions" "available-music-functions"))
    ("languageRestore" ("available-music-functions#index-languageRestore" "A.16 Available music functions" "available-music-functions"))
    ("languageSaveAndChange" ("available-music-functions#index-languageSaveAndChange" "A.16 Available music functions" "available-music-functions"))
    ("large" ("formatting-text#index-large-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-large" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("larger" ("formatting-text#index-larger-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("formatting-text#index-larger" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("layout file" ("setting-the-staff-size#index-layout-file" "4.2.2 Setting the staff size" "setting-the-staff-size"))
    ("ledger line" ("modifying-single-staves#index-ledger-line" "See also" "modifying-single-staves#See-also-146"))
    ("ledger-line-spanner-interface" ("note-heads#index-ledger_002dline_002dspanner_002dinterface" "See also" "note-heads#See-also-235"))
    ("left-align" ("formatting-text#index-left_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("length" ("common-notation-for-keyboards#index-length" "Cross-staff stems" "common-notation-for-keyboards#cross_002dstaff-stems"))
    ("ligature" ("typesetting-gregorian-chant#index-ligature-4" "See also" "typesetting-gregorian-chant#See-also-195")
       ("typesetting-gregorian-chant#index-ligature-3" "See also" "typesetting-gregorian-chant#See-also-218")
       ("typesetting-mensural-music#index-ligature-2" "See also" "typesetting-mensural-music#See-also-5")
       ("ancient-notation_002d_002dcommon-features#index-ligature-1" "See also" "ancient-notation_002d_002dcommon-features#See-also-84")
       ("ancient-notation#index-ligature" "See also" "ancient-notation#See-also-178"))
    ("line" ("modifying-single-staves#index-line" "See also" "modifying-single-staves#See-also-146"))
    ("line-spanner-interface" ("spanners#index-line_002dspanner_002dinterface" "See also" "spanners#See-also-208"))
    ("line-width" ("line-length#index-line_002dwidth-2" "4.5.4 Line length" "line-length"))
    ("locrian" ("displaying-pitches#index-locrian" "Key signature" "displaying-pitches#key-signature"))
    ("longa" ("writing-rests#index-longa-3" "Rests" "writing-rests#rests")
       ("writing-rhythms#index-longa" "Durations" "writing-rhythms#durations"))
    ("lower" ("formatting-text#index-lower" "Text alignment" "formatting-text#text-alignment"))
    ("ly:add-context-mod" ("scheme-functions#index-ly_003aadd_002dcontext_002dmod" "A.18 Scheme functions" "scheme-functions"))
    ("ly:add-file-name-alist" ("scheme-functions#index-ly_003aadd_002dfile_002dname_002dalist" "A.18 Scheme functions" "scheme-functions"))
    ("ly:add-interface" ("scheme-functions#index-ly_003aadd_002dinterface" "A.18 Scheme functions" "scheme-functions"))
    ("ly:add-listener" ("scheme-functions#index-ly_003aadd_002dlistener" "A.18 Scheme functions" "scheme-functions"))
    ("ly:add-option" ("scheme-functions#index-ly_003aadd_002doption" "A.18 Scheme functions" "scheme-functions"))
    ("ly:all-grob-interfaces" ("scheme-functions#index-ly_003aall_002dgrob_002dinterfaces" "A.18 Scheme functions" "scheme-functions"))
    ("ly:all-options" ("scheme-functions#index-ly_003aall_002doptions" "A.18 Scheme functions" "scheme-functions"))
    ("ly:all-stencil-expressions" ("scheme-functions#index-ly_003aall_002dstencil_002dexpressions" "A.18 Scheme functions" "scheme-functions"))
    ("ly:assoc-get" ("scheme-functions#index-ly_003aassoc_002dget" "A.18 Scheme functions" "scheme-functions"))
    ("ly:axis-group-interface::add-element" ("scheme-functions#index-ly_003aaxis_002dgroup_002dinterface_003a_003aadd_002delement" "A.18 Scheme functions" "scheme-functions"))
    ("ly:beam-score-count" ("scheme-functions#index-ly_003abeam_002dscore_002dcount" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-add-bookpart!" ("scheme-functions#index-ly_003abook_002dadd_002dbookpart_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-add-score!" ("scheme-functions#index-ly_003abook_002dadd_002dscore_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-book-parts" ("scheme-functions#index-ly_003abook_002dbook_002dparts" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-header" ("scheme-functions#index-ly_003abook_002dheader" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-paper" ("scheme-functions#index-ly_003abook_002dpaper" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-process" ("scheme-functions#index-ly_003abook_002dprocess" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-process-to-systems" ("scheme-functions#index-ly_003abook_002dprocess_002dto_002dsystems" "A.18 Scheme functions" "scheme-functions"))
    ("ly:book-scores" ("scheme-functions#index-ly_003abook_002dscores" "A.18 Scheme functions" "scheme-functions"))
    ("ly:box?" ("scheme-functions#index-ly_003abox_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:bp" ("scheme-functions#index-ly_003abp" "A.18 Scheme functions" "scheme-functions"))
    ("ly:bracket" ("scheme-functions#index-ly_003abracket" "A.18 Scheme functions" "scheme-functions"))
    ("ly:broadcast" ("scheme-functions#index-ly_003abroadcast" "A.18 Scheme functions" "scheme-functions"))
    ("ly:camel-case->lisp-identifier" ("scheme-functions#index-ly_003acamel_002dcase_002d_003elisp_002didentifier" "A.18 Scheme functions" "scheme-functions"))
    ("ly:chain-assoc-get" ("scheme-functions#index-ly_003achain_002dassoc_002dget" "A.18 Scheme functions" "scheme-functions"))
    ("ly:cm" ("scheme-functions#index-ly_003acm" "A.18 Scheme functions" "scheme-functions"))
    ("ly:command-line-code" ("scheme-functions#index-ly_003acommand_002dline_002dcode" "A.18 Scheme functions" "scheme-functions"))
    ("ly:command-line-options" ("scheme-functions#index-ly_003acommand_002dline_002doptions" "A.18 Scheme functions" "scheme-functions"))
    ("ly:command-line-verbose?" ("scheme-functions#index-ly_003acommand_002dline_002dverbose_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:connect-dispatchers" ("scheme-functions#index-ly_003aconnect_002ddispatchers" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-current-moment" ("scheme-functions#index-ly_003acontext_002dcurrent_002dmoment" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-event-source" ("scheme-functions#index-ly_003acontext_002devent_002dsource" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-events-below" ("scheme-functions#index-ly_003acontext_002devents_002dbelow" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-find" ("scheme-functions#index-ly_003acontext_002dfind" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-grob-definition" ("scheme-functions#index-ly_003acontext_002dgrob_002ddefinition" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-id" ("scheme-functions#index-ly_003acontext_002did" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-name" ("scheme-functions#index-ly_003acontext_002dname" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-now" ("scheme-functions#index-ly_003acontext_002dnow" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-parent" ("scheme-functions#index-ly_003acontext_002dparent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-property" ("scheme-functions#index-ly_003acontext_002dproperty" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-property-where-defined" ("scheme-functions#index-ly_003acontext_002dproperty_002dwhere_002ddefined" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-pushpop-property" ("scheme-functions#index-ly_003acontext_002dpushpop_002dproperty" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-set-property!" ("scheme-functions#index-ly_003acontext_002dset_002dproperty_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context-unset-property" ("scheme-functions#index-ly_003acontext_002dunset_002dproperty" "A.18 Scheme functions" "scheme-functions"))
    ("ly:context?" ("scheme-functions#index-ly_003acontext_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:default-scale" ("scheme-functions#index-ly_003adefault_002dscale" "A.18 Scheme functions" "scheme-functions"))
    ("ly:dimension?" ("scheme-functions#index-ly_003adimension_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:dir?" ("scheme-functions#index-ly_003adir_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:dispatcher?" ("scheme-functions#index-ly_003adispatcher_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:duration->string" ("scheme-functions#index-ly_003aduration_002d_003estring" "A.18 Scheme functions" "scheme-functions"))
    ("ly:duration-dot-count" ("scheme-functions#index-ly_003aduration_002ddot_002dcount" "A.18 Scheme functions" "scheme-functions"))
    ("ly:duration-factor" ("scheme-functions#index-ly_003aduration_002dfactor" "A.18 Scheme functions" "scheme-functions"))
    ("ly:duration-length" ("scheme-functions#index-ly_003aduration_002dlength" "A.18 Scheme functions" "scheme-functions"))
    ("ly:duration-log" ("scheme-functions#index-ly_003aduration_002dlog" "A.18 Scheme functions" "scheme-functions"))
    ("ly:duration<?" ("scheme-functions#index-ly_003aduration_003c_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:duration?" ("scheme-functions#index-ly_003aduration_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:effective-prefix" ("scheme-functions#index-ly_003aeffective_002dprefix" "A.18 Scheme functions" "scheme-functions"))
    ("ly:encode-string-for-pdf" ("scheme-functions#index-ly_003aencode_002dstring_002dfor_002dpdf" "A.18 Scheme functions" "scheme-functions"))
    ("ly:engraver-announce-end-grob" ("scheme-functions#index-ly_003aengraver_002dannounce_002dend_002dgrob" "A.18 Scheme functions" "scheme-functions"))
    ("ly:engraver-make-grob" ("scheme-functions#index-ly_003aengraver_002dmake_002dgrob" "A.18 Scheme functions" "scheme-functions"))
    ("ly:error" ("scheme-functions#index-ly_003aerror" "A.18 Scheme functions" "scheme-functions"))
    ("ly:eval-simple-closure" ("scheme-functions#index-ly_003aeval_002dsimple_002dclosure" "A.18 Scheme functions" "scheme-functions"))
    ("ly:event-deep-copy" ("scheme-functions#index-ly_003aevent_002ddeep_002dcopy" "A.18 Scheme functions" "scheme-functions"))
    ("ly:event-property" ("scheme-functions#index-ly_003aevent_002dproperty" "A.18 Scheme functions" "scheme-functions"))
    ("ly:event-set-property!" ("scheme-functions#index-ly_003aevent_002dset_002dproperty_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:expand-environment" ("scheme-functions#index-ly_003aexpand_002denvironment" "A.18 Scheme functions" "scheme-functions"))
    ("ly:export" ("scheme-functions#index-ly_003aexport" "A.18 Scheme functions" "scheme-functions"))
    ("ly:find-file" ("scheme-functions#index-ly_003afind_002dfile" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-config-add-directory" ("scheme-functions#index-ly_003afont_002dconfig_002dadd_002ddirectory" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-config-add-font" ("scheme-functions#index-ly_003afont_002dconfig_002dadd_002dfont" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-config-display-fonts" ("scheme-functions#index-ly_003afont_002dconfig_002ddisplay_002dfonts" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-config-get-font-file" ("scheme-functions#index-ly_003afont_002dconfig_002dget_002dfont_002dfile" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-design-size" ("scheme-functions#index-ly_003afont_002ddesign_002dsize" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-file-name" ("scheme-functions#index-ly_003afont_002dfile_002dname" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-get-glyph" ("scheme-functions#index-ly_003afont_002dget_002dglyph" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-glyph-name-to-charcode" ("scheme-functions#index-ly_003afont_002dglyph_002dname_002dto_002dcharcode" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-glyph-name-to-index" ("scheme-functions#index-ly_003afont_002dglyph_002dname_002dto_002dindex" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-index-to-charcode" ("scheme-functions#index-ly_003afont_002dindex_002dto_002dcharcode" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-magnification" ("scheme-functions#index-ly_003afont_002dmagnification" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-metric?" ("scheme-functions#index-ly_003afont_002dmetric_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-name" ("scheme-functions#index-ly_003afont_002dname" "A.18 Scheme functions" "scheme-functions"))
    ("ly:font-sub-fonts" ("scheme-functions#index-ly_003afont_002dsub_002dfonts" "A.18 Scheme functions" "scheme-functions"))
    ("ly:format" ("scheme-functions#index-ly_003aformat" "A.18 Scheme functions" "scheme-functions"))
    ("ly:format-output" ("scheme-functions#index-ly_003aformat_002doutput" "A.18 Scheme functions" "scheme-functions"))
    ("ly:get-all-function-documentation" ("scheme-functions#index-ly_003aget_002dall_002dfunction_002ddocumentation" "A.18 Scheme functions" "scheme-functions"))
    ("ly:get-all-translators" ("scheme-functions#index-ly_003aget_002dall_002dtranslators" "A.18 Scheme functions" "scheme-functions"))
    ("ly:get-context-mods" ("scheme-functions#index-ly_003aget_002dcontext_002dmods" "A.18 Scheme functions" "scheme-functions"))
    ("ly:get-listened-event-classes" ("scheme-functions#index-ly_003aget_002dlistened_002devent_002dclasses" "A.18 Scheme functions" "scheme-functions"))
    ("ly:get-option" ("scheme-functions#index-ly_003aget_002doption" "A.18 Scheme functions" "scheme-functions"))
    ("ly:gettext" ("scheme-functions#index-ly_003agettext" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-alist-chain" ("scheme-functions#index-ly_003agrob_002dalist_002dchain" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-array->list" ("scheme-functions#index-ly_003agrob_002darray_002d_003elist" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-array-length" ("scheme-functions#index-ly_003agrob_002darray_002dlength" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-array-ref" ("scheme-functions#index-ly_003agrob_002darray_002dref" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-array?" ("scheme-functions#index-ly_003agrob_002darray_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-basic-properties" ("scheme-functions#index-ly_003agrob_002dbasic_002dproperties" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-chain-callback" ("scheme-functions#index-ly_003agrob_002dchain_002dcallback" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-common-refpoint" ("scheme-functions#index-ly_003agrob_002dcommon_002drefpoint" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-common-refpoint-of-array" ("scheme-functions#index-ly_003agrob_002dcommon_002drefpoint_002dof_002darray" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-default-font" ("scheme-functions#index-ly_003agrob_002ddefault_002dfont" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-extent" ("scheme-functions#index-ly_003agrob_002dextent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-interfaces" ("scheme-functions#index-ly_003agrob_002dinterfaces" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-layout" ("scheme-functions#index-ly_003agrob_002dlayout" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-object" ("scheme-functions#index-ly_003agrob_002dobject" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-original" ("scheme-functions#index-ly_003agrob_002doriginal" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-parent" ("scheme-functions#index-ly_003agrob_002dparent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-pq<?" ("scheme-functions#index-ly_003agrob_002dpq_003c_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-properties" ("scheme-functions#index-ly_003agrob_002dproperties" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-property" ("scheme-functions#index-ly_003agrob_002dproperty" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-property-data" ("scheme-functions#index-ly_003agrob_002dproperty_002ddata" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-relative-coordinate" ("scheme-functions#index-ly_003agrob_002drelative_002dcoordinate" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-robust-relative-extent" ("scheme-functions#index-ly_003agrob_002drobust_002drelative_002dextent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-script-priority-less" ("scheme-functions#index-ly_003agrob_002dscript_002dpriority_002dless" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-set-nested-property!" ("scheme-functions#index-ly_003agrob_002dset_002dnested_002dproperty_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-set-object!" ("scheme-functions#index-ly_003agrob_002dset_002dobject_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-set-parent!" ("scheme-functions#index-ly_003agrob_002dset_002dparent_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-set-property!" ("scheme-functions#index-ly_003agrob_002dset_002dproperty_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-staff-position" ("scheme-functions#index-ly_003agrob_002dstaff_002dposition" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-suicide!" ("scheme-functions#index-ly_003agrob_002dsuicide_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-system" ("scheme-functions#index-ly_003agrob_002dsystem" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob-translate-axis!" ("scheme-functions#index-ly_003agrob_002dtranslate_002daxis_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:grob?" ("scheme-functions#index-ly_003agrob_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:gulp-file" ("scheme-functions#index-ly_003agulp_002dfile" "A.18 Scheme functions" "scheme-functions"))
    ("ly:hash-table-keys" ("scheme-functions#index-ly_003ahash_002dtable_002dkeys" "A.18 Scheme functions" "scheme-functions"))
    ("ly:inch" ("scheme-functions#index-ly_003ainch" "A.18 Scheme functions" "scheme-functions"))
    ("ly:input-both-locations" ("scheme-functions#index-ly_003ainput_002dboth_002dlocations" "A.18 Scheme functions" "scheme-functions"))
    ("ly:input-file-line-char-column" ("scheme-functions#index-ly_003ainput_002dfile_002dline_002dchar_002dcolumn" "A.18 Scheme functions" "scheme-functions"))
    ("ly:input-location?" ("scheme-functions#index-ly_003ainput_002dlocation_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:input-message" ("scheme-functions#index-ly_003ainput_002dmessage" "A.18 Scheme functions" "scheme-functions"))
    ("ly:interpret-music-expression" ("scheme-functions#index-ly_003ainterpret_002dmusic_002dexpression" "A.18 Scheme functions" "scheme-functions"))
    ("ly:interpret-stencil-expression" ("scheme-functions#index-ly_003ainterpret_002dstencil_002dexpression" "A.18 Scheme functions" "scheme-functions"))
    ("ly:intlog2" ("scheme-functions#index-ly_003aintlog2" "A.18 Scheme functions" "scheme-functions"))
    ("ly:is-listened-event-class" ("scheme-functions#index-ly_003ais_002dlistened_002devent_002dclass" "A.18 Scheme functions" "scheme-functions"))
    ("ly:item-break-dir" ("scheme-functions#index-ly_003aitem_002dbreak_002ddir" "A.18 Scheme functions" "scheme-functions"))
    ("ly:item?" ("scheme-functions#index-ly_003aitem_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:iterator?" ("scheme-functions#index-ly_003aiterator_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:lexer-keywords" ("scheme-functions#index-ly_003alexer_002dkeywords" "A.18 Scheme functions" "scheme-functions"))
    ("ly:lily-lexer?" ("scheme-functions#index-ly_003alily_002dlexer_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:lily-parser?" ("scheme-functions#index-ly_003alily_002dparser_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:listener?" ("scheme-functions#index-ly_003alistener_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-book" ("scheme-functions#index-ly_003amake_002dbook" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-book-part" ("scheme-functions#index-ly_003amake_002dbook_002dpart" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-dispatcher" ("scheme-functions#index-ly_003amake_002ddispatcher" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-duration" ("scheme-functions#index-ly_003amake_002dduration" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-global-context" ("scheme-functions#index-ly_003amake_002dglobal_002dcontext" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-global-translator" ("scheme-functions#index-ly_003amake_002dglobal_002dtranslator" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-listener" ("scheme-functions#index-ly_003amake_002dlistener" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-moment" ("scheme-functions#index-ly_003amake_002dmoment" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-music" ("scheme-functions#index-ly_003amake_002dmusic" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-music-function" ("scheme-functions#index-ly_003amake_002dmusic_002dfunction" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-output-def" ("scheme-functions#index-ly_003amake_002doutput_002ddef" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-page-label-marker" ("scheme-functions#index-ly_003amake_002dpage_002dlabel_002dmarker" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-page-permission-marker" ("scheme-functions#index-ly_003amake_002dpage_002dpermission_002dmarker" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-pango-description-string" ("scheme-functions#index-ly_003amake_002dpango_002ddescription_002dstring" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-paper-outputter" ("scheme-functions#index-ly_003amake_002dpaper_002doutputter" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-pitch" ("scheme-functions#index-ly_003amake_002dpitch" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-prob" ("scheme-functions#index-ly_003amake_002dprob" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-scale" ("scheme-functions#index-ly_003amake_002dscale" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-score" ("scheme-functions#index-ly_003amake_002dscore" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-simple-closure" ("scheme-functions#index-ly_003amake_002dsimple_002dclosure" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-stencil" ("scheme-functions#index-ly_003amake_002dstencil" "A.18 Scheme functions" "scheme-functions"))
    ("ly:make-stream-event" ("scheme-functions#index-ly_003amake_002dstream_002devent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:message" ("scheme-functions#index-ly_003amessage" "A.18 Scheme functions" "scheme-functions"))
    ("ly:minimal-breaking" ("scheme-functions#index-ly_003aminimal_002dbreaking-2" "A.18 Scheme functions" "scheme-functions")
       ("minimal-page-breaking#index-ly_003aminimal_002dbreaking" "4.3.5 Minimal page breaking" "minimal-page-breaking"))
    ("ly:mm" ("scheme-functions#index-ly_003amm" "A.18 Scheme functions" "scheme-functions"))
    ("ly:module->alist" ("scheme-functions#index-ly_003amodule_002d_003ealist" "A.18 Scheme functions" "scheme-functions"))
    ("ly:module-copy" ("scheme-functions#index-ly_003amodule_002dcopy" "A.18 Scheme functions" "scheme-functions"))
    ("ly:modules-lookup" ("scheme-functions#index-ly_003amodules_002dlookup" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-add" ("scheme-functions#index-ly_003amoment_002dadd" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-div" ("scheme-functions#index-ly_003amoment_002ddiv" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-grace-denominator" ("scheme-functions#index-ly_003amoment_002dgrace_002ddenominator" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-grace-numerator" ("scheme-functions#index-ly_003amoment_002dgrace_002dnumerator" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-main-denominator" ("scheme-functions#index-ly_003amoment_002dmain_002ddenominator" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-main-numerator" ("scheme-functions#index-ly_003amoment_002dmain_002dnumerator" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-mod" ("scheme-functions#index-ly_003amoment_002dmod" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-mul" ("scheme-functions#index-ly_003amoment_002dmul" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment-sub" ("scheme-functions#index-ly_003amoment_002dsub" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment<?" ("scheme-functions#index-ly_003amoment_003c_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:moment?" ("scheme-functions#index-ly_003amoment_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-compress" ("scheme-functions#index-ly_003amusic_002dcompress" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-deep-copy" ("scheme-functions#index-ly_003amusic_002ddeep_002dcopy" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-duration-compress" ("scheme-functions#index-ly_003amusic_002dduration_002dcompress" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-duration-length" ("scheme-functions#index-ly_003amusic_002dduration_002dlength" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-function-extract" ("scheme-functions#index-ly_003amusic_002dfunction_002dextract" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-function?" ("scheme-functions#index-ly_003amusic_002dfunction_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-length" ("scheme-functions#index-ly_003amusic_002dlength" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-list?" ("scheme-functions#index-ly_003amusic_002dlist_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-mutable-properties" ("scheme-functions#index-ly_003amusic_002dmutable_002dproperties" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-output?" ("scheme-functions#index-ly_003amusic_002doutput_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-property" ("scheme-functions#index-ly_003amusic_002dproperty" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-set-property!" ("scheme-functions#index-ly_003amusic_002dset_002dproperty_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music-transpose" ("scheme-functions#index-ly_003amusic_002dtranspose" "A.18 Scheme functions" "scheme-functions"))
    ("ly:music?" ("scheme-functions#index-ly_003amusic_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:note-column-accidentals" ("scheme-functions#index-ly_003anote_002dcolumn_002daccidentals" "A.18 Scheme functions" "scheme-functions"))
    ("ly:note-column-dot-column" ("scheme-functions#index-ly_003anote_002dcolumn_002ddot_002dcolumn" "A.18 Scheme functions" "scheme-functions"))
    ("ly:note-head::stem-attachment" ("scheme-functions#index-ly_003anote_002dhead_003a_003astem_002dattachment" "A.18 Scheme functions" "scheme-functions"))
    ("ly:number->string" ("scheme-functions#index-ly_003anumber_002d_003estring" "A.18 Scheme functions" "scheme-functions"))
    ("ly:optimal-breaking" ("scheme-functions#index-ly_003aoptimal_002dbreaking-2" "A.18 Scheme functions" "scheme-functions")
       ("optimal-page-breaking#index-ly_003aoptimal_002dbreaking" "4.3.3 Optimal page breaking" "optimal-page-breaking"))
    ("ly:option-usage" ("scheme-functions#index-ly_003aoption_002dusage" "A.18 Scheme functions" "scheme-functions"))
    ("ly:otf->cff" ("scheme-functions#index-ly_003aotf_002d_003ecff" "A.18 Scheme functions" "scheme-functions"))
    ("ly:otf-font-glyph-info" ("scheme-functions#index-ly_003aotf_002dfont_002dglyph_002dinfo" "A.18 Scheme functions" "scheme-functions"))
    ("ly:otf-font-table-data" ("scheme-functions#index-ly_003aotf_002dfont_002dtable_002ddata" "A.18 Scheme functions" "scheme-functions"))
    ("ly:otf-font?" ("scheme-functions#index-ly_003aotf_002dfont_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:otf-glyph-count" ("scheme-functions#index-ly_003aotf_002dglyph_002dcount" "A.18 Scheme functions" "scheme-functions"))
    ("ly:otf-glyph-list" ("scheme-functions#index-ly_003aotf_002dglyph_002dlist" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-def-clone" ("scheme-functions#index-ly_003aoutput_002ddef_002dclone" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-def-lookup" ("scheme-functions#index-ly_003aoutput_002ddef_002dlookup" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-def-parent" ("scheme-functions#index-ly_003aoutput_002ddef_002dparent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-def-scope" ("scheme-functions#index-ly_003aoutput_002ddef_002dscope" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-def-set-variable!" ("scheme-functions#index-ly_003aoutput_002ddef_002dset_002dvariable_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-def?" ("scheme-functions#index-ly_003aoutput_002ddef_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-description" ("scheme-functions#index-ly_003aoutput_002ddescription" "A.18 Scheme functions" "scheme-functions"))
    ("ly:output-formats" ("scheme-functions#index-ly_003aoutput_002dformats" "A.18 Scheme functions" "scheme-functions"))
    ("ly:outputter-close" ("scheme-functions#index-ly_003aoutputter_002dclose" "A.18 Scheme functions" "scheme-functions"))
    ("ly:outputter-dump-stencil" ("scheme-functions#index-ly_003aoutputter_002ddump_002dstencil" "A.18 Scheme functions" "scheme-functions"))
    ("ly:outputter-dump-string" ("scheme-functions#index-ly_003aoutputter_002ddump_002dstring" "A.18 Scheme functions" "scheme-functions"))
    ("ly:outputter-module" ("scheme-functions#index-ly_003aoutputter_002dmodule" "A.18 Scheme functions" "scheme-functions"))
    ("ly:outputter-output-scheme" ("scheme-functions#index-ly_003aoutputter_002doutput_002dscheme" "A.18 Scheme functions" "scheme-functions"))
    ("ly:outputter-port" ("scheme-functions#index-ly_003aoutputter_002dport" "A.18 Scheme functions" "scheme-functions"))
    ("ly:page-marker?" ("scheme-functions#index-ly_003apage_002dmarker_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:page-turn-breaking" ("scheme-functions#index-ly_003apage_002dturn_002dbreaking-2" "A.18 Scheme functions" "scheme-functions")
       ("optimal-page-turning#index-ly_003apage_002dturn_002dbreaking" "4.3.4 Optimal page turning" "optimal-page-turning"))
    ("ly:pango-font-physical-fonts" ("scheme-functions#index-ly_003apango_002dfont_002dphysical_002dfonts" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pango-font?" ("scheme-functions#index-ly_003apango_002dfont_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-book-header" ("scheme-functions#index-ly_003apaper_002dbook_002dheader" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-book-pages" ("scheme-functions#index-ly_003apaper_002dbook_002dpages" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-book-paper" ("scheme-functions#index-ly_003apaper_002dbook_002dpaper" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-book-performances" ("scheme-functions#index-ly_003apaper_002dbook_002dperformances" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-book-scopes" ("scheme-functions#index-ly_003apaper_002dbook_002dscopes" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-book-systems" ("scheme-functions#index-ly_003apaper_002dbook_002dsystems" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-book?" ("scheme-functions#index-ly_003apaper_002dbook_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-fonts" ("scheme-functions#index-ly_003apaper_002dfonts" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-get-font" ("scheme-functions#index-ly_003apaper_002dget_002dfont" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-get-number" ("scheme-functions#index-ly_003apaper_002dget_002dnumber" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-outputscale" ("scheme-functions#index-ly_003apaper_002doutputscale" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-score-paper-systems" ("scheme-functions#index-ly_003apaper_002dscore_002dpaper_002dsystems" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-system-minimum-distance" ("scheme-functions#index-ly_003apaper_002dsystem_002dminimum_002ddistance" "A.18 Scheme functions" "scheme-functions"))
    ("ly:paper-system?" ("scheme-functions#index-ly_003apaper_002dsystem_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parse-file" ("scheme-functions#index-ly_003aparse_002dfile" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-clear-error" ("scheme-functions#index-ly_003aparser_002dclear_002derror" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-clone" ("scheme-functions#index-ly_003aparser_002dclone" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-define!" ("scheme-functions#index-ly_003aparser_002ddefine_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-error" ("scheme-functions#index-ly_003aparser_002derror" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-has-error?" ("scheme-functions#index-ly_003aparser_002dhas_002derror_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-include-string" ("scheme-functions#index-ly_003aparser_002dinclude_002dstring" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-lexer" ("scheme-functions#index-ly_003aparser_002dlexer" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-lookup" ("scheme-functions#index-ly_003aparser_002dlookup" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-output-name" ("scheme-functions#index-ly_003aparser_002doutput_002dname" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-parse-string" ("scheme-functions#index-ly_003aparser_002dparse_002dstring" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-set-note-names" ("scheme-functions#index-ly_003aparser_002dset_002dnote_002dnames" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-set-repetition-function" ("scheme-functions#index-ly_003aparser_002dset_002drepetition_002dfunction" "A.18 Scheme functions" "scheme-functions"))
    ("ly:parser-set-repetition-symbol" ("scheme-functions#index-ly_003aparser_002dset_002drepetition_002dsymbol" "A.18 Scheme functions" "scheme-functions"))
    ("ly:performance-write" ("scheme-functions#index-ly_003aperformance_002dwrite" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pfb->pfa" ("scheme-functions#index-ly_003apfb_002d_003epfa" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-alteration" ("scheme-functions#index-ly_003apitch_002dalteration" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-diff" ("scheme-functions#index-ly_003apitch_002ddiff" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-negate" ("scheme-functions#index-ly_003apitch_002dnegate" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-notename" ("scheme-functions#index-ly_003apitch_002dnotename" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-octave" ("scheme-functions#index-ly_003apitch_002doctave" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-quartertones" ("scheme-functions#index-ly_003apitch_002dquartertones" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-semitones" ("scheme-functions#index-ly_003apitch_002dsemitones" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-steps" ("scheme-functions#index-ly_003apitch_002dsteps" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch-transpose" ("scheme-functions#index-ly_003apitch_002dtranspose" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch<?" ("scheme-functions#index-ly_003apitch_003c_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pitch?" ("scheme-functions#index-ly_003apitch_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pointer-group-interface::add-grob" ("scheme-functions#index-ly_003apointer_002dgroup_002dinterface_003a_003aadd_002dgrob" "A.18 Scheme functions" "scheme-functions"))
    ("ly:position-on-line?" ("scheme-functions#index-ly_003aposition_002don_002dline_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:prob-immutable-properties" ("scheme-functions#index-ly_003aprob_002dimmutable_002dproperties" "A.18 Scheme functions" "scheme-functions"))
    ("ly:prob-mutable-properties" ("scheme-functions#index-ly_003aprob_002dmutable_002dproperties" "A.18 Scheme functions" "scheme-functions"))
    ("ly:prob-property" ("scheme-functions#index-ly_003aprob_002dproperty" "A.18 Scheme functions" "scheme-functions"))
    ("ly:prob-property?" ("scheme-functions#index-ly_003aprob_002dproperty_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:prob-set-property!" ("scheme-functions#index-ly_003aprob_002dset_002dproperty_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:prob-type?" ("scheme-functions#index-ly_003aprob_002dtype_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:prob?" ("scheme-functions#index-ly_003aprob_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:programming-error" ("scheme-functions#index-ly_003aprogramming_002derror" "A.18 Scheme functions" "scheme-functions"))
    ("ly:progress" ("scheme-functions#index-ly_003aprogress" "A.18 Scheme functions" "scheme-functions"))
    ("ly:property-lookup-stats" ("scheme-functions#index-ly_003aproperty_002dlookup_002dstats" "A.18 Scheme functions" "scheme-functions"))
    ("ly:protects" ("scheme-functions#index-ly_003aprotects" "A.18 Scheme functions" "scheme-functions"))
    ("ly:pt" ("scheme-functions#index-ly_003apt" "A.18 Scheme functions" "scheme-functions"))
    ("ly:register-stencil-expression" ("scheme-functions#index-ly_003aregister_002dstencil_002dexpression" "A.18 Scheme functions" "scheme-functions"))
    ("ly:relative-group-extent" ("scheme-functions#index-ly_003arelative_002dgroup_002dextent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:reset-all-fonts" ("scheme-functions#index-ly_003areset_002dall_002dfonts" "A.18 Scheme functions" "scheme-functions"))
    ("ly:round-filled-box" ("scheme-functions#index-ly_003around_002dfilled_002dbox" "A.18 Scheme functions" "scheme-functions"))
    ("ly:round-filled-polygon" ("scheme-functions#index-ly_003around_002dfilled_002dpolygon" "A.18 Scheme functions" "scheme-functions"))
    ("ly:run-translator" ("scheme-functions#index-ly_003arun_002dtranslator" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score-add-output-def!" ("scheme-functions#index-ly_003ascore_002dadd_002doutput_002ddef_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score-embedded-format" ("scheme-functions#index-ly_003ascore_002dembedded_002dformat" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score-error?" ("scheme-functions#index-ly_003ascore_002derror_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score-header" ("scheme-functions#index-ly_003ascore_002dheader" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score-music" ("scheme-functions#index-ly_003ascore_002dmusic" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score-output-defs" ("scheme-functions#index-ly_003ascore_002doutput_002ddefs" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score-set-header!" ("scheme-functions#index-ly_003ascore_002dset_002dheader_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:score?" ("scheme-functions#index-ly_003ascore_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:set-default-scale" ("scheme-functions#index-ly_003aset_002ddefault_002dscale" "A.18 Scheme functions" "scheme-functions"))
    ("ly:set-grob-modification-callback" ("scheme-functions#index-ly_003aset_002dgrob_002dmodification_002dcallback" "A.18 Scheme functions" "scheme-functions"))
    ("ly:set-middle-C!" ("scheme-functions#index-ly_003aset_002dmiddle_002dC_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:set-option" ("scheme-functions#index-ly_003aset_002doption" "A.18 Scheme functions" "scheme-functions"))
    ("ly:set-property-cache-callback" ("scheme-functions#index-ly_003aset_002dproperty_002dcache_002dcallback" "A.18 Scheme functions" "scheme-functions"))
    ("ly:simple-closure?" ("scheme-functions#index-ly_003asimple_002dclosure_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:skyline-pair?" ("scheme-functions#index-ly_003askyline_002dpair_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:skyline?" ("scheme-functions#index-ly_003askyline_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:slur-score-count" ("scheme-functions#index-ly_003aslur_002dscore_002dcount" "A.18 Scheme functions" "scheme-functions"))
    ("ly:smob-protects" ("scheme-functions#index-ly_003asmob_002dprotects" "A.18 Scheme functions" "scheme-functions"))
    ("ly:solve-spring-rod-problem" ("scheme-functions#index-ly_003asolve_002dspring_002drod_002dproblem" "A.18 Scheme functions" "scheme-functions"))
    ("ly:source-file?" ("scheme-functions#index-ly_003asource_002dfile_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:spanner-bound" ("scheme-functions#index-ly_003aspanner_002dbound" "A.18 Scheme functions" "scheme-functions"))
    ("ly:spanner-broken-into" ("scheme-functions#index-ly_003aspanner_002dbroken_002dinto" "A.18 Scheme functions" "scheme-functions"))
    ("ly:spanner-set-bound!" ("scheme-functions#index-ly_003aspanner_002dset_002dbound_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:spanner?" ("scheme-functions#index-ly_003aspanner_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:spawn" ("scheme-functions#index-ly_003aspawn" "A.18 Scheme functions" "scheme-functions"))
    ("ly:staff-symbol-line-thickness" ("scheme-functions#index-ly_003astaff_002dsymbol_002dline_002dthickness" "A.18 Scheme functions" "scheme-functions"))
    ("ly:staff-symbol-staff-space" ("scheme-functions#index-ly_003astaff_002dsymbol_002dstaff_002dspace" "A.18 Scheme functions" "scheme-functions"))
    ("ly:start-environment" ("scheme-functions#index-ly_003astart_002denvironment" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stderr-redirect" ("scheme-functions#index-ly_003astderr_002dredirect" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-add" ("scheme-functions#index-ly_003astencil_002dadd" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-aligned-to" ("scheme-functions#index-ly_003astencil_002daligned_002dto" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-combine-at-edge" ("scheme-functions#index-ly_003astencil_002dcombine_002dat_002dedge" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-empty?" ("scheme-functions#index-ly_003astencil_002dempty_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-expr" ("scheme-functions#index-ly_003astencil_002dexpr" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-extent" ("scheme-functions#index-ly_003astencil_002dextent" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-fonts" ("scheme-functions#index-ly_003astencil_002dfonts" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-in-color" ("scheme-functions#index-ly_003astencil_002din_002dcolor" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-rotate" ("scheme-functions#index-ly_003astencil_002drotate" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-rotate-absolute" ("scheme-functions#index-ly_003astencil_002drotate_002dabsolute" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-scale" ("scheme-functions#index-ly_003astencil_002dscale" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-translate" ("scheme-functions#index-ly_003astencil_002dtranslate" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil-translate-axis" ("scheme-functions#index-ly_003astencil_002dtranslate_002daxis" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stencil?" ("scheme-functions#index-ly_003astencil_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:stream-event?" ("scheme-functions#index-ly_003astream_002devent_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:string-percent-encode" ("scheme-functions#index-ly_003astring_002dpercent_002dencode" "A.18 Scheme functions" "scheme-functions"))
    ("ly:string-substitute" ("scheme-functions#index-ly_003astring_002dsubstitute" "A.18 Scheme functions" "scheme-functions"))
    ("ly:success" ("scheme-functions#index-ly_003asuccess" "A.18 Scheme functions" "scheme-functions"))
    ("ly:system-font-load" ("scheme-functions#index-ly_003asystem_002dfont_002dload" "A.18 Scheme functions" "scheme-functions"))
    ("ly:text-interface::interpret-markup" ("scheme-functions#index-ly_003atext_002dinterface_003a_003ainterpret_002dmarkup" "A.18 Scheme functions" "scheme-functions"))
    ("ly:translator-context" ("scheme-functions#index-ly_003atranslator_002dcontext" "A.18 Scheme functions" "scheme-functions"))
    ("ly:translator-description" ("scheme-functions#index-ly_003atranslator_002ddescription" "A.18 Scheme functions" "scheme-functions"))
    ("ly:translator-group?" ("scheme-functions#index-ly_003atranslator_002dgroup_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:translator-name" ("scheme-functions#index-ly_003atranslator_002dname" "A.18 Scheme functions" "scheme-functions"))
    ("ly:translator?" ("scheme-functions#index-ly_003atranslator_003f" "A.18 Scheme functions" "scheme-functions"))
    ("ly:transpose-key-alist" ("scheme-functions#index-ly_003atranspose_002dkey_002dalist" "A.18 Scheme functions" "scheme-functions"))
    ("ly:truncate-list!" ("scheme-functions#index-ly_003atruncate_002dlist_0021" "A.18 Scheme functions" "scheme-functions"))
    ("ly:ttf->pfa" ("scheme-functions#index-ly_003attf_002d_003epfa" "A.18 Scheme functions" "scheme-functions"))
    ("ly:ttf-ps-name" ("scheme-functions#index-ly_003attf_002dps_002dname" "A.18 Scheme functions" "scheme-functions"))
    ("ly:unit" ("scheme-functions#index-ly_003aunit" "A.18 Scheme functions" "scheme-functions"))
    ("ly:usage" ("scheme-functions#index-ly_003ausage" "A.18 Scheme functions" "scheme-functions"))
    ("ly:version" ("scheme-functions#index-ly_003aversion" "A.18 Scheme functions" "scheme-functions"))
    ("ly:warning" ("scheme-functions#index-ly_003awarning" "A.18 Scheme functions" "scheme-functions"))
    ("ly:wide-char->utf-8" ("scheme-functions#index-ly_003awide_002dchar_002d_003eutf_002d8" "A.18 Scheme functions" "scheme-functions"))
    ("lydian" ("displaying-pitches#index-lydian" "Key signature" "displaying-pitches#key-signature"))
    ("m" ("chord-mode#index-m" "Common chords" "chord-mode#common-chords"))
    ("magnify" ("formatting-text#index-magnify" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("magstep" ("distances-and-measurements#index-magstep-2" "5.4.4 Distances and measurements" "distances-and-measurements")
       ("inside-the-staff#index-magstep" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("maj" ("chord-mode#index-maj" "Common chords" "chord-mode#common-chords"))
    ("major" ("displaying-pitches#index-major" "Key signature" "displaying-pitches#key-signature"))
    ("major seven symbols" ("displaying-chords#index-major-seven-symbols" "Predefined commands" "displaying-chords#Predefined-commands-23"))
    ("majorSevenSymbol" ("displaying-chords#index-majorSevenSymbol" "Customizing chord names" "displaying-chords#customizing-chord-names"))
    ("makam" ("turkish-classical-music#index-makam-2" "See also" "turkish-classical-music#See-also-110"))
    ("makamlar" ("turkish-classical-music#index-makamlar-3" "See also" "turkish-classical-music#See-also-110")
       ("common-notation-for-non_002dwestern-music#index-makamlar" "See also" "common-notation-for-non_002dwestern-music#See-also-200"))
    ("make-dynamic-script" ("expressive-marks-attached-to-notes#index-make_002ddynamic_002dscript-1" "New dynamic marks" "expressive-marks-attached-to-notes#new-dynamic-marks"))
    ("make-pango-font-tree" ("fonts#index-make_002dpango_002dfont_002dtree" "Entire document fonts" "fonts#entire-document-fonts"))
    ("makeClusters" ("available-music-functions#index-makeClusters-2" "A.16 Available music functions" "available-music-functions")
       ("single-voice#index-makeClusters" "Clusters" "single-voice#clusters"))
    ("makeDefaultStringTunings" ("available-music-functions#index-makeDefaultStringTunings" "A.16 Available music functions" "available-music-functions"))
    ("makeStringTuning" ("available-music-functions#index-makeStringTuning-2" "A.16 Available music functions" "available-music-functions")
       ("common-notation-for-fretted-strings#index-makeStringTuning" "Custom tablatures" "common-notation-for-fretted-strings#custom-tablatures"))
    ("maqam" ("arabic-music#index-maqam-1" "See also" "arabic-music#See-also-142"))
    ("mark" ("writing-text#index-mark-2" "Text marks" "writing-text#text-marks")
       ("bars#index-mark" "Rehearsal marks" "bars#rehearsal-marks"))
    ("markup" ("formatting-text#index-markup-7" "Text markup introduction" "formatting-text#text-markup-introduction")
       ("writing-text#index-markup-4" "Separate text" "writing-text#separate-text")
       ("writing-text#index-markup-2" "Separate text" "writing-text#separate-text")
       ("writing-text#index-markup" "Text marks" "writing-text#text-marks"))
    ("markuplines" ("formatting-text#index-markuplines-4" "See also" "formatting-text#See-also-131")
       ("formatting-text#index-markuplines-2" "Multi-page markup" "formatting-text#multi_002dpage-markup")
       ("writing-text#index-markuplines" "Separate text" "writing-text#separate-text"))
    ("maxima" ("writing-rests#index-maxima-3" "Rests" "writing-rests#rests")
       ("writing-rhythms#index-maxima" "Durations" "writing-rhythms#durations"))
    ("measureLength" ("special-rhythmic-concerns#index-measureLength-2" "Time administration" "special-rhythmic-concerns#time-administration")
       ("beams#index-measureLength" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior"))
    ("measurePosition" ("special-rhythmic-concerns#index-measurePosition-2" "Time administration" "special-rhythmic-concerns#time-administration")
       ("displaying-rhythms#index-measurePosition" "Upbeats" "displaying-rhythms#upbeats"))
    ("melisma" ("common-notation-for-vocal-music#index-melisma-2" "See also" "common-notation-for-vocal-music#See-also-98")
       ("common-notation-for-vocal-music#index-melisma-1" "Multiple notes to one syllable" "common-notation-for-vocal-music#multiple-notes-to-one-syllable"))
    ("mensural notation" ("typesetting-mensural-music#index-mensural-notation-9" "See also" "typesetting-mensural-music#See-also-256")
       ("typesetting-mensural-music#index-mensural-notation-8" "See also" "typesetting-mensural-music#See-also-248")
       ("typesetting-mensural-music#index-mensural-notation-7" "See also" "typesetting-mensural-music#See-also-190")
       ("typesetting-mensural-music#index-mensural-notation-6" "See also" "typesetting-mensural-music#See-also-74")
       ("typesetting-mensural-music#index-mensural-notation-5" "See also" "typesetting-mensural-music#See-also-156")
       ("typesetting-mensural-music#index-mensural-notation-4" "See also" "typesetting-mensural-music#See-also-19")
       ("typesetting-mensural-music#index-mensural-notation-3" "See also" "typesetting-mensural-music#See-also-170")
       ("ancient-notation_002d_002dcommon-features#index-mensural-notation-2" "See also" "ancient-notation_002d_002dcommon-features#See-also-88")
       ("overview-of-the-supported-styles#index-mensural-notation-1" "See also" "overview-of-the-supported-styles#See-also-38")
       ("ancient-notation#index-mensural-notation" "See also" "ancient-notation#See-also-178"))
    ("mergeDifferentlyDottedOff" ("multiple-voices#index-mergeDifferentlyDottedOff" "Collision resolution" "multiple-voices#collision-resolution"))
    ("mergeDifferentlyDottedOn" ("multiple-voices#index-mergeDifferentlyDottedOn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("mergeDifferentlyHeadedOff" ("multiple-voices#index-mergeDifferentlyHeadedOff" "Collision resolution" "multiple-voices#collision-resolution"))
    ("mergeDifferentlyHeadedOn" ("multiple-voices#index-mergeDifferentlyHeadedOn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("meter" ("displaying-rhythms#index-meter-1" "See also" "displaying-rhythms#See-also-4"))
    ("metronome" ("displaying-rhythms#index-metronome" "See also" "displaying-rhythms#See-also-95"))
    ("metronome mark" ("displaying-rhythms#index-metronome-mark-1" "See also" "displaying-rhythms#See-also-95"))
    ("metronomic indication" ("displaying-rhythms#index-metronomic-indication" "See also" "displaying-rhythms#See-also-95"))
    ("mf" ("expressive-marks-attached-to-notes#index-mf" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("minimum-Y-extent" ("flexible-vertical-spacing-within-systems#index-minimum_002dY_002dextent" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("minimumFret" ("common-notation-for-fretted-strings#index-minimumFret-2" "Automatic fret diagrams" "common-notation-for-fretted-strings#automatic-fret-diagrams")
       ("common-notation-for-fretted-strings#index-minimumFret" "Default tablatures" "common-notation-for-fretted-strings#default-tablatures"))
    ("minimumPageTurnLength" ("optimal-page-turning#index-minimumPageTurnLength" "4.3.4 Optimal page turning" "optimal-page-turning"))
    ("minimumRepeatLengthForPageTurn" ("optimal-page-turning#index-minimumRepeatLengthForPageTurn" "4.3.4 Optimal page turning" "optimal-page-turning"))
    ("minor" ("displaying-pitches#index-minor" "Key signature" "displaying-pitches#key-signature"))
    ("mixed" ("piano#index-mixed" "Piano pedals" "piano#piano-pedals"))
    ("mixolydian" ("displaying-pitches#index-mixolydian" "Key signature" "displaying-pitches#key-signature"))
    ("modalInversion" ("available-music-functions#index-modalInversion-2" "A.16 Available music functions" "available-music-functions")
       ("changing-multiple-pitches#index-modalInversion" "Modal inversion" "changing-multiple-pitches#Modal-inversion"))
    ("modalTranspose" ("available-music-functions#index-modalTranspose-2" "A.16 Available music functions" "available-music-functions")
       ("changing-multiple-pitches#index-modalTranspose" "Modal transposition" "changing-multiple-pitches#Modal-transposition"))
    ("modern" ("displaying-pitches#index-modern" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("modern-cautionary" ("displaying-pitches#index-modern_002dcautionary" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("modern-voice" ("displaying-pitches#index-modern_002dvoice" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("modern-voice-cautionary" ("displaying-pitches#index-modern_002dvoice_002dcautionary" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("mp" ("expressive-marks-attached-to-notes#index-mp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("multi-measure rest" ("writing-rests#index-multi_002dmeasure-rest" "See also" "writing-rests#See-also-30"))
    ("musicMap" ("available-music-functions#index-musicMap" "A.16 Available music functions" "available-music-functions"))
    ("musicglyph" ("bars#index-musicglyph" "Rehearsal marks" "bars#rehearsal-marks"))
    ("name" ("defining-new-contexts#index-name" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("neo-modern" ("displaying-pitches#index-neo_002dmodern" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("neo-modern-cautionary" ("displaying-pitches#index-neo_002dmodern_002dcautionary" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("neo-modern-voice" ("displaying-pitches#index-neo_002dmodern_002dvoice" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("neo-modern-voice-cautionary" ("displaying-pitches#index-neo_002dmodern_002dvoice_002dcautionary" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("no-reset" ("displaying-pitches#index-no_002dreset" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("noBeam" ("beams#index-noBeam" "Manual beams" "beams#manual-beams"))
    ("noPageBreak" ("available-music-functions#index-noPageBreak" "A.16 Available music functions" "available-music-functions"))
    ("noPageTurn" ("available-music-functions#index-noPageTurn" "A.16 Available music functions" "available-music-functions"))
    ("nonstaff-nonstaff-spacing" ("flexible-vertical-spacing-within-systems#index-nonstaff_002dnonstaff_002dspacing" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("nonstaff-relatedstaff-spacing" ("flexible-vertical-spacing-within-systems#index-nonstaff_002drelatedstaff_002dspacing" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("nonstaff-unrelatedstaff-spacing" ("flexible-vertical-spacing-within-systems#index-nonstaff_002dunrelatedstaff_002dspacing" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("normalsize" ("formatting-text#index-normalsize-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-normalsize" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("note head" ("typesetting-mensural-music#index-note-head" "See also" "typesetting-mensural-music#See-also-74"))
    ("note value" ("writing-rhythms#index-note-value" "See also" "writing-rhythms#See-also-96"))
    ("note-collision-interface" ("layout-properties#index-note_002dcollision_002dinterface-2" "A.15 Layout properties" "layout-properties")
       ("layout-properties#index-note_002dcollision_002dinterface-1" "A.15 Layout properties" "layout-properties")
       ("layout-properties#index-note_002dcollision_002dinterface" "A.15 Layout properties" "layout-properties"))
    ("note-event" ("note-heads#index-note_002devent-2" "See also" "note-heads#See-also-130")
       ("note-heads#index-note_002devent-1" "See also" "note-heads#See-also-202")
       ("note-heads#index-note_002devent" "See also" "note-heads#See-also-235"))
    ("note-head-interface" ("note-heads#index-note_002dhead_002dinterface-2" "See also" "note-heads#See-also-130")
       ("note-heads#index-note_002dhead_002dinterface-1" "See also" "note-heads#See-also-202")
       ("note-heads#index-note_002dhead_002dinterface" "See also" "note-heads#See-also-235"))
    ("null" ("formatting-text#index-null" "Text alignment" "formatting-text#text-alignment"))
    ("numericTimeSignature" ("displaying-rhythms#index-numericTimeSignature" "Time signature" "displaying-rhythms#time-signature"))
    ("octavation" ("displaying-pitches#index-octavation-1" "See also" "displaying-pitches#See-also-126"))
    ("octaveCheck" ("available-music-functions#index-octaveCheck-2" "A.16 Available music functions" "available-music-functions")
       ("changing-multiple-pitches#index-octaveCheck" "Octave checks" "changing-multiple-pitches#octave-checks"))
    ("oneVoice" ("multiple-voices#index-oneVoice" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("ossia" ("modifying-single-staves#index-ossia-1" "See also" "modifying-single-staves#See-also-16"))
    ("ottava" ("available-music-functions#index-ottava-3" "A.16 Available music functions" "available-music-functions")
       ("displaying-pitches#index-ottava-1" "Ottava brackets" "displaying-pitches#ottava-brackets"))
    ("ottava-bracket-interface" ("displaying-pitches#index-ottava_002dbracket_002dinterface" "See also" "displaying-pitches#See-also-126"))
    ("outside-staff-horizontal-padding" ("vertical-collision-avoidance#index-outside_002dstaff_002dhorizontal_002dpadding" "4.4.3 Vertical collision avoidance" "vertical-collision-avoidance"))
    ("outside-staff-padding" ("vertical-collision-avoidance#index-outside_002dstaff_002dpadding" "4.4.3 Vertical collision avoidance" "vertical-collision-avoidance"))
    ("outside-staff-priority" ("vertical-collision-avoidance#index-outside_002dstaff_002dpriority" "4.4.3 Vertical collision avoidance" "vertical-collision-avoidance"))
    ("overrideProperty" ("available-music-functions#index-overrideProperty" "A.16 Available music functions" "available-music-functions"))
    ("overrideTimeSignatureSettings" ("available-music-functions#index-overrideTimeSignatureSettings" "A.16 Available music functions" "available-music-functions"))
    ("p" ("expressive-marks-attached-to-notes#index-p" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("pad-around" ("formatting-text#index-pad_002daround" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("pad-markup" ("formatting-text#index-pad_002dmarkup" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("pad-to-box" ("formatting-text#index-pad_002dto_002dbox" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("pad-x" ("formatting-text#index-pad_002dx" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("pageBreak" ("available-music-functions#index-pageBreak" "A.16 Available music functions" "available-music-functions"))
    ("pageTurn" ("available-music-functions#index-pageTurn" "A.16 Available music functions" "available-music-functions"))
    ("palmMute" ("available-music-functions#index-palmMute" "A.16 Available music functions" "available-music-functions"))
    ("palmMuteOn" ("available-music-functions#index-palmMuteOn" "A.16 Available music functions" "available-music-functions"))
    ("parallelMusic" ("available-music-functions#index-parallelMusic-2" "A.16 Available music functions" "available-music-functions")
       ("multiple-voices#index-parallelMusic" "Writing music in parallel" "multiple-voices#writing-music-in-parallel"))
    ("parentheses-interface" ("inside-the-staff#index-parentheses_002dinterface" "See also" "inside-the-staff#See-also-132"))
    ("parenthesize" ("available-music-functions#index-parenthesize-2" "A.16 Available music functions" "available-music-functions")
       ("inside-the-staff#index-parenthesize" "Parentheses" "inside-the-staff#parentheses"))
    ("part" ("multiple-voices#index-part" "See also" "multiple-voices#See-also-221"))
    ("partcombine" ("available-music-functions#index-partcombine-2" "A.16 Available music functions" "available-music-functions")
       ("multiple-voices#index-partcombine" "Automatic part combining" "multiple-voices#automatic-part-combining"))
    ("partcombineForce" ("available-music-functions#index-partcombineForce" "A.16 Available music functions" "available-music-functions"))
    ("partial" ("displaying-rhythms#index-partial" "Upbeats" "displaying-rhythms#upbeats"))
    ("pedalSustainStyle" ("piano#index-pedalSustainStyle" "Piano pedals" "piano#piano-pedals"))
    ("percent" ("short-repeats#index-percent" "Percent repeats" "short-repeats#percent-repeats"))
    ("percent repeat" ("short-repeats#index-percent-repeat" "See also" "short-repeats#See-also-220"))
    ("phrasingSlurDashPattern" ("available-music-functions#index-phrasingSlurDashPattern-2" "A.16 Available music functions" "available-music-functions")
       ("expressive-marks-as-curves#index-phrasingSlurDashPattern" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurDashed" ("expressive-marks-as-curves#index-phrasingSlurDashed" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurDotted" ("expressive-marks-as-curves#index-phrasingSlurDotted" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurDown" ("expressive-marks-as-curves#index-phrasingSlurDown" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurHalfDashed" ("expressive-marks-as-curves#index-phrasingSlurHalfDashed" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurHalfSolid" ("expressive-marks-as-curves#index-phrasingSlurHalfSolid" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurNeutral" ("expressive-marks-as-curves#index-phrasingSlurNeutral" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurSolid" ("expressive-marks-as-curves#index-phrasingSlurSolid" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrasingSlurUp" ("expressive-marks-as-curves#index-phrasingSlurUp" "Phrasing slurs" "expressive-marks-as-curves#phrasing-slurs"))
    ("phrygian" ("displaying-pitches#index-phrygian" "Key signature" "displaying-pitches#key-signature"))
    ("piano" ("displaying-pitches#index-piano" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("piano-cautionary" ("displaying-pitches#index-piano_002dcautionary" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("pipeSymbol" ("bars#index-pipeSymbol" "Bar and bar number checks" "bars#bar-and-bar-number-checks"))
    ("pitchedTrill" ("available-music-functions#index-pitchedTrill-2" "A.16 Available music functions" "available-music-functions")
       ("expressive-marks-as-lines#index-pitchedTrill" "Trills" "expressive-marks-as-lines#trills"))
    ("pointAndClickOff" ("available-music-functions#index-pointAndClickOff" "A.16 Available music functions" "available-music-functions"))
    ("pointAndClickOn" ("available-music-functions#index-pointAndClickOn" "A.16 Available music functions" "available-music-functions"))
    ("polymetric" ("displaying-rhythms#index-polymetric-1" "See also" "displaying-rhythms#See-also-4")
       ("writing-rhythms#index-polymetric" "See also" "writing-rhythms#See-also-99"))
    ("polymetric time signature" ("displaying-rhythms#index-polymetric-time-signature" "See also" "displaying-rhythms#See-also-4"))
    ("polyphony" ("multiple-voices#index-polyphony" "See also" "multiple-voices#See-also-177"))
    ("portato" ("expressive-marks-attached-to-notes#index-portato-1" "See also" "expressive-marks-attached-to-notes#See-also-77"))
    ("postscript" ("formatting-text#index-postscript-1" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("power chord" ("guitar#index-power-chord" "See also" "guitar#See-also-127"))
    ("powerChords" ("guitar#index-powerChords" "Indicating power chords" "guitar#indicating-power-chords"))
    ("pp" ("expressive-marks-attached-to-notes#index-pp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("ppp" ("expressive-marks-attached-to-notes#index-ppp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("pppp" ("expressive-marks-attached-to-notes#index-pppp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("ppppp" ("expressive-marks-attached-to-notes#index-ppppp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("predefinedFretboardsOff" ("common-notation-for-fretted-strings#index-predefinedFretboardsOff" "Automatic fret diagrams" "common-notation-for-fretted-strings#automatic-fret-diagrams"))
    ("predefinedFretboardsOn" ("common-notation-for-fretted-strings#index-predefinedFretboardsOn" "Automatic fret diagrams" "common-notation-for-fretted-strings#automatic-fret-diagrams"))
    ("quarter tone" ("writing-pitches#index-quarter-tone" "See also" "writing-pitches#See-also-258"))
    ("quoteDuring" ("available-music-functions#index-quoteDuring-4" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-quoteDuring-2" "Formatting cue notes" "writing-parts#formatting-cue-notes")
       ("writing-parts#index-quoteDuring" "Quoting other voices" "writing-parts#quoting-other-voices"))
    ("quotedCueEventTypes" ("writing-parts#index-quotedCueEventTypes" "Selected Snippets" "writing-parts#Selected-Snippets-32"))
    ("quotedEventTypes" ("writing-parts#index-quotedEventTypes" "Selected Snippets" "writing-parts#Selected-Snippets-32"))
    ("r" ("writing-rests#index-r" "Rests" "writing-rests#rests"))
    ("ragged-last" ("line-length#index-ragged_002dlast-2" "4.5.4 Line length" "line-length"))
    ("ragged-right" ("line-length#index-ragged_002dright-2" "4.5.4 Line length" "line-length"))
    ("raise" ("formatting-text#index-raise" "Text alignment" "formatting-text#text-alignment"))
    ("rast" ("arabic-music#index-rast" "See also" "arabic-music#See-also-142"))
    ("relative" ("common-notation-for-keyboards#index-relative-7" "Changing staff automatically" "common-notation-for-keyboards#changing-staff-automatically")
       ("changing-multiple-pitches#index-relative-5" "See also" "changing-multiple-pitches#See-also-254")
       ("writing-pitches#index-relative-3" "See also" "writing-pitches#See-also-171")
       ("writing-pitches#index-relative-1" "Relative octave entry" "writing-pitches#relative-octave-entry"))
    ("removeWithTag" ("available-music-functions#index-removeWithTag" "A.16 Available music functions" "available-music-functions"))
    ("repeat" ("long-repeats#index-repeat" "See also" "long-repeats#See-also"))
    ("repeatCommands" ("long-repeats#index-repeatCommands" "Manual repeat marks" "long-repeats#manual-repeat-marks"))
    ("repeatTie" ("writing-rhythms#index-repeatTie" "Ties" "writing-rhythms#ties"))
    ("resetRelativeOctave" ("available-music-functions#index-resetRelativeOctave" "A.16 Available music functions" "available-music-functions"))
    ("rest" ("writing-rests#index-rest-1" "Rests" "writing-rests#rests"))
    ("retrograde" ("available-music-functions#index-retrograde-2" "A.16 Available music functions" "available-music-functions")
       ("changing-multiple-pitches#index-retrograde" "Retrograde" "changing-multiple-pitches#retrograde"))
    ("revertTimeSignatureSettings" ("available-music-functions#index-revertTimeSignatureSettings" "A.16 Available music functions" "available-music-functions"))
    ("rfz" ("expressive-marks-attached-to-notes#index-rfz" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("rgb-color" ("inside-the-staff#index-rgb_002dcolor-1" "Coloring objects" "inside-the-staff#coloring-objects"))
    ("right-align" ("formatting-text#index-right_002dalign" "Text alignment" "formatting-text#text-alignment"))
    ("rightHandFinger" ("available-music-functions#index-rightHandFinger-2" "A.16 Available music functions" "available-music-functions")
       ("common-notation-for-fretted-strings#index-rightHandFinger" "Right-hand fingerings" "common-notation-for-fretted-strings#right_002dhand-fingerings"))
    ("rounded-box" ("formatting-text#index-rounded_002dbox" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("s" ("writing-rests#index-s" "Invisible rests" "writing-rests#invisible-rests"))
    ("sacredHarpHeads" ("note-heads#index-sacredHarpHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("sacredHarpHeadsMinor" ("note-heads#index-sacredHarpHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("scaleDurations" ("available-music-functions#index-scaleDurations-4" "A.16 Available music functions" "available-music-functions")
       ("displaying-rhythms#index-scaleDurations-2" "Polymetric notation" "displaying-rhythms#polymetric-notation")
       ("writing-rhythms#index-scaleDurations" "Scaling durations" "writing-rhythms#scaling-durations"))
    ("scordatura" ("displaying-pitches#index-scordatura" "See also" "displaying-pitches#See-also-151"))
    ("self-alignment-X" ("flexible-vertical-spacing-within-systems#index-self_002dalignment_002dX" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("self-alignment-interface" ("aligning-objects#index-self_002dalignment_002dinterface-1" "5.5.1 Aligning objects" "aligning-objects")
       ("layout-interfaces#index-self_002dalignment_002dinterface" "5.2.2 Layout interfaces" "layout-interfaces"))
    ("semai" ("arabic-music#index-semai" "See also" "arabic-music#See-also-23"))
    ("set" ("beams#index-set" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior"))
    ("set-accidental-style" ("displaying-pitches#index-set_002daccidental_002dstyle" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("set-octavation" ("displaying-pitches#index-set_002doctavation" "Ottava brackets" "displaying-pitches#ottava-brackets"))
    ("sf" ("expressive-marks-attached-to-notes#index-sf" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("sff" ("expressive-marks-attached-to-notes#index-sff" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("sfz" ("expressive-marks-attached-to-notes#index-sfz" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("sharp" ("writing-pitches#index-sharp-1" "See also" "writing-pitches#See-also-258"))
    ("shiftDurations" ("available-music-functions#index-shiftDurations" "A.16 Available music functions" "available-music-functions"))
    ("shiftOff" ("multiple-voices#index-shiftOff" "Collision resolution" "multiple-voices#collision-resolution"))
    ("shiftOn" ("multiple-voices#index-shiftOn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("shiftOnn" ("multiple-voices#index-shiftOnn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("shiftOnnn" ("multiple-voices#index-shiftOnnn" "Collision resolution" "multiple-voices#collision-resolution"))
    ("short-indent" ("writing-parts#index-short_002dindent" "Instrument names" "writing-parts#instrument-names"))
    ("show-available-fonts" ("fonts#index-show_002davailable_002dfonts" "Single entry fonts" "fonts#single-entry-fonts"))
    ("showFirstLength" ("skipping-corrected-music#index-showFirstLength" "3.4.2 Skipping corrected music" "skipping-corrected-music"))
    ("showKeySignature" ("bagpipes#index-showKeySignature" "Bagpipe definitions" "bagpipes#bagpipe-definitions"))
    ("showLastLength" ("skipping-corrected-music#index-showLastLength" "3.4.2 Skipping corrected music" "skipping-corrected-music"))
    ("showStaffSwitch" ("common-notation-for-keyboards#index-showStaffSwitch" "Staff-change lines" "common-notation-for-keyboards#staff_002dchange-lines"))
    ("side-position-interface" ("aligning-objects#index-side_002dposition_002dinterface-1" "5.5.1 Aligning objects" "aligning-objects")
       ("layout-interfaces#index-side_002dposition_002dinterface" "5.2.2 Layout interfaces" "layout-interfaces"))
    ("sikah" ("arabic-music#index-sikah" "See also" "arabic-music#See-also-142"))
    ("simile" ("short-repeats#index-simile" "See also" "short-repeats#See-also-220"))
    ("skip" ("writing-rests#index-skip-1" "Invisible rests" "writing-rests#invisible-rests"))
    ("skipTypesetting" ("skipping-corrected-music#index-skipTypesetting" "3.4.2 Skipping corrected music" "skipping-corrected-music"))
    ("slur" ("expressive-marks-as-curves#index-slur" "See also" "expressive-marks-as-curves#See-also-176"))
    ("slurDashPattern" ("available-music-functions#index-slurDashPattern-2" "A.16 Available music functions" "available-music-functions")
       ("expressive-marks-as-curves#index-slurDashPattern" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurDashed" ("expressive-marks-as-curves#index-slurDashed" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurDotted" ("expressive-marks-as-curves#index-slurDotted" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurDown" ("expressive-marks-as-curves#index-slurDown" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurHalfDashed" ("expressive-marks-as-curves#index-slurHalfDashed" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurHalfSolid" ("expressive-marks-as-curves#index-slurHalfSolid" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurNeutral" ("expressive-marks-as-curves#index-slurNeutral" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurSolid" ("expressive-marks-as-curves#index-slurSolid" "Slurs" "expressive-marks-as-curves#slurs"))
    ("slurUp" ("expressive-marks-as-curves#index-slurUp" "Slurs" "expressive-marks-as-curves#slurs"))
    ("small" ("formatting-text#index-small-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-small" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("smaller" ("formatting-text#index-smaller-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("formatting-text#index-smaller" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("sostenutoOff" ("piano#index-sostenutoOff" "Piano pedals" "piano#piano-pedals"))
    ("sostenutoOn" ("piano#index-sostenutoOn" "Piano pedals" "piano#piano-pedals"))
    ("southernHarmonyHeads" ("note-heads#index-southernHarmonyHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("southernHarmonyHeadsMinor" ("note-heads#index-southernHarmonyHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("sp" ("expressive-marks-attached-to-notes#index-sp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("spacing" ("horizontal-spacing-overview#index-spacing" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview"))
    ("spacing-spanner-interface" ("layout-properties#index-spacing_002dspanner_002dinterface-1" "A.15 Layout properties" "layout-properties")
       ("layout-properties#index-spacing_002dspanner_002dinterface" "A.15 Layout properties" "layout-properties"))
    ("spacingTweaks" ("available-music-functions#index-spacingTweaks" "A.16 Available music functions" "available-music-functions"))
    ("spp" ("expressive-marks-attached-to-notes#index-spp" "Dynamics" "expressive-marks-attached-to-notes#dynamics"))
    ("staccato" ("expressive-marks-attached-to-notes#index-staccato-1" "See also" "expressive-marks-attached-to-notes#See-also-77"))
    ("staff" ("modifying-single-staves#index-staff-2" "See also" "modifying-single-staves#See-also-16")
       ("modifying-single-staves#index-staff-1" "See also" "modifying-single-staves#See-also-146")
       ("displaying-staves#index-staff" "See also" "displaying-staves#See-also-81"))
    ("staff-affinity" ("flexible-vertical-spacing-within-systems#index-staff_002daffinity" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("staff-staff-spacing" ("flexible-vertical-spacing-within-systems#index-staff_002dstaff_002dspacing" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("staff-symbol-interface" ("modifying-single-staves#index-staff_002dsymbol_002dinterface-1" "See also" "modifying-single-staves#See-also-146")
       ("modifying-single-staves#index-staff_002dsymbol_002dinterface" "Staff symbol" "modifying-single-staves#staff-symbol"))
    ("staffgroup-staff-spacing" ("flexible-vertical-spacing-within-systems#index-staffgroup_002dstaff_002dspacing" "Within-system spacing properties" "flexible-vertical-spacing-within-systems#within_002dsystem-spacing-properties"))
    ("start-repeat" ("long-repeats#index-start_002drepeat" "Manual repeat marks" "long-repeats#manual-repeat-marks"))
    ("startGroup" ("outside-the-staff#index-startGroup" "Analysis brackets" "outside-the-staff#analysis-brackets"))
    ("startStaff" ("modifying-single-staves#index-startStaff-2" "Ossia staves" "modifying-single-staves#ossia-staves")
       ("modifying-single-staves#index-startStaff" "Staff symbol" "modifying-single-staves#staff-symbol"))
    ("startTrillSpan" ("expressive-marks-as-lines#index-startTrillSpan" "Trills" "expressive-marks-as-lines#trills"))
    ("staves" ("displaying-staves#index-staves" "See also" "displaying-staves#See-also-81"))
    ("stem-interface" ("inside-the-staff#index-stem_002dinterface" "See also" "inside-the-staff#See-also-185"))
    ("stem-spacing-correction" ("horizontal-spacing-overview#index-stem_002dspacing_002dcorrection" "4.5.1 Horizontal spacing overview" "horizontal-spacing-overview"))
    ("stemDown" ("inside-the-staff#index-stemDown" "Stems" "inside-the-staff#stems"))
    ("stemLeftBeamCount" ("beams#index-stemLeftBeamCount" "Manual beams" "beams#manual-beams"))
    ("stemNeutral" ("inside-the-staff#index-stemNeutral" "Stems" "inside-the-staff#stems"))
    ("stemRightBeamCount" ("beams#index-stemRightBeamCount" "Manual beams" "beams#manual-beams"))
    ("stemUp" ("inside-the-staff#index-stemUp" "Stems" "inside-the-staff#stems"))
    ("stopGroup" ("outside-the-staff#index-stopGroup" "Analysis brackets" "outside-the-staff#analysis-brackets"))
    ("stopStaff" ("modifying-single-staves#index-stopStaff-4" "Hiding staves" "modifying-single-staves#hiding-staves")
       ("modifying-single-staves#index-stopStaff-2" "Ossia staves" "modifying-single-staves#ossia-staves")
       ("modifying-single-staves#index-stopStaff" "Staff symbol" "modifying-single-staves#staff-symbol"))
    ("stopTrillSpan" ("expressive-marks-as-lines#index-stopTrillSpan" "Trills" "expressive-marks-as-lines#trills"))
    ("storePredefinedDiagram" ("available-music-functions#index-storePredefinedDiagram-2" "A.16 Available music functions" "available-music-functions")
       ("common-notation-for-fretted-strings#index-storePredefinedDiagram" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams"))
    ("stringTunings" ("common-notation-for-fretted-strings#index-stringTunings" "Predefined fret diagrams" "common-notation-for-fretted-strings#predefined-fret-diagrams"))
    ("styledNoteHeads" ("available-music-functions#index-styledNoteHeads" "A.16 Available music functions" "available-music-functions"))
    ("sub" ("formatting-text#index-sub" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("super" ("formatting-text#index-super" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("sus" ("chord-mode#index-sus" "Extended and altered chords" "chord-mode#extended-and-altered-chords"))
    ("sustainOff" ("piano#index-sustainOff" "Piano pedals" "piano#piano-pedals"))
    ("sustainOn" ("piano#index-sustainOn" "Piano pedals" "piano#piano-pedals"))
    ("tabChordRepetition" ("available-music-functions#index-tabChordRepetition" "A.16 Available music functions" "available-music-functions"))
    ("tag" ("available-music-functions#index-tag-1" "A.16 Available music functions" "available-music-functions"))
    ("taor" ("bagpipes#index-taor" "Bagpipe definitions" "bagpipes#bagpipe-definitions"))
    ("taqasim" ("arabic-music#index-taqasim-1" "See also" "arabic-music#See-also-23"))
    ("teaching" ("displaying-pitches#index-teaching" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("teeny" ("formatting-text#index-teeny-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-teeny" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("tempo" ("displaying-rhythms#index-tempo-1" "Metronome marks" "displaying-rhythms#metronome-marks"))
    ("tempo indication" ("displaying-rhythms#index-tempo-indication" "See also" "displaying-rhythms#See-also-95"))
    ("tenuto" ("expressive-marks-attached-to-notes#index-tenuto-1" "See also" "expressive-marks-attached-to-notes#See-also-77"))
    ("text" ("piano#index-text" "Piano pedals" "piano#piano-pedals"))
    ("text-interface" ("other#index-text_002dinterface-1" "A.9.6 Other" "other")
       ("layout-interfaces#index-text_002dinterface" "5.2.2 Layout interfaces" "layout-interfaces"))
    ("text-script-interface" ("layout-interfaces#index-text_002dscript_002dinterface" "5.2.2 Layout interfaces" "layout-interfaces"))
    ("textLengthOff" ("writing-text#index-textLengthOff-2" "Text scripts" "writing-text#text-scripts")
       ("writing-rests#index-textLengthOff" "Full measure rests" "writing-rests#full-measure-rests"))
    ("textLengthOn" ("writing-text#index-textLengthOn-2" "Text scripts" "writing-text#text-scripts")
       ("writing-rests#index-textLengthOn" "Full measure rests" "writing-rests#full-measure-rests"))
    ("textSpannerDown" ("writing-text#index-textSpannerDown" "Text spanners" "writing-text#text-spanners"))
    ("textSpannerNeutral" ("writing-text#index-textSpannerNeutral" "Text spanners" "writing-text#text-spanners"))
    ("textSpannerUp" ("writing-text#index-textSpannerUp" "Text spanners" "writing-text#text-spanners"))
    ("thumb" ("inside-the-staff#index-thumb" "Fingering instructions" "inside-the-staff#fingering-instructions"))
    ("tie" ("displaying-rhythms#index-tie-2" "See also" "displaying-rhythms#See-also-37")
       ("writing-rhythms#index-tie-1" "See also" "writing-rhythms#See-also-223"))
    ("tieDashPattern" ("available-music-functions#index-tieDashPattern" "A.16 Available music functions" "available-music-functions"))
    ("tieDashed" ("writing-rhythms#index-tieDashed" "Ties" "writing-rhythms#ties"))
    ("tieDotted" ("writing-rhythms#index-tieDotted" "Ties" "writing-rhythms#ties"))
    ("tieDown" ("writing-rhythms#index-tieDown" "Ties" "writing-rhythms#ties"))
    ("tieNeutral" ("writing-rhythms#index-tieNeutral" "Ties" "writing-rhythms#ties"))
    ("tieSolid" ("writing-rhythms#index-tieSolid" "Ties" "writing-rhythms#ties"))
    ("tieUp" ("writing-rhythms#index-tieUp" "Ties" "writing-rhythms#ties"))
    ("time" ("beams#index-time-2" "Setting automatic beam behavior" "beams#setting-automatic-beam-behavior")
       ("displaying-rhythms#index-time" "Time signature" "displaying-rhythms#time-signature"))
    ("time signature" ("displaying-rhythms#index-time-signature-1" "See also" "displaying-rhythms#See-also-40"))
    ("timeSignatureFraction" ("displaying-rhythms#index-timeSignatureFraction" "Polymetric notation" "displaying-rhythms#polymetric-notation"))
    ("times" ("displaying-rhythms#index-times-2" "Polymetric notation" "displaying-rhythms#polymetric-notation")
       ("writing-rhythms#index-times" "Tuplets" "writing-rhythms#tuplets"))
    ("tiny" ("formatting-text#index-tiny-2" "Selecting font and font size" "formatting-text#selecting-font-and-font-size")
       ("inside-the-staff#index-tiny" "Selecting notation font size" "inside-the-staff#selecting-notation-font-size"))
    ("tocItem" ("available-music-functions#index-tocItem" "A.16 Available music functions" "available-music-functions"))
    ("translate" ("formatting-text#index-translate" "Text alignment" "formatting-text#text-alignment"))
    ("translate-scaled" ("formatting-text#index-translate_002dscaled" "Text alignment" "formatting-text#text-alignment"))
    ("transpose" ("changing-multiple-pitches#index-transpose-5" "See also" "changing-multiple-pitches#See-also-254")
       ("changing-multiple-pitches#index-transpose-3" "Transpose" "changing-multiple-pitches#transpose")
       ("writing-pitches#index-transpose" "See also" "writing-pitches#See-also-171"))
    ("transposedCueDuring" ("available-music-functions#index-transposedCueDuring-2" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-transposedCueDuring" "Formatting cue notes" "writing-parts#formatting-cue-notes"))
    ("transposing instrument" ("opera-and-stage-musicals#index-transposing-instrument-2" "See also" "opera-and-stage-musicals#See-also-196")
       ("displaying-pitches#index-transposing-instrument-1" "See also" "displaying-pitches#See-also-58"))
    ("transposition" ("available-music-functions#index-transposition-5" "A.16 Available music functions" "available-music-functions")
       ("writing-parts#index-transposition-3" "Quoting other voices" "writing-parts#quoting-other-voices")
       ("displaying-pitches#index-transposition-1" "Instrument transpositions" "displaying-pitches#instrument-transpositions"))
    ("treCorde" ("piano#index-treCorde" "Piano pedals" "piano#piano-pedals"))
    ("tremolo" ("short-repeats#index-tremolo-1" "Tremolo repeats" "short-repeats#tremolo-repeats"))
    ("tremoloFlags" ("short-repeats#index-tremoloFlags" "Tremolo repeats" "short-repeats#tremolo-repeats"))
    ("triangle" ("formatting-text#index-triangle" "Graphic notation inside markup" "formatting-text#graphic-notation-inside-markup"))
    ("trill" ("expressive-marks-as-lines#index-trill-1" "Trills" "expressive-marks-as-lines#trills"))
    ("triplet" ("writing-rhythms#index-triplet" "See also" "writing-rhythms#See-also-99"))
    ("tuplet" ("writing-rhythms#index-tuplet" "See also" "writing-rhythms#See-also-99"))
    ("tupletDown" ("writing-rhythms#index-tupletDown" "Tuplets" "writing-rhythms#tuplets"))
    ("tupletNeutral" ("writing-rhythms#index-tupletNeutral" "Tuplets" "writing-rhythms#tuplets"))
    ("tupletNumberFormatFunction" ("writing-rhythms#index-tupletNumberFormatFunction" "Selected Snippets" "writing-rhythms#Selected-Snippets-36"))
    ("tupletSpannerDuration" ("writing-rhythms#index-tupletSpannerDuration" "Selected Snippets" "writing-rhythms#Selected-Snippets-36"))
    ("tupletUp" ("writing-rhythms#index-tupletUp" "Tuplets" "writing-rhythms#tuplets"))
    ("tweak" ("available-music-functions#index-tweak" "A.16 Available music functions" "available-music-functions"))
    ("type" ("defining-new-contexts#index-type" "5.1.6 Defining new contexts" "defining-new-contexts"))
    ("unHideNotes" ("inside-the-staff#index-unHideNotes" "Hidden notes" "inside-the-staff#hidden-notes"))
    ("unaCorda" ("piano#index-unaCorda" "Piano pedals" "piano#piano-pedals"))
    ("unbreakable-spanner-interface" ("beams#index-unbreakable_002dspanner_002dinterface" "See also" "beams#See-also-206"))
    ("underline" ("formatting-text#index-underline" "Selecting font and font size" "formatting-text#selecting-font-and-font-size"))
    ("unfold" ("long-repeats#index-unfold" "Written-out repeats" "long-repeats#written_002dout-repeats"))
    ("unfoldRepeats" ("available-music-functions#index-unfoldRepeats" "A.16 Available music functions" "available-music-functions"))
    ("voice" ("displaying-pitches#index-voice-2" "Automatic accidentals" "displaying-pitches#automatic-accidentals")
       ("displaying-pitches#index-voice" "Automatic accidentals" "displaying-pitches#automatic-accidentals"))
    ("voiceOne" ("multiple-voices#index-voiceOne" "Single-staff polyphony" "multiple-voices#single_002dstaff-polyphony"))
    ("volta" ("long-repeats#index-volta-1" "See also" "long-repeats#See-also"))
    ("walkerHeads" ("note-heads#index-walkerHeads" "Shape note heads" "note-heads#shape-note-heads"))
    ("walkerHeadsMinor" ("note-heads#index-walkerHeadsMinor" "Shape note heads" "note-heads#shape-note-heads"))
    ("whichBar" ("bars#index-whichBar" "Selected Snippets" "bars#Selected-Snippets-1"))
    ("with-color" ("inside-the-staff#index-with_002dcolor-1" "Coloring objects" "inside-the-staff#coloring-objects"))
    ("withMusicProperty" ("available-music-functions#index-withMusicProperty" "A.16 Available music functions" "available-music-functions"))
    ("wordwrap" ("formatting-text#index-wordwrap" "Text alignment" "formatting-text#text-alignment"))
    ("wordwrap-lines" ("formatting-text#index-wordwrap_002dlines" "Multi-page markup" "formatting-text#multi_002dpage-markup"))
    ("x11-color" ("inside-the-staff#index-x11_002dcolor-3" "See also" "inside-the-staff#See-also-53")
       ("inside-the-staff#index-x11_002dcolor-1" "Coloring objects" "inside-the-staff#coloring-objects"))
    ("xNote" ("available-music-functions#index-xNote" "A.16 Available music functions" "available-music-functions"))
    ("xNotesOn" ("available-music-functions#index-xNotesOn" "A.16 Available music functions" "available-music-functions"))
    ("|" ("bars#index-_007c-2" "Bar and bar number checks" "bars#bar-and-bar-number-checks")
       ("bars#index-_007c" "Bar and bar number checks" "bars#bar-and-bar-number-checks"))
    ("~" ("writing-rhythms#index-_007e" "Ties" "writing-rhythms#ties"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Help
;;;


(defun lyqi:help (entry)
  (interactive
   (list (completing-read "Entry:" lyqi:help-index nil t)))
  (lyqi:help-describe-entry entry))

(defun lyqi:help-describe-entry (entry)
  (save-excursion
    (with-output-to-temp-buffer "*Help*"
      (princ (format "%s\n\n" entry))
      (princ (format "In LilyPond documentation:\n"))
      (let ((urls (loop with data = (cdr (assoc entry lyqi:help-index))
                        for (entry-link section section-link) in data
                        for entry-url = (format "%s/%s" lyqi:help-url-base entry-link)
                        do (princ (format "  `%s'\n" section))
                        collect entry-url)))
        (with-current-buffer standard-output
          (save-excursion
            (save-match-data
              (loop for match = (re-search-backward "`\\([^`']+\\)'" nil t)
                    for url in urls
                    while (and match url)
                    do (help-xref-button 1 'help-url url))))))
      (with-current-buffer standard-output
        ;; Return the text we displayed.
        (buffer-string)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Lyqi mode
;;;

(eval-when-compile (require 'cl))
(require 'eieio)










;;;
;;; Language selection
;;;
;;; 1) if \include "language.ly" is found is the buffer, use that language
;;; 2) if the buffer filename is in one of the directories listed in
;;; `lyqi:projects-language', then use the specified language
;;; 3) try to detect note names in the buffer
;;; 4) otherwise, use the prefered language.
;;;

(defvar lyqi:sorted-projects-language nil)

(eval-when (load)
  (setq lyqi:sorted-projects-language
        (sort (loop for (directory language) in lyqi:projects-language
                    collect (cons (file-name-as-directory (file-truename directory)) language))
              (lambda (elt1 elt2)
                (let ((str1 (first elt1))
                      (str2 (first elt2)))
                  (>= (length str1) (length str2)))))))

(defun lyqi:file-in-defined-projects-p (filename)
  "If `filename' is in a sub-directory of one of the directories
specified in `lyqi:projects-lanugages', return the associated langugage.
Otherwise, return NIL."
  (loop with file-dir = (file-name-directory (file-truename filename))
        with file-dir-length = (length file-dir)
        for (dir . lang) in lyqi:sorted-projects-language
        for len = (length dir)
        if (and (<= len file-dir-length)
                (string= dir (substring file-dir 0 (length dir))))
        return lang))

(defun lyqi:detect-buffer-language ()
  "Detect language used in current buffer.

- if \include \"<language>.ly\" is found is the buffer, use that language
- if the buffer filename is in one of the directories listed in
`lyqi:projects-language', then use the specified language
- try to detect note names in the buffer
- otherwise, use the prefered language."
  (save-excursion
    (goto-char (point-min))
    (or (and (re-search-forward "\\\\include \"\\(italiano\\|english\\|deutsch\\)\\.ly\"" nil t)
             (intern (match-string-no-properties 1)))
        (lyqi:file-in-defined-projects-p (buffer-file-name))
        (and (re-search-forward "\\(^\\|[ \t]\\)\\(do\\|re\\|mi\\|fa\\|sol\\|la\\|si\\)[',]*[1248]" nil t)
             'italiano) ;; TODO: choose first do-re-mi language from `lyqi:prefered-languages'
        (and (re-search-forward "\\(^\\|[ \t]\\)[a-h][',]*[1248]" nil t)
             'nederlands) ;; TODO: choose first a-b-c language from `lyqi:prefered-languages'
        (first lyqi:prefered-languages))))

(defun lyqi:select-next-language (&optional syntax)
  (interactive)
  (let* ((syntax (or syntax (lp:current-syntax)))
         (current-language (slot-value (lyqi:language syntax) 'name))
         (next-language (loop with possible-languages = (slot-value syntax 'possible-languages)
                              for langs on possible-languages
                              for lang = (first langs)
                              if (eql lang current-language)
                              return (or (cadr langs) (first possible-languages)))))
    (set-slot-value syntax 'language (lyqi:select-language next-language))
    (force-mode-line-update)
    (lp:parse-and-highlight-buffer)))

;;;
;;; Mode maps
;;;
(defvar lyqi:normal-mode-map nil
  "Keymap used in `lyqi-mode', in normal editing.")

(defvar lyqi:quick-insert-mode-map nil
  "Keymap used in `lyqi-mode', in quick insertion editing.")

(defvar lyqi:quick-insert-string-mode-map nil
  "Keymap used in `lyqi-mode', when inserting a string in quick insertion editing.")

(defvar lyqi:quick-insert-command-mode-map nil
  "Keymap used in `lyqi-mode', when inserting a \\command in quick insertion editing.")

(defun lyqi:toggle-quick-edit-mode (&optional syntax)
  (interactive)
  (let* ((syntax (or syntax (lp:current-syntax)))
         (quick-edit-mode (not (slot-value syntax 'quick-edit-mode))))
    (set-slot-value syntax 'quick-edit-mode quick-edit-mode)
    (if quick-edit-mode
        (use-local-map lyqi:quick-insert-mode-map)
        (use-local-map lyqi:normal-mode-map)))
  (force-mode-line-update))

(defun lyqi:enter-quick-insert-string ()
  (interactive)
  (lyqi:insert-delimiter)
  (use-local-map lyqi:quick-insert-string-mode-map))

(defun lyqi:quit-quick-insert-string ()
  (interactive)
  (lyqi:insert-delimiter)
  (use-local-map lyqi:quick-insert-mode-map))

(defun lyqi:enter-quick-insert-command ()
  (interactive)
  (insert-char last-command-event 1)
  (use-local-map lyqi:quick-insert-command-mode-map))

(defun lyqi:quit-quick-insert-command ()
  (interactive)
  (insert-char last-command-event 1)
  (use-local-map lyqi:quick-insert-mode-map))

(defconst lyqi:+azerty-mode-map+
  '(;; Rest, skip, etc
    ("v" lyqi:insert-rest)
    ("b" lyqi:insert-spacer)
    ("q" lyqi:insert-chord-repetition)
    ;; also available: lyqi:insert-spacer lyqi:insert-skip
    ;; Pitches
    ("e" lyqi:insert-note-c)
    ("r" lyqi:insert-note-d)
    ("t" lyqi:insert-note-e)
    ("d" lyqi:insert-note-f)
    ("f" lyqi:insert-note-g)
    ("g" lyqi:insert-note-a)
    ("c" lyqi:insert-note-b)
    ;; Alterations
    ("z" lyqi:change-alteration-down)
    ("y" lyqi:change-alteration-up)
    ("a" lyqi:change-alteration-neutral)
    ;; Octaves
    ("s" lyqi:change-octave-down)
    ("h" lyqi:change-octave-up)
    ;; also available: lyqi:change-octave-zero
    ;; Durations
    ("i" lyqi:change-duration-1)
    ("o" lyqi:change-duration-2)
    ("j" lyqi:change-duration-4)
    ("k" lyqi:change-duration-8)
    ("l" lyqi:change-duration-16)
    ("m" lyqi:change-duration-32)
    ("p" lyqi:change-duration-dots)
    ;; also available: lyqi:change-duration-64 lyqi:change-duration-128
    ;; Undo
    ("u" undo)))

(defconst lyqi:+qwerty-mode-map+
  '(;; Rest, skip, etc
    ("v" lyqi:insert-rest)
    ("b" lyqi:insert-spacer)
    ("q" lyqi:insert-chord-repetition)
    ;; also available: lyqi:insert-spacer lyqi:insert-skip
    ;; Pitches
    ("e" lyqi:insert-note-c)
    ("r" lyqi:insert-note-d)
    ("t" lyqi:insert-note-e)
    ("d" lyqi:insert-note-f)
    ("f" lyqi:insert-note-g)
    ("g" lyqi:insert-note-a)
    ("c" lyqi:insert-note-b)
    ;; Alterations
    ("w" lyqi:change-alteration-down)
    ("y" lyqi:change-alteration-up)
    ;; also available: lyqi:change-alteration-neutral
    ;; Octaves
    ("s" lyqi:change-octave-down)
    ("h" lyqi:change-octave-up)
    ("a" lyqi:change-octave-zero)
    ;; Durations
    ("i" lyqi:change-duration-1)
    ("o" lyqi:change-duration-2)
    ("j" lyqi:change-duration-4)
    ("k" lyqi:change-duration-8)
    ("l" lyqi:change-duration-16)
    (";" lyqi:change-duration-32)
    ("p" lyqi:change-duration-dots)
    ;; also available: lyqi:change-duration-64 lyqi:change-duration-128
    ;; Undo
    ("u" undo)))

(defmacro lyqi:define-string-insert-command (string &optional with-space-around)
  (let ((fn-name (intern (format "insert-string-%s" string))))
    `(progn
       (defun ,fn-name ()
         ,(format "Insert string \"%s\"" string)
         (interactive)
         ,(if with-space-around
              `(lyqi:with-space-around
                (insert ,string))
              `(insert ,string)))
       (byte-compile ',fn-name)
       ',fn-name)))

(defun lyqi:force-mode-map-definition ()
  "Force the (re-)definition if `lyqi:quick-insert-mode-map', by
copying `lyqi:normal-mode-map' and using bindings set in either
`lyqi:+qwerty-mode-map+' or `lyqi:+azerty-mode-map+' (depending
on the value of `lyqi:keyboard-mapping'), and bindings from
`lyqi:custom-key-map'."
  (interactive)
  (setq lyqi:quick-insert-string-mode-map (copy-keymap lyqi:normal-mode-map))
  (define-key lyqi:quick-insert-string-mode-map "\"" 'lyqi:quit-quick-insert-string)
  (setq lyqi:quick-insert-command-mode-map (copy-keymap lyqi:normal-mode-map))
  (define-key lyqi:quick-insert-command-mode-map " " 'lyqi:quit-quick-insert-command)
  (setq lyqi:quick-insert-mode-map (copy-keymap lyqi:normal-mode-map))
  (define-key lyqi:quick-insert-mode-map "\"" 'lyqi:enter-quick-insert-string)
  (define-key lyqi:quick-insert-mode-map "\\" 'lyqi:enter-quick-insert-command)
  (loop for (key command) in (case lyqi:keyboard-mapping
                               ((azerty) lyqi:+azerty-mode-map+)
                               (t lyqi:+qwerty-mode-map+))
        do (define-key lyqi:quick-insert-mode-map key command))
  (loop for (key command) in lyqi:custom-key-map
        if (stringp command)
        do (define-key lyqi:quick-insert-mode-map
             key (eval `(lyqi:define-string-insert-command ,command))) ;; urgh
        else if (and (listp command) (eql (car command) 'space-around))
        do (define-key lyqi:quick-insert-mode-map
             key (eval `(lyqi:define-string-insert-command ,(cdr command) t)))
        else do (define-key lyqi:quick-insert-mode-map key command)))

(eval-when (load)
  (setq lyqi:normal-mode-map (make-sparse-keymap))
  (define-key lyqi:normal-mode-map "\C-cq" 'lyqi:toggle-quick-edit-mode)
  (define-key lyqi:normal-mode-map "\C-c\C-t" 'lyqi:transpose-region)
  (define-key lyqi:normal-mode-map "\C-c\C-l" 'lyqi:compile-ly)
  (define-key lyqi:normal-mode-map "\C-c\C-s" 'lyqi:open-pdf)
  (define-key lyqi:normal-mode-map [(control c) return] 'lyqi:open-midi)
  ;(define-key lyqi:normal-mode-map "(" 'lyqi:insert-opening-delimiter)
  ;(define-key lyqi:normal-mode-map "{" 'lyqi:insert-opening-delimiter)
  (define-key lyqi:normal-mode-map "<" 'lyqi:insert-opening-delimiter)
  (define-key lyqi:normal-mode-map "\"" 'lyqi:insert-delimiter)
  ;(define-key lyqi:normal-mode-map ")" 'lyqi:insert-closing-delimiter)
  (define-key lyqi:normal-mode-map "}" 'lyqi:insert-closing-delimiter)
  (define-key lyqi:normal-mode-map ">" 'lyqi:insert-closing-delimiter)
  (define-key lyqi:normal-mode-map [tab] 'lyqi:indent-line)
  (define-key lyqi:normal-mode-map [(control tab)] 'lyqi:complete-word)
  (lyqi:force-mode-map-definition))

;;;
;;; Header line
;;;

(defun lyqi:mode-line-select-next-language (event)
  "Like `lyqi:select-next-language', but temporarily select EVENT's window."
  (interactive "e")
  (save-selected-window
    (select-window (posn-window (event-start event)))
    (lyqi:select-next-language)))

(defun lyqi:mode-line-toggle-quick-edit-mode (event)
  "Like `lyqi:toggle-quick-edit-mode', but temporarily select EVENT's window."
  (interactive "e")
  (save-selected-window
    (select-window (posn-window (event-start event)))
    (lyqi:toggle-quick-edit-mode)))

(defun lyqi:mode-line-set-global-master-file (event filename)
  "Like `lyqi:set-global-master-file', but temporarily select EVENT's window."
  (interactive "e\nfGlobal master file: ")
  (save-selected-window
    (select-window (posn-window (event-start event)))
    (lyqi:set-global-master-file filename)))

(defun lyqi:mode-line-set-buffer-master-file (event filename)
  "Like `lyqi:set-buffer-master-file', but temporarily select EVENT's window."
  (interactive "e\nfBuffer master file:")
  (save-selected-window
    (select-window (posn-window (event-start event)))
    (lyqi:set-buffer-master-file filename)))

(defun lyqi:mode-line-unset-master-file (event)
  "Like `lyqi:unset-master-file', but temporarily select EVENT's window."
  (interactive "e")
  (save-selected-window
    (select-window (posn-window (event-start event)))
    (lyqi:unset-master-file)))

(defun lyqi:mode-line-lyqi-mode (event)
  "Like `lyqi-mode', but temporarily select EVENT's window."
  (interactive "e")
  (save-selected-window
    (select-window (posn-window (event-start event)))
    (lyqi-mode)))

(defun lyqi:set-mode-line-modes ()
  (setq mode-line-modes
        '("%[("
          (:eval (propertize (if (slot-value (lp:current-syntax) 'quick-edit-mode)
                                 (format "%s:quick insert" mode-name)
                                 mode-name)
                             'help-echo "mouse-1: toggle edit mode, mouse-2: major mode help, mouse-3: toggle minor modes"
                             'mouse-face 'mode-line-highlight
                             'local-map '(keymap
                                          (mode-line
                                           keymap (down-mouse-3 . mode-line-mode-menu-1)
                                           (mouse-2 . describe-mode)
                                           (down-mouse-1 . lyqi:mode-line-toggle-quick-edit-mode)))))
          ", "
          (:eval (propertize (format "lang: %s"
                                     (substring (symbol-name (slot-value (lyqi:language (lp:current-syntax)) 'name))
                                                0 2))
                             'help-echo "select next language"
                             'mouse-face 'mode-line-highlight
                             'local-map '(keymap
                                          (mode-line
                                           keymap (mouse-1 . lyqi:mode-line-select-next-language)))))
          (:eval (if (lyqi:defined-master-file) ", " ""))
          (:eval (if (lyqi:defined-master-file)
                     (propertize (format "Master file: %s" (file-name-nondirectory (lyqi:master-file)))
                                 'help-echo "mouse-1: set global master file, mouse-2: unset master file, mouse-3 set buffer-local master file"
                                 'mouse-face 'mode-line-highlight
                                 'local-map '(keymap
                                              (mode-line
                                               keymap (mouse-1 . lyqi:mode-line-set-global-master-file)
                                               (mouse-3 . lyqi:mode-line-set-buffer-master-file)
                                               (mouse-2 . lyqi:mode-line-unset-master-file))))
                     ""))
          (:eval (if after-change-functions "" ", "))
          (:eval (if after-change-functions
                     ""
                     (propertize "¡BUG!"
                                 'help-echo "re-run lyqi-mode"
                                 'mouse-face 'mode-line-highlight
                                 'local-map '(keymap (mode-line
                                                      keymap (mouse-1 . lyqi:mode-line-lyqi-mode))))))
          ")%]  ")))

;;;
;;; lyqi-mode
;;;

(defvar lyqi-mode-syntax-table nil
  "Syntax table used in `lyqi-mode' buffers.")

(defun lyqi-mode ()
  "Major mode for editing LilyPond music files, with quick insertion.

\\{lyqi:normal-mode-map}

In quick insertion mode:
\\{lyqi:quick-insert-mode-map}
"
  (interactive)
  (kill-all-local-variables)
  (setq major-mode 'lyqi-mode)
  (setq mode-name "Lyqi")
  ;; syntax table
  (make-local-variable 'lyqi-mode-syntax-table)
  (setq lyqi-mode-syntax-table (make-syntax-table (standard-syntax-table)))
  (modify-syntax-entry ?\' "w" lyqi-mode-syntax-table)
  (modify-syntax-entry ?\, "w" lyqi-mode-syntax-table)
  (modify-syntax-entry ?\. "w" lyqi-mode-syntax-table)
  (modify-syntax-entry ?\* "w" lyqi-mode-syntax-table)
  (modify-syntax-entry ?\/ "w" lyqi-mode-syntax-table)
  (set-syntax-table lyqi-mode-syntax-table)
  ;; indentation
  (make-local-variable 'indent-line-function)
  (setq indent-line-function 'lyqi:indent-line)
  (make-local-variable 'indent-region-function)
  (setq indent-region-function 'lyqi:indent-region)
  ;; before and after change function (for parse update)
  (make-local-variable 'before-change-functions)
  (make-local-variable 'after-change-functions)
  (pushnew 'lp:before-parse-update before-change-functions)
  (setq after-change-functions '(lp:parse-update))
  ;; avoid point adjustment
  (make-local-variable 'global-disable-point-adjustment)
  (setq global-disable-point-adjustment t)
  ;; buffer syntax
  (make-local-variable 'lp:*current-syntax*)
  (unless lp:*current-syntax*
    (let ((lang (lyqi:detect-buffer-language)))
      (setq lp:*current-syntax*
            (make-instance 'lyqi:lilypond-syntax
                           :language (lyqi:select-language lang)))
      (pushnew lang (slot-value lp:*current-syntax* 'possible-languages))))
  (lp:parse-and-highlight-buffer)
  ;; buffer master file
  (make-local-variable 'lyqi:buffer-master-file)
  ;; mode line modes
  (make-local-variable 'mode-line-modes)
  (lyqi:set-mode-line-modes)
  ;; midi backend
  (lyqi:start-midi-backend)
  ;; default mode-map
  (use-local-map lyqi:normal-mode-map))


(provide 'lyqi)
