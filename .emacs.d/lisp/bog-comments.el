;; Bog COMMENTS keybinding

(define-skeleton gnu-licence-header
  "Set the GNU Licence"
  nil
  
  "/* Copyright (C) "
  
  (skeleton-read "Year : ")|"2016"
  " "
  (skeleton-read "Author : ")|"bog"
  
  "

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/"
  \n
  \n
  _
  )

(define-skeleton doxy-comment-header
  "Print a comment with author, date and version."
  nil
  "/** " > \n
  "* " (setq desc (skeleton-read "Description : "))  > \n
  "* "  > \n
  "* @author bog" > \n
  "* @date 2016" > \n
  "* @version 1" > \n
  "*/" > \n
  )

(global-set-key (kbd "C-c l") 'gnu-licence-header)
(global-set-key (kbd "C-c x") 'doxy-comment-header)
