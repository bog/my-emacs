;; Bog CPP keybinding

(define-skeleton cpp-header-skeleton
  "Set the header source code"
  ""
  "#ifndef " (setq maj (upcase (setq str (skeleton-read "Class name ? "))))
  "_HPP" \n
  "#define " maj "_HPP" \n
  "#include <iostream>" \n \n
  "class "  str \n
  "{" \n
  "public: " > \n
  "explicit " str "();" \n
  "virtual ~" str "();" \n \n
  "protected: " > \n \n
  "private: " > \n
  str "( " str " const& " (downcase str) " ) = delete;" > \n
  str "& operator=( " str " const& " (downcase str) " ) = delete;" > \n
  "};" > \n
  \n "#endif" > \n
  )

(define-skeleton cpp-skeleton
  "Set the cpp source code"
  "Class name: "
  ;;preprocessor
  "#include <" str ".hpp>" \n \n
  ;;constructor
  str "::" str "(" _ ")" \n
  "{" \n \n "}" > \n
  \n \n \n
  ;;destructor
  str "::~" str "(" _ ")" \n
  "{" \n \n "}" > \n
  )

(define-skeleton cpp-if-skeleton
  "Code of an if-statement"
  ""
  "if" "( " _ " )" > \n
  "{" > \n
  \n
  "}" > \n
  "else" > \n
  "{" > \n
  \n
  "}" > \n
  
  )

(define-skeleton cpp-switch-skeleton
  "Code of a switch-statement"
  "Condition: "
  "switch( " str " )" \n
  "{" > \n
  \n _ \n
  "default: " > \n
  "break;" > \n
  "}" > \n

  )

(define-skeleton cpp-cout-skeleton
  "Code for a cout"
  nil
  "std::cout<< " _ " <<std::endl;"
  )

(define-skeleton cpp-cerr-skeleton
  "Code for a cerr"
  nil
  "std::cerr<< " _ " <<std::endl;"
  )

(global-set-key (kbd "C-c h") 'cpp-header-skeleton )
(global-set-key (kbd "C-c c") 'cpp-skeleton )
(global-set-key (kbd "C-c i") 'cpp-if-skeleton )
(global-set-key (kbd "C-c s") 'cpp-switch-skeleton )
(global-set-key (kbd "C-c o") 'cpp-cout-skeleton)
(global-set-key (kbd "C-c e") 'cpp-cerr-skeleton)
(global-set-key (kbd "C-c u") 'uncomment-region)
